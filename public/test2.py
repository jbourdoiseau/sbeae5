#! /usr/local/bin/python3.9
# -*-coding:utf-8 -*
import pytesseract
from PIL import Image, ImageEnhance, ImageFilter
from sqlescapy import sqlescape
import mysql.connector
from mysql.connector import Error
try:
    connection = mysql.connector.connect(host='localhost',
                                         database='eae',
                                         user='eae',
                                         password='!3qzr56K')

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("You're connected to database: ", record)

except Error as e:
    print("Error while connecting to MySQL", e)


# finally:
#     if (connection.is_connected()):
#         cursor.close()
#         connection.close()
#         print("MySQL connection is closed")

##sql_select_Query = "select id, doc_name1, doc_name2, doc_name3, doc_name3, doc_name4, doc_name5, doc_name6, doc_name7, doc_name8 from enregistrement where ocr = 0 LIMIT 0,40"
sql_select_Query = "select id from enregistrement where ocr = 0 LIMIT 0,40"

cursor = connection.cursor()
cursor.execute(sql_select_Query)
records = cursor.fetchall()
print("Total number of rows in Laptop is: ", cursor.rowcount)

for row in records:
  text=''
  if(row[0]):
    sql_select_Query2 = "select file_name from document where enregistrement_id ="+str(row[0])
    cursor2 = connection.cursor()
    cursor2.execute(sql_select_Query2)
    records2 = cursor2.fetchall()
    print("Total number of rows2 in Laptop is: ", cursor2.rowcount)
    for row2 in records2:
      if(row2[0]):
        ##print(row2[0])
        im = Image.open('/var/www/vhosts/releve-eae.com/public/uploads/documents/'+row2[0])
        ##im = im.filter(ImageFilter.MedianFilter())
        ##enhancer = ImageEnhance.Contrast(im)
        ##enhancer.enhance(2)
        ##im = im.convert('1')
        text = text+ pytesseract.image_to_string(im, lang='eng', config="-psm 17")
        ##print(text)
  sql= "update enregistrement set ocr = 2, textocr ='"+sqlescape(text)+"' WHERE id = "+str(row[0])

  cursor = connection.cursor()
  cursor.execute(sql, (text))
  connection.commit()
  print(sql)

print("\nPrinting each laptop record")
# Filtrage (augmentation du contraste)
#im = im.filter(ImageFilter.MedianFilter())
#enhancer = ImageEnhance.Contrast(im)
#im = enhancer.enhance(2)
#im = im.convert('1')

#text = pytesseract.image_to_string(im)

