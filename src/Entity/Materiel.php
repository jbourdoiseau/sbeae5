<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

use app\Entity\Prestation;

/**
 * @ORM\Table(name="materiel")
 * @ORM\Entity(repositoryClass="App\Repository\MaterielRepository")
 */
class Materiel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $color;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Prestation", inversedBy="materiels")
    */
    private $prestation;


    /**
     * @ORM\Column(type="decimal",precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarif;

	public function __construct()
		  {
		  }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
        public function getColor()
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;

        return $this;
    }
    public function getPrestation()
    {
        return $this->prestation;
    }

    public function setPrestation(Prestation $prestation)
    {
        $this->prestation = $prestation;

        return $this;
    }

        public function getTarif()
    {
        return $this->tarif;
    }

    public function setTarif($tarif)
    {
        $this->tarif = $tarif;

        return $this;
    }

}
