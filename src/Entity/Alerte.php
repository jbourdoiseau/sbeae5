<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\AlerteRepository")
 */
class Alerte
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\User")
	*/
    private $user;

     /**
     * @var \Date
     *
     * @ORM\Column(name="datealerte", type="date")
     */

    private $datealerte;


	function __construct()
	{
		$this->datealerte = new \DateTime('now');
	}

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser()
    {
		return $this->user;
    }

    public function setUser(\App\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }


    /**
     * Set datealerte.
     *
     * @param \DateTime $datealerte
     *
     */
    public function setDatealerte($datealerte)
    {
        $this->datealerte = $datealerte;

        return $this;
    }

    /**
     * Get datealerte.
     *
     * @return \DateTime
     */
    public function getDatealerte()
    {
        return $this->datealerte;
    }

}
