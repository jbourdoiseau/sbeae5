<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use app\Entity\Chantier;


class Graph
{
    /**
     *
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Chantier")
    */
    private $chantier;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

  public function getChantier()
  {
      return $this->chantier;
  }

  public function setChantier(Chantier $chantier)
  {
      $this->chantier = $chantier;

      return $this;
  }

}
