<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="typeocr")
 * @ORM\Entity(repositoryClass="App\Repository\TypeocrRepository")
 */
class Typeocr
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
	
	/**
  
	
    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;
	
    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
   
}
