<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\MessagecronRepository")
 */
class Messagecron
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
	
	
    /**
     * @ORM\Column(type="text", length=1000, options={"default":""})
     */
    private $message;
	
	 
	function __construct()
	{
	}
	
    public function getId(): int
    {
        return $this->id;
    }

    public function getMessage()
    {        
		return $this->message;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }
	
}
