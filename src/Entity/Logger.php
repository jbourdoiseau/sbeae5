<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * Logger
 *
 * @ORM\Table(name="logger")
 * @ORM\Entity(repositoryClass="App\Repository\LoggerRepository")
 */
class Logger
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="enregistrements")
	*/
    private $user;

	/**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $titre;

    /**
     * @var \Date

     *
     * @ORM\Column(name="datecrea", type="datetime")
     */
    private $datecrea;



	function __construct()
    {
        $this->datecrea = new \DateTime('now +2 hour');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTitre()
    {
		return $this->titre;
    }

    public function setTitre(string $titre)
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDatecrea()
    {
        return $this->datecrea;
    }

    public function setDatecrea(\DateTimeInterface $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }


    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

}
