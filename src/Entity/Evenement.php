<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Evenement
 *
 * @ORM\Table(name="evenement")
 * @ORM\Entity(repositoryClass="App\Repository\EvenementRepository")
 * @Vich\Uploadable
 */
class Evenement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="evenements")
	*/
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", options={"default" : 0})
     */
    private $type;

    /**
     * @var \Date
     *
     * @ORM\Column(name="datecrea", type="datetime")
     */
    private $datecrea;

    /**
     * @var \Date
     *
     * @ORM\Column(name="datedebut", type="date")
     */
    private $datedebut;
    /**
     * @var \Date
     *
     * @ORM\Column(name="datefin", type="date", nullable=true)
     */
    private $datefin;

	 /**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png","application/pdf","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png, pdf, doc, docx)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName1")
     *
     * @var File
     */
    private $imageFile1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName1;

	 /**
	 * @Assert\File(
	 *     maxSize = "8000k",
     *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png","application/pdf","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
     *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png, pdf, doc, docx)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName2")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName2;

	/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName3")
     *
     * @var File
     */
    private $imageFile3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName3;
	/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */

    private $commentaire;

    /**
     * @var text
     *
     * @ORM\Column(name="commentaire2", type="text", nullable=true)
     */

    private $commentaire2;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer", options={"default" : 0})
     */
    private $etat;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
	*/
    private $zone;



	function __construct()
                {
                                    $this->datecrea = new \DateTime('now +2 hour');
									$this->etat = 0;
                }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

  public function getType()
  {
      return $this->type;
  }

  public function setType($type)
  {
      $this->type = $type;

      return $this;
  }



    public function getDatecrea()
    {
        return $this->datecrea;
    }

    public function setDatecrea(\DateTimeInterface $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }

  /**
     * Set datedebut.
     *
     * @param \DateTime $datedebut
     *
     * @return Chantier
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut.
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin.
     *
     * @param \DateTime $datefin
     *
     * @return Chantier
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin.
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }


	public function setImageFile1(File $doc = null)
                                                                {
                                                                    $this->imageFile1 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile1()
    {
        return $this->imageFile1;
    }

    public function getDocName1()
    {
        return $this->docName1;
    }

    public function setDocName1($docName1)
    {
        $this->docName1 = $docName1;

        return $this;
    }

	public function setImageFile2(File $doc = null)
                                                                {
                                                                    $this->imageFile2 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile2()
    {
        return $this->imageFile2;
    }


    public function getDocName2()
    {
        return $this->docName2;
    }

    public function setDocName2($docName2)
    {
        $this->docName2 = $docName2;

        return $this;
    }

	public function setImageFile3(File $doc = null)
                                                                {
                                                                    $this->imageFile3 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile3()
    {
        return $this->imageFile3;
    }

    public function getDocName3()
    {
        return $this->docName3;
    }

    public function setDocName3($docName3)
    {
        $this->docName3 = $docName3;

        return $this;
    }


    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

   public function getCommentaire2()
    {
        return $this->commentaire2;
    }

    public function setCommentaire2($commentaire2): self
    {
        $this->commentaire2 = $commentaire2;

        return $this;
    }

    public function getEtat()
    {
        return $this->etat;
    }

    public function setEtat($etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getZone()
  {
      return $this->zone;
  }

  public function setZone(Zone $zone)
  {
      $this->zone = $zone;

      return $this;
  }

}
