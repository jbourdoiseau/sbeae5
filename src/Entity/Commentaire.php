<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\User")
	*/
    private $user;

		/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
	*/
    private $zone;

	    /**
     * @var \Date
     *
     * @ORM\Column(name="datedebut", type="date")

     */

    private $datedebut;

    /**
     * @var \Date

	 * @Assert\Expression(
	 *     "this.getDatefin() > this.getDatedebut()",
	 *     message="Erreur : Date de début après la date de fin"
	 * )
     *
     * @ORM\Column(name="datefin", type="date")
     */
    private $datefin;

	/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */

    private $commentaire;





	function __construct()
	{
		$this->datedebut = new \DateTime('now');
	}

    public function getId(): int
    {
        return $this->id;
    }




    /**
     * Set datedebut.
     *
     * @param \DateTime $datedebut
     *
     * @return Chantier
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut.
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin.
     *
     * @param \DateTime $datefin
     *
     * @return Chantier
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin.
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

       public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
	public function getZone()
  {
      return $this->zone;
  }

  public function setZone(Zone $zone)
  {
      $this->zone = $zone;

      return $this;
  }
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }
}
