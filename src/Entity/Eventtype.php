<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="typeevenement")
  * @ORM\Entity(repositoryClass="App\Repository\EventtypeRepository")
 */
class Eventtype
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**


    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $formulaire;


    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
  public function getFormulaire()
  {
      return $this->getFormulaire;
  }

  public function setFormulaire($formulaire)
  {
      $this->formulaire = $formulaire;

      return $this;
  }
}
