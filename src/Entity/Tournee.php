<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Enregistrement
 *
 * @ORM\Table(name="tournee")
 * @ORM\Entity(repositoryClass="App\Repository\TourneeRepository")
 */
class Tournee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\Enregistrement", inversedBy="tournees")
	*/
    private $enregistrement;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;


    /**
     * @ORM\Column(name="total", type="integer", nullable=true, options={"default" : 0})
     */
    private $total;

	/**
     *
     * @ORM\Column(name="releve", type="integer", nullable=true, options={"default" : 0})
     */
    private $releve;

	/**
     * @var int
     *
     * @ORM\Column(name="reste", type="integer", nullable=true, options={"default" : 0})
     */
    private $restant;

	/**
     * @var int
     *
     * @ORM\Column(name="infructueux", type="integer", nullable=true, options={"default" : 0})
     */
    private $infructueux;

    /**
     * @var int
     *
     * @ORM\Column(name="infructueuxsuez", type="integer", nullable=true, options={"default" : 0})
     */
    private $infructueuxsuez;

	/**
     * @var smallint
     *
     * @ORM\Column(name="cloture", type="smallint", options={"default" : 0})
     */
    private $cloture;
	/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */

    private $commentaire;

    /**
     * @var \Date

     *
     * @ORM\Column(name="datecrea", type="date")
     */
    private $datecrea;



	function __construct()
        {
            $this->datecrea = new \DateTime();
        }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getReleve()
    {
        return $this->releve;
    }

    public function setReleve($releve): self
    {
        $this->releve = $releve;

        return $this;
    }

    public function getRestant()
    {
        return $this->restant;
    }

    public function setRestant(int $restant): self
    {
        $this->restant = $restant;

        return $this;
    }

    public function getDatecrea(): \DateTimeInterface
    {
        return $this->datecrea;
    }

    public function setDatecrea(\DateTimeInterface $datecrea): self
    {
        $this->datecrea = $datecrea;

        return $this;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }


     public function getInfructueux()
     {
         return $this->infructueux;
     }

     public function setInfructueux(int $infructueux): self
     {
         $this->infructueux = $infructueux;

         return $this;
     }
     public function getInfructueuxsuez()
     {
         return $this->infructueuxsuez;
     }

     public function setInfructueuxsuez(int $infructueuxsuez): self
     {
         $this->infructueuxsuez = $infructueuxsuez;

         return $this;
     }
     public function getEnregistrement()
     {
         return $this->enregistrement;
     }

     public function setEnregistrement(Enregistrement $enregistrement): self
     {
         $this->enregistrement = $enregistrement;

         return $this;
     }

     public function getCloture()
     {
         return $this->cloture;
     }

     public function setCloture($cloture): self
     {
         $this->cloture = $cloture;

         return $this;
     }


}
