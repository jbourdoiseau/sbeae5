<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 * @Vich\Uploadable
 */
class Document
{
/**
 * @var int
 *
 * @ORM\Column(name="id", type="integer")
 * @ORM\Id
 * @ORM\GeneratedValue(strategy="AUTO")
 */
private $id;

/**
 * @ORM\ManyToOne(targetEntity="App\Entity\Enregistrement", inversedBy="documents")
 */
private $enregistrement;


/**
 * @Vich\UploadableField(mapping="documents", fileNameProperty="fileName")
 * @Assert\File(
 *     maxSize = "15M",
 *     mimeTypes = {"image/JPEG", "image/JPG","image/jpeg", "image/jpg", "image/gif", "image/png"},
 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
 * )
 */
private $file;

/**
 * @ORM\Column(type="string", length=255, nullable = true)
 * @var string
 */
private $fileName;

/**
 * @ORM\Column(name="updated_at", type="datetime")
 * @var \DateTime $updatedAt
 */
private $updatedAt;


/**
 * Get id
 *
 * @return int
 */
public function getId()
{
    return $this->id;
}
/**
 * Constructor
 */
public function __construct()
{
    $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
}

/**
 * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
 *
 * @return Documentation
 */
public function setFile(File $file = null)
{
    $this->file = $file;
    if ($file) {
        $this->updatedAt = new \DateTimeImmutable();
    }
    return $this;
}

/**
 * @return File|null
 */
public function getFile()
{
    return $this->file;
}

/**
 * @param string $fileName
 *
 * @return Documentation
 */
public function setFileName($fileName)
{
    $this->fileName = $fileName;

    return $this;
}

/**
 * @return string|null
 */
public function getFileName()
{
    return $this->fileName;
}

/**
 * Set updatedAt
 *
 * @param \DateTime $updatedAt
 *
 * @return Documentation
 */
public function setUpdatedAt($updatedAt)
{
    $this->updatedAt = $updatedAt;

    return $this;
}

/**
 * Get updatedAt
 *
 * @return \DateTime
 */
public function getUpdatedAt()
{
    return $this->updatedAt;
}

/**
 * Set product
 *
 * @param \App\Entity\Enregistrement $enregistrement
 *
 * @return Document
 */
public function setEnregistrement(Enregistrement $enregistrement)
{
    $this->enregistrement = $enregistrement;

    return $this;
}

/**
 * Get product
 *
 * @return \App\Entity\Enregistrement
 */
public function getEnregistrement()
{
    return $this->enregistrement;
}
}
