<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use app\Entity\Chantier;
/**
 * @ORM\Table(name="zone")
 * @ORM\Entity(repositoryClass="App\Repository\ZoneRepository")
 */
class Zone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
	
	
    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;
	
	 /**
     * @ORM\Column(type="string", length=10)
     */
    private $abrev;
	
	
	/**
	* @ORM\OneToOne(targetEntity="App\Entity\User")
	*/
    private $responsable;
	
		/**
		* @ORM\ManyToMany(targetEntity="App\Entity\Chantier", mappedBy="zones")
	   */
	private $chantiers;
	   
	public function __construct()
		  {
			$this->chantiers = new ArrayCollection();
		  }
		  
    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
	
	public function getAbrev()
    {
        return $this->abrev;
    }

    public function setAbrev(string $abrev)
    {
        $this->abrev = $abrev;

        return $this;
    }
	
	 public function getResponsable()
    {
        return $this->responsable;
    }

    public function setResponsable(User $responsable)
    {
        $this->responsable = $responsable;

        return $this;
    }
	 // Notez le singulier, on ajoute une seule catégorie à la fois
	public function addChantier(Chantier $chantier)
		  {
			// Ici, on utilise l'ArrayCollection vraiment comme un tableau
			if ($this->chantiers->contains($chantier)) {
				return;
			}
			$this->chantiers[] = $chantier;

			return $this;
		  }

	 public function removeChantier(Chantier $chantier)
		  {
			// Ici on utilise une méthode de l'ArrayCollection, pour supprimer la catégorie en argument
			$this->chantiers->removeElement($chantier);
		  }

		  // Notez le pluriel, on récupère une liste de catégories ici !
	public function getChantiers()
		  {
			return $this->chantiers;
		  }
}
