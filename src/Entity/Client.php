<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
    * @ORM\OneToMany(targetEntity="App\Entity\Chantier", mappedBy="client")
	*/
    private $chantiers;

		/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Formulaire")
	*/
    private $formulaire;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;

        /**
     * @ORM\Column(type="string", length=10)
     */
    private $cle;

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCle()
    {
        return $this->cle;
    }

    public function setCle(string $cle)
    {
        $this->cle = $cle;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->chantiers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add chantier.
     *
     * @param \App\Entity\Chantier $chantier
     *
     * @return Client
     */
    public function addChantier(\App\Entity\Chantier $chantier)
    {
        $this->chantiers[] = $chantier;

        return $this;
    }

    /**
     * Remove chantier.
     *
     * @param \App\Entity\Chantier $chantier
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChantier(\App\Entity\Chantier $chantier)
    {
        return $this->chantiers->removeElement($chantier);
    }

    /**
     * Get chantiers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChantiers()
    {
        return $this->chantiers;
    }

    /**
     * Set formulaire.
     *
     * @param \App\Entity\Formulaire|null $formulaire
     *
     * @return Client
     */
    public function setFormulaire(\App\Entity\Formulaire $formulaire = null)
    {
        $this->formulaire = $formulaire;

        return $this;
    }

    /**
     * Get formulaire.
     *
     * @return \App\Entity\Formulaire|null
     */
    public function getFormulaire()
    {
        return $this->formulaire;
    }
}
