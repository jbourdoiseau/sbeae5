<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChantierMaterielRepository")
 * @UniqueEntity(fields = {"chantier", "materiel"},message="Ce chantiermateriel existe déja")
 */
class ChantierMateriel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Chantier",inversedBy="chantiermateriels", cascade={"persist"})
	* @ORM\JoinColumn(nullable=false)
	*/
    private $chantier;

	 /**
	   * @ORM\ManyToOne(targetEntity="App\Entity\Materiel")
	   * @ORM\JoinColumn(nullable=false)
	   */
	private $materiel;

    /**
     * @ORM\Column(type="decimal",precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarif;


    public function getId(): int
    {
        return $this->id;
    }

    public function getTarif()
    {
		return $this->tarif;
    }

    public function setTarif(string $tarif)
    {
        $this->tarif = $tarif;

        return $this;
    }

    /**
     * Set client.
     *
     * @param \App\Entity\Client|null $client
     *
     * @return Chantier
     */
    public function setChantier(\App\Entity\Chantier $chantier)
    {
        $this->chantier = $chantier;
        return $this;
    }

    public function getChantier()
    {
        return $this->chantier;
    }
    /**
     */
    public function setMateriel(\App\Entity\Materiel $materiel)
    {
		$this->materiel = $materiel;
        return $this->materiel;
    }

    public function getMateriel()
    {
        return $this->materiel;
    }

}
