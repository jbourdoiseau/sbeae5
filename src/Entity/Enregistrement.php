<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use app\Entity\Zone;
use app\Entity\Typeocr;

/**
 * Enregistrement
 *
 * @ORM\Table(name="enregistrement")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\EnregistrementRepository")
 * @Vich\Uploadable
 */

class Enregistrement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	 /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $titre;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="enregistrements")
	*/
    private $user;


	/**
	 * @Assert\Type(type="integer", message="Format incorrect : chiffres")
     *
     * @ORM\Column(name="releve", type="integer", nullable=true, options={"default" : 0})
     */
    private $releve;

	/**
	 * @Assert\Type(type="integer", message="Format incorrect : chiffres")
     *
     * @ORM\Column(name="infructueux", type="integer", nullable=true, options={"default" : 0})
     */
    private $infructueux;

	/**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=true, options={"default" : 0})
     */
    private $total;

    /**
     * @Assert\Type(type="integer", message="Merci d'entrer un nombre.")
     *
     * @ORM\Column(name="newcompteur", type="integer", nullable=true, options={"default" : 0})
     */

    private $newcompteur;

	/**
	 * @Assert\Type(type="integer", message="Format incorrect : chiffres.")
     *
     * @ORM\Column(name="totaltournee", type="integer", nullable=true, options={"default" : 0})
     */

    private $totaltournee;

		/**
	 * @Assert\Type(type="integer", message="Format incorrect : chiffres")
     *
     * @ORM\Column(name="cloturereleve", type="integer", nullable=true, options={"default" : 0})
     */
    private $cloturereleve;

	/**
	 * @Assert\Type(type="integer", message="Format incorrect : chiffres.")
     *
     * @ORM\Column(name="clotureinfructueux", type="integer", nullable=true, options={"default" : 0})
     */
    private $clotureinfructueux;

	/**
     * @var int
     *
     * @ORM\Column(name="cloturetotal", type="integer", nullable=true, options={"default" : 0})
     */
    private $cloturetotal;

	/**
     * @var int
     *
     * @ORM\Column(name="cloturetotall", type="integer", nullable=true, options={"default" : 0})
     */
    private $cloturetotall;
    /**
     * @var \Date

     *
     * @ORM\Column(name="datecrea", type="datetime")
     */
    private $datecrea;

    /**
     * @var \Date

     *
     * @ORM\Column(name="datereleve", type="date")
     */
    private $datereleve;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="enregistrement", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $documents;

	 /**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName1")
     *
     * @var File
     */
    private $imageFile1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName1;

	 /**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName2")
     *
     * @var File
     */
    private $imageFile2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName2;

	/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName3")
     *
     * @var File
     */
    private $imageFile3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName3;

		/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName4")
     *
     * @var File
     */
    private $imageFile4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName4;

		/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName5")
     *
     * @var File
     */
    private $imageFile5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName5;

		/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName6")
     *
     * @var File
     */
    private $imageFile6;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName6;

		/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName7")
     *
     * @var File
     */
    private $imageFile7;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName7;

		/**
	 * @Assert\File(
	 *     maxSize = "8000k",
	 *     mimeTypes = {"image/jpeg", "image/jpg", "image/gif", "image/png"},
	 *     mimeTypesMessage = "Merci de charger un format d'image valide (jpg, jpeg, gif, png)"
	 * )
     * @Vich\UploadableField(mapping="documents", fileNameProperty="docName8")
     *
     * @var File
     */
    private $imageFile8;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $docName8;

	/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */

    private $commentaire;

    /**
     * @ORM\Column(name="totalimage", type="integer", nullable=true, options={"default" : 0})
     */
    private $totalimage;

	/**
	 * @Assert\Type(type="integer", message="Format incorrect : entier.")
     *
     * @ORM\Column(name="releveimage", type="integer", nullable=true, options={"default" : 0})
     */
    private $releveimage;

	/**
     * @var int
     *
     * @ORM\Column(name="restantOcr", type="integer", nullable=true, options={"default" : 0})
     */
    private $restantOcr;

	/**
     * @var int
     *
     * @ORM\Column(name="ocr", type="integer", options={"default" : 0})
     */
    private $ocr;

	/**
     * @var int
     *
     * @ORM\Column(name="totalocr", type="integer", nullable=true, options={"default" : 0})
     */
    private $totalOcr;

	/**
     * @var int
     *
     * @ORM\Column(name="totalocrtournee", type="integer", nullable=true, options={"default" : 0})
     */
    private $totalOcrTournee;

		/**
     * @var text
     *
     * @ORM\Column(name="textocr", type="text", nullable=true)
     */

    private $textocr;

	/**
	 * @ORM\Column(name="alerte", type="boolean", nullable=true, options={"default" : 0})
	 */
	private $alerte;

		/**
     * @var text
     *
     * @ORM\Column(name="textalerte", type="text", nullable=true)
     */

    private $textalerte;

	/**
	 * @ORM\Column(name="controle", type="boolean", nullable=true, options={"default" : 0})
	 */
	private $controle;

		/**
	 * @ORM\Column(name="defaut", type="boolean", nullable=true, options={"default" : 0})
	 */
	private $defaut;

     /**
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    private $secteur;

	   /**
     * @ORM\Column(name="tarifjour", type="boolean", nullable=true, options={"default" : 0})
     */
    private $tarifjour;

	/**
	 * @ORM\Column(name="datecontrole", type="datetime", nullable=true)
	 */
	private $datecontrole;

	/**
     * @var smallint
     *
     * @ORM\Column(name="cloture", type="smallint", options={"default" : 0})
     */
    private $cloture;

		/**
     * @var smallint
     *
     * @ORM\Column(name="radioreleve", type="smallint", nullable=true, options={"default" : 0})
     */
    private $radioreleve;

	/**
     * @ORM\OneToMany(targetEntity="App\Entity\Tournee", mappedBy="enregistrement",cascade={"persist"})
     */
    private $tournees;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Chantier",inversedBy="chantierusers", cascade={"persist"})
	* @ORM\JoinColumn(nullable=false)
	*/
    private $chantier;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
	*/
    private $zone;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\Typeocr")
     */
    private $typeocr;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\Formulaire")
     */
    private $typeformulaire;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Typechantier")
     */
    private $typechantier;

		/**
     * @var int
     *
     * @ORM\Column(name="sumtournee", type="integer", nullable=true, options={"default" : 0})
     */
    private $sumtournee;

			/**
     * @var int
     *
     * @ORM\Column(name="sumtotal", type="integer", nullable=true, options={"default" : 0})
     */
    private $sumtotal;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\EnregistrementChantierMateriel", mappedBy="enregistrement", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=true)
    */
    private $enregistrementchantiermateriels;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\Totalrenou", mappedBy="enregistrement", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=true)

    */
    private $totalrenous;

        /**
     * @var smallint
     *
     * @ORM\Column(name="valide", type="smallint", nullable=true, options={"default" : 1})
     */
    private $valide;

	/**
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 */
	private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Ncompteur::class, mappedBy="enregistrement", cascade={"persist", "remove"})
     */
    private $ncompteurs;

	function __construct()
                                               {
                                                   $this->datecrea = new \DateTime('now +2 hour');
                                                   $this->valide = 1;
                           						$this->ocr = 0;
               									$this->totalOcr = 0;
               									$this->totalOcrFinal = 0;
               									$this->alerte = 0;
               									$this->infructueux = 0;
               									$this->controle = 0;
               									$this->defaut = 0;
                                                   $this->tarifjour = 0;
               									$this->total = 0;
               									$this->tournees = new ArrayCollection();
                                                   $this->documents = new ArrayCollection();
                                                   $this->enregistrementchantiermateriels = new ArrayCollection();
                                                   $this->totalrenous = new ArrayCollection();
                                                   $this->ncompteurs = new ArrayCollection();
                                                 }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTitre()
    {
		return $this->titre;
    }

    public function setTitre(string $titre)
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal(int $total)
    {
        $this->total = $total;

        return $this;
    }
    public function getTotaltournee()
    {
        return $this->totaltournee;
    }

    public function setTotaltournee(int $totaltournee)
    {
        $this->totaltournee = $totaltournee;

        return $this;
    }

    public function getReleve()
    {
        return $this->releve;
    }

    public function setReleve(int $releve): self
    {
        $this->releve = $releve;

        return $this;
    }

     public function getInfructueux()
     {
         return $this->infructueux;
     }

     public function setInfructueux(int $infructueux): self
     {
         $this->infructueux = $infructueux;

         return $this;
     }

         public function getNewcompteur()
    {
        return $this->newcompteur;
    }

    public function setNewcompteur(int $newcompteur): self
    {
        $this->newcompteur = $newcompteur;

        return $this;
    }

	public function getCloturereleve()
                   {
                       return $this->cloturereleve;
                   }

    public function setCloturereleve(int $cloturereleve): self
    {
        $this->cloturereleve = $cloturereleve;

        return $this;
    }

	 public function getClotureinfructueux()
     {
         return $this->clotureinfructueux;
     }

     public function setClotureinfructueux(int $clotureinfructueux): self
     {
         $this->clotureinfructueux = $clotureinfructueux;

         return $this;
     }

	public function getCloturetotal()
                   {
                       return $this->cloturetotal;
                   }

    public function setCloturetotal(int $cloturetotal)
    {
        $this->cloturetotal = $cloturetotal;

        return $this;
    }

	public function getCloturetotall()
                   {
                       return $this->cloturetotall;
                   }

    public function setCloturetotall(int $cloturetotall)
    {
        $this->cloturetotall = $cloturetotall;

        return $this;
    }

    public function getRestantOcr()
    {
        return $this->restantOcr;
    }

    public function setRestantOcr(int $restantOcr)
    {
        $this->restantOcr = $restantOcr;

        return $this;
    }

    public function getDatecrea()
    {
        return $this->datecrea;
    }

    public function setDatecrea(\DateTimeInterface $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }

	public function getDatereleve()
                   {
                       return $this->datereleve;
                   }

    public function setDatereleve($datereleve)
    {
        $this->datereleve = $datereleve;

        return $this;
    }
/**
 * Add document
 *
 * @param \App\Entity\Document $document
 *
 * @return Enregistrement
 */
public function addDocument(\App\Entity\Document $document)
{
    $document->setEnregistrement($this);
    $this->documents[] = $document;
    return $this;
}

/**
 * Remove document
 *
 * @param \App\Entity\Document $document
 */
public function removeDocument(\App\Entity\Document $document)
{
    $this->documents->removeElement($document);
}

/**
 * Get documents
 *
 * @return \Doctrine\Common\Collections\Collection
 */
public function getDocuments()
{
    return $this->documents;
}

	public function setImageFile1(File $doc = null)
                                                                               {
                                                                                   $this->imageFile1 = $doc;

                                                                                   if (null !== $doc) {
                                                                                       // It is required that at least one field changes if you are using doctrine
                                                                                       // otherwise the event listeners won't be called and the file is lost
                                                                                       $this->updatedAt = new \DateTimeImmutable();
                                                                                   }
                                                                               }

    public function getImageFile1()
    {
        return $this->imageFile1;
    }

    public function getDocName1()
    {
        return $this->docName1;
    }

    public function setDocName1($docName1)
    {
        $this->docName1 = $docName1;

        return $this;
    }

	public function setImageFile2(File $doc = null)
                                                                               {
                                                                                   $this->imageFile2 = $doc;

                                                                                   if (null !== $doc) {
                                                                                       // It is required that at least one field changes if you are using doctrine
                                                                                       // otherwise the event listeners won't be called and the file is lost
                                                                                       $this->updatedAt = new \DateTimeImmutable();
                                                                                   }
                                                                               }

    public function getImageFile2()
    {
        return $this->imageFile2;
    }


    public function getDocName2()
    {
        return $this->docName2;
    }

    public function setDocName2($docName2)
    {
        $this->docName2 = $docName2;

        return $this;
    }

	public function setImageFile3(File $doc = null)
                                                                               {
                                                                                   $this->imageFile3 = $doc;

                                                                                   if (null !== $doc) {
                                                                                       // It is required that at least one field changes if you are using doctrine
                                                                                       // otherwise the event listeners won't be called and the file is lost
                                                                                       $this->updatedAt = new \DateTimeImmutable();
                                                                                   }
                                                                               }

    public function getImageFile3()
    {
        return $this->imageFile3;
    }

    public function getDocName3()
    {
        return $this->docName3;
    }

    public function setDocName3($docName3)
    {
        $this->docName3 = $docName3;

        return $this;
    }

	public function setImageFile4(File $doc = null)
                                                                               {
                                                                                   $this->imageFile4 = $doc;

                                                                                   if (null !== $doc) {
                                                                                       // It is required that at least one field changes if you are using doctrine
                                                                                       // otherwise the event listeners won't be called and the file is lost
                                                                                       $this->updatedAt = new \DateTimeImmutable();
                                                                                   }
                                                                               }

    public function getImageFile4()
    {
        return $this->imageFile4;
    }


    public function getDocName4()
    {
        return $this->docName4;
    }

    public function setDocName4($docName4)
    {
        $this->docName4 = $docName4;

        return $this;
    }

	public function setImageFile5(File $doc = null)
                                                                               {
                                                                                   $this->imageFile5 = $doc;

                                                                                   if (null !== $doc) {
                                                                                       // It is required that at least one field changes if you are using doctrine
                                                                                       // otherwise the event listeners won't be called and the file is lost
                                                                                       $this->updatedAt = new \DateTimeImmutable();
                                                                                   }
                                                                               }

    public function getImageFile5()
    {
        return $this->imageFile5;
    }


    public function getDocName5()
    {
        return $this->docName5;
    }

    public function setDocName5($docName5)
    {
        $this->docName5 = $docName5;

        return $this;
    }

		public function setImageFile6(File $doc = null)
                                                                {
                                                                    $this->imageFile6 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile6()
    {
        return $this->imageFile6;
    }


    public function getDocName6()
    {
        return $this->docName6;
    }

    public function setDocName6($docName6)
    {
        $this->docName6 = $docName6;

        return $this;
    }
public function setImageFile7(File $doc = null)
                                                                {
                                                                    $this->imageFile7 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile7()
    {
        return $this->imageFile7;
    }


    public function getDocName7()
    {
        return $this->docName7;
    }

    public function setDocName7($docName7)
    {
        $this->docName7 = $docName7;

        return $this;
    }

public function setImageFile8(File $doc = null)
                                                                {
                                                                    $this->imageFile8 = $doc;

                                                                    if (null !== $doc) {
                                                                        // It is required that at least one field changes if you are using doctrine
                                                                        // otherwise the event listeners won't be called and the file is lost
                                                                        $this->updatedAt = new \DateTimeImmutable();
                                                                    }
                                                                }

    public function getImageFile8()
    {
        return $this->imageFile8;
    }


    public function getDocName8()
    {
        return $this->docName8;
    }

    public function setDocName8($docName8)
    {
        $this->docName8 = $docName8;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }
		/**
	 * @ORM\PreUpdate
	 */
	public function updateDate()
                                       {
                                               $this->setUpdatedAt(new \Datetime());
                                       }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
	*/
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getTotalimage()
    {
        return $this->totalimage;
    }

    public function setTotalimage(int $totalimage): self
    {
        $this->totalimage = $totalimage;

        return $this;
    }

    public function getReleveimage()
    {
        return $this->releveimage;
    }

    public function setReleveimage(int $releveimage): self
    {
        $this->releveimage = $releveimage;

        return $this;
    }


    public function getAlerte()
    {
        return $this->alerte;
    }

    public function setAlerte($alerte): self
    {
        $this->alerte = $alerte;

        return $this;
    }

    public function getControle()
    {
        return $this->controle;
    }

    public function setControle($controle): self
    {
        $this->controle = $controle;
		$this->datecontrole = new \DateTimeImmutable();
        return $this;
    }

    public function getDatecontrole()
    {
        return $this->datecontrole;
    }

    public function setDatecontrole(\DateTimeInterface $datecontrole): self
    {
        $this->datecontrole = $datecontrole;

        return $this;
    }
    public function getDefaut()
    {
        return $this->defaut;
    }

    public function setDefaut($defaut)
    {
        $this->defaut = $defaut;
        return $this;
    }

    public function getSecteur()
    {
        return $this->secteur;
    }

    public function setSecteur($secteur)
    {
        $this->secteur = $secteur;
        return $this;
    }

    public function setTarifjour($tarifjour)
    {
        $this->tarifjour = $tarifjour;

        return $this;
    }

    public function getTarifjour()
    {
        return $this->tarifjour;
    }

    public function getOcr()
    {
        return $this->ocr;
    }

    public function setOcr(int $ocr): self
    {
        $this->ocr = $ocr;

        return $this;
    }

    public function getTextocr()
    {
        return $this->textocr;
    }

    public function setTextocr(string $textocr): self
    {
        $this->textocr = $textocr;

        return $this;
    }

	public function getTextalerte()
                   {
                       return $this->textalerte;
                   }

    public function setTextalerte(string $textalerte): self
    {
        $this->textalerte = $textalerte;

        return $this;
    }


	public function addTournee(\App\Entity\Tournee $tournee)
                           	  {
                           			if ($this->tournees->contains($tournee)) {
                           				return;
                           			}

                           		$this->tournees[] = $tournee;

                           		// On lie le document au pool
                           		$tournee->setEnregistrement($this);
                           		return $this;

                           	  }

	  public function removeTournee(\App\Entity\Tournee $tournee)
	  {
		$this->tournees->removeElement($tournee);
	  }

		/**
		 * Get tournees
		 *
		 * @return \App\Entity\Tournee
		 */
		public function getTournees()
		{
			return $this->tournees;
		}

		  public function getCloture()
		  {
			  return $this->cloture;
		  }

		  public function setCloture($cloture)
		  {
			  $this->cloture = $cloture;

			  return $this;
		  }

		  public function getRadioreleve()
		  {
			  return $this->radioreleve;
		  }

		  public function setRadioreleve($radioreleve)
		  {
			  $this->radioreleve = $radioreleve;

			  return $this;
		  }

    /**
     * Set totalOcr.
     *
     * @param int $totalOcr
     *
     * @return Enregistrement
     */
    public function setTotalOcr($totalOcr)
    {
        $this->totalOcr = $totalOcr;

        return $this;
    }

    /**
     * Get totalOcr.
     *
     * @return int
     */
    public function getTotalOcr()
    {
        return $this->totalOcr;
    }

    /**
     * Set totalOcrFinal.
     *
     * @param int $totalOcrFinal
     *
     * @return Enregistrement
     */
    public function setTotalOcrTournee($totalOcrTournee)
    {
        $this->totalOcrTournee = $totalOcrTournee;

        return $this;
    }

    /**
     * Get totalOcrTournee.
     *
     * @return int
     */
    public function getTotalOcrTournee()
    {
        return $this->totalOcrTournee;
    }

	public function getZone()
                 {
                     return $this->zone;
                 }

  public function setZone(Zone $zone)
  {
      $this->zone = $zone;

      return $this;
  }
  	public function getChantier()
  {
      return $this->chantier;
  }

  public function setChantier(Chantier $chantier)
  {
      $this->chantier = $chantier;

      return $this;
  }
   public function setTypeocr(Typeocr $typeocr)
    {
        $this->typeocr = $typeocr;

        return $this;
    }

	    public function getTypeocr()
    {
        return $this->typeocr;
    }

	 public function setTypeformulaire(Formulaire $typeformulaire)
    {
        $this->typeformulaire = $typeformulaire;

        return $this;
    }
    public function getTypeformulaire()
    {
        return $this->typeformulaire;
    }
   public function setTypechantier(Typechantier $typechantier)
    {
        $this->typechantier = $typechantier;

        return $this;
    }

        public function getTypechantier()
    {
        return $this->typechantier;
    }


	    public function getSumtournee()
    {
        return $this->sumtournee;
    }

    public function setSumtournee($sumtournee)
    {
        $this->sumtournee = $sumtournee;

        return $this;
    }

	 public function getSumtotal()
    {
        return $this->sumtotal;
    }

    public function setSumtotal($sumtotal)
    {
        $this->sumtotal = $sumtotal;

        return $this;
    }


     /**
     * Add enregistrementchantiermateriel.
     *
     * @param \App\Entity\EnregistrementChantierMateriel $enregistrementchantiermateriel
     *
     * @return Chantier
     */
    public function addEnregistrementchantiermateriel(\App\Entity\EnregistrementChantierMateriel $enregistrementchantiermateriel)
    {
        $this->enregistrementchantiermateriels[] = $enregistrementchantiermateriel;
        $enregistrementchantiermateriel->setEnregistrement($this);
        return $this;
    }

    /**
     * Remove chantieruser.
     *
     * @param \App\Entity\EnregistrementChantierMateriel $chantieruser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEnregistrementchantiermateriel(\App\Entity\EnregistrementChantierMateriel $enregistrementchantiermateriel)
    {
        return $this->enregistrementchantiermateriels->removeElement($enregistrementchantiermateriel);
    }

    /**
     * Get enregistrementchantiermateriels.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnregistrementchantiermateriels()
    {
        return $this->enregistrementchantiermateriels;
    }


     /**
     * Add totalrenou
     *
     * @param \App\Entity\Totalrenou $totalrenou
     *
     */
    public function addTotalrenou(\App\Entity\Totalrenou $totalrenou)
    {
        $this->totalrenous[] = $totalrenou;
        $totalrenou->setEnregistrement($this);
        return $this;
    }

    /**
     * Remove totalrenou.
     *
     * @param \App\Entity\Totalrenou $totalrenou
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTotalrenou(\App\Entity\Totalrenou $totalrenou)
    {
        return $this->totalrenous->removeElement($totalrenou);
    }

    /**
     * Get totalrenous.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTotalrenous()
    {
        return $this->totalrenous;
    }

    public function getValide()
    {
        return $this->valide;
    }

    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * @return Collection|Ncompteur[]
     */
    public function getNcompteurs(): Collection
    {
        return $this->ncompteurs;
    }

    public function addNcompteur(Ncompteur $ncompteur): self
    {
        if (!$this->ncompteurs->contains($ncompteur)) {
            $this->ncompteurs[] = $ncompteur;
            $ncompteur->setEnregistrement($this);
        }

        return $this;
    }

    public function removeNcompteur(Ncompteur $ncompteur): self
    {
        if ($this->ncompteurs->removeElement($ncompteur)) {
            // set the owning side to null (unless already changed)
            if ($ncompteur->getEnregistrement() === $this) {
                $ncompteur->setEnregistrement(null);
            }
        }

        return $this;
    }

}
