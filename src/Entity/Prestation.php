<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;


/**
 * @ORM\Table(name="prestation")
 * @ORM\Entity(repositoryClass="App\Repository\PrestationRepository")
 */
class Prestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $color;

    /**
     * @var smallint
     *
     * @ORM\Column(name="type", type="smallint", options={"default" : 1})
     */
    private $type;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\Materiel", mappedBy="prestation")
    */
    private $materiels;

	public function __construct()
		  {
            $this->materiels = new ArrayCollection();
		  }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }
    public function getColor()
    {
        return $this->color;
    }

    public function setColor(string $color)
    {
        $this->color = $color;

        return $this;
    }
        public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

   public function addMateriel(Materiel $materiel)
      {
        $this->materiels[] = $materiel;

        return $this;
      }

  public function removeMateriel(Materiel $materiel)
  {
    $this->materiels->removeElement($materiel);
  }

  public function getMateriels()
  {
    return $this->materiels;
  }

}
