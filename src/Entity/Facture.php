<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

use app\Entity\Chantier;


class Facture
{
    /**
     *
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Chantier")
    */
    private $chantier;

        /**
     * @var \Date
     */

    private $datedebut;

    /**
     * @var \Date
     */
    private $datefin;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datedebut.
     *
     * @param \DateTime $datedebut
     *
     * @return Chantier
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut.
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin.
     *
     * @param \DateTime $datefin
     *
     * @return Chantier
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin.
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

        public function getChantier()
  {
      return $this->chantier;
  }

  public function setChantier(Chantier $chantier)
  {
      $this->chantier = $chantier;

      return $this;
  }

}
