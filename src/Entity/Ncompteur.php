<?php

namespace App\Entity;

use App\Repository\NcompteurRepository;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=NcompteurRepository::class)
 */
class Ncompteur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commune;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $indexc;

    /**
     * @ORM\ManyToOne(targetEntity=Enregistrement::class, inversedBy="ncompteurs")
     */
    private $enregistrement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(?string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getIndexc(): ?string
    {
        return $this->indexc;
    }

    public function setIndexc(?string $indexc): self
    {
        $this->indexc = $indexc;

        return $this;
    }

    public function getEnregistrement(): ?Enregistrement
    {
        return $this->enregistrement;
    }

    public function setEnregistrement(?Enregistrement $enregistrement): self
    {
        $this->enregistrement = $enregistrement;

        return $this;
    }
}
