<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=App\Repository\UserRepository::class)
 * @ORM\Table(name="user")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=80, nullable=true)
     */
    private $username;

   /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    protected $plainPassword;

     /**
     * @var string
     * @ORM\Column(name="password",length=80,type="string", nullable=true)
     */
    private $password;

	/**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=40, nullable=true)
     */
    private $nom;

	/**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=40, nullable=true)
     */
    private $prenom;

	/**
     *
     * @ORM\Column(name="mobile",  type="string", length=20, nullable=true)
     */
    private $mobile;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Horaire")
    */
    private $horaire;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\Civilite")
	*/
    private $civilite;

	/**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=150, nullable=true)
     */
    private $adresse;


	/**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=10, nullable=true)
     */
    private $cp;

	/**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=true)
     */
    private $ville;

    /**
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas valide."
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

	/**
     * @var string
     *
     * @ORM\Column(name="plain", type="string", length=100, nullable=true)
     */
    private $plain;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
	*/
    private $zone;

	/**
    * @ORM\OneToMany(targetEntity="App\Entity\Enregistrement", mappedBy="user")
	*/
    private $enregistrements;

        /**
    * @ORM\OneToMany(targetEntity="App\Entity\Pointage", mappedBy="user")
    */
    private $pointages;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\Evenement", mappedBy="user")
    */
    private $evenements;

    /**
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    public function __construct()
    {
        // your own logic
            $this->lastLogin = new \DateTime('now +2 hour');
    		$this->enregistrements = new ArrayCollection();
            $this->evenements = new ArrayCollection();
	}

	 public function addEnregistrement(Enregistrement $enregistrement)
	  {
		$this->enregistrements[] = $enregistrement;

		return $this;
	  }

  public function removeEnregistrement(Enregistrement $enregistrement)
  {
    $this->enregistrements->removeElement($enregistrement);
  }

  public function getEnregistrements()
  {
    return $this->enregistrements;
  }

public function addPointage(Pointage $pointage)
      {
        $this->pointages[] = $pointage;

        return $this;
      }

  public function removePointage(Pointage $pointage)
  {
    $this->pointages->removeElement($pointage);
  }

  public function getPointages()
  {
    return $this->pointages;
  }

 public function addEvenement (Evenement $evenement)
      {
        $this->evenements[] = $evenement;

        return $this;
      }

  public function removeEvenement(Evenement $evenement)
  {
    $this->evenements->removeElement($evenement);
  }

  public function getEvenements()
  {
    return $this->evenements;
  }


  public function getId(): ?int
  {
      return $this->id;
  }

  public function getZone()
  {
      return $this->zone;
  }

  public function setZone(Zone $zone)
  {
      $this->zone = $zone;

      return $this;
  }

    /**
     * Set nom.
     *
     * @param string|null $nom
     *
     * @return User
     */
    public function setNom($nom = null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string|null
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string|null $prenom
     *
     * @return User
     */
    public function setPrenom($prenom = null)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string|null
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set mobile.
     *
     * @param string|null $mobile
     *
     * @return User
     */
    public function setMobile($mobile = null)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile.
     *
     * @return string|null
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set adresse.
     *
     * @param string|null $adresse
     *
     * @return User
     */
    public function setAdresse($adresse = null)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string|null
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp.
     *
     * @param string|null $cp
     *
     * @return User
     */
    public function setCp($cp = null)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string|null
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville.
     *
     * @param string|null $ville
     *
     * @return User
     */
    public function setVille($ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string|null
     */
    public function getVille()
    {
        return $this->ville;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPlain($plain= null)
    {
        $this->plain = $plain;

        return $this;
    }

    public function getPlain()
    {
        return $this->plain;
    }

    /**
     * Set civilite.
     *
     * @param \App\Entity\Civilite|null $civilite
     *
     * @return User
     */
    public function setCivilite(\App\Entity\Civilite $civilite = null)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite.
     *
     * @return \App\Entity\Civilite|null
     */
    public function getCivilite()
    {
        return $this->civilite;
    }
    public function getHoraire()
  {
      return $this->horaire;
  }

  public function setHoraire(Horaire $horaire)
  {
      $this->horaire = $horaire;
      return $this;
  }


    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

 /**
     * @see UserInterface
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[] = 'ROLE_FRONT';

        return $roles;
    }

    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function setLastLogin(\DateTimeInterface $lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }
}
