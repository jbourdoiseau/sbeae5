<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChantierUserRepository")
 * @UniqueEntity(fields = {"chantier", "user"},message="Ce partenaire existe déja")
 */
class ChantierUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
	
	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Chantier",inversedBy="chantierusers", cascade={"persist"})
	* @ORM\JoinColumn(nullable=false)
	*/
    private $chantier;
	
	 /**
	   * @ORM\ManyToOne(targetEntity="App\Entity\User")
	   * @ORM\JoinColumn(nullable=false)
	   */
	private $user;
	
    /**
     * @ORM\Column(type="integer")
     */
    private $panier;
	
	/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;
	
    public function getId(): int
    {
        return $this->id;
    }

    public function getPanier()
    {        
		return $this->panier;
    }

    public function setPanier(string $panier)
    {
        $this->panier = $panier;

        return $this;
    }

    /**
     * Set client.
     *
     * @param \App\Entity\Client|null $client
     *
     * @return Chantier
     */
    public function setChantier(\App\Entity\Chantier $chantier)
    {
        $this->chantier = $chantier;
        return $this;
    }
	
    public function getChantier()
    {
        return $this->chantier;
    }
    /**
     */
    public function setUser(\App\Entity\User $user)
    {
		$this->user = $user;
        return $this->user;
    }
	
    public function getUser()
    {
        return $this->user;
    }
	
	public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }
	
}
