<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use app\Entity\Zone;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChantierRepository")
 */
class Chantier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Typechantier")
    */
    private $typechantier;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="chantiers")
	*/
    private $client;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Societe")
    */
    private $societe;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\ChantierUser", mappedBy="chantier", cascade={"all"})
    */
    private $chantierusers;

    /**
    * @ORM\OneToMany(targetEntity="App\Entity\ChantierMateriel", mappedBy="chantier", cascade={"all"})
    */
    private $chantiermateriels;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $nom;

	    /**
     * @var \Date
     *
     * @ORM\Column(name="datedebut", type="date")

     */

    private $datedebut;

    /**
     * @var \Date

	 * @Assert\Expression(
	 *     "this.getDatefin() >= this.getDatedebut()",
	 *     message="Erreur : Date de début après la date de fin"
	 * )
     *
     * @ORM\Column(name="datefin", type="date")
     */
    private $datefin;

	/**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 0})
     */
    private $totalreleve;

		/**
     * @ORM\Column(type="integer", nullable=true, options={"default" : 1})
     */
    private $releveur;

	/**
     * @ORM\Column(type="decimal", precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarifreleve;

		/**
     * @ORM\Column(type="decimal", precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarifradioreleve;

		/**
     * @ORM\Column(type="decimal",precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarifinfructueux;

            /**
     * @ORM\Column(type="decimal",precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tarifjour;

    /**
     * @ORM\Column(type="decimal",precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $forfait;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
     */
    private $cadenceforfait;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
     */
    private $cadencehomme;

	/**
    * zone maitre
    * @ORM\ManyToOne(targetEntity="App\Entity\Zone")
	*/
    private $zone;

	/**
	   * @ORM\ManyToMany(targetEntity="App\Entity\Zone", inversedBy="chantiers", cascade={"persist"})
	   	* @ORM\JoinTable(name="chantier_zone",
		*    joinColumns={@ORM\JoinColumn(name="chantier_id", referencedColumnName="id")},
		*    inverseJoinColumns={@ORM\JoinColumn(name="zone_id", referencedColumnName="id")}
		* )
	 */
    protected $zones;

		/**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

	/**
     * @ORM\Column(type="decimal", precision=6, scale=2,nullable=true, options={"default" : 0})
     */
    private $tauxreleve;

    /**
     * @ORM\Column(name="captureobligatoire", type="boolean", options={"default" : 1})
     */
    private $captureobligatoire;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
     */
    private $totalafaire;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
    */
    private $budgetannuel;

      /**
     * @var text
     *
     * @ORM\Column(name="emails", type="text", nullable=true)
     */
    private $emails;

     /**
     * @ORM\Column(name="pointage", type="boolean", options={"default" : 1})
     */
    private $pointage;

	function __construct()
	{
		$this->datedebut = new \DateTime('now');
		//$this->datefin = $this->datedebut->add(new DateInterval('P30D'));
		$this->chantierusers = new ArrayCollection();
        $this->chantiermateriels = new ArrayCollection();
		$this->zones = new ArrayCollection();
        $this->captureobligatoire=0;
        $this->pointage=0;
	}

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom()
    {
		return $this->nom;
    }

    public function setNom(string $nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set typechantier.
     *
     * @param \App\Entity\Typechantier|null $typechantier
     *
     * @return Typechantier
     */
    public function setTypechantier(\App\Entity\Typechantier $typechantier = null)
    {
        $this->typechantier = $typechantier;

        return $this;
    }

    /**
     * Get typechantier.
     *
     * @return \App\Entity\Typechantier|null
     */
    public function getTypechantier()
    {
        return $this->typechantier;
    }

    /**
     * Set datedebut.
     *
     * @param \DateTime $datedebut
     *
     * @return Chantier
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut.
     *
     * @return \DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set datefin.
     *
     * @param \DateTime $datefin
     *
     * @return Chantier
     */
    public function setDatefin($datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin.
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set client.
     *
     * @param \App\Entity\Client|null $client
     *
     * @return Chantier
     */
    public function setClient(\App\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return \App\Entity\Client|null
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * Add chantieruser.
     *
     * @param \App\Entity\ChantierUser $chantieruser
     *
     * @return Chantier
     */
    public function addChantieruser(\App\Entity\ChantierUser $chantieruser)
    {
        $this->chantierusers[] = $chantieruser;

        return $this;
    }

    /**
     * Remove chantieruser.
     *
     * @param \App\Entity\ChantierUser $chantieruser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChantieruser(\App\Entity\ChantierUser $chantieruser)
    {
        return $this->chantierusers->removeElement($chantieruser);
    }

    /**
     * Get chantierusers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChantierusers()
    {
        return $this->chantierusers;
    }

    /**
     * Add chantiermateriel.
     *
     * @param \App\Entity\ChantierMateriel $chantiermateriel
     *
     * @return Chantier
     */
    public function addChantiermateriel(\App\Entity\ChantierMateriel $chantiermateriel)
    {
        $this->chantiermateriels[] = $chantiermateriel;

        return $this;
    }

    /**
     * Remove chantieruser.
     *
     * @param \App\Entity\ChantierMateriel $chantieruser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChantiermateriel(\App\Entity\ChantierMateriel $chantiermateriel)
    {
        return $this->chantiermateriels->removeElement($chantiermateriel);
    }

    /**
     * Get chantiermateriels.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChantiermateriels()
    {
        return $this->chantiermateriels;
    }
    /**
     * Set totalreleve.
     *
     * @param int $totalreleve
     *
     * @return Chantier
     */
    public function setTotalreleve($totalreleve)
    {
        $this->totalreleve = $totalreleve;

        return $this;
    }

    /**
     * Get totalreleve.
     *
     * @return int
     */
    public function getTotalreleve()
    {
        return $this->totalreleve;
    }

 /**
     * Set releveur.
     *
     * @param int $releveur
     *
     */
    public function setReleveur($releveur)
    {
        $this->releveur = $releveur;

        return $this;
    }

    /**
     * Get releveur.
     *
     * @return int
     */
    public function getReleveur()
    {
        return $this->releveur;
    }

    /**
     * Set tarifreleve.
     *
     * @param string $tarifreleve
     *
     * @return Chantier
     */
    public function setTarifreleve($tarifreleve)
    {
        $this->tarifreleve = $tarifreleve;

        return $this;
    }

    public function getTarifreleve()
    {
        return $this->tarifreleve;
    }

    /**
     * Get tarifreleve.
     *
     * @return string
     */
    public function getTarifradioreleve()
    {
        return $this->tarifradioreleve;
    }

    public function setTarifradioreleve($tarifradioreleve)
    {
        $this->tarifradioreleve = $tarifradioreleve;

        return $this;
    }


    /**
     * Set tarifinfructueux.
     *
     * @param string $tarifinfructueux
     *
     * @return Chantier
     */
    public function setTarifinfructueux($tarifinfructueux)
    {
        $this->tarifinfructueux = $tarifinfructueux;

        return $this;
    }

    /**
     * Get tarifinfructueux.
     *
     * @return string
     */
    public function getTarifinfructueux()
    {
        return $this->tarifinfructueux;
    }

    /**
     * Set tarifjour.
     *
     * @param decimal $tarifjour
     *
     * @return Tarifjour
     */
    public function setTarifjour($tarifjour)
    {
        $this->tarifjour = $tarifjour;

        return $this;
    }

    public function getTarifjour()
    {
        return $this->tarifjour;
    }

	public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Set tauxreleve.
     *
     * @param string $tauxreleve
     *
     */
    public function setTauxreleve($tauxreleve)
    {
        $this->tauxreleve = $tauxreleve;

        return $this;
    }

    /**
     * Get $tauxreleve.
     *
     */
    public function getTauxreleve()
    {
        return $this->tauxreleve;
    }
	public function getZone()
  {
      return $this->zone;
  }

  public function setZone(Zone $zone)
  {
      $this->zone = $zone;

      return $this;
  }
    public function getSociete()
  {
      return $this->societe;
  }

  public function setSociete(Societe $societe)
  {
      $this->societe = $societe;

      return $this;
  }

    public function getForfait()
  {
      return $this->forfait;
  }

  public function setForfait($forfait)
  {
      $this->forfait = $forfait;

      return $this;
  }
      public function getCadenceforfait()
  {
      return $this->cadenceforfait;
  }

  public function setCadenceforfait($cadenceforfait)
  {
      $this->cadenceforfait = $cadenceforfait;

      return $this;
  }
        public function getCadencehomme()
  {
      return $this->cadencehomme;
  }

  public function setCadencehomme($cadencehomme)
  {
      $this->cadencehomme = $cadencehomme;

      return $this;
  }
	public function addZone(Zone $zone)
		  {
			// Ici, on utilise l'ArrayCollection vraiment comme un tableau
			if ($this->zones->contains($zone)) {
				return;
			}
			$this->zones[] = $zone;
			$zone->addChantier($this);

			return $this;
		  }

		  public function removeZone(Zone $zone)
		  {
			// Ici on utilise une méthode de l'ArrayCollection, pour supprimer la catégorie en argument
			$this->zones->removeElement($zone);
			$zone->removeChantier($this);
		  }

		  // Notez le pluriel, on récupère une liste de catégories ici !
		  public function getZones()
		  {
			return $this->zones;
		  }

  public function getCaptureobligatoire()
  {
      return $this->captureobligatoire;
  }

  public function setCaptureobligatoire($captureobligatoire)
  {
      $this->captureobligatoire = $captureobligatoire;

      return $this;
  }

    public function getTotalafaire()
  {
      return $this->totalafaire;
  }

  public function setTotalafaire($totalafaire)
  {
      $this->totalafaire = $totalafaire;

      return $this;
  }
      public function getBudgetannuel()
  {
      return $this->budgetannuel;
  }

  public function setBudgetannuel($budgetannuel)
  {
      $this->budgetannuel = $budgetannuel;

      return $this;
  }

public function getEmails()
  {
      return $this->emails;
  }

  public function setEmails($emails)
  {
      $this->emails = $emails;

      return $this;
  }

    public function getPointage()
  {
      return $this->pointage;
  }

  public function setPointage($pointage)
  {
      $this->pointage = $pointage;

      return $this;
  }
}
