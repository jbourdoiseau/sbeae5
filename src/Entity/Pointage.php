<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use app\Entity\Prestation;

/**
 * @ORM\Table(name="pointage")
 * @ORM\Entity(repositoryClass="App\Repository\PointageRepository")
 */
class Pointage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="enregistrements")
    */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="typ", type="integer", options={"default" : 0})
     */
    private $typ;
    /**
     * @var \Date
     * @Assert\Date()
     *
     * @ORM\Column(name="datecrea", type="datetime")
     */
    private $datecrea;

    /**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */

    private $commentaire;


	public function __construct()
		  {
            $this->datecrea = new \DateTime('now +2 hour');
		  }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getTyp()
    {
        return $this->typ;
    }

    public function setTyp($typ): self
    {
        $this->typ = $typ;

        return $this;
    }

    public function getDatecrea()
    {
        return $this->datecrea;
    }

    public function setDatecrea(\DateTimeInterface $datecrea)
    {
        $this->datecrea = $datecrea;

        return $this;
    }

    public function getCommentaire()
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }


}
