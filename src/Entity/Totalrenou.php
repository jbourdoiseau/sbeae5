<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

use app\Entity\Prestation;
use App\Entity\Enregistrement;
/**
 * @ORM\Table(name="totalrenou")
 * @ORM\Entity(repositoryClass="App\Repository\TotalrenouRepository")
 */
class Totalrenou
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Enregistrement", inversedBy="totalrenous")
    */
    private $enregistrement;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Prestation")
    */
    private $prestation;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
     */
    private $total;

	public function __construct()
		  {
		  }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEnregistrement()
    {
        return $this->enregistrement;
    }

    public function setEnregistrement(Enregistrement $enregistrement)
    {
        $this->enregistrement = $enregistrement;

        return $this;
    }

    public function getPrestation()
    {
        return $this->prestation;
    }

    public function setPrestation(Prestation $prestation)
    {
        $this->prestation = $prestation;

        return $this;
    }

        public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

}
