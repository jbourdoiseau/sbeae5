<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnregistrementChantierMaterielRepository")
 * @UniqueEntity(fields = {"enrgistrement", "chantiermateriel"},message="Cet enregchantiermateriel existe déja")
 */
class EnregistrementChantierMateriel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
    * @ORM\ManyToOne(targetEntity="App\Entity\Enregistrement",inversedBy="enregistrementchantiermateriels", cascade={"persist"})
	* @ORM\JoinColumn(nullable=false)
	*/
    private $enregistrement;

	 /**
	   * @ORM\ManyToOne(targetEntity="App\Entity\ChantierMateriel")
	   * @ORM\JoinColumn(nullable=false)
	   */
	private $chantiermateriel;

    /**
     * @ORM\Column(type="integer",nullable=true, options={"default" : 0})
     */
    private $nombre;


    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
		return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Set client.
     *
     * @param \App\Entity\ChantierMateriel|null $client
     *
     * @return ChantierMateriel
     */
    public function setChantierMateriel(\App\Entity\ChantierMateriel $chantiermateriel)
    {
        $this->chantiermateriel = $chantiermateriel;
        return $this;
    }

    public function getChantierMateriel()
    {
        return $this->chantiermateriel;
    }
    /**
     */
    public function setEnregistrement(\App\Entity\Enregistrement $enregistrement)
    {
		$this->enregistrement = $enregistrement;
        return $this->enregistrement;
    }

    public function getEnregistrement()
    {
        return $this->enregistrement;
    }

}
