<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Tournee;
use App\Entity\Cloture;
use App\Entity\Enregistrement;
use TesseractOCR;
use Doctrine\ORM\EntityManagerInterface;
//use thiagoalessio\TesseractOCR\TesseractOCR;

class ImageController extends AbstractController
{
	private $upload;
	public function __construct(EntityManagerInterface $entityManager, $upload = '')
	{
		$this->em = $entityManager;
		$this->upload = $upload;
	}
	 public function analyse(Enregistrement $enreg = null)
    {
		//$executablePath = '"C:/Program Files (x86)/Tesseract-OCR/tesseract.exe"';
		$em = $this->em ;

        if(isset($enreg)){$enregs[0]=$enreg;}
				else{exit;}//$enregs = $em->getRepository('App:Enregistrement')->findByOcr(0);


		foreach ($enregs as $enreg){

			// si formulaire avec image
			if(($enreg->getChantier()->getClient()->getFormulaire()->getNom() == 'image')||($enreg->getChantier()->getClient()->getFormulaire()->getNom() == 'veolia_v')){
				if($enreg->getTextocr()==""){
					$enreg->setAlerte(1);
				}
				$em->flush();
			}
		}
		/**/
		// Show recognized text

		//variables ocr
		$result=$tournee_global='';

//RAJOUTER LES CONTROLES
		$matches=$ocr= array();
		$erreur=$add=0;
		//erreur 1 : total ocr non detecté - pas d'en tete
		foreach ($enregs as $enreg){
			$acteur='';
			$k=0;
			$q=-1;
			$TOTAL=$TOTAL0=$total_releve=$total_tournees=$coord_tournee=$releve_tournee=$infruct_tournee=$restant_tournee=$releve_tournee_total=$infruct_tournee_total=0;
			$coord_tournee=$titre='';
			$lines=$enreg->getTextocr();
			//echo $lines.'<br><br>';
			$lines = explode("\n", $lines);
			$releve = '';
			$inf ='';
			$numline=$new=0;

			echo "LANCEMENT ANALYSE";
				foreach($lines as $line){
					$numline++;
					$line=str_replace("|", "", $line);
					//$vowels = array("o", "O","O");
					//$line=str_replace($vowels, "0", $line);//T0urnée manuelle 09 — 01 Q
					echo $line."\n".'<br>';
					if(preg_match('#Tournée manuelle(\ )*(\d+)(\ )*-(\ )*(\d+)(\ )*(\w+)*#', $line, $matches)){

					//echo '<br>'.'<br>'.'ok'.$line."\n".'<br>';
					$tournee_global=$matches[2].'-'.$matches[5];
					$titre="Tournée manuelle ".$matches[2].'-'.$matches[5];
					$coord_tournee='';
							//gestion alerte
							//ai je eu un enregistrement ocr sur le precedent sinon alerte
							if(isset($enreg_temp)){
								if($enreg_temp->getOcr() == 0 ){
									$message="OCR non finalisé - ".$acteur;
									$enreg_temp->setOcr(1);
									if($enreg_temp->getCloture() == 1){
										$enreg_temp->setAlerte(1);
									}
									$enreg_temp->setTextalerte($message);
									$em->flush();
								}
							}
							//je stocke l'enregistrement temporaire
							$enreg_temp=$enreg;
					$acteur='veolia';
					echo 'detect'.$acteur;
					}

					if(preg_match('#^Commune#', $line, $matches)){
							$titre="";
							$coord_tournee='';
							$acteur='suez';
							echo 'detect'.$acteur;
					}

					if($acteur == 'suez'){
								//echo $line.'<br>';

								if(preg_match('#Commune (\d+)\/(\d+)( )*([a-zA-Z]+(?:[\s-][a-zA-Z]+)*)*#', $line, $matches)){
										$commune=$matches[4];
										$new=$numline;
										$releve = $totaljour=$total=$restant=$inf = '';
										$add=1;
										$titre=($titre??'').$commune.' ';
										echo "idenreg".$enreg->getId().':'.$commune.'<br>';
								}
								if(preg_match('#(\d+)/(\d+)(\ )(\d+)(\w+)*#', $line, $matches) && ($add == 1)){
										if($numline>$new){
											$add=2;
											$totaljour=$matches[1];$total=$matches[2];$restant=$total-$totaljour;$new=$numline;
											echo 'totj'.$totaljour.'-tot-'.$total.'<br>';
										}
								}
								if(preg_match('#^(\d+)#', $line, $matches) && ($add == 2)){
									if($numline>$new){
										echo 'rl'.$releve;
										$releve=$matches[1];
										$inf=$totaljour-$releve;
										$new=$numline;
									}
										echo 'rel'.$releve.'<br>inf:'.$inf.'<br>'.$line.'<br>';
								}
								if(($releve != '')&&($inf !='') && ($add == 2)){
								$tournee=new Tournee();
								$tournee->setEnregistrement($enreg);
								$tournee->setCode($commune);
								$tournee->setReleve($releve);
								//recherche des inf du precedent surla commune dans les 7 jours
								$infprec=$em->getRepository('App:Tournee')->finLastInf($commune, $total);
										//echo 'infprec'.$infprec.'-<br>';

								if($infprec == ''){
									$infprec['infructueux']=0;
									echo 'rien troyuvé';
									//debut de tournee - alerte
									$message="SUEZ Préc non trouvé ";
									$enreg->setAlerte(1);
									$enreg->setTextalerte($message);
								}
								else{

										echo 'infprec'.$infprec['infructueux'].'<br>';

								}
								//print_r($infprec);
								$infcalcul=$inf-$infprec['infructueux'];
								$total_releve=($total_releve??0)+$infcalcul+$releve;
								$tournee->setInfructueux($infcalcul);
								$tournee->setInfructueuxsuez($inf);
								$totalcorr=$infcalcul+$releve;
								$tournee->setTotal($totalcorr);
								$tournee->setRestant($restant);
								$tournee->setCloture(1);
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(1));
								$enreg->setTitre($titre);
								$em->persist($tournee);
								$releve = '';
								$inf ='';
								$ocr[$commune]=1;
								$add=0;
								}

					}

					if($acteur == 'veolia'){
						// par défaut alerte
						$alerte=1;
						echo 'VEOLIA'.$line.'<br>';
						if(preg_match('#Nombre(\ )*de(\ )*relevé(s)*(\ )*total :(\ )*(\d+)(\ )*#', $line, $matches)){
							echo 'NRT'.$line.'<br>';
							if($q==-1){
								$total_releve=$matches[6];
							echo 'NRT'.$total_releve.'<br>';
								$enreg->setTotalOcr($total_releve);
							}
							else{
								$total_tournee=$matches[6];
								echo 'OK'.$coord_tournee;
							}
						}
						if(preg_match('#(\w+)(\ )*-(\ )*(\w+)(\ )*-(\ )*(\w+)#', $line, $matches)){
							$coord_tournee=$matches[1].'-'.$matches[4].'-'.$matches[7];
							$enreg->setSecteur($matches[1]);
							//depart à 0
						}
						if((preg_match('#Nombre(\ )*dé(..)*(\ )*relevé(s)*(\ )*:(\ )*(\d+)(\ )*(\D+)(\ )*(\d+)#', $line, $matches))&&($q>=0)){

							$releve_tournee=$matches[7];
							echo $line.'<br>';
							echo 'RT'.$releve_tournee.'<br>';
							if (isset($matches[11])){
							$infruct_tournee=$matches[11];
							echo $line.'<br>';
							echo 'INF'.$infruct_tournee.'<br>';
							}
							else {$infruct_tournee=0;}
							//echo 'RL-'.$matches[0][0].'-'.$matches[0][1].'-'.$matches[0][2].'-'.$matches[0][3];
							//echo 'releves'.$releve_tournee.'tournee'.$infruct_tournee.'-';
						}
						if((preg_match('#Nombre(\ )*dé(..)*(\ )*relevé(s)*(\ )*:(\ )*(\d+)(\ )*(\D+)(\ )*(\d+)#', $line, $matches))&&($q<0)){

							$releve_tournee_total=$matches[7];
							echo $line.'<br>';
							echo 'RTI'.$releve_tournee_total.'<br>';

							if(isset($matches[11])){
							$infruct_tournee_total=$matches[11];
							echo $line.'<br>';
							echo 'INFI'.$infruct_tournee_total.'<br>';
							}
							else{$infruct_tournee_total=0;}
							$total_tournee=$releve_tournee_total + $infruct_tournee_total;
							$enreg->setCloturereleve($releve_tournee_total);
							$enreg->setClotureinfructueux($infruct_tournee_total);
							$enreg->setCloturetotal($total_tournee);
							//echo 'releve'.$releve_tournee.'tournee'.$infruct_tournee.'-';
						}
						if(preg_match('#Nombre de relevé(s)* restant(s)* : (\d+)#', $line, $matches)){
							//print_r($matches);
							preg_match_all('/\d+/', $line, $matches);
							$restant_tournee=(int)$matches[0][0];
							//$restant_tournee=$matches[0];
							echo 'relevé restant'.$restant_tournee.'-';
							if($q==-1){
								$enreg->setTotalOcr($total_releve);
								$enreg->setRestantocr($restant_tournee);
							}
							if($q>=0){
								if($coord_tournee==''){
									$coord_tournée="erreur coord tournée";
									$message="OCR non finalisé - ".$acteur;
									if($enreg->getCloture() == 1){
										$enreg->setAlerte(1);
									}
									$enreg->setTextalerte($message);
								}
								if(isset($total_tournee)){
									if($coord_tournee != ''){
								$coord_tournee.='_'.$total_tournee;}
								//insertion dans la base

								//detection si tournee déja enregistré sur l'ocr en cours
								if(!isset($ocr[$coord_tournee])&&(isset($total_tournee))){
								$total_tournee=intval(str_replace('o','0',$total_tournee));
								$total_tournee=intval(str_replace('O','0',$total_tournee));
								echo '<br>tt'.$total_tournees.'-'.$total_tournee;
								$total_tournees=$total_tournees+$total_tournee;
								$TOTAL0=$TOTAL0+$total_tournee;
								$tournee=new Tournee();
								$tournee->setEnregistrement($enreg);
								$tournee->setCode($coord_tournee);
								$tournee->setTotal($total_tournee);
								$tournee->setReleve($releve_tournee);
								$tournee->setInfructueux($infruct_tournee);
								$tournee->setRestant($restant_tournee);
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(1));
								$enreg->setTitre($titre);
								$tournee->setCloture($enreg->getCloture());

								//$enreg->setOcr(1);
								$enreg->setTotalOcrTournee($TOTAL0);
								$em->persist($tournee);
								$em->flush();
								$ocr[$coord_tournee]=1;
								}
								}
								$coord_tournee='';
								$total_tournee=$releve_tournee=$infruct_tournee=$restant_tournee=0;

							}
							//je passe le 1er enregistrement du haut
							$q++;
						}

					}//fin veolia standard

					// cas RR veolia
					if(preg_match('#Tournées radi0#', $line, $matches)){
					//$tournee_global=$matches[1].'-'.$matches[2];
					$titre=$line;
					$coord_tournee='';
							//gestion alerte
							//ai je eu un enregistrement ocr sur le precedent sinon alerte
							if(isset($enreg_temp)){
								if($enreg_temp->getOcr() == 0 ){
									$message="OCR non finalisé - ".$acteur;
									//$enreg_temp->setOcr(1);
									if($enreg_temp->getCloture() == 1){
										$enreg_temp->setAlerte(1);
									}
									$enreg_temp->setTextalerte($message);
									$em->flush();
								}
							}
							//je stocke l'enregistrement temporaire
							$enreg_temp=$enreg;
					$acteur='veoliaRR';
					}

					if($acteur == 'veoliaRR'){
						//echo 'veolia';
						// par défaut alerte
						$alerte=1;
						if(preg_match('#Nombre(\ )*de(\ )*relevé(\ )*total :(\ )*(\d+)(\ )*#', $line, $matches)){
							//echo $line;
							if($q==-1){
								$total_releve=$matches[5];
								$enreg->setTotalOcr($total_releve);
							}
							else{
								$total_tournee=$matches[5];
							}
						}
						if(preg_match('#(\w+)_(\w+)_(\w+)#', $line, $matches)){
							$coord_tournee=$matches[1].'_'.$matches[2].'_'.$matches[3];
							$enreg->setSecteur($matches[1]);
							//echo $line;
							//depart à 0
						}
						if(preg_match('#Nombre(\ )*dé(...)*(\ )*relevé(\ )*:(\ )*(\d+)(\ )*(\D+)#', $line, $matches)&&($q>=0)){

							$releve_tournee=$matches[6];
							$infruct_tournee=0;
							//echo 'releves'.$releve_tournee.'tournee'.$infruct_tournee.'-';
						}
						if(preg_match('#Nombre(\ )*dé(...)*(\ )*relevé(\ )*:(\ )*(\d+)(\ )*(\D+)#', $line, $matches)&&($q<0)){
							$releve_tournee_total=$matches[6];
							$total_tournee=$releve_tournee_total;
							$enreg->setCloturereleve($releve_tournee_total);
							$enreg->setClotureinfructueux(0);
							$enreg->setCloturetotal($total_tournee);
							//echo 'releve'.$releve_tournee.'tournee'.$infruct_tournee.'-';
						}
						if(preg_match('#Nombre de relevé restant : (\d+)#', $line, $matches)){
							preg_match_all('/\d+/', $line, $matches);
							$restant_tournee=$matches[0][1];
							//echo 'relevé restant'.$restant_tournee.'-';
							if($q==-1){
								$enreg->setTotalOcr($total_releve);
								$enreg->setRestantocr($restant_tournee);
							}
							if($q>=0){
								if($coord_tournee==''){
									$coord_tournée="erreur coord tournée";
									$message="OCR non finalisé - ".$acteur;
									if($enreg->getCloture() == 1){
										$enreg->setAlerte(1);
									}
									$enreg->setTextalerte($message);
								}
								if(isset($total_tournee)){
								$coord_tournee.='_'.$total_tournee;}
								//insertion dans la base

								//detection si tournee déja enregistré sur l'ocr en cours
								if(!isset($ocr[$coord_tournee])&&(isset($total_tournee))){
								$total_tournees+=$total_tournee;
								$TOTAL0=$TOTAL0+$total_tournee;
								$tournee=new Tournee();
								$tournee->setEnregistrement($enreg);
								$tournee->setCode($coord_tournee);
								$tournee->setTotal($total_tournee);
								$tournee->setReleve($releve_tournee);
								$tournee->setInfructueux(0);
								$tournee->setRestant($restant_tournee);
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(5));
								$enreg->setTitre($titre);
								$enreg->setRadioreleve(1);
								$tournee->setCloture($enreg->getCloture());
								//$enreg->setOcr(1);
								$enreg->setTotalOcrTournee($TOTAL0);

								$em->persist($tournee);
								$em->flush();
								$ocr[$coord_tournee]=1;
								}
								$coord_tournee='';
								$total_tournee=$releve_tournee=$infruct_tournee=$restant_tournee=0;

							}
							//je passe le 1er enregistrement du haut
							$q++;
						}

					}//fin veolia RR

					//cas VEOLIA vendée

					if(preg_match('#TOURNEE/LOT TOTAL LUS RESTE#', $line, $matches)){
						$titre='VEOLIA Vendée';
						echo 'vendée';
						$line=str_replace($line,'.','');
							//gestion alerte
							//ai je eu un enragistrement ocr sur le precedent sinon alerte
							/*
							if(isset($enreg_temp)){
								if($enreg_temp->getOcr() == 0){
									$message="OCR non finalisé - ".$acteur;
									$enreg_temp->setOcr(1);
									if($enreg_temp->getCloture() == 1){
										$enreg_temp->setAlerte(1);
									}
									$enreg_temp->setTextalerte($message);
									$em->flush();
								}
							}
							*/
							//je stocke l'enregistrement temporaire
							$enreg->setCloture(1);
							$enreg_temp=$enreg;
						$acteur = 'veolia_v';
					}
					if($acteur == 'veolia_v'){
						// par défaut alerte
						$alerte=1;
							//RY1/014 91 62 29
							if(preg_match('#([a-zA-Z0-9\s\.\/]+) (\d+) (\d+) (\d+)#', $line, $matches)){
								//echo $line;
								$coord_tournee=$matches[1];
								$total_tournee=$matches[2];
								$releve_tournee=$matches[3];
								$restant_tournee=$matches[4];
								$infruct_tournee=0;

								if(isset($total_tournee)){$coord_tournee.='_'.$total_tournee;}

								if(!isset($ocr[$coord_tournee])&&(isset($total_tournee))){
								$total_tournees+=$total_tournee;
								$TOTAL0=$TOTAL0+$releve_tournee;
								$tournee=new Tournee();
								$tournee->setEnregistrement($enreg);
								$tournee->setCode($coord_tournee);
								$tournee->setTotal($total_tournee);
								$tournee->setReleve($releve_tournee);
								$tournee->setInfructueux($infruct_tournee);
								$tournee->setRestant($restant_tournee);
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(2));
								$enreg->setTitre($coord_tournee);
								$tournee->setCloture(true);
								//$enreg->setOcr(1);
								$enreg->setTotalOcrTournee($total_tournees);
								$enreg->setTotalOcr($TOTAL0);
								//$enreg->setReleve($TOTAL0);
								$em->persist($tournee);
								$em->flush();
								$ocr[$coord_tournee]=1;
								//$k=0;
								}
							}

					}
					//echo $line."\n";
					if(preg_match('#Synchr0nisation Tournées#', $line, $matches)){
						$line=str_replace($line,'.','');
						$titre='Rennes';
							//gestion alerte
							//ai je eu un enragistrement ocr sur le precedent sinon alerte
							/*
							if(isset($enreg_temp)){
								if($enreg_temp->getOcr() == 0){
									$message="OCR non finalisé - ".$acteur;
									$enreg_temp->setOcr(1);
									if($enreg_temp->getCloture() == 1){
										$enreg_temp->setAlerte(1);
									}
									$enreg_temp->setTextalerte($message);
									$em->flush();
								}
							}*/
							//je stocke l'enregistrement temporaire
							$enreg_temp=$enreg;
							$acteur = 'rennes';

						//echo $acteur;
					}
					if($acteur == 'rennes'){
						//echo $line;
							if(preg_match('#^(\d+)$#', $line, $matches)){
							$coord_tournee=$matches[1];
							}
							if(preg_match('#Nb ab0nnés : (\d+)#', $line, $matches)){
							$total_tournee=$matches[1];
							}
							if(preg_match('#Nb ab0nnés relevés : (\d+)#', $line, $matches)){
							$releve_tournee=$matches[1];

								if(isset($total_tournee)){$coord_tournee.='_'.$total_tournee;}

								if(!isset($ocr[$coord_tournee])&&(isset($total_tournee))){
								$total_tournees+=$total_tournee;
								$TOTAL0=$TOTAL0+$total_tournee;
								$tournee=new Tournee();
								$tournee->setEnregistrement($enreg);
								$tournee->setCode($coord_tournee);
								$tournee->setTotal($total_tournee);
								$tournee->setReleve($releve_tournee);
								$tournee->setInfructueux($infruct_tournee);
								$tournee->setRestant($restant_tournee);
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(3));
								$tournee->setCloture(true);
								$enreg->setTitre($titre);
								$enreg->setTotalOcrTournee($TOTAL0);
								$enreg->setTotalOcr($TOTAL0);
								$enreg->setOcr(1);
								$em->persist($tournee);
								$em->flush();
								$ocr[$coord_tournee]=1;
								}
							}
					}
				}

		//ALERTES
		//gestion des alertes
				//controle sur suez
				if($acteur == 'suez'){
								$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(6));
								$enreg->setTotalOcrTournee($total_releve);
								$enreg->setTotalOcr($total_releve);
								//$enreg->setOcr(1);

				}
				//cas veolia pas de total releve detecté en 1
				//echo 'total'.$enreg->getTotalOcr().$acteur;
				if(($enreg->getTotalOcr()== 0)&&(($acteur == 'veolia')||($acteur == 'veolia_v'))){$erreur='1';}
				//echo 'totaly'.$enreg->getTotalOcr();
				//Cas OCR non reconnu
				if(count($lines) == 0){
					$message="Pas d'OCR";
					$enreg->setOcr(1);
					$enreg->setAlerte(1);
					$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(4));
					$enreg->setTextalerte($message);
					$em->flush();
				}
				//cas acteur non reconnu
				elseif($acteur == ''){
					$message="Acteur non reconnu";
					//$enreg->setOcr(1);
					$enreg->setTypeocr($em->getRepository('App:Typeocr')->find(4));
					$enreg->setAlerte(1);
					$enreg->setTextalerte($message);
					$em->flush();
				}
 			if(($enreg->getTotalOcr()!= $enreg->getTotal())&&($acteur == 'veolia_v')){
					$message="Delta RL OCR / RL";
					//echo 'delta veolia_v';

					$enreg->setAlerte(1);
					$enreg->setTextalerte($message);
				}

				//echo $message;
				//detection des totaux non conformes sur les enregistrements de cloture
					//alerte si non controlé (?)
					//incoherence total tournée total ocr
					$enregs = $em->getRepository('App:Enregistrement')->findTotalDiff();
					$message='Incohérence total OCR / tournées';
					foreach($enregs as $enreg){
						if($enreg->getCloture() == 1){
							$enreg->setAlerte(1);
						}
						$enreg->setTextalerte($message);
						$em->flush();
					}
		}
		//exit;
		//retour
			return $erreur;
		//retour
			//return new Response(1);

		/**/
		//return new Response($result);
		//return $this->render('image.html.twig');


    }
	private function grey($filepath){
		$info = new \SplFileInfo($filepath);
		$ext=$info->getExtension();
		$im='';
		if(($ext == 'jpg') || ($ext == 'jpeg')){
		$im = imagecreatefromjpeg($filepath);
		}
		if($ext == 'png'){
		$im = imagecreatefrompng($filepath);
		}

		if($im !=''){
			imagefilter($im,IMG_FILTER_GRAYSCALE);
			//imagefilter($im,IMG_FILTER_CONTRAST,-100);

			if(($ext == 'jpg') || ($ext == 'jpeg')){
			$im = imagejpeg($im,$filepath);
			}
			if($ext == 'png'){
			$im = imagepng($im,$filepath);
			}
		}
	}


}
