<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use App\Service\FonctionsService;
use App\Controller\SecurityController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Knp\Snappy\Pdf;
use Twig\Environment;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\Graph;

class GraphController extends AbstractController
{

  public function __construct(FonctionsService $fonctions, SecurityController $security, \Swift_Mailer $mailer, Environment $twig, Pdf $pdf)
    {
        $this->fonctions = $fonctions;
        $this->security = $security;
        $this->titre = 'Statistiques';
        $this->mailer = $mailer;
        $this->pdf = $pdf;
        $this->twig = $twig;
    }
public function stathtmlClient(Request $request){
    $this->security->checkUser($this);
    $this->titre="Activité";
    $zone = $this->getUser()->getZone()->getId();
    $em = $this->getDoctrine()->getManager();
    $chantiers = $this->getUser()->getZone()-> getChantiers();
    //echo $date1->format('Y-m-d');;

    //$chantiers=$em->getRepository('App:Chantier')->rechercheChantiersActif($zone);
    $options=array();
    foreach ($chantiers as $chantier){
        $idch=$chantier->getId();
        //echo 'id'.$idch;
        $options = $this->calcul_options($chantier, $options);
      $delta=0;

      if($options[$idch]["totalrel"] >0){
            $delta=$totaleffect-round($options[$idch]["totalrel"]);
            if($Datecur==$Datefin){$delta=$totaleffect-$options[$idch]["totalr"];}
            $options[$idch]["deltad"]=round($delta);
            if($delta<0){$options[$idch]["avancement"] = 'class="bg-danger"';}
            elseif ($delta==0) {$options[$idch]["avancement"] = '';}
            else{$options[$idch]["avancement"]= 'class="bg-success"';}
      }
      $TR=$chantier->getTauxreleve();
      if($TR==0){$TR='';}
      $options[$idch]["delta"]='';

  }

    //echo 'count'.count($chantiers);
usort($chantiers, function($a, $b) {
    $c = $a->getZone()->getId() - $b->getZone()->getId();
    if($a->getNom() > $b->getNom()){
    $c .= 1;
    }
    if($a->getNom() < $b->getNom()){
    $c .= -1;
    }
    return $c;
});

      if($this->getUser()->getZone()->getAbrev() == 'CL'){$rep="Client";$repo="client";}
      else{$rep="Admin";$repo="admin";}


    if($zone==1001){$rep="SuperAdmin";$repo="superadmin";}
    if($zone==1000){$rep="Hebline";$repo="hebline";}

      return $this->render('Generique/Stats/Activite/etat2.html.twig', array(
        'titre' => $this->titre,
        'chantiers' => $chantiers,
        'options' => $options,
        'repo' => $repo,
       ));
  }

  public function statClient(Request $request, $site = null){

    $this->titre = 'Board';
    $em = $this->getDoctrine()->getManager();
    //recherche
    $enregs = $em->getRepository('App:Enregistrement')->findByChantier($site);
    $chantier = $em->getRepository('App:Chantier')->find($site);
    $total=$chantier->getTotalreleve();
    $cadenceN1=$chantier->getTauxreleve();
    $cloture=$CLinfruct=$releve=$RLinfruct=0;

    //recuperer les dates debut et fin
    $Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
    $Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
    $curday2= new \DateTime('now');
    $curday=$curday2->format('Y-m-d');
    $opendays=$this->fonctions->get_open_days($Datedebut,$Datefin);
    $nbopendays=$this->fonctions->get_nb_open_days($Datedebut,$Datefin);

    $cadence=$total/$nbopendays;

    $donnees=$donnees2='';
    $q=0;
    foreach($opendays as $op){
      $color='';
      $cadenceq=round($q*$cadence);
      $cadencen=round($q*$cadenceN1);
      if($op == $curday){$color=', "fill": "green"';}
      if($op < $curday){
        $totr=$em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)['totreleve'];
      }else{
        $totr=0;
      }

      $donnees.='{"x": "'.$op.'", "value": "'.$totr.'", "text":"Releve"'.$color.'},';
      $donnees2.='{"x": "'.$op.'", "value": "'.$cadenceq.'", "title":""},';
      $donnees3.='{"x": "'.$op.'", "value": "'.$cadencen.'", "title":""},';
      $q++;
    }
    $donnees=substr($donnees, 0, -1);
    $donnees2=substr($donnees2, 0, -1);
    //exit;

$data_string = '{
      "chart": {
        "type": "column",
        "title": "'.$site.'",
        "series": [{
          "seriesType": "column",
          "data": ['.$donnees.']
              },
          {
           "seriesType": "line",
          "data": ['.$donnees2.']
          },
          {
           "seriesType": "line",
          "data": ['.$donnees3.']
          }
          ]

      }
      }';

    $data_string = json_encode($data_string);
    return new Response($data_string);

    return $this->render('Admin/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
      'titre' => 'test',
        ));
  }

public function stathtmlDetailGraph(Request $request){
    $this->security->checkUser($this);
    $this->titre="Activité";
    $zone = $this->getUser()->getZone()->getId();
    $em = $this->getDoctrine()->getManager();

    $graph = new Graph();
    $form_graph = $this->get('form.factory')->create(\App\Form\GraphType::class, $graph);
    $chantiers=$options=$datess=$donnees=$donnees2=$donnees3=$donnees4=array();
    $form_graph->handleRequest($request);
    $options= array();
    if ($form_graph->isSubmitted()){
        $chantier=$graph->getChantier();
        $chantiers[]=$chantier;
        $idch=$chantier->getId();
        //echo 'id'.$idch;
        $options = $this->calcul_options($chantier, $options);

      $TR=$chantier->getTauxreleve();
      if($TR==0){$TR='';}

    $enregs = $em->getRepository('App:Enregistrement')->findBy(array('chantier' => $idch, 'valide' => 1));
    $chantier = $em->getRepository('App:Chantier')->find($idch);
    $total=$chantier->getTotalreleve();
    $cloture=$CLinfruct=$releve=$RLinfruct=0;

    //recuperer les dates debut et fin
    $Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
    $Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
    $curday2= new \DateTime('now');
    $curday=$curday2->format('Y-m-d');
    $opendays=$repo = $this->fonctions->get_open_days($Datedebut,$Datefin);
    $nbopendays=$this->fonctions->get_nb_open_days($Datedebut,$Datefin);
    $cadence=$total/$nbopendays;
    $count=$em->getRepository('App:Enregistrement')->findDistinctDay($chantier);
    $totaln=((100 - $options[$idch]["tauxreleveglobal"])*$total)/100;
    $totaln1=((100 - $chantier->getTauxreleve())*$total)/100;
    $options[$idch]["tauxreleveglobaln1"] = $chantier->getTauxreleve();
    //caclul des données
    $donnees[$idch]=$donnees2[$idch]=$donnees3[$idch]=$donnees4[$idch]=$datess[$idch]=array();
    $q=0;
$totaljour=0;
    foreach($opendays as $op){
      if($op < $curday){
        if(isset($em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)['total'])){
        $totaljour=$em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)['total'];
      }
      }
      $datess[$idch][]=$op;
      $donnees[$idch][]=$totaljour;
      $donnees2[$idch][]='';
      $donnees3[$idch][]='';
      $donnees4[$idch][]='';
      //if($op >= $curday){$donnees3[$idch][$q]=$totaln;}
      $q++;
    }
      $donnees2[$idch][0]=0;
      $donnees2[$idch][$q]=$total;
      $donnees3[$idch][0]=0;
      $donnees3[$idch][$q]=$totaln;
      $donnees4[$idch][0]=0;
      $donnees4[$idch][$q]=$totaln1;


  }//fin chantiers



      if($this->getUser()->getZone()->getAbrev() == 'CL'){$rep="Client";$repo="client2";}
      else{$rep="Admin";$repo="admin2";}

    if($zone==1001){$rep="SuperAdmin";$repo="superadmin";}
    if($zone==1000){$rep="Hebline";$repo="hebline";}

    return $this->render('Generique/Stats/Activite/etatgraph.html.twig', array(
        'titre' => $this->titre,
        'form_graph'       => $form_graph->createView(),
        'chantiers' => $chantiers,
        'options' => $options,
        'repo' => $repo,
        'dates' => $datess,
        'donnees' => $donnees,
        'donnees2' => $donnees2,
        'donnees3' => $donnees3,
        'donnees4' => $donnees4,
       ));
  }

public function stathtmlClientGraph(Request $request){

    $this->security->checkUser($this);
    $this->titre="Activité";
    $zone = $this->getUser()->getZone()->getId();
    //echo $zone;
    $em = $this->getDoctrine()->getManager();

    //$chantiers = $this->getUser()->getZone()-> getChantiers();
    $chantiers = $em->getRepository('App:Chantier')->findBy(array('zone' => $this->getUser()->getZone()), array('nom' => 'ASC'));
    $chantiers =$this->getUser()->getZone() -> getChantiers();
    //echo $date1->format('Y-m-d');;
    if($zone==1000){
          $chantiers=$em->getRepository('App:Chantier')->findBy(array(), array('nom' => 'ASC'));
    }
//echo 'id'.count($chantiers);
    //$chantiers=$em->getRepository('App:Chantier')->rechercheChantiersActif($zone);
    $donnees=$donnees2=$donnees3=$donnees4=$options=$datess=array();
    foreach ($chantiers as $chantier){
        $idch=$chantier->getId();
        echo 'id'.$idch;
      $options= $this->calcul_options($chantier, $options);
      $TR=$chantier->getTauxreleve();
      if($TR==0){$TR='';}

    $enregs = $em->getRepository('App:Enregistrement')->findBy(array('chantier' => $idch, 'valide' => 1));
    $chantier = $em->getRepository('App:Chantier')->find($idch);
    $total=$chantier->getTotalreleve();
    $cloture=$CLinfruct=$releve=$RLinfruct=0;

    //recuperer les dates debut et fin
    $Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
    $Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
    $curday2= new \DateTime('now');
    $curday=$curday2->format('Y-m-d');
    $opendays=$repo = $this->fonctions->get_open_days($Datedebut,$Datefin);
    $nbopendays=$this->fonctions->get_nb_open_days($Datedebut,$Datefin);
    $count=$em->getRepository('App:Enregistrement')->findDistinctDay($chantier);
    $totaln=((100 - $options[$idch]["tauxreleveglobal"])*$total)/100;
    $totaln1=((100 - $chantier->getTauxreleve())*$total)/100;
    $options[$idch]["tauxreleveglobaln1"] = $chantier->getTauxreleve();

    //caclul des données
    $donnees[$idch]=$donnees2[$idch]=$donnees3[$idch]=$donnees4[$idch]=$datess[$idch]=array();
    $q=0;

    foreach($opendays as $op){
      if($op < $curday){
        $totaljour=$em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)['total'];
      }else{
        $totaljour=0;
      }
      $datess[$idch][]=$op;
      $donnees[$idch][]=$totaljour;
      $donnees2[$idch][]='';
      $donnees3[$idch][]='';
      $donnees4[$idch][]='';
      //if($op >= $curday){$donnees3[$idch][$q]=$totaln;}
      $q++;
    }
      $donnees2[$idch][0]=0;
      $donnees2[$idch][$q]=$total;
      $donnees3[$idch][0]=0;
      $donnees3[$idch][$q]=$totaln;
      $donnees4[$idch][0]=0;
      $donnees4[$idch][$q]=$totaln1;


  }//fin chantiers



      if($this->getUser()->getZone()->getAbrev() == 'CL'){$rep="Client";$repo="client2";}
      else{$rep="Admin";$repo="admin2";}

    if($zone==1001){$rep="SuperAdmin";$repo="superadmin";}
    if($zone==1000){$rep="Hebline";$repo="hebline";}

      return $this->render('Generique/Stats/Activite/etatgraph.html.twig', array(
        'titre' => $this->titre,
        'chantiers' => $chantiers,
        'options' => $options,
        'repo' => $repo,
        'dates' => $datess,
        'donnees' => $donnees,
        'donnees2' => $donnees2,
        'donnees3' => $donnees3,
        'donnees4' => $donnees4,
       ));
  }

  public function exportPDFGraph(Request $request){

    $em = $this->getDoctrine()->getManager();
    //recup de la liste des chantiers actifs pour les clients actifs
    //recherche dimanche précédent
    $date2=new \DateTime('last Sunday');
    $date1 = clone $date2;
    $date1 = $date1->sub(new \DateInterval('P6D'));
    //echo date_format($date1, 'Y-m-d');
   //echo date_format($date2, 'Y-m-d');

    $chantiers2 = $em->getRepository('App:Chantier')->findChantiersEnCoursClientEnCours($date1,$date2,1);
    //$chantiers2[] = $em->getRepository('App:Chantier')->find(61);
    $graph = new Graph();

    $options=array();
    foreach ($chantiers2 as $chantier){
      if(($chantier->getEmails() != '')&&($chantier->getTypechantier()->getId() == 1)){//que chantier releve
      $chantiers=$options=$datess=$donnees=$donnees2=$donnees3=$donnees4=array();
      $chantiers[] = $chantier;
      $idch=$chantier->getId();
      //echo 'idch'.$idch.'-';
      $options= $this->calcul_options($chantier, $options);
      $TR=$chantier->getTauxreleve();
      if($TR==0){$TR='';}

      $enregs = $em->getRepository('App:Enregistrement')->findBy(array('chantier' => $idch, 'valide' => 1));
      $chantier = $em->getRepository('App:Chantier')->find($idch);
      $total=$chantier->getTotalreleve();
      $cloture=$CLinfruct=$releve=$RLinfruct=0;

      //recuperer les dates debut et fin
      $Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
      $Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
      $curday2= new \DateTime('now');
      $curday=$curday2->format('Y-m-d');
      $opendays=$repo = $this->fonctions->get_open_days($Datedebut,$Datefin);
      $nbopendays=$this->fonctions->get_nb_open_days($Datedebut,$Datefin);
      $count=$em->getRepository('App:Enregistrement')->findDistinctDay($chantier);
    $totaln=((100 - $options[$idch]["tauxreleveglobal"])*$total)/100;
    $totaln1=((100 - $chantier->getTauxreleve())*$total)/100;
    $options[$idch]["tauxreleveglobaln1"] = $chantier->getTauxreleve();


      //caclul des données
      $donnees[$idch]=$donnees2[$idch]=$donnees3[$idch]=$donnees4[$idch]=$datess[$idch]=array();
      $q=0;

          foreach($opendays as $op){
            if($op < $curday){
              if(null != $em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)){
              $totaljour=$em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotalDate($chantier,$op)['total'];}else{$totaljour=0;}
            }else{
              $totaljour=0;
            }
      $datess[$idch][]=$op;
      $donnees[$idch][]=$totaljour;
      $donnees2[$idch][]='';
      $donnees3[$idch][]='';
      $donnees4[$idch][]='';
      //if($op >= $curday){$donnees3[$idch][$q]=$totaln;}
      $q++;
    }
      $donnees2[$idch][0]=0;
      $donnees2[$idch][$q]=$total;
      $donnees3[$idch][0]=0;
      $donnees3[$idch][$q]=$totaln;
      $donnees4[$idch][0]=0;
      $donnees4[$idch][$q]=$totaln1;


/*
      $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView(
        'Generique/Stats/Activite/etatgraph_pdf.html.twig',
        array(
        'titre' => $this->titre,
        'chantiers' => $chantiers,
        'options' => $options,
        'repo' => $repo,
        'dates' => $datess,
        'donnees' => $donnees,
        'donnees2' => $donnees2,
        'donnees3' => $donnees3,
       )
    );

return new Response($html);

        $filename = 'export_sbeae_PDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
*/
        $logo = $chantier->getSociete()->getLogo();
        $datenow=new \DateTime('now');
        $fileName = 'Export-SBEAE_'.$datenow->format('d-m-Y').'.pdf';
        $temp_file = $this->getParameter('upload').'uploads/chantier'.$idch.'.pdf';
        $html = $this->twig->render(
            'Generique/Stats/Activite/etatgraph_pdf.html.twig',
            array(
            'titre' => $this->titre,
            'chantiers' => $chantiers,
            'options' => $options,
            'repo' => $repo,
            'dates' => $datess,
            'dateetat' => $curday,
            'donnees' => $donnees,
            'donnees2' => $donnees2,
            'donnees3' => $donnees3,
            'donnees4' => $donnees4,
            'logo' => $logo,
           )
        );

        $this->pdf->generateFromHtml($html, $temp_file, array('encoding' => 'utf-8'));
        //return new Response($html);


    //on recupere les mails des clients et on envoie le fichier
    /*
      $emails=explode(';',$chantier->getEmails());
          foreach ($emails as $email){
          echo 'email'.$email.'---';
              if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $datenow=new \DateTime('now');
              $sujet='Informations SBEAE '.$datenow->format('d-m-Y');
              $mess='Bonjour,<br>
              <br/>Merci de trouver ci-joint l\'état du chantier '.$chantier->getNom().'
              <br><br/>
              <br/>Bonne journée à vous<br/>L\'équipe de SBEAE';
              //$email='j.b@hebline.com';
              $this->sendConfirmationEmailMessage($email, $sujet, $mess, $temp_file, $fileName);
              }
          }
          //on efface fichier
      unlink($temp_file);
      */
    }

    }//fin foreach chantiers2

    return new Response('ok');

  }

  public function sendConfirmationEmailMessage($email, $subject, $mess, $data = null, $file= null, $data2 = null, $file2= null)
      {

        $message = (new \Swift_Message($subject));
        $message->setFrom('contact@releve-eae.com');

        /**/

        if($data != null){
          $data=file_get_contents($data);
          $attachment = new \Swift_Attachment($data, $file, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          $message->attach($attachment);
        }
        if($data2 != null){
          $data2=file_get_contents($data2);
          $attachment2 = new \Swift_Attachment($data2, $file2, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          $message->attach($attachment2);
        }
        $email="untel44@gmail.com";
        $message->setTo($email)->setBcc('f.m@hebline.com','j.b@hebline.com')->setBody($mess, 'text/html');

        try {
          $result = $this->mailer->send($message);
        }
        catch (\Swift_TransportException $e) {
          echo $e->getMessage();
        }

  }

  function calcul_options($chantier, $options){

       $em = $this->getDoctrine()->getManager();
       $idch = $chantier ->getId();
      $options[$idch]["totalreleveur"]=$options[$idch]["totalr"]=$options[$idch]["totaleffectp"]=$options[$idch]["tauxreleveglobal"]=$options[$idch]["pourcent_effect"]=$options[$idch]["avancement"]=$options[$idch]["cadence"]=$options[$idch]["cadence_ok"]=$options[$idch]["deltad"]=$options[$idch]["cadence_rl"] =0;
        $options[$idch]["totalreleveur"]=$chantier->getReleveur();
        $totaleffect=($em->getRepository('App:Enregistrement')->findEnregistrementsTotalSite($idch)[0]["total"]??0);
        $totalreleve=($em->getRepository('App:Enregistrement')->findEnregistrementsTotalSite($idch)[0]["totreleve"]??0);
        $totalinfructueux=($em->getRepository('App:Enregistrement')->findEnregistrementsTotalSite($idch)[0]["totinfructueux"]??0);
        $cadence=$cadence_ok=$delta='';
        $options[$idch]["totalr"]=$chantier->getTotalreleve();
        $options[$idch]["totaleffectp"]=$totaleffect;
        if(isset($options[$idch]["totalr"])&&($options[$idch]["totalr"] > 0)&&($totaleffect>0)){
        $options[$idch]["pourcent_effect"]=$options[$idch]["totaleffectp"]/$options[$idch]["totalr"]*100;
        $options[$idch]["tauxreleveglobal"]=round($totalreleve/($totaleffect)*10000)/100;
        }
        else{$options[$idch]["pourcent_effect"]=0;$options[$idch]["tauxreleveglobal"]=0;}
      $avancement=$nbdays=$curdays=0;$pourcent_days='';
      $Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
      $Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
      $Datecur=strtotime('today');
      if($Datecur>$Datefin) {$Datecur=$Datefin;}
      $nbdays=$this->fonctions->get_nb_open_days($Datedebut, $Datefin);
      $curdays=$this->fonctions->get_nb_open_days($Datedebut, $Datecur);

      if($Datedebut >= $Datecur){$curdays=1;}
      if($curdays > 0){
        if($options[$idch]["totalreleveur"] > 0){
      $options[$idch]["cadence_rl"] = round(($totaleffect/$curdays)/$options[$idch]["totalreleveur"]);}
      else{$options[$idch]["cadence_rl"] = 0;}
      $options[$idch]["cadence"] = round($totaleffect/$curdays);
      }
      $options[$idch]["cadence_ok"] = ($chantier->getTotalreleve()/$nbdays);
      $options[$idch]["totalrel"] = $options[$idch]["cadence_ok"]*($curdays-1);
      $delta=0;

      //calcul du nombre de jour homme du chantier
      $jours_homme = array();

      $jours_homme = $em->getRepository('App:Enregistrement')->calculJourHomme($chantier);
      $nbjour_homme = count($jours_homme);
      if($nbjour_homme > 0){$cadence_homme = round($totaleffect / $nbjour_homme);}
      else{$cadence_homme = 'indéfini';}

      $options[$idch]["jour_homme"] = $nbjour_homme;
      $options[$idch]["cadence_homme"] = $cadence_homme;
      $options[$idch]["cadence_homme_n1"] = ($chantier->getCadencehomme()??0);
      if($options[$idch]["totalrel"] >0){
            $delta=$totaleffect-round($options[$idch]["totalrel"]);
            if($Datecur==$Datefin){$delta=$totaleffect-$options[$idch]["totalr"];}
            $options[$idch]["deltad"]=round($delta);
            if($delta<0){$options[$idch]["avancement"] = 'class="bg-danger"';}
            elseif ($delta==0) {$options[$idch]["avancement"] = '';}
            else{$options[$idch]["avancement"]= 'class="bg-success"';}
      }
      //echo 'tot'.$idch.':'.$options[$idch]["totalr"].'-';
      return $options;
  }

}
