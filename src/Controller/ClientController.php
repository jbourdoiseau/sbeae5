<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

use App\Entity\Client;
use App\Repository\ClientRepository;


class ClientController extends AbstractController
{

	public function __construct()
    {
        $this->titre = 'Clients';
    }

	 public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
		if ($page < 1) {
			  $page=1;
		}

		$enregs = $em->getRepository('App:Client')->findAll();
        return $this->render('Admin/Client/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre
        ));
	}


	public function detail(Client $client)
    {
        return $this->render('Admin/Client/show.html.twig', array(
            'zone' => $zone,
        ));
    }


	 public function edit(Request $request, Client $client)
    {

        $editForm =  $this->createForm('App\Form\AdminClientType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$this->addFlash('success',
            'Vos modifications ont été enregistrées'
			);
			$this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_clients');
        }

        return $this->render('Admin/Client/edit.html.twig', array(
            'enreg' => $client,
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre
        ));
    }

	public function add(Request $request)
    {

        $editForm =  $this->createForm('App\Form\AdminClientType');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

			$enreg=$editForm->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($enreg);
            $em->flush();
			$this->addFlash('success',
            'Nouvel enregistrement'
			);
            return $this->redirectToRoute('admin_clients');
        }

        return $this->render('Admin/Client/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre
        ));
    }

}
