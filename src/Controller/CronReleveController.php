<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\Alerte;

class CronReleveController extends AbstractController
{

	public function Analyse()
    {
		$em = $this->getDoctrine()->getManager();
		//Rechercher tous les utilisateurs n'ayant pas declaré le jour même
		//Rechercher tous les utilisateurs qui auraient du declarer
		$date1 = new \DateTime('now');
		//echo $date1->format('Y-m-d');
		//$date1='2019/11/14';
		$users=$em->getRepository('App:ChantierUser')->getUsersValideChantierDate($date1);
		//$users[]=$em->getRepository('App:ChantierUser')->find(1);
		//echo count($users).'<br/>';
		$q=0;
		$k=0;
		//if(!$this->is_ferie($date1)){echo 'Férié/Smedi/Dimanche';exit;}

		//on recoupe avec les arrets CPAM du jour
//recherche des CPS AM
    $date2=new \DateTime('now +1 day');
    $indications=array();
		$notes=$em->getRepository('App:Evenement')->findEvenementsNotes($date1, $date2);
		foreach($notes as $note){
			$user_id=$note->getUser()->getId();
			$datedebut=$note->getDatedebut();
			$datefin=$note->getDatefin();
			$com=$note->getCommentaire();
			$type=$note->getType();
			if(($type<3)||($type>6)){
				for($date=clone $datedebut; $date<$datefin; $date->modify('+1 day')){
							$indications[$user_id]=1;
							//echo 'bd'.$type.' '.$date->format('Y-m-d').' '.$user_id."\n";
				}
			}
		}

		foreach($users as $utilisateur){
			$count=$em->getRepository('App:Enregistrement')->findEnregistrementsDateUser($date1, $utilisateur->getUser());
			//echo 'total'.$utilisateur->getUser()->getId().$utilisateur->getUser()->getNom().'-'.$count['total'].'<br>';
			//echo $count['total'].'-';
			if($count['total'] == 0){
				//echo 'ko'.$utilisateur->getUser()->getId().'<br>';
				$message='';
				$user_id=$utilisateur->getUser()->getId();
				$nom= $utilisateur->getUser()->getNom();
				$prenom= $utilisateur->getUser()->getPrenom();
				$mobile= $utilisateur->getUser()->getMobile();
				$login= $utilisateur->getUser()->getUsername();
				$pass= $utilisateur->getUser()->getPlain();

				//enregistrement de l'alerte
				if(!isset($indications[$user_id])) {
				$alerte= new Alerte();
				$alerte->setUser($utilisateur->getUser());
				$em->persist($alerte);
      	$em->flush();
				$url=$this->getParameter('url');
				echo $user_id.$nom."<br>\n";
				$message="Bonjour, Merci de faire votre rapport quotidien pour que votre journée soit payée. JL DOUARAN\n";
				//$message="Bonjour, Désolé, c'est une erreur. Bon  week-end. Julien EAE\n";
				$message=$this->get('sbeae.sms')->smsSend($mobile, $message);
				$q++;
				}
			}
			/****/

		}

	//return $this->render('Generique/test.html.twig');

		echo 'mail:'.$q.'<br>';
	return new Response(2);
	}

		public function EnvoiActif()
    {
		$em = $this->getDoctrine()->getManager();
		//Rechercher tous les utilisateurs n'ayant pas declaré le jour même
		//Rechercher tous les utilisateurs qui auraient du declarer
		$date1 = new \DateTime('now');

		echo $date1->format('Y-m-d');
		//$date1='2019/11/14';
		$users=$em->getRepository('App:ChantierUser')->getUsersValideChantierDate($date1);
		//$users[]=$em->getRepository('App:User')->find(1);
		//echo count($users).'<br/>';
		$q=0;
		$k=0;
		if(!$this->is_ferie($date1)){echo 'Férié/Samedi/Dimanche';exit;}

		foreach($users as $utilisateur){

				//echo 'ko'.$utilisateur->getUser()->getId().'<br>';
				$message='';
				$nom= $utilisateur->getUser()->getNom();
				$prenom= $utilisateur->getUser()->getPrenom();
				$mobile= $utilisateur->getUser()->getMobile();
				$login= $utilisateur->getUser()->getUsername();
				$pass= $utilisateur->getUser()->getPlain();
				$url=$this->getParameter('url');
				$message="Bonjour Pour réaliser la facturation et votre paye impératif de faire votre rapport jour du 29/11 avant 19h JL DOUARAN\n";
				$tab=array(510,512,486,500,531,528,501,502,529,430,432,433,515,429,427,107,96,546,435,436,130,442,420,419,379,115,186,83,82);
				if(in_array($utilisateur->getId(), $tab)){
				//echo $utilisateur->getId().'---';
				$message=$this->get('sbeae.sms')->smsSend($mobile, $message);
				$q++;
				}

		}

		echo 'mail:'.$q.'<br>';
	return new Response(2);
	}

	private function is_ferie($date){
		$date=strtotime($date->format('Y-m-d'));
		$open=false;
		$arr_bank_holidays = array(); // Tableau des jours feriés
		$year = (int)date('Y', $date);
		// Liste des jours feriés
		$arr_bank_holidays[] = '1_1_'.$year; // Jour de l'an
		$arr_bank_holidays[] = '1_5_'.$year; // Fete du travail
		$arr_bank_holidays[] = '8_5_'.$year; // Victoire 1945
		$arr_bank_holidays[] = '14_7_'.$year; // Fete nationale
		$arr_bank_holidays[] = '15_8_'.$year; // Assomption
		$arr_bank_holidays[] = '1_11_'.$year; // Toussaint
		$arr_bank_holidays[] = '11_11_'.$year; // Armistice 1918
		$arr_bank_holidays[] = '25_12_'.$year; // Noel

		// Récupération de paques. Permet ensuite d'obtenir le jour de l'ascension et celui de la pentecote
		$easter = easter_date($year);
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + 86400); // Paques
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*39)); // Ascension
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*50)); // Pentecote
		if (!in_array(date('w', $date), array(0, 6))
		&& !in_array(date('j_n_'.date('Y', $date), $date), $arr_bank_holidays)) {
			$open=true;
		}
		return $open;

	}
}
