<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use App\Service\FonctionsService;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\Pointage;

use App\Form\PointageGeneriqueType;


class PointageController extends AbstractController
{
	public function __construct(FonctionsService $fonctions)
    {
        $this->fonctions = $fonctions;
        $this->titre = 'Pointage';
        setlocale(LC_ALL, 'fr_FR');
    }

   public function indexGen(Request $request)
    {
        setlocale(LC_ALL, 'fr_FR');
    $em = $this->getDoctrine()->getManager();
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone_id);

    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    if($flag_hebline == true){$zone=null;}
    $page=1;
    $nbPerPage = $this->getParameter('nbPerPage');
    //-------------FORMULAIRE---------------------
    $enregistrement = new Enregistrement();
    $form = $this->get('form.factory')->create(PointageGeneriqueType::class, $enregistrement, array('ref' => null,'flag_hebline' => $flag_hebline));
    $form->handleRequest($request);
    $page=$form->get('page')->getData();
    $date1=$form->get('date1')->getData();
    $date2=$form->get('date2')->getData();
    $ref=$form->get('ref')->getData();
    $excel=$form->get('excel')->getData();
    $enregistrementCriteres=$form->getData();
    if($date1==""){
      $date1 = new \DateTime('now -7 day');
    }
    if($date2==""){
      $date2 = new \DateTime('now');
    }

    if(null != $form->get('chantier')->getData()){
        $chantiers[] = $form->get('chantier')->getData();
    }
    else{
    $chantiers = $em->getRepository('App:Chantier')->findChantiersEnCoursUser($zone,$date1,$date2,$ref);
    }
    $pointages = $em->getRepository('App:Pointage')->findPointages($enregistrementCriteres, $zone, $date1, $date2, $ref);
    $pointeurs=array();
    foreach ($pointages as $pointage) {
        //echo $pointage->getId().'-';
        $datecrea = $pointage->getDateCrea();
        $user = $pointage->getUser()->getId();
        $pointeurs[$user][$datecrea->format('Y-m-d')]=$pointage;
    }
    //recup des evenements
    $commentaire= $indications = $nbjours = array();
        $notes=$em->getRepository('App:Evenement')->findEvenementsNotes($date1, $date2);
        //echo '<br>date'.$date2->format('Y-m-d').'<br>';
        foreach($notes as $note){
            //echo 'ID'.$note->getId().'-'.$note->getCommentaire().'<br>';
            $user_id=$note->getUser()->getId();
            $datedebut=$note->getDatedebut();
            $datefin=$note->getDatefin();
            if($datefin == null){$datefin = $datedebut;}
            //else{$datefin->modify('-1 day');}
            $com=$note->getCommentaire();
            $type=$note->getType();
            if(($type<3)||($type>6)){
                $action="";
                for($date=clone $datedebut; $date<$datefin; $date->modify('+1 day')){
            if($type==1){
                $action = 'AM';
            }
            if($type==2){
                $action = 'CP';
            }
            if($type==7){
                $action = 'AT';
            }
            if($type==8){
                $action = 'Abs';
            }
            if($type==9){
                $action = $note->getCommentaire();
            }
            $indications[$user_id][$date->format('Y-m-d')]=$action;
            //echo $action;                //echo 'bd'.$type.' '.$date->format('Y-m-d').' '.$user_id."<br>\n";
                }
            }
        }

    if($excel == 1){return $this->calcul_export($date1, $date2, $pointeurs, $chantiers, $indications);}

    return $this->render('Generique/Pointage/index.html.twig', array(
            'flag_hebline' => $flag_hebline,
            'repo' => $repo,
            'pointeurs' => $pointeurs,
            'chantiers' => $chantiers,
            'indications' => $indications,
            'date1' => $date1,
            'date2' => $date2,
            'titre' => $this->titre,
            'form'      => $form->createView(),
        ));
    }

public function calcul_export($date1, $date2, $pointeurs, $chantiers, $indications){
        //STYLES
        $styleArray = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '337ab7'),
                'size'  => 10,
                'name'  => 'Verdana'
            ));
        $styleArray2 = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'ffffff'),
                'size'  => 10,
                'name'  => 'Verdana'
            ));

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $date3=date('d-m-Y').'-'.time();
        $sheet = $spreadsheet->getActiveSheet();
        //$sheet->setTitle('Pointage_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y'));
        $startDate = clone $date1;
        $stopDate = clone $date2;

        $q=1;
        $k=3;
        $sheet->setCellValue('A'.$q, 'TABLEAU DE POINTAGE');
        $sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);
        $q++;
        $sheet->setCellValue('A'.$q, 'Chantier');
        $sheet->setCellValue('B'.$q, 'Agent');
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);

        $week_name = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
        for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
            $sheet->setCellValueByColumnAndRow( $k, $q,$week_name[date('w', strtotime($date->format('d-m-Y')))].' '.$date->format('d-m-Y'));
            $letter = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($k);
            $sheet->getColumnDimension($letter)->setWidth(20);
            $k++;
        }

        //background-color:#fe7066
        $k=3;
        $q++;
                $chantier0=0;
                foreach ($chantiers as $chantier){
                    foreach ($chantier->getChantierusers() as $chantieruser){

                            if ($chantier->getId() != $chantier0){

                                $sheet->setCellValue('A'.$q, $chantier->getNom());$chantier0=$chantier->getId();
                                        $sheet->getStyleByColumnAndRow(1, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
                                        $sheet->getCellByColumnAndRow(1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                            }
                            $sheet->setCellValue('B'.$q, $chantieruser->getUser()->getNom().' '.$chantieruser->getUser()->getPrenom());

                            for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
                                if (($chantier->getDatedebut()->format('Ymd') <= $date->format('Ymd')) && ($chantier->getDatefin()->format('Ymd')  >= $date->format('Ymd'))) {
                                    if(isset($pointeurs[$chantieruser->getUser()->getId()][$date->format('Y-m-d')])){
                                        $pointage = $pointeurs[$chantieruser->getUser()->getId()][$date->format('Y-m-d')];
                                        $sheet->setCellValueByColumnAndRow( $k, $q,($pointage->getDatecrea()->format('d/m/Y h:i')??'').' '.($indications[$chantieruser->getUser()->getId()][$date->format('Y-m-d')]??''));
                                    }
                                    elseif(isset($indications[$chantieruser->getUser()->getId()][$date->format('Y-m-d')]) && (date('w', strtotime($date->format('d-m-Y'))) != 0)){
                                        $sheet->setCellValueByColumnAndRow( $k, $q,($indications[$chantieruser->getUser()->getId()][$date->format('Y-m-d')]??''));
                                    }
                                    else{
                                        if(date('w', strtotime($date->format('d-m-Y'))) != 0){
                                            $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                                            $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FE7066');
                                            $sheet->setCellValueByColumnAndRow( $k, $q,'');
                                        }
                                        else{
                                             $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                                            $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
                                        }
                                    }
                                }
                                else{
                                         $sheet->setCellValueByColumnAndRow( $k, $q,($indications[$chantieruser->getUser()->getId()][$date->format('Y-m-d')]??''));
                                }
                                $k++;
                            }

                            $k=3;
                            $q++;
                    }
                }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = 'Pointage_'.$date3.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
        }

}
