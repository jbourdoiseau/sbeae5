<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\Alerte;

class CadenceController extends AbstractController
{

	public function Analyse()
    {
		$em = $this->getDoctrine()->getManager();
		//Rechercher tous les utilisateurs n'ayant pas declaré le jour même
		$chantiers=$em->getRepository('App:Chantier')->findAll();
$temp='';
		foreach($chantiers as $chantier){

			$count2=$em->getRepository('App:Enregistrement')->findTotalEnreg($chantier);
			$count=$em->getRepository('App:Enregistrement')->findDistinctDay($chantier);
			$cadence=0;
			if($count['totalday'] > 0){
			$cadence = round($count2['totalrel']/$count['totalday']);}
			$cadenceN = round($chantier->getTauxreleve());
			$delta=0;
			if($cadence >0 & $cadenceN>0){$delta=$cadence-$cadenceN;}

      //echo $chantier->getId().';'.$chantier->getNom().';'.$chantier->getZone()->getAbrev().';'.$cadenceN.';'.$cadence.';'.$delta.';'.$count2['totalrel'].';'.$count['totalday']."\n";
      $temp.=$chantier->getId().';'.$cadence."\n";

		}

	file_put_contents ('/var/www/vhosts/releve-eae2019.com/cadence.csv', $temp);

	return new Response(2);
	}

}
