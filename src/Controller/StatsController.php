<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Service\FonctionsService;

use App\Entity\Enregistrement;
use App\Entity\Chantier;

use App\Form\AdminStatsClotureType;
use App\Form\AdminStatsOperateursType;
use App\Form\SuperAdminStatsClotureType;
use App\Form\SuperAdminStatsOperateursType;
use App\Form\ActiviteJourType;

// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class StatsController extends AbstractController
{

	public function __construct(FonctionsService $fonctions, \Swift_Mailer $mailer, ValidatorInterface $validator)
    {
        $this->titre = 'Statistiques';
        $this->fonctions = $fonctions;
        $this->mailer = $mailer;
        $this->validator = $validator;
    }
	 public function indexCloture(Request $request)
    {
		$this->titre = 'Clotûres';
	  	$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(AdminStatsClotureType::class, $enregistrement, array('zone' => $zone));


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
				if($date1 == ''){$date1 = new \DateTime('first day of this month');}
		if($date2 == ''){$date2 = new \DateTime('now');}
		$cloture=$form->get('cloture')->getData();
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();

		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsClotures($enregistrementCriteres, $zone, $date1, $date2,1);

		if($excel == 1){return $this->exportClotures ($enregs, $date1, $date2);}

        return $this->render('Admin/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
        ));
    }

	 public function indexOperateurs(Request $request)
    {
		$this->titre = 'Suivi Agents';
	  	$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(AdminStatsOperateursType::class, $enregistrement, array('zone' => $zone));


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}
		//echo $date1->format('Y-m-d h:m:s');
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,1);
		$totaux = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateursTotal($enregistrementCriteres, $zone, $date1, $date2,1);
		$totday=array();
		foreach($totaux as $total){
			$date=$total['datereleve'];
			$tot=$total['total'];
			$totday[$date->format('Y-m-d')]=$tot;
			//echo $date->getTimestamp().'-'.$tot.'_';
		}

		$donnees=$users=$chantiers=Array();
		foreach ($enregs as $enreg){
			$date_rel=$enreg->getDatereleve()->format('Y-m-d');
			$user=$enreg->getUser();
			$user_id=$enreg->getUser()->getId();
			$chantier_id=$enreg->getChantier()->getId();
			$donnees[$user_id][$date_rel][$chantier_id][]=$enreg;
			$users[$user_id]=$user;
			if($enreg->getChantier()->getNom() !=''){
			$chantiers[$user_id][$enreg->getChantier()->getId()]=$enreg->getChantier();
			}
		}

        return $this->render('Admin/Stats/Operateur/index.html.twig', array(
      'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
			'date1' => $date1,
			'date2' => $date2,
			'totday' => $totday,
			'donnees'  => $donnees,
			'users' => $users,
			'chantiers' => $chantiers,

        ));
    }

	 public function rapportAdmin(Request $request)
    {
		$this->titre = 'Rapport';
	  $em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=$totaleffect=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(AdminStatsOperateursType::class, $enregistrement, array('zone' => $zone));


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}


		$zone = $this->getUser()->getZone()->getId();
		$feuille='export';
		if($excel == 1){$action = 'excel';}else {$action='frontadmin';}
		return $this->calcul_export_2($feuille, null, $action, $enregistrementCriteres, $zone, $date1, $date2, $form, $this->titre);
    }

	 public function exportClient()
    {

			$this->titre = 'Export client';
			$em = $this->getDoctrine()->getManager();

				//-------------FORMULAIRE---------------------
    $date2=new \DateTime('last Sunday');
    $date1 = clone $date2;
    $date1 = $date1->sub(new \DateInterval('P6D'));

				$feuille='export-client';
				$action = 'rapport';


    		$chantiers2 = $em->getRepository('App:Chantier')->findChantiersEnCoursClientEnCours($date1,$date2,1);
    		//$chantiers2 = array();
//$chantiers2[] = $em->getRepository('App:Chantier')->find(18);//189 190 192

    	foreach ($chantiers2 as $chantier1){
    		$emails=array();
      if(($chantier1->getEmails() != '')&&($chantier1->getTypechantier()->getId() == 1)){//que chantier releve
                  $enregistrementCriteres = new Enregistrement();
                  $enregistrementCriteres->setChantier($chantier1);
						    	$idch = $chantier1->getId();
									$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
							    $spreadsheet->getProperties()->setCreator("SBEAE")->setLastModifiedBy("SBEAE")->setCategory("");
									$spreadsheet= $this->calcul_export_2($feuille, $spreadsheet, $action, $enregistrementCriteres, null, $date1, $date2, null, $this->titre);

						    	$writer = new Xlsx($spreadsheet);
						      $fileName = 'Rapport_hebdo_'.date('d-m-Y').'.xlsx';
						      $temp_file = tempnam(sys_get_temp_dir(), $fileName);
						      $writer->save($temp_file);

						      $datenow=new \DateTime('now');

						      $fileName2 = 'Export-SBEAE_'.$idch.'_'.$datenow->format('d-m-Y').'.pdf';
									$temp_file2 = $this->getParameter('upload').'uploads/chantier'.$idch.'.pdf';



$sujet='SBEAE '.$chantier1->getClient()->getNom().'/'.$chantier1->getNom().' '.$datenow->format('d-m-Y');
$mess='Bonjour,<br/><br/>
Nous vous prions de trouver ci joint le rapport et le graphique d\'activité pour la période du '.$date1->format('d-m-Y').' au '.$date2->format('d-m-Y').'<br>
Pour plus d\'informations, nous vous invitons à vous connecter à votre interface sur
<a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a><br><br>Cordialement,<br><br>
L\'équipe EAE<br><img src=\''.$chantier1->getSociete()->getLogo().'\' width="150">';

$emailConstraint = new Assert\Email();
$emailConstraint->message = 'Invalid email address';

									$emails2=explode(';',$chantier1->getEmails());
									foreach($emails2 as $ml){
										    $errors = $this->validator->validate(
										        $ml,
										        $emailConstraint
										    );
										    if (0 === count($errors)) {
										    	$emails[]=$ml;
											  } else {
											        //echo $errors[0]->getMessage();
											  }

									}
									$emails[]='untel44@gmail.com';$emails[]='f.m@hebline.com';$emails[]='jean-luc.douaran@sbeae.com';
										$this->sendConfirmationEmailMessage($emails, $sujet, $mess, $temp_file, $fileName, $temp_file2, $fileName2);
									unlink($temp_file2);
					}
			}
			return new Response('ExportClient');
    }

	public function detail(Enregistrement $enregistrement)
    {
		$tournees = $enregistrement->getTournees();
        return $this->render('Admin/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }

	public function indexSACloture(Request $request)
    {
		$this->titre = 'Clotûres';
	  	$em = $this->getDoctrine()->getManager();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsClotureType::class, $enregistrement, array());


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$cloture=$form->get('cloture')->getData();
		$zone=$form->get('zone')->getData();
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();

		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsClotures($enregistrementCriteres, $zone, $date1, $date2,1);

		if($excel == 1){return $this->exportClotures ($enregs, $date1, $date2);}

        return $this->render('SuperAdmin/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
        ));
    }

	public function indexSAOperateurs(Request $request)
    {
		$this->titre = 'Suivi Agents';
	  	$em = $this->getDoctrine()->getManager();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsOperateursType::class, $enregistrement, array());


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$zone=$form->get('zone')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}
		//echo $date1->format('Y-m-d h:m:s');
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,1);
		$totaux = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateursTotal($enregistrementCriteres, $zone, $date1, $date2,1);
		$totday=array();
		foreach($totaux as $total){
			$date=$total['datereleve'];
			$tot=$total['total'];
			$totday[$date->format('Y-m-d')]=$tot;
			//echo $date->getTimestamp().'-'.$tot.'_';
		}

		$donnees=$users=$chantiers=Array();
		foreach ($enregs as $enreg){
			$date_rel=$enreg->getDatereleve()->format('Y-m-d');
			$user=$enreg->getUser();
			$user_id=$enreg->getUser()->getId();
			$chantier_id=$enreg->getChantier()->getId();
			$donnees[$user_id][$date_rel][$chantier_id][]=$enreg;
			$users[$user_id]=$user;
			if($enreg->getChantier()->getNom() !=''){
			$chantiers[$user_id][$enreg->getChantier()->getId()]=$enreg->getChantier();
			}
		}

        return $this->render('SuperAdmin/Stats/Operateur/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
			'date1' => $date1,
			'date2' => $date2,
			'totday' => $totday,
			'donnees'  => $donnees,
			'users' => $users,
			'chantiers' => $chantiers,

        ));
    }

	public function detailSA(Enregistrement $enregistrement)
    {
		$tournees = $enregistrement->getTournees();
        return $this->render('SuperAdmin/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }
	public function indexHeblineCloture(Request $request)
    {
		$this->titre = 'Clotûres';
	  	$em = $this->getDoctrine()->getManager();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsClotureType::class, $enregistrement, array());

		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		if($date1 == ''){$date1 = new \DateTime('first day of this month');}
		if($date2 == ''){$date2 = new \DateTime('now');}
		$cloture=$form->get('cloture')->getData();
		$zone=$form->get('zone')->getData();
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();

		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsClotures($enregistrementCriteres, $zone, $date1, $date2,1);

		if($excel == 1){return $this->exportClotures($enregs, $date1, $date2);}

        return $this->render('Hebline/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
        ));
    }

	 public function indexHeblineOperateurs(Request $request)
    {
		$this->titre = 'Suivi Agents';
	  	$em = $this->getDoctrine()->getManager();

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsOperateursType::class, $enregistrement, array());


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$zone=$form->get('zone')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}
		//echo $date1->format('Y-m-d h:m:s');
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,1);
		$totaux = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateursTotal($enregistrementCriteres, $zone, $date1, $date2,1);
		$totday=array();
		foreach($totaux as $total){
			$date=$total['datereleve'];
			$tot=$total['total'];
			$totday[$date->format('Y-m-d')]=$tot;
			//echo $date->getTimestamp().'-'.$tot.'_';
		}

		$donnees=$users=$chantiers=Array();
		foreach ($enregs as $enreg){
			$date_rel=$enreg->getDatereleve()->format('Y-m-d');
			$user=$enreg->getUser();
			$user_id=$enreg->getUser()->getId();
			$chantier_id=$enreg->getChantier()->getId();
			$donnees[$user_id][$date_rel][$chantier_id][]=$enreg;
			$users[$user_id]=$user;
			if($enreg->getChantier()->getNom() !=''){
			$chantiers[$user_id][$enreg->getChantier()->getId()]=$enreg->getChantier();
			}
		}

        return $this->render('Hebline/Stats/Operateur/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'form' 			=> $form->createView(),
			'date1' => $date1,
			'date2' => $date2,
			'totday' => $totday,
			'donnees'  => $donnees,
			'users' => $users,
			'chantiers' => $chantiers,

        ));
    }

	public function rapportHeblineOperateurs(Request $request)
    {
		$this->titre = 'Rapport';

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$zone = null;
		$enregs=$totaleffect=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsOperateursType::class, $enregistrement, array());
		$em = $this->getDoctrine()->getManager();

		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$zone_id=$form->get('zone')->getData();
		if($zone_id!=null){$zone=$em->getRepository('App:Zone')->find($zone_id);}
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();
		//var_dump($enregistrementCriteres);
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}


		if($excel == 1){$action = 'excel';}else {$action='frontadmin';}
		$feuille='export';
		return $this->calcul_export_2( $feuille,null, $action, $enregistrementCriteres, $zone, $date1, $date2, $form, $this->titre);

	}

public function controleHeblineCloture(Request $request)
    {
		$this->titre = 'Contrôle';

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$zone = null;
		$enregs=$totaleffect=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(SuperAdminStatsOperateursType::class, $enregistrement, array());
		$em = $this->getDoctrine()->getManager();

		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$zone_id=$form->get('zone')->getData();
		if($zone_id!=null){$zone=$em->getRepository('App:Zone')->find($zone_id);}
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();
		//var_dump($enregistrementCriteres);
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}
		if($excel == 1){$action = 'controle';}else {$action='frontadmin';}


		$feuille='export';
		return $this->calcul_export_2( $feuille,null, $action, $enregistrementCriteres, $zone, $date1, $date2, $form, $this->titre, null, null);

	}

	public function detailHebline(Enregistrement $enregistrement)
    {
		$tournees = $enregistrement->getTournees();
        return $this->render('Hebline/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }

	public function capture(Enregistrement $enregistrement)
    {
        return $this->render('Admin/Enregistrement/capture.html.twig', array(
            'enreg' => $enregistrement
		));
    }

	public function edit(Request $request, Enregistrement $enregistrement)
    {

		$em = $this->getDoctrine()->getManager();
		$OriginalPools = new ArrayCollection();
		foreach ($enregistrement->getTournees() as $pool) {
		$OriginalPools -> add($pool);
		}
		$nomformulaire = $enregistrement->getTypeformulaire()->getNom();

        $editForm = $this->createForm('App\Form\EnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

				foreach ($OriginalPools as $pool) {
					if (false === $enregistrement->getTournees()->contains($pool)) {
						$em->remove($pool);
					}
				}
			$em->persist($enregistrement);
			$em->flush();
        }
        $editForm = $this->createForm('App\Form\EnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);
        return $this->render('Admin/Enregistrement/edit.html.twig', array(
            'enreg' => $enregistrement,
			'form' 			=> $editForm->createView(),
        ));
    }

	public function add (Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();
		$enregistrement= new Enregistrement();
		$nomformulaire = 'admin';
      $editForm = $this->createForm('App\Form\AddEnregistrementTourneeType', $enregistrement, array('zone' => $zone));
      $editForm->handleRequest($request);

      if ($editForm->isSubmitted() && $editForm->isValid()) {
			//check cohérence utilisateur chantier
			//echo $enregistrement->getChantier()->getId();
			$chantier = $enregistrement->getChantier();
			$utilisateur = $enregistrement->getUser();
			$chantiers = $em->getRepository('App:ChantierUser')->findBy(array('chantier' => $chantier, 'user' => $utilisateur));
			if(isset ($chantiers) && count($chantiers)>0){
			$enregistrement->setType('admin');
			$enregistrement->setTypeFormulaire($em->getRepository('App:Formulaire')->find(1));
			$enregistrement->setZone($zone);
			$em->persist($enregistrement);
			$em->flush();
					$type='success';
					$message='Enregistrement créé';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
					return $this->redirectToRoute('admin_enregistrements_edit', array('id' => $enregistrement->getId()));
			}
			else{
					$type='error';
					$message='Opération impossible : cet utilisateur n\'est pas utilisé sur ce chantier';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
			}
        }

        return $this->render('Admin/Enregistrement/add.html.twig', array(
			'form' 			=> $editForm->createView(),
        ));
    }

	public function controle (Request $request, Enregistrement $enregistrement, $controle)
    {
		$em = $this->getDoctrine()->getManager();
        $enregistrement->setControle($controle);
		$em->flush();
		return new Response(1);
	}

	public function activite(Request $request, $date = null, $site = null){

		$this->titre = 'Activité journalière';
		$em = $this->getDoctrine()->getManager();
		if($date==""){
			$date1 = new \DateTime('now');
		}
		else{$date1=date('Y-m-d', strtotime($date));}

		$chantierusers = $enregs=array();

		//recherche
		//2. enregs  date
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsDate($date1, $site);
		$chantierusers = $em->getRepository('App:ChantierUser')->getUsersChantierDate($date1, $site);
		//echo 'count'.count($enregs);
		$cloture=$CLinfruct=$releve=$RLinfruct=array();
		$q=0;
		foreach ($chantierusers as $chantieruser){
			$user=$chantieruser->getUser()->getId();
			$chantier=$chantieruser->getChantier()->getId();
			$cloture[$chantier][$user]=$CLinfruct[$chantier][$user]=$RLinfruct[$chantier][$user]=$releve[$chantier][$user]=0;
			//echo $chantier.'-'.$user."<br>";
		}
		//echo "-------------<br>";
		foreach ($enregs as $enreg){
			$user=$enreg->getUser()->getId();
			$chantier=$enreg->getChantier()->getId();
			//echo $chantier.'-'.$user.':'.$enreg->getId().':'.$enreg->getReleve().':'.$enreg->getCloture()."<br>";
			if($enreg->getCloture() == 1){
				$cloture[$chantier][$user]+=$enreg->getCloturereleve();
				if($enreg->getInfructueux() > 0){$CLinfruct[$chantier][$user]+=$enreg->getClotureinfructueux();}
			}
			//if($enreg->getCloture() == 0){
				$releve[$chantier][$user]+=$enreg->getReleve();
				if($enreg->getInfructueux() > 0){$RLinfruct[$chantier][$user]+=$enreg->getInfructueux();}
			//}
			$q++;
		}
		//echo "-------------------------------------<br>";
		$site0='';
		$arrayCollection=array();
		$Collection=array();
		$donnees1=$donnees2=$donnees3=$donnees4='';
		$chantierusers = $em->getRepository('App:ChantierUser')->getUsersChantierDate($date1, $site);
			foreach ($chantierusers as $chantieruser){
			$user=$chantieruser->getUser();
			$user_id=$user->getId();

			$chantier=$chantieruser->getChantier()->getId();
			//echo $chantier.'-'.$user_id."<br>";
			$site=$chantieruser->getChantier()->getNom();
			$nom=$user->getNom().' '.$user->getPrenom();
			if(isset($cloture[$chantier][$user_id])){
				$donnees1.='{"x": "'.$nom.'", "value": "'.$CLinfruct[$chantier][$user_id].'", "title":"infructueux"},';
				$donnees2.='{"x": "'.$nom.'", "value": "'.$cloture[$chantier][$user_id].'", "title":"Clôture"},';
				//echo $donnees2."<br>";
				}
			if(isset($releve[$chantier][$user_id])){
				$donnees3.='{"x": "'.$nom.'", "value": "'.$RLinfruct[$chantier][$user_id].'", "title":"infructueux"},';
				$donnees4.='{"x": "'.$nom.'", "value": "'.$releve[$chantier][$user_id].'", "title":"Relève"},';
				//echo $donnees4."<br>";
				}
			}
			$donnees1 = substr($donnees1, 0, -1);
			$donnees2 = substr($donnees2, 0, -1);
			$donnees3 = substr($donnees3, 0, -1);
			$donnees4 = substr($donnees4, 0, -1);

		//echo $data_string;
		$data_string = '{
			"chart": {
			  "type": "bar",
			  "title": "'.$site.'",
			  "labels":{"enabled":true},
			  "series": [{
					"seriesType": "bar",
					"data": ['.$donnees2.']
						  },
					{
					 "seriesType": "bar",
					"data": ['.$donnees1.']
					},
					{
					"seriesType": "bar",
					"data": ['.$donnees4.']},
					{
					"seriesType": "bar",
					"data": ['.$donnees3.']}

					]

			}
		  }';

		$data_string = json_encode($data_string);
		return new Response($data_string);

		return $this->render('Admin/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => 'test',
        ));
	}



		//return new Response(1);
		public function activitehtml(Request $request){
			$this->titre="Suivi Jour";
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone()->getId();

		$enregistrement = new Enregistrement();
		$form = $this->createForm('App\Form\DateActiviteType', $enregistrement);
		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();

		if($date1==""){
			$date1 = new \DateTime('now');
		}

		//echo $date1->format('Y-m-d');;
		$chantiers=$em->getRepository('App:Chantier')->rechercheDateChantier($date1, $zone);
		//echo 'count'.count($chantiers);
		if($zone<1000){$rep="Admin";}
		if($zone==1001){$rep="SuperAdmin";}
		if($zone==1000){$rep="Hebline";}

		return $this->render($rep.'/Stats/Activite/jour.html.twig', array(
			'titre' => $this->titre,
			'chantiers' => $chantiers,
			'date' => $date1,
			'form' 			=> $form->createView(),
        ));
	}

	public function etat(Request $request, $site = null){

		$this->titre = 'Board';
		$em = $this->getDoctrine()->getManager();
		//recherche
		$enregs = $em->getRepository('App:Enregistrement')->findByChantier($site);
		$chantier = $em->getRepository('App:Chantier')->find($site);
		$total=$chantier->getTotalreleve();
		$cloture=$CLinfruct=$releve=$RLinfruct=0;
		//echo "-------------<br>";
		foreach ($enregs as $enreg){
			if($enreg->getCloture() == 1){
				$cloture+=$enreg->getCloturereleve();
				if($enreg->getInfructueux() > 0){$CLinfruct+=$enreg->getClotureinfructueux();}
			}
			//if($enreg->getCloture() == 0){
				$releve+=$enreg->getReleve();
				if($enreg->getInfructueux() > 0){$RLinfruct+=$enreg->getInfructueux();}
			//}
		}
		$restant=$total-$cloture-$CLinfruct;
		if($restant < 0){$restant=0;}
		//echo "-------------------------------------<br>";


				$donnees1='{"x": "Clôture Infructueux", "value": "'.$CLinfruct.'", "title":"infructueux"}';
				$donnees2='{"x": "Clôture", "value": "'.$cloture.'", "title":"Clôture"}';
				$donnees3='{"x": "Relevé Infructueux", "value": "'.$RLinfruct.'", "title":"infructueux"}';
				$donnees4='{"x": "Relevé", "value": "'.$releve.'", "title":"Relève"}';
				$donnees5='{"x": "Restant", "value": "'.$restant.'", "title":"Restant"}';


		//echo $data_string;
		$data_string = '{
			"chart": {
			  "type": "bar",
			  "title": "'.$site.'",
			  "labels":{"enabled":true},
			  "series": [{
					"seriesType": "bar",
					"data": ['.$donnees2.']
						  },
					{
					 "seriesType": "bar",
					"data": ['.$donnees1.']
					},
					{
					"seriesType": "bar",
					"data": ['.$donnees4.']},
					{
					"seriesType": "bar",
					"data": ['.$donnees3.']},
					{
					"seriesType": "bar",
					"data": ['.$donnees5.']}

					]

			}
		  }';

		$data_string = json_encode($data_string);
		return new Response($data_string);

		return $this->render('Admin/Stats/Cloture/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => 'test',
        ));
	}



		//return new Response(1);
		public function etathtml(Request $request){
		$this->titre="Board";
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone()->getId();

		//echo $date1->format('Y-m-d');;
		$chantiers=$em->getRepository('App:Chantier')->rechercheChantiersActif($zone);
		//echo 'count'.count($chantiers);
		if($zone<1000){$rep="Admin";}
		if($zone==1001){$rep="SuperAdmin";}
		if($zone==1000){$rep="Hebline";}

		return $this->render($rep.'/Stats/Activite/etat.html.twig', array(
			'titre' => $this->titre,
			'chantiers' => $chantiers
        ));
	}

		public function etathtmlClient(Request $request){

		$this->titre="Board";
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();
		$chantiers = $zone -> getChantiers();

		//echo $date1->format('Y-m-d');;
		//$chantiers=$em->getRepository('App:Chantier')->rechercheChantiersActif($zone);

		return $this->render('Client/Stats/Activite/etat.html.twig', array(
			'titre' => $this->titre,
			'chantiers' => $chantiers
        ));
		}

		public function indexNonClotures (Request $request){
			$this->titre="Non Clôturés 10 jours (2 derniers mois)";
			$em = $this->getDoctrine()->getManager();
			$zone = $this->getUser()->getZone()->getId();
			$tournees=$em->getRepository('App:Enregistrement')->recherchenoncloture($zone);
			$enregs=$task=array();

			foreach($tournees as $tournee){
				$tt= $tournee['id'];
				$tournee2=$em->getRepository('App:Tournee')->find($tt);
				$enreg=$tournee2->getEnregistrement();
				$id=$enreg->getId();

				if($enreg->getCloture() == 0 && !isset($task[$id])){$enregs[]=$enreg;$task[$id]=1;}

			}
			//exit;
			if($zone<1000){$rep="Admin";}
			if($zone==1001){$rep="SuperAdmin";}
			if($zone==1000){$rep="Hebline";}

		return $this->render($rep.'/Stats/Activite/nonclotures.html.twig', array(
			'titre' => $this->titre,
			'enregs' => $enregs
        ));

		}

		public function exportClotures ($enregs, $date1, $date2){

		$date3=date('d-m-Y').'-'.time();
		 $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Export_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y'));
		$q=1;
		$sheet->setCellValue('A'.$q, 'Id Enreg');
		$sheet->setCellValue('B'.$q, 'Date');
		$sheet->setCellValue('C'.$q, 'Zone');
		$sheet->setCellValue('D'.$q, 'Soc');
		$sheet->setCellValue('E'.$q, 'Site');
		$sheet->setCellValue('F'.$q, 'Utilisateur');
		//$sheet->setCellValue('G'.$q, 'Relevé');
		//$sheet->setCellValue('H'.$q, 'Infructueux');
		//$sheet->setCellValue('I'.$q, 'Total');
		$sheet->setCellValue('G'.$q, 'Tournée');
		$sheet->setCellValue('H'.$q, 'Secteurs');
		$sheet->setCellValue('I'.$q, 'Relevé');
		$sheet->setCellValue('J'.$q, 'Infructueux');
		$sheet->setCellValue('K'.$q, 'Restant');
		$sheet->setCellValue('L'.$q, 'Total');
		$sheet->setCellValue('M'.$q, 'Commentaire');
		foreach ($enregs as $enreg){
			//echo $enreg->getId().'-';
			$tournees=$enreg->getTournees();
			$commentaire=$enreg->getCommentaire();
			$k=0;
			if(count($tournees) > 0){
			foreach ($tournees as $tournee){
					$q++;
					$k++;

				$sheet->setCellValue('A'.$q, $enreg->getId());
				$sheet->setCellValue('B'.$q, $enreg->getDatereleve()->format('d-m-Y'));
				$sheet->setCellValue('C'.$q, $enreg->getZone()->getNom());
				$sheet->setCellValue('D'.$q, $enreg->getChantier()->getSociete()->getNom());
				$sheet->setCellValue('E'.$q, $enreg->getChantier()->getNom());
				$sheet->setCellValue('F'.$q, $enreg->getUser()->getNom().' '.$enreg->getUser()->getPrenom());
				//$sheet->setCellValue('G'.$q, $enreg->getCloturereleve());
				//$sheet->setCellValue('H'.$q, $enreg->getClotureinfructueux());
				//$sheet->setCellValue('I'.$q, $enreg->getCloturetotal());
				$sheet->setCellValue('G'.$q, $enreg->getTitre());
				$sheet->setCellValue('H'.$q, $tournee->getCode());
				$sheet->setCellValue('I'.$q, $tournee->getReleve());
				$sheet->setCellValue('J'.$q, $tournee->getInfructueux());
				$sheet->setCellValue('K'.$q, $tournee->getRestant());
				$sheet->setCellValue('L'.$q, $tournee->getTotal());
				$sheet->setCellValue('M'.$q, $commentaire);
				if($k == 1){$commentaire='';}
			}
			}
			else{
				$q++;
				$sheet->setCellValue('A'.$q, $enreg->getId());
				$sheet->setCellValue('B'.$q, $enreg->getDatereleve()->format('d-m-Y'));
				$sheet->setCellValue('C'.$q, $enreg->getZone()->getNom());
				$sheet->setCellValue('D'.$q, $enreg->getChantier()->getSociete()->getNom());
				$sheet->setCellValue('E'.$q, $enreg->getChantier()->getNom());
				$sheet->setCellValue('F'.$q, $enreg->getUser()->getNom().' '.$enreg->getUser()->getPrenom());
				$sheet->setCellValue('G'.$q, $enreg->getCloturereleve());
				$sheet->setCellValue('H'.$q, $enreg->getClotureinfructueux());
				$sheet->setCellValue('I'.$q, $enreg->getCloturetotal());
				$sheet->setCellValue('J'.$q, $enreg->getTitre());
				$sheet->setCellValue('K'.$q, '');
				$sheet->setCellValue('L'.$q, '');
				$sheet->setCellValue('M'.$q, '');
				$sheet->setCellValue('N'.$q, '');
				$sheet->setCellValue('O'.$q, '');
				$sheet->setCellValue('P'.$q, $commentaire);
			}
		}
        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $date3.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
		}


		public function exportRapport ($defauts, $commentaire, $cumulRel,$cumulInf, $causersite, $casite, $cadaysite, $cadaytotal, $catotal, $enregs, $totday, $totdaySite, $donnees, $users, $date1,$date2,$chantiers,  $chantiersencours, $totaleffect,  $mail = null){

		$em = $this->getDoctrine()->getManager();

		//echo $date1->format('Y-m-d');;
		$date3='Sem'.$date1->format('W').'-'.time();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
		//$spreadsheet->getActiveSheet()->getCellByColumnAndRow(1,1)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		//$spreadsheet->getActiveSheet()->getCellByColumnAndRow(1,1)->getStyle()->getFill()->getStartColor()->setARGB('FFFF0000');

		$sheet = $spreadsheet->getActiveSheet();
		 //$sheet->getCellByColumnAndRow(1,1)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		 //$sheet->getCellByColumnAndRow(1,1)->getStyle()->getFill()->getStartColor()->setARGB('FFFF0000');
		$sheet->setTitle('Avancement Chantiers');

		// Create a new worksheet called "My Data"
		$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Rapport_Sem_'.$date1->format('W'));

		// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
		$spreadsheet->addSheet($myWorkSheet, 1);

		$startDate = $date1;
		$stopDate = $date2;
		$datenow=new \DateTime('now - 12 hours');
		//$sheet->setCellValueByColumnAndRow(1, 2, $date1->format('d-m-Y'));
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);

        $borderDateOn = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF3469F7'),
        ),
    ),
);
		//$phpExcel->getActiveSheet()->getCell('A1')->setValue('Some text');
		//$phpExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		//$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setColor( $phpColor );
		//$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('FF0000');
//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('B2'), 'B3:B7');
		$sheet = $spreadsheet->getSheet(0);
		$q=1;
		$k=1;
		$sheet->setCellValue('A'.$q, 'CHANTIERS EN COURS');
		$sheet->getStyleByColumnAndRow( 1, 1)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$sheet->setCellValue('A'.$q, 'Site');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('B'.$q, 'Client');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('C'.$q, 'Responsable');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('D'.$q, 'Date début');$k++;
		$sheet->setCellValue('E'.$q, 'Date fin');$k++;
		$sheet->setCellValue('F'.$q, 'Prévu');$k++;
		$sheet->setCellValue('G'.$q, 'Réalisé');$k++;
		$sheet->setCellValue('H'.$q, '% Réal.');$k++;
		$sheet->setCellValue('I'.$q, 'Jours');$k++;
		$sheet->setCellValue('J'.$q, 'Delta Compteur');$k++;
		$sheet->setCellValue('K'.$q, 'Releves / jour');$k++;
		$sheet->setCellValue('L'.$q, 'Nb agents');$k++;
		$sheet->setCellValue('M'.$q, 'Cadence A');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('N'.$q, 'Cadence A-1');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('O'.$q, 'Cadence OK');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
										$k++;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
		$sheet->getColumnDimension('M')->setAutoSize(true);
		$sheet->getColumnDimension('N')->setAutoSize(true);
		$sheet->getColumnDimension('O')->setAutoSize(true);
		$sheet->getColumnDimension('P')->setAutoSize(true);
		$q++;
		foreach ($chantiersencours as $chantier){
			$cadence=$cadence_ok=$delta='';
			$totalr=$chantier->getTotalreleve();
			$totaleffectp=$totaleffect[$chantier->getId()];
			if(isset($totalr)&&($totalr > 0)){
			$pourcent_effect=(round(($totaleffectp/$totalr)*10000)/100);
			}
			else{$pourcent_effect='';}
		$avancement=$nbdays=$curdays=0;$pourcent_days='';
		$Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
		$Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
		$Datecur=strtotime('today');
		$nbdays=$this->fonctions->get_nb_open_days($Datedebut, $Datefin);
		$curdays=$this->fonctions->get_nb_open_days($Datedebut, $Datecur);
		$cadence = round(($totaleffect[$chantier->getId()]/$curdays));
		$cadence_ok= round(($chantier->getTotalreleve()/$nbdays));
		if($pourcent_effect != ''){
		$pourcent_days=(round(($curdays/$nbdays)*10000)/100);
		if($pourcent_days > $pourcent_effect){$avancement = 2;}else{$avancement=1;}
		}
		$TR=$chantier->getCadencehomme();
		if($TR==0){$TR='';}
		if($chantier->getTotalreleve() >0){$delta=$totaleffect[$chantier->getId()]-($curdays*$cadence_ok);}
		$k=1;
		$sheet->setCellValue('A'.$q, $chantier->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('B'.$q, $chantier->getClient()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('C'.$q, $chantier->getZone()->getResponsable());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('D'.$q, $chantier->getDatedebut()->format('d-m-Y'));$k++;
		$sheet->setCellValue('E'.$q, $chantier->getDatefin()->format('d-m-Y'));$k++;
		$sheet->setCellValue('F'.$q, $chantier->getTotalreleve());$k++;
		$sheet->setCellValue('G'.$q, $totaleffect[$chantier->getId()]);$k++;
		$sheet->setCellValue('H'.$q, $pourcent_effect);$k++;
		$sheet->setCellValue('I'.$q, $curdays.'/'.$nbdays);$k++;
		$sheet->setCellValue('J'.$q, $delta);$k++;
		$sheet->setCellValue('K'.$q, $cadence);	$k++;
		$sheet->setCellValue('L'.$q, $chantier->getReleveur());$k++;
		if($chantier->getReleveur() > 0){$cadence_j=$cadence/$chantier->getReleveur();}else{$cadence_j='';}
		if($chantier->getReleveur() > 0){$cadence_ok2=$cadence_ok/$chantier->getReleveur();}else{$cadence_ok2='';}
		$sheet->setCellValue('M'.$q, $cadence_j);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('N'.$q, $TR);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('O'.$q, $cadence_ok2);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		if($avancement == 2){
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FF0000');
		}
		if($avancement == 1){
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3008000');
		}
		$q++;
		}
		$q++;
		/*
			$causersite[$user_id][$chantier_id]=($causersite[$user_id][$chantier_id]??0)+$ca;
			$casite[$chantier_id]=($casite[$chantier_id]??0)+$ca;
			$cadaysite[$date_rel][$chantier_id]=$ca;
			$cadaytotal[$date_rel]=($cadaytotal[$date_rel]??0)+$ca;
			$catotal=$catotal+$ca;
		*/
		$sheet = $spreadsheet->getSheet(1);
		$q=1;
		$sheet->setCellValue('A'.$q, 'TABLEAU DE RELEVES');
		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$sheet->setCellValue('A'.$q, 'Site');
		$sheet->setCellValue('B'.$q, 'Utilisateur');
		$sheet->setCellValue('C'.$q, 'Soc');
		$sheet->setCellValue('D'.$q, 'CA');
		$sheet->setCellValue('E'.$q, 'Cumul Rel');
									$sheet->getStyleByColumnAndRow( 5, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
									$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$sheet->setCellValue('F'.$q, 'Cumul Inf');
									$sheet->getStyleByColumnAndRow( 6, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
									$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('N')->setAutoSize(true);
		$sheet->getColumnDimension('R')->setAutoSize(true);
		$sheet->getColumnDimension('V')->setAutoSize(true);
		$sheet->getColumnDimension('Z')->setAutoSize(true);
		$sheet->getColumnDimension('AJ')->setAutoSize(true);
		$sheet->getColumnDimension('AP')->setAutoSize(true);
		$cran=4;
		$k=7;
		$nbday=0;

		$CAtotaljournee=array();
		for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$sheet->setCellValueByColumnAndRow( $k, $q,$date->format('d-m-Y'));
			$k=$k + $cran;
			$nbday++;

		}
		$q++;
		$sheet->setCellValueByColumnAndRow($k, 1, 'Total Site');
			for($h=1;$h<=$k+2;$h++){
			//$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
			//$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			//$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
			//$sheet->getStyleByColumnAndRow( $h, $q);
			}

		//$q=2;
		$k=6;
		for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Tournée');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Relevé');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infruct');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Total');
			/*
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA');
			*/
		}
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Relevé');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infruct');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Total');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, '% Infruct');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA Hebdo');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA Quotidien');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Taux de relève Hebdo');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Jours Homme');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Cadence');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Cadence A-1');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Panier');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infos');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Commentaires');
		$lastC = 0;
		$CAsitejour=array();
		$CAT=0;
$zone_encours=0;
		foreach ($chantiers as $chantier){
			$CATS=0;
			$dayswork=array();
			$totreleveTS=array();
			$totinfructTS=array();
			$idCh = $chantier->getId();
			$tarifRL=0;
			$tarifIF=0;
			$tottotalCh=0;
			$totreleveCh=0;

			if ($lastC > 0 & $lastC != $idCh ){
				$q++;
				$k=10;
				$tots=0;
				$taux_releve = '';
				if ($tottotalCh>0){
				$taux_releve = ($totreleveCh / $tottotalCh)*100;
				}

				$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE');
				for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
					if (isset($totdaySite[$date->format('Y-m-d')][$lastC])){$tot = $totdaySite[$date->format('Y-m-d')][$lastC] ;$tots=$tots+$tot;}else {$tot = 0;}
					$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
					$cads=($cadaysite[$date->format('Y-m-d')][$lastC]??0);
					/*$sheet->setCellValueByColumnAndRow($k+1, $q, $cads);*/
					$k=$k + $cran;
				}
				$k=$k - 1;
				$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
				/*
				$k=$k + 2;
				$sheet->setCellValueByColumnAndRow(  $k, $q,$casite[$lastC]);
				*/
				$lastC = $idCh;
				for($h=1;$h<=$k+1;$h++){
				$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
				$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
				//$sheet->getStyleByColumnAndRow( $h, $q);
				}
			}
			if ($lastC == 0) {$lastC = $idCh;}
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$CAsitejour[$date->format('Y-m-d')]=0;
			}
			$listk=0;
			foreach ($users[$idCh] as $user ){
				$listk++;
				$dayswork=array();
				$user_id=$user->getId();
				if ($chantier->getNom() != '' ){
									$charge=$chantier->getZone()->getAbrev();
									$societe=$chantier->getSociete()->getNom();
									$k=0;
									$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
									$q++;
									$k++;
									if($listk == 1){
									if($chantier->getZone()->getId() != $zone_encours){
										$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getZone()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$zone_encours = $chantier->getZone()->getId();
										$q++;
									}
									$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.count($users[$idCh]).' agents) - Du '.$chantier->getDatedebut()->format('d-m').' au '.$chantier->getDatefin()->format('d-m-Y'));
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $username);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $charge);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $societe);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($cumulRel[$idCh][$user_id]??0));
									$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($cumulInf[$idCh][$user_id]??0));
									$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
								$totreleveT=0;
								$totinfructT=0;
								$tottotalT=0;

					for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){

							if (isset($donnees[$idCh][$date->format('Y-m-d')][$user->getId()])){
								$totreleve=0;
								$totinfruct=0;
								$tottotal=0;

								foreach($donnees[$idCh][$date->format('Y-m-d')][$user->getId()] as $enreg){
									$totreleve=$totreleve+$enreg->getReleve();
									$totinfruct=$totinfruct+$enreg->getInfructueux();
									$tottotal=$tottotal+$enreg->getTotal();
									$totreleveT=$totreleveT+$enreg->getReleve();
									$totinfructT=$totinfructT+$enreg->getInfructueux();
									$tottotalT=$tottotalT+$enreg->getTotal();
									$tottotalCh = $tottotalCh + $enreg->getTotal();
									$totreleveCh = $totreleveCh + $enreg->getReleve();
									$ref=$enreg->getTitre();
									if($tottotal > 0){$dayswork[$user->getId()][$date->format('Y-m-d')]=1;}
									else {$tottotal = 0;}
									if($enreg->getTotal() > 0){
											$totaluserchantier[$idCh][$user->getId()][$date->format('Y-m-d')]=($totaluserchantier[$idCh][$user->getId()][$date->format('Y-m-d')]??1);
									}
								}

									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, $ref);
									$defaut=0;
									if(isset($defauts[$date->format('Y-m-d')][$user->getId()][$idCh])){
										if($defaut[$date->format('Y-m-d')][$user->getId()][$idCh] == 1){
											$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
											$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FFFF0000');
											$defaut= 1;
										}
										if($defauts[$date->format('Y-m-d')][$user->getId()][$idCh] == 2){
											$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
											$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFC0AC');
											$defaut= 1;
										}

									}
									if($defaut==0){
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
									}
									if($listk == 1){
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($borderDateOn);//->applyFromArray($styleArray);
									}

									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totreleve);
									if($listk == 1){
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($borderDateOn);//->applyFromArray($styleArray);
									}
									if(($tottotal == 0)&&(defaut!=1)){
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totinfruct);
									if(($tottotal == 0)&&(defaut!=1)){
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $tottotal);
									if($listk == 1){
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($borderDateOn);//->applyFromArray($styleArray);
									}
									if($tottotal == 0){
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
														$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
									}
							}
							else{
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, '');
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, '');
										if($date<$datenow){
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
										}
									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, '');
										if($listk == 1){
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($borderDateOn);//->applyFromArray($styleArray);
										}
										if($date<$datenow){
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
										}
									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, '');
									if($listk == 1){
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($borderDateOn);//->applyFromArray($styleArray);
									}
										if($date<$datenow){
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FFFF00');
										}
							}
					}//fin dates

									$CA=$tarifRL*$totreleveT+$tarifIF*$totinfructT;
									if($tottotalT>0){
									$pourcent_infruct=(round(($totinfructT/$tottotalT)*10000)/100).'%';}
									else{$pourcent_infruct=0;}
									if(isset($dayswork[$user_id])){
									$countdays = array_sum($dayswork[$user_id]);}
									else{
									$countdays = 0;}
									if($countdays  > 0){
									$CAday=round(($causersite[$user->getId()][$idCh]??0)/$countdays );
									}else{$CAday=0;}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totreleveT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totinfructT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, $tottotalT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $pourcent_infruct);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($causersite[$user->getId()][$idCh])??0);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $CAday);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									if($tottotalT > 0){
										$tauxhebdo = (round(($totreleveT/$tottotalT)*10000)/100).'%';
										$sheet->setCellValueByColumnAndRow($k, $q, $tauxhebdo);
									}
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray3);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
									$k++;

									if(isset($totaluserchantier[$idCh][$user->getId()])){
										$nbj=array_sum($totaluserchantier[$idCh][$user->getId()]);
										$cadence=($tottotalT / $nbj);
										$sheet->setCellValueByColumnAndRow($k, $q, (array_sum($totaluserchantier[$idCh][$user->getId()])));
										$sheet->setCellValueByColumnAndRow($k+1, $q, $cadence);
									}

												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;

												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;

										$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getTauxreleve());
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
									$k++;
									$chantiersusers=$em->getRepository('App:ChantierUser')->findByChantierandUser($chantier, $user);
									if(count($chantiersusers)>0){
									$panier=$chantiersusers[0]->getPanier();$comment=$chantiersusers[0]->getCommentaire();}
									else{$panier=0;$comment='';}
									if(($panier>0)&&(isset($totaluserchantier[$idCh][$user->getId()]))){
										$sheet->setCellValueByColumnAndRow($k, $q, (array_sum($totaluserchantier[$idCh][$user->getId()])));
										}
									else{
										$sheet->setCellValueByColumnAndRow($k, $q, 0);
										}
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, $comment);
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, $commentaire[$user->getId()]);
										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray3);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');

				}
			}

		}
			$q++;
			$k=10;
			$tots=0;
			$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE');
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
				if (isset($totdaySite[$date->format('Y-m-d')][$lastC])){$tot = $totdaySite[$date->format('Y-m-d')][$lastC] ;$tots=$tots+$tot;}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				/*
				$sheet->setCellValueByColumnAndRow($k+1, $q, ($cadaysite[$date->format('Y-m-d')][$lastC]??0));
				*/
				$k=$k + $cran;

			}
			$k=$k - 1;
			$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
			$k=$k + 2;
			$sheet->setCellValueByColumnAndRow(  $k, $q,($casite[$lastC]??0));
			for($h=1;$h<=$k+2;$h++){
			$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
			//$sheet->getStyleByColumnAndRow( $h, $q);
			}
			$q++;
			$k=10;
			$tots=0;
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
				$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAUX');
				if (isset($totday[$date->format('Y-m-d')])){$tot = $totday[$date->format('Y-m-d')] ;$tots=$tots+$tot;}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				/*
				$sheet->setCellValueByColumnAndRow( $k+1,$q,($cadaytotal[$date->format('Y-m-d')]??0));
				*/
				$k=$k + $cran;
			}
			$k=$k-1;
			$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
			$sheet->setCellValueByColumnAndRow( $k+2, $q,$catotal);
			for($h=1;$h<=$k+2;$h++){
			$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
			}
		$q++;$q++;
		$sheet->setCellValue('A'.$q, 'PLANIFIES SANS RELEVES');

		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		foreach ($chantiersencours as $chantier){
			$listk=0;
			if(!in_array($chantier,$chantiers)){
				$idCh=$chantier->getId();
				if(isset($users[$idCh])){
					$count_agent=count($users[$idCh]);
				}
				else{$count_agent=0;}
				if($listk == 0){
								$sheet->setCellValueByColumnAndRow(1, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.$count_agent.' agents) - Fin le '.$chantier->getDatefin()->format('d-m-Y'));
				}
				if(isset($users[$idCh])){
				foreach ($users[$idCh] as $user ){
					$listk++;
					$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
					$sheet->setCellValue('B'.$q, $username);
					$q++;
				}
				}
				if(!isset($users[$idCh])){
				$q++;
				}
			}
		}	//fin chantierencours
		$spreadsheet->setActiveSheetIndex(0);


        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $date3.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

		if($mail==1){
			$em = $this->getDoctrine()->getManager();
			$message_obj = $em->getRepository('App:Messagecron')->find(1);
			$datenow=new \DateTime('now');
			$sujet='suivi-eae.com '.$datenow->format('d-m-Y');
			$mess='Bonjour,<br>
			<br/>'.$message_obj->getMessage().'<br>
			<br>Pour plus d\'infos, se connecter à son interface : <a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a><br/>
			<br/>Bonne journée à tous';
			$this->sendConfirmationEmailMessage(null, $sujet, $mess, $temp_file, $fileName);
			// Return the excel file as an attachment
			return new Response(1);
        }
		else{
			// Return the excel file as an attachment
			return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
		}
	}

	//export excel
	public function fluxExcel(Request $request)
    {
		$this->titre = 'Flux';
	  	$em = $this->getDoctrine()->getManager();
		$zone = 2;

		//-------------FORMULAIRE---------------------
		$date1='';
		$date2='';
		$enregs=array();
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(AdminStatsClotureType::class, $enregistrement, array('zone' => $zone));


		$form->handleRequest($request);
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$cloture=$form->get('cloture')->getData();
		$excel=$form->get('excel')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('Monday this week');
		}
		if($date2==""){
			$date2 = new \DateTime('Saturday this week');
		}
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsClotures($enregistrementCriteres, $zone, $date1, $date2,1);
		$excel=1;
		if($excel == 1){return $this->exportClotures ($enregs, $date1, $date2);}
    }


	public function sendConfirmationEmailMessage($emails, $subject, $mess, $data = null, $file= null, $data2 = null, $file2= null)
			{

				$message = (new \Swift_Message($subject));
				$message->setFrom('contact@releve-eae.com');
				if($emails == null){
				//
				$emails=array('jean-luc.douaran@sbeae.com','sylvie.caron@sbeae.com','arnaud.tanguy@sbeae.com','jean-baptiste.cros@sbeae.com','anne-marie.payen@sbeae.com','mathilde.douaran@sbeae.com','alan.grolleau@sbeae.com','eae@hebline.com','f.m@hebline.com','j.b@hebline.com','michael.hervouet@sbeae.com');
				}

				//TEST
				//$emails=array('untel44@gmail.com','f.m@hebline.com','j.b@hebline.com');
				//$emails=array('j.b@hebline.com');

				if($data != null){
					$data=file_get_contents($data);
					$attachment = new \Swift_Attachment($data, $file, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					$message->attach($attachment);
				}
				if($data2 != null){
					$data2=file_get_contents($data2);
					$attachment2 = new \Swift_Attachment($data2, $file2, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					$message->attach($attachment2);
				}

				//echo $subject.$file.':';
				foreach($emails as $email){
					echo $email.'-';
				}
				echo "<br>\n";
				/**/
				$message->setTo($emails)->setBody($mess, 'text/html');

				try {
					$result = $this->mailer->send($message);
				}
				catch (\Swift_TransportException $e) {
					echo $e->getMessage();
				}



	}
		//export excel
	public function planningExcel(Request $request)
    {
	  	$em = $this->getDoctrine()->getManager();

		//-------------FORMULAIRE---------------------
		$zone= null;
		$date1= '';
		$date2 = '';
		//$date1= new \DateTime('Monday last week');
		//$date2 = new \DateTime('Friday last week');
		$date1 = new \DateTime('first day of January');
		$date2 = new \DateTime('last day of December');
		//echo $date1->format('d-m-Y');
		$date2->add(new \DateInterval('P2M'));
		return $this->calcul_planning($zone, $date1, $date2);
	}


	public function calcul_planning($zone, $date1, $date2) {

		$em = $this->getDoctrine()->getManager();
		$chantiersencours = $em->getRepository('App:Chantier')->findChantiersEnCours($zone, $date1, $date2,1);

		$users=Array();
		$enregistrementCriteres = new Enregistrement();
		foreach($chantiersencours as $chantier){
			$idch=$chantier->getId();
			$chantiersusersencours=$chantier->getChantierUsers();
			foreach($chantiersusersencours as $chantiersusersencour){
			$user=$chantiersusersencour->getUser();
			$commentaire=$chantiersusersencour->getCommentaire();
				$user_id=$user->getId();
				$users[$idch][$user_id]=$user;
				$commentaires[$idch][$user_id]=$commentaire;
			}
		}
		return $this->exportPlanning ($chantiersencours, $commentaires, $users, $date1, $date2);
	}

	public function exportPlanning ($chantiersencours, $commentaires, $users, $date1, $date2){

		$em = $this->getDoctrine()->getManager();
		$datenow=new \DateTime('now');
		//echo $date1->format('Y-m-d');;
		$date3='Planning_'.$datenow->format('d-m-Y').'-'.time();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Planning_'.$datenow->format('d-m-Y'));

		//$sheet = $spreadsheet->getSheet(0);

		$startDate = $date1;
		$stopDate = $date2;

		//$sheet->setCellValueByColumnAndRow(1, 2, $date1->format('d-m-Y'));
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);
        $borderDateOn = array(
				'borders' => array(
				'top' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
					'color' => array('argb' => 'FF3469F7'),
				),
			),
		);
		$q=1;
		$sheet->setCellValue('A'.$q, 'PLANNING - Du '.$date1->format('d-m-Y').' au '.$date2->format('d-m-Y').' (tri par date début)');
		$sheet->getStyleByColumnAndRow( 1, 1)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$q++;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$k=1;
		$zone_nom='';
		foreach ($chantiersencours as $chantier){
										$idch=$chantier->getId();
										$zone_nom2=$chantier->getZone()->getNom();
										$totalrel=$chantier->getReleveur();
										if($zone_nom2 != $zone_nom){
										$q++;
										$sheet->setCellValueByColumnAndRow($k, $q, $zone_nom2);
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF#87CEEB');
										$q++;
										$zone_nom = $zone_nom2;
										}
										$sheet->setCellValueByColumnAndRow($k, $q,$chantier->getNom().' '.$chantier->getClient()->getNom().' - '.$totalrel.' agents - Du '.$chantier->getDatedebut()->format('d-m').' au '.$chantier->getDatefin()->format('d-m-Y'));
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$q++;
										$count=0;
										if(isset($users[$idch])){
										$usersencours=$users[$idch];

										foreach ($usersencours as $user){
										$count++;
										$user_id = $user->getId();
										$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
										$sheet->setCellValueByColumnAndRow($k, $q,$username);
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
										$sheet->setCellValueByColumnAndRow($k+1, $q,($commentaires[$idch][$user_id]??''));
										$sheet->getStyleByColumnAndRow( $k+1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
										$q++;
										}
										}
										if($totalrel > $count){
											for($z=$count;$z<$totalrel;$z++){
												$q++;
											}
										}
		}
		$q++;
		        // Create your Office 2007 Excel (XLSX Format)
        /*
        $macro='Sub bonjour()
		MsgBox ("Salut tout le monde!")
		End Sub';
		 $spreadsheet->setMacrosCode($macro);
         $spreadsheet->setHasMacros(true);
		 */
        // Create a Temporary file in the system
		$writer = new Xlsx($spreadsheet);
        $fileName = 'Planning.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);
			$sujet='Planning '.$datenow->format('d-m-Y');
			$mess='Bonjour,<br><strong>le planning actualisé est en pièce jointe.</strong>
			Le nombre d\'agents par chantier se modifie dans la déclaration de chantier.<br/>Il est également possible d\'ajouter des commentaires pour chaque agent sur un chantier (par exemple date de début et de fin).<br>
			Bonne journée à tous';

			$this->sendConfirmationEmailMessage(null, $sujet, $mess, $temp_file, $fileName);
			// Return the excel file as an attachment
			return new Response(1);
	}

			public function calcul_export_2($feuille,$spreadsheet,  $action, $enregistrementCriteres = null, $zone, $date1, $date2, $form = null, $titre = null ) {

		$em = $this->getDoctrine()->getManager();
//echo $date2->format('Y-m-d');
		$cumulRel=$commentaires=$commentaire=$cumulInf=array();
		//$enregistrementCriteres=null;
		$enregs=array();
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,1);
		$totaux = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateursTotal($enregistrementCriteres, $zone, $date1, $date2,1);
		$totauxSite = $em->getRepository('App:Enregistrement')->findEnregistrementsSiteTotal($enregistrementCriteres, $zone, $date1, $date2,1);
		$totauxUserSite = $em->getRepository('App:Enregistrement')->findEnregistrementsSiteUserTotal($enregistrementCriteres, $zone, $date1, $date2,1);


		$users=$totaleffect=array();

		if ($enregistrementCriteres != '') {

			if($enregistrementCriteres->getChantier() != null){
			$chantier[]=$enregistrementCriteres->getChantier();
			$userencours=$enregistrementCriteres->getUser();
			}
		}

		if(isset($chantier)){
			 $chantiersencours=$chantier;
		}
		else{
		   $chantiersencours = $em->getRepository('App:Chantier')->findChantiersEnCours($zone, $date1, $date2,1);
	  }


		$enregistrementCriteres = new Enregistrement();


		foreach($chantiersencours as $chantier){
			$idch=$chantier->getId();

			$chantiersusersencours=$chantier->getChantierUsers();
			if(count($chantiersusersencours)){
					foreach($chantiersusersencours as $chantiersusersencour){
						$user=$chantiersusersencour->getUser();
						//echo $user->getNom().'-';
							if(!isset($userencours) || ($userencours == $user)){
							//foreach($usersencours as $user){
							$user_id=$user->getId();
							$users[$idch][$user_id]=$user;
							$commentaire[$user_id]='';
							$commentaires = $em->getRepository('App:Commentaire')->findCommentairesEnCours($user, $date1, $date2);
								foreach($commentaires as $comm){
									$commentaire[$user_id].='Du '.$comm->getDatedebut()->format('d-m-Y').' au '.$comm->getDatefin()->format('d-m-Y').' : '.$comm->getCommentaire().' | ';
									//echo $commentaire[$user_id].'-';
								}
							}
						//echo $idch.'-'.$user_id.'___';
					//}
					}
				}
				$tot=$em->getRepository('App:Enregistrement')->findEnregistrementsTotalSite($chantier);
				if(isset($tot[0]['total'])){
				$totaleffect[$idch]=$tot[0]['total'];
				}
				else{
					$totaleffect[$idch]=0;
				}

			$enregistrementCriteres->setChantier($chantier);
			$totalreleve2 = $em->getRepository('App:Enregistrement')->findEnregistrementsSiteUserTotal($enregistrementCriteres, $zone,'',$date2,1);
			foreach ($totalreleve2 as $totalreleve){

			if(isset($totalreleve['totreleve'])){
				$userid=$totalreleve['user'];
			//echo $idch.':'.$userid.'-'.$totalreleve['totreleve'].'_\n';
			if(isset($totalreleve['totreleve'])){
				$cumulRel[$idch][$userid]=$totalreleve['totreleve'];
			}
			else{
				$cumulRel[$idch][$userid]=0;
			}
			if(isset($totalreleve['totinfructueux'])){
				$cumulInf[$idch][$userid]=$totalreleve['totinfructueux'];
			}
			else{
				$cumulInf[$idch][$userid]=0;
			}
			}

			if ($idch >0 ){
						$tarifRL[$idch]=($chantier->getTarifreleve()??0);
						$tarifIF[$idch]=($chantier->getTarifinfructueux()??0);
						$tarifRR[$idch]=($chantier->getTarifradioreleve()??0);
			}
			}
		}
		//exit;
		$catotal=0;
		$totday=$cadaysite=$cadaytotal=array();
		foreach($totaux as $total){
			$date=$total['datereleve'];
			$tot=$total['total'];
			$totday[$date->format('Y-m-d')]=$tot;
			//echo $date->getTimestamp().'-'.$tot.'_';
		}
		$totdaySite=array();
		$q=0;
		foreach($totauxSite as $totalite){

			$date=$totalite['datereleve'];
			$tot=$totalite['total'];
			$site=$totalite['chantier'];
			$totreleve=$totalite['totreleve'];
			$totinfructueux=$totalite['totinfructueux'];
			$totdaySite[$date->format('Y-m-d')][$site]=$tot;
			$chantier=$em->getRepository('App:Chantier')->find($site);
		}

		$causersite=$casite=$infruct=array();
		foreach($totauxUserSite as $totalite){
			//print_r($total);
			$user=$totalite['user'];
			$site=$totalite['chantier'];
			$total=$totalite['total'];
			$totreleve=$totalite['totreleve'];
			$totinfructueux=$totalite['totinfructueux'];
			$chantier=$em->getRepository('App:Chantier')->find($site);
			if($total>0){
			$infruct[$user][$site]=round(($totinfructueux/$total)*10000)/100;
			}else{
			$infruct[$user][$site]=0;
			}
			//echo $date->getTimestamp().'-'.$tot.'_';
		}
		$catotal=$newcompteursT =0;
		$tarifNcompteur = 25; //tarif new compteur;
		$donnees=$commentairesUser=$chantiers=$totjourchantieruser=$defauts=$totaljourchantieruser=$paniers=$causersite=$casite=$cadaysite=$cadaytotal=$counter=$counterT = $newcompteurs = $newcompteursCH =  Array();

		foreach ($enregs as $enreg){
			//decompte si enreg valide
			if($enreg->getValide() == 1){
				$date_rel=$enreg->getDatereleve()->format('Y-m-d');
				$user=$enreg->getUser();
				$user_id=$enreg->getUser()->getId();
				$chantier_id=$enreg->getChantier()->getId();
				$totaljourchantieruser[$chantier_id][$user_id][$date_rel]=1;
				$donnees[$chantier_id][$date_rel][$user_id][]=$enreg;
				$chantiers[$chantier_id]=$enreg->getChantier();
				$totreleve=($enreg->getReleve()??0);
				$totinfructueux=($enreg->getInfructueux()??0);
				$nbc = $enreg->getNewcompteur();

					if($enreg->getUser()->getNom() !=''){
					$users[$enreg->getChantier()->getId()][$user_id]=$enreg->getUser();
					}
					//calculs des tarifs jours
					if($enreg->getRadioreleve() == 1){$ca=$enreg->getChantier()->getTarifradioreleve()*$totreleve;}
					else{
					if(!isset($tarifIF[$chantier_id])){$tarifIF[$chantier_id]=0;}
					if(!isset($tarifRL[$chantier_id])){$tarifRL[$chantier_id]=0;}
					$ca=$tarifRL[$chantier_id]*$totreleve+$tarifIF[$chantier_id]*$totinfructueux;
					}
					if($enreg->getTarifjour() == 1){$ca=($enreg->getChantier()->getForfait()??0);}

				//echo $ca.'-';
				if($enreg->getDefaut() == 1){$defauts[$date_rel][$user_id][$chantier_id] = 1;}
				if($enreg->getValide() == 0){$defauts[$date_rel][$user_id][$chantier_id] = 2;}

				$ca = $ca + ($nbc * $tarifNcompteur);
				$causersite[$user_id][$chantier_id]=($causersite[$user_id][$chantier_id]??0) + $ca;
				$casite[$chantier_id]=($casite[$chantier_id]??0) + $ca;
				$cadaysite[$date_rel][$chantier_id]=($cadaysite[$date_rel][$chantier_id]??0) + $ca;
				$cadaytotal[$date_rel]=($cadaytotal[$date_rel]??0) + $ca;
				$catotal=$catotal + $ca;

				$newcompteurs[$user_id] = ($newcompteurs[$user_id] ?? 0) + $nbc;
				$newcompteursCH[$chantier_id] = ($newcompteursCH[$chantier_id] ?? 0) + $nbc;
                $newcompteursT = ($newcompteursT??0) + $nbc;
				$counterT[$date_rel][$user_id]=$enreg;
			}

		}
		//calcul de stotaux jours hommes
		for($date=clone $date1; $date<=$date2; $date->modify('+1 day')){
				if(isset($counterT[$date->format('Y-m-d')])){
					foreach($counterT[$date->format('Y-m-d')] as $enreg){
						$client = $enreg->getChantier()->getClient()->getCle();
						$counter[$date->format('Y-m-d')][$client]=($counter[$date->format('Y-m-d')][$client]??0)+1;
						$counter[$date->format('Y-m-d')]['tot']=($counter[$date->format('Y-m-d')]['tot']??0)+1;
						$reponsable = $enreg->getChantier()->getZone()->getResponsable()->getNom();
						$counter[$date->format('Y-m-d')][$reponsable]=($counter[$date->format('Y-m-d')][$reponsable]??0)+1;
					}
				}
		}
		/* tri des chantiers par nom */
		//usort($chantiers, array($this, "cmp"));
		foreach($chantiers as $chantier){
			foreach($chantier->getChantierusers() as $chantieruser){
				if(!isset($userencours) || ($userencours == $chantieruser)){
					$users[$chantieruser->getChantier()->getId()][$chantieruser->getUser()->getId()]=$chantieruser->getUser();
				}
			}
			foreach($users[$chantier->getId()] as $user){
				if(isset($totaljourchantieruser[$chantier->getId()][$user->getId()])){
					$totjourchantieruser[$chantier->getId()][$user->getId()]=array_sum($totaljourchantieruser[$chantier->getId()][$user->getId()]);
				}
				else{
					$totjourchantieruser[$chantier->getId()][$user->getId()]=0;
				}
				$chantiersusers=$em->getRepository('App:ChantierUser')->findByChantierandUser($chantier, $user);
				if(count($chantiersusers)>0){
				$paniers[$chantier->getId()][$user->getId()]=$chantiersusers[0]->getPanier();}
			}
		}

//exit;
		$alertes_array=$em->getRepository('App:Alerte')->findAlertes($date1,$date2);
		$alertes=array();
		if(is_array($alertes_array)){
			foreach ($alertes_array as $alerte){
				$user_alerte_id=$alerte->getUser()->getId();
				$user_alerte_date=$alerte->getDatealerte()->format('Y-m-d');
				$alertes[$user_alerte_id][$user_alerte_date]=1;
			}
		}
		//echo$totaljourchantieruser
		if($action=='rapport'){
					return $this->exportRapport_mail ($feuille,$spreadsheet,$defauts,$commentaire,$cumulRel,$cumulInf,$causersite, $casite, $cadaysite, $cadaytotal, $catotal,$enregs, $totday, $totdaySite, $donnees, $users, $date1, $date2,$chantiers,  $chantiersencours, $totaleffect, $alertes, $counter,$newcompteurs, $newcompteursCH, $newcompteursT);
		}
		if($action=='planning'){
				return $this->exportChantiersEnCours ($feuille, $spreadsheet, $defauts, $commentaire, $cumulRel,$cumulInf, $causersite, $casite, $cadaysite, $cadaytotal, $catotal, $enregs, $totday, $totdaySite, $donnees, $users, $date1,$date2,$chantiers,  $chantiersencours, $totaleffect);
		}
		if($action=='excel'){
				$nom='export_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y');

				$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
				$spreadsheet=$this->exportRapport_mail ($feuille,$spreadsheet,$defauts,$commentaire,$cumulRel,$cumulInf,$causersite, $casite, $cadaysite, $cadaytotal, $catotal,$enregs, $totday, $totdaySite, $donnees, 	$users, $date1, $date2,$chantiers,  $chantiersencours, $totaleffect, $alertes, $counter,$newcompteurs, $newcompteursCH, $newcompteursT);

				$spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $fileName = $nom.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
				return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
		}

		if($action=='controle'){
				$nom='controle_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y');

				$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
				$spreadsheet=$this->exportRapport_hebline ($feuille,$spreadsheet,$defauts,$commentaire,$cumulRel,$cumulInf,$causersite, $casite, $cadaysite, $cadaytotal, $catotal,$enregs, $totday, $totdaySite, $donnees, $users, $date1, $date2,$chantiers,  $chantiersencours, $totaleffect, $alertes);

				$spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $fileName = $nom.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
				return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
		}

		if($action=='frontadmin'){
			//recup des commenataires
			$notes=array();
			$notes = $this->getCommentaires($date1, $date2);
			$zone_id = $this->getUser()->getZone()->getId();
			$repo = $this->fonctions->getRepo($zone_id);
			$zone = $this->getUser()->getZone()->getId();
        return $this->render('Generique/Stats/Rapport/indexReleve.html.twig', array(
        		'action' => $action,
            'enregs' => $enregs,
						'titre' => $this->titre,
						'form' 			=> $form->createView(),
						'date1' => $date1,
						'date2' => $date2,
						'totday' => $totday,
						'totdaySite' => $totdaySite,
						'donnees'  => $donnees,
						'users' => $users,
						'chantiers' => $chantiers,
						'cadaysite' => $cadaysite,
						'cadaytotal' => $cadaytotal,
						'catotal' => $catotal,
						'infruct' => $infruct,
						'causersite' => $causersite,
						'casite' => $casite,
						'totjourchantieruser' => $totjourchantieruser,
						'paniers' => $paniers,
						'repo' => $repo,
						'commentaires' => $notes[0]
        ));
		}
	}

		public function exportRapport_mail ($feuille, $spreadsheet,$defauts, $commentaire, $cumulRel,$cumulInf, $causersite, $casite, $cadaysite, $cadaytotal, $catotal, $enregs, $totday, $totdaySite, $donnees, $users, $date1,$date2,$chantiers,  $chantiersencours, $totaleffect, $alertes, $counter, $newcompteurs, $newcompteursCH, $newcompteursT){
		$em = $this->getDoctrine()->getManager();
		$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $feuille);
		$spreadsheet->addSheet($myWorkSheet, 0);
			//$sheet = $spreadsheet->getActiveSheet();
		$startDate = $date1;
		$stopDate = $date2;
		$datenow=new \DateTime('now - 12 hours');

		//STYLES
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray4 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'FFC0AC'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);

    $borderDateOn = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF3469F7'),
		        ),
		    ),
		);
    $borderRight = array(
    'borders' => array(
        'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF000000'),
		        ),
		    ),
		);
		 $borderalertLeft = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		        ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     ),
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     )
		    ),
		);
		 $borderalertRight = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		        ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     ),
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     )
		    ),
		);

		 	$notes = $commentaire = $indications = $nbjours = array();
			$notes = $this->getCommentaires($date1, $date2);
			$commentaire = $notes[0];
			$nbjours = $notes[1];
			$indications = $notes[2];
			$tottotalCh=0;
			$totreleveCh=0;
		$sheet = $spreadsheet->getSheet(0);
		$q=1;

		if(count($enregs)>0){
		$sheet->setCellValue('A'.$q, 'TABLEAU DE RELEVES '.$feuille);
		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$sheet->setCellValue('A'.$q, 'Site');
		$sheet->setCellValue('B'.$q, 'Utilisateur');
		$sheet->setCellValue('C'.$q, 'H');
		$sheet->setCellValue('D'.$q, 'CA');
		$sheet->setCellValue('E'.$q, 'Soc');
		$sheet->setCellValue('F'.$q, 'JH');
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('S')->setWidth(40);
		$sheet->getColumnDimension('Z')->setAutoSize(true);
		//$sheet->getColumnDimension('AJ')->setAutoSize(true);
		$cran=2;
		$k=7;
		$nbday=0;
		$taux_releve=0;
		$week_name = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
		$CAtotaljournee=array();
		for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$sheet->setCellValueByColumnAndRow( $k, $q,$week_name[date('w', strtotime($date->format('d-m-Y')))].' '.$date->format('d-m-Y'));
			$k=$k + $cran;
			$nbday++;
		}
		$q++;
		$sheet->setCellValueByColumnAndRow($k+1, 1, 'Total Site');
			for($h=1;$h<=$k+2;$h++){
			//$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
			//$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			//$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
			//$sheet->getStyleByColumnAndRow( $h, $q);
			}

		//$q=2;
		$k=6;
		for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			/*
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Alrt');
			*/
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Relevé');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infruct');
			/*$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Total');

			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA');
			*/
		}
//NEW

			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Commentaires');
			$k++;
            $sheet->setCellValueByColumnAndRow($k, $q, 'N.C.');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Relevé');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infruct');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Total');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, '% Infruct');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA Hebdo');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CA Quotidien');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Taux de relève Hebdo');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Cadence');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Cadence A-1');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Panier');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'CP');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'AM');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'AT');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Abs');
			$k++;
			$sheet->setCellValueByColumnAndRow($k, $q, 'Infos');

		$lastC = 0;
		$CAsitejour=array();
		$CAT=0;
$zone_encours=0;

		//usort($users[$idCh], array($this, "cmp"));
		//Create independent arrays

//usort($chantiers, array($this, "compareTabAndOrder"));
usort($chantiers, function($a, $b) {
    $c = $a->getZone()->getId() - $b->getZone()->getId();
    if($a->getNom() > $b->getNom()){
    $c .= 1;
  	}
  	if($a->getNom() < $b->getNom()){
    $c .= -1;
  	}
    return $c;
});

		foreach ($chantiers as $chantier){
			$CATS=0;
			$dayswork=array();
			$totreleveTS=array();
			$totinfructTS=array();
			$idCh = $chantier->getId();
			$tarifRL=0;
			$tarifIF=0;

			if ($lastC > 0 & $lastC != $idCh ){
				$q++;
				$k=8;
				$tots=0;
				$taux_releve = '';
				if ($tottotalCh>0){
				$taux_releve = round(($totreleveCh / $tottotalCh)*100);
				}
			$tottotalCh=0;
			$totreleveCh=0;
				$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE - Taux relève hebdo : '.$taux_releve.' (N-1 : '.($taux_releveN_1??0).')');
				for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
					if (isset($totdaySite[$date->format('Y-m-d')][$lastC])){$tot = $totdaySite[$date->format('Y-m-d')][$lastC] ;$tots=$tots+$tot;}else {$tot = 0;}
					$sheet->setCellValueByColumnAndRow( $k, $q, $tot);
					$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
					$cads=($cadaysite[$date->format('Y-m-d')][$lastC]??0);
					/*$sheet->setCellValueByColumnAndRow($k+1, $q, $cads);*/
					$k=$k + $cran;
				}
				$sheet->setCellValueByColumnAndRow( $k, $q,$newcompteursCH[$lastC]);
				$k=$k + 3;
				$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
				$k=$k + 2;
				$sheet->setCellValueByColumnAndRow(  $k, $q,$casite[$lastC]);
				/**/
				$lastC = $idCh;
				for($h=1;$h<=$k+2;$h++){
				$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
				$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
				//$sheet->getStyleByColumnAndRow( $h, $q);
				}
			}
			if ($lastC == 0) {$lastC = $idCh;}
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$CAsitejour[$date->format('Y-m-d')]=0;
			}
			$listk=0;
			if(isset($users[$idCh])){
			usort($users[$idCh], array($this, "cmp"));}

			foreach ($users[$idCh] as $user ){
				$listk++;
				$dayswork=array();
				$user_id=$user->getId();
				if ($chantier->getNom() != '' ){
									$charge=$chantier->getZone()->getAbrev();
									$societe=$chantier->getSociete()->getNom();
									$k=0;
									$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
									$horaire=$user->getHoraire()->getNom();
									$q++;
									$k++;
									if($listk == 1){
									if($chantier->getZone()->getId() != $zone_encours){
										$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getZone()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$zone_encours = $chantier->getZone()->getId();
										$q++;
									}
									$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.count($users[$idCh]).' agents) - Du '.$chantier->getDatedebut()->format('d-m').' au '.$chantier->getDatefin()->format('d-m-Y'));
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $username);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $horaire);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $charge);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $societe);
								$totreleveT=0;
								$totinfructT=0;
								$tottotalT=0;
$k++;
					for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){

							$color='D3FFFFFF';

								//defaut jaune si pas enreg
									$defaut=0;
									//echo 'def'.$date->format('Y-m-d').'-'.$user_id.'-'.$idCh.':'.($defauts[$date->format('Y-m-d')][$user_id][$idCh]??0).'<br>' ;
									//defaut rouge pb enreg // orange si annulé
									if(isset($defauts[$date->format('Y-m-d')][$user_id][$idCh])){
										if($defauts[$date->format('Y-m-d')][$user_id][$idCh] == 1){
											$color='D3FF0000';
											$defaut= 1;
										}
										if($defauts[$date->format('Y-m-d')][$user_id][$idCh] == 2){
											$color='D3FFC0AC';
											$defaut= 1;
										}
									}

/* cas enregistrement défini */
							if (isset($donnees[$idCh][$date->format('Y-m-d')][$user_id])){
									$totreleve = 0;
									$totinfruct = 0;
									$tottotal  = 0;
									$commentaireUser = '';
									foreach($donnees[$idCh][$date->format('Y-m-d')][$user_id] as $enreg){
										$totreleve=$totreleve+$enreg->getReleve();
										$totinfruct=$totinfruct+$enreg->getInfructueux();
										$tottotal=$tottotal+$enreg->getTotal();
										$totreleveT=$totreleveT+$enreg->getReleve();
										$totinfructT=$totinfructT+$enreg->getInfructueux();
										$tottotalT=$tottotalT+$enreg->getTotal();
										$tottotalCh = $tottotalCh + $enreg->getTotal();
										$totreleveCh = $totreleveCh + $enreg->getReleve();
										$ref=$enreg->getTitre();
										if($enreg->getCommentaire() != ''){
										    $commentaireUser = $commentaireUser.' '.$enreg->getCommentaire()."\n";
										}
										//le jour est compté meme si 0 à l'enreg
										if(($tottotal >= 0)&&($enreg->getValide() == 1)){$dayswork[$user->getId()][$date->format('Y-m-d')]=1;}
										//else {$tottotal = 0;}
										if($enreg->getTotal() >= 0){
												$totaluserchantier[$idCh][$user->getId()][$date->format('Y-m-d')]=($totaluserchantier[$idCh][$user_id][$date->format('Y-m-d')]??1);
										}
									}



									//Evenements
									$info='';
									if (isset($indications[$user_id][$date->format('Y-m-d')][2]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][2])){
													$color='D3318CE7';
													$info="CP";
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][1]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][1])){
													$color='D3318CE7';
													$info="AM";
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][3]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][3])){
												$color='D3318CE7';
												$info="AT";
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][4]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][4])){
													$color='D3318CE7';
													$info="Abs";
									}

									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $info.' '.$totreleve);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $info.' '.$totinfruct);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
									if($commentaireUser != ''){
										$sheet->getCommentByColumnAndRow( $k, $q)->getText()->createTextRun($commentaireUser);
										$sheet->getCommentByColumnAndRow( $k, $q)->setHeight("200px");
										$sheet->getCommentByColumnAndRow( $k, $q)->setWidth("400px");
									}
									//regle cadre violet si inf< 70
									//if($tottotal < 70){
									//cas forfait
									if($enreg->getTarifjour() == 1){
										$sheet->getCellByColumnAndRow( $k-1, $q)->getStyle()->applyFromArray($borderalertLeft);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderalertRight);
									}
									if(isset($alertes[$user->getId()][$date->format('Y-m-d')])){
										if($alertes[$user->getId()] [$date->format('Y-m-d')]== 1){
											$color='D3FFA500';
											$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
										}
									}
							}
/* cas pas d'enregistrement défini */
							else{

									$color='';
									if($user->isEnabled()){$color='D3FFFF00';}else{$color='D3FFFFFF';}
									if(isset($defauts[$date->format('Y-m-d')][$user_id][$idCh])){
										if($defauts[$date->format('Y-m-d')][$user_id][$idCh] == 2){
											$color='D3FFC0AC';
											$defaut= 1;
										}
									}
										$info='';
									if (isset($indications[$user_id][$date->format('Y-m-d')][2]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][2])){
													$color='D3318CE7';
													$info="CP";
													//echo '_'.$user_id.':'.$date->format('Y-m-d');
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][1]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][1])){
													$color='D3318CE7';
													$info="AM";
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][3]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][3])){
												$color='D3318CE7';
												$info="AT";
									}
									if (isset($indications[$user_id][$date->format('Y-m-d')][4]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][4])){
													$color='D3318CE7';
													$info="Abs";
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $info);
									if($color != ''){
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
									}
									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, '');
										if($color != ''){
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
										}
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
							}
					}//fin dates

									$CA=$tarifRL*$totreleveT+$tarifIF*$totinfructT;
									if($tottotalT>0){
									$pourcent_infruct=(round(($totinfructT/$tottotalT)*10000)/100).'%';}
									else{$pourcent_infruct=0;}
									if(isset($dayswork[$user_id])){
									$countdays = array_sum($dayswork[$user_id]);}
									else{
									$countdays = 0;}
									if($countdays  > 0){
									$CAday=round(($causersite[$user->getId()][$idCh]??0)/$countdays );
									}else{$CAday=0;}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($commentaire[$user->getId()]??''));
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($newcompteurs[$user->getId()]??0));
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totreleveT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $totinfructT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;

									$sheet->setCellValueByColumnAndRow($k, $q, $tottotalT);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $pourcent_infruct);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, ($causersite[$user->getId()][$idCh])??0);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $CAday);
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
									$k++;
									if($tottotalT > 0){
										$tauxhebdo = (round(($totreleveT/$tottotalT)*10000)/100).'%';
										$sheet->setCellValueByColumnAndRow($k, $q, $tauxhebdo);
									}
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray3);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
									//$k++;

									if(isset($totaluserchantier[$idCh][$user->getId()])){
										$nbj=array_sum($totaluserchantier[$idCh][$user->getId()]);
										$cadence=($tottotalT / $nbj);
										$sheet->setCellValueByColumnAndRow(6, $q, (array_sum($totaluserchantier[$idCh][$user->getId()])));
										$sheet->setCellValueByColumnAndRow($k+1, $q, $cadence);
									}

												$sheet->getStyleByColumnAndRow( 6, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;

												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;

										$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getCadencehomme());
										$taux_releveN_1=$chantier->getTauxreleve();
												$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
												$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
									$k++;
									$chantiersusers=$em->getRepository('App:ChantierUser')->findByChantierandUser($chantier, $user);
									$panier=0;
									if(count($chantiersusers)>0){
									$panier=$chantiersusers[0]->getPanier();$comment=$chantiersusers[0]->getCommentaire();}
									else{$panier=0;$comment='';}
									if(($panier>0)&&(isset($totaluserchantier[$idCh][$user->getId()]))){
										$sheet->setCellValueByColumnAndRow($k, $q, (array_sum($totaluserchantier[$idCh][$user->getId()])));
										}
									else{
										$sheet->setCellValueByColumnAndRow($k, $q, 0);
										}
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][2] ?? 0));
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][1] ?? 0));
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][7] ?? 0));
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][8] ?? 0));
									$k++;
										$sheet->setCellValueByColumnAndRow($k, $q, $comment);
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);

										//$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray3);//->applyFromArray($styleArray);
										//$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										//$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');

				}
			}

		}
			$q++;
			$k=8;
			$tots=0;
				$taux_releve = '';
				if ($tottotalCh>0){
				$taux_releve = round(($totreleveCh / $tottotalCh)*100);
				}
			$tottotalCh=0;
			$totreleveCh=0;
				$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE - Taux relève hebdo : '.$taux_releve.' (N-1 : '.round($chantier->getTauxreleve()).')');
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
				if (isset($totdaySite[$date->format('Y-m-d')][$lastC])){$tot = $totdaySite[$date->format('Y-m-d')][$lastC] ;$tots=$tots+$tot;}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
				/*
				$sheet->setCellValueByColumnAndRow($k+1, $q, ($cadaysite[$date->format('Y-m-d')][$lastC]??0));
				*/
				$k=$k + $cran;

			}
			$sheet->setCellValueByColumnAndRow( $k, $q,$newcompteursCH[$lastC]);
			$k=$k + 3;
			$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
			$k=$k + 2;
			$sheet->setCellValueByColumnAndRow(  $k, $q,($casite[$lastC]??0));
			for($h=1;$h<=$k+3;$h++){
			$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
			//$sheet->getStyleByColumnAndRow( $h, $q);
			}
			$q++;
			$k=8;
			$tots=0;
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
				$sheet->setCellValueByColumnAndRow( 1, $q,'TOTAUX');
				if (isset($totday[$date->format('Y-m-d')])){$tot = $totday[$date->format('Y-m-d')] ;$tots=$tots+$tot;}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				/*
				$sheet->setCellValueByColumnAndRow( $k+1,$q,($cadaytotal[$date->format('Y-m-d')]??0));
				*/
				$k=$k + $cran;
			}
			$sheet->setCellValueByColumnAndRow( $k, $q,$newcompteursT);
			$k=$k+3;
			$sheet->setCellValueByColumnAndRow( $k, $q,$tots);
			$sheet->setCellValueByColumnAndRow( $k+2, $q,$catotal);
			for($h=1;$h<=$k+2;$h++){
			$sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
			}
			//decompte des jours hommes
    $q++;$q++;
    if($feuille != 'export-client'){
    $k=8;
    $color='FFb1b0b3';
    $sheet->setCellValueByColumnAndRow( 5, $q,'TOTAL AGENTS');
    													$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
															$sheet->getStyleByColumnAndRow( 5, $q)->applyFromArray($styleArray);
    $sheet->setCellValueByColumnAndRow( 5, $q+1,'AGENTS VEOLIA');
    $sheet->setCellValueByColumnAndRow( 5, $q+2,'AGENTS SUEZ');
    $sheet->setCellValueByColumnAndRow( 5, $q+3,'AGENTS AUTRES');
    $sheet->setCellValueByColumnAndRow( 5, $q+5,'AGENTS AG');
    $sheet->setCellValueByColumnAndRow( 5, $q+6,'AGENTS AT');
    $sheet->setCellValueByColumnAndRow( 5, $q+7,'AGENTS JBC');
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){

				if (isset($counter[$date->format('Y-m-d')]['tot'])){$tot = $counter[$date->format('Y-m-d')]['tot'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
				$sheet->getCellByColumnAndRow( $k-1, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
				$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);

				if (isset($counter[$date->format('Y-m-d')]['VEOLIA'])){$tot = $counter[$date->format('Y-m-d')]['VEOLIA'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+1,$tot);

				if (isset($counter[$date->format('Y-m-d')]['SUEZ'])){$tot = $counter[$date->format('Y-m-d')]['SUEZ'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+2,$tot);

				if (isset($counter[$date->format('Y-m-d')]['AUTRES'])){$tot = $counter[$date->format('Y-m-d')]['AUTRES'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+3,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Grolleau'])){$tot = $counter[$date->format('Y-m-d')]['Grolleau'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+5,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Tanguy'])){$tot = $counter[$date->format('Y-m-d')]['Tanguy'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+6,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Cros'])){$tot = $counter[$date->format('Y-m-d')]['Cros'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+7,$tot);
				$k=$k + $cran;
			}
		$q=$q+8;
		}
		}//fin count
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->setCellValue('A'.$q, 'PLANIFIES SANS RELEVES '.$feuille);

		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		foreach ($chantiersencours as $chantier){
			$listk=0;
			if(!in_array($chantier,$chantiers)){
				$idCh=$chantier->getId();
				if(isset($users[$idCh])){
					$count_agent=count($users[$idCh]);
				}
				else{$count_agent=0;}
				if($listk == 0){
								$sheet->setCellValueByColumnAndRow(1, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.$count_agent.' agents) - Fin le '.$chantier->getDatefin()->format('d-m-Y'));
				}
				if(isset($users[$idCh])){
				foreach ($users[$idCh] as $user ){
					$listk++;
					$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
					$sheet->setCellValue('B'.$q, $username);
					$q++;
				}
				}
				if(!isset($users[$idCh])){
				$q++;
				}
			}
		}	//fin chantierencours


		return $spreadsheet;
	}

public function exportRapport_hebline ($feuille, $spreadsheet,$defauts, $commentaire, $cumulRel,$cumulInf, $causersite, $casite, $cadaysite, $cadaytotal, $catotal, $enregs, $totday, $totdaySite, $donnees, $users, $date1,$date2,$chantiers,  $chantiersencours, $totaleffect, $alertes){

		$em = $this->getDoctrine()->getManager();
		$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $feuille);
		$spreadsheet->addSheet($myWorkSheet, 0);
		//$sheet = $spreadsheet->getActiveSheet();

		$startDate = $date1;
		$stopDate = $date2;
		$datenow=new \DateTime('now - 12 hours');

		//STYLES
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);

    $borderDateOn = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF3469F7'),
		        ),
		    ),
		);
    $borderRight = array(
    'borders' => array(
        'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF000000'),
		        ),
		    ),
		);
		 $borderalertLeft = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		        ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     ),
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     )
		    ),
		);
		 $borderalertRight = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		        ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     ),
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
		     )
		    ),
		);
		$sheet = $spreadsheet->getSheet(0);
		$q=1;
		if(count($enregs)>0){
		$sheet->setCellValue('A'.$q, 'TABLEAU DE RELEVES '.$feuille);
		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$sheet->setCellValue('A'.$q, 'Site');
		$sheet->setCellValue('B'.$q, 'Utilisateur');
		$sheet->setCellValue('C'.$q, 'H');
		$sheet->setCellValue('D'.$q, 'CA');
		$sheet->setCellValue('E'.$q, 'Soc');
		$cran=2;
		$k=6;
		$nbday=0;
		$week_name = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
		$CAtotaljournee=array();
		for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$sheet->setCellValueByColumnAndRow( $k, $q,$week_name[date('w', strtotime($date->format('d-m-Y')))].' '.$date->format('d-m-Y'));
			$k=$k + $cran;
			$nbday++;

		}
		$q++;

		$lastC = 0;
		$CAsitejour=array();
		$CAT=0;
$zone_encours=0;

		//usort($users[$idCh], array($this, "cmp"));
		//Create independent arrays

//usort($chantiers, array($this, "compareTabAndOrder"));
usort($chantiers, function($a, $b) {
    $c = $a->getZone()->getId() - $b->getZone()->getId();
    if($a->getNom() > $b->getNom()){
    $c .= 1;
  	}
  	if($a->getNom() < $b->getNom()){
    $c .= -1;
  	}
    return $c;
});

		foreach ($chantiers as $chantier){
			$CATS=0;
			$dayswork=array();
			$totreleveTS=array();
			$totinfructTS=array();
			$idCh = $chantier->getId();
			$tarifRL=0;
			$tarifIF=0;
			if ($lastC == 0) {$lastC = $idCh;}
			for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
			$CAsitejour[$date->format('Y-m-d')]=0;
			}
			$listk=0;
			if(isset($users[$idCh])){
			usort($users[$idCh], array($this, "cmp"));}

			foreach ($users[$idCh] as $user ){
				$listk++;
				$dayswork=array();
				$user_id=$user->getId();
				if ($chantier->getNom() != '' ){
									$charge=$chantier->getZone()->getAbrev();
									$societe=$chantier->getSociete()->getNom();
									$k=0;
									$username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
									$horaire=$user->getHoraire()->getNom();
									$q++;
									$k++;
									if($listk == 1){
									if($chantier->getZone()->getId() != $zone_encours){
										$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getZone()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
										$zone_encours = $chantier->getZone()->getId();
										$q++;
									}
									$sheet->setCellValueByColumnAndRow($k, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.count($users[$idCh]).' agents) - Du '.$chantier->getDatedebut()->format('d-m').' au '.$chantier->getDatefin()->format('d-m-Y'));
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $username);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $horaire);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $charge);
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $societe);

								$totreleveT=0;
								$totinfructT=0;
								$tottotalT=0;
								$secteur0='';
								$arraycloture=array();
					for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){

							if (isset($donnees[$idCh][$date->format('Y-m-d')][$user_id])){
								$totreleve=0;
								$totinfruct=0;
								$tottotal=0;
								$alerte=0;
								$cloture=0;
								$concatSecteur="";
								foreach($donnees[$idCh][$date->format('Y-m-d')][$user_id] as $enreg){
									$secteur=$enreg->getTitre();
									$secteur=str_replace('Tournée manuelle ','',$secteur );
									$totreleve=	$totreleve+$enreg->getReleve();
									$totinfruct=$enreg->getInfructueux();
									$tottotal=$enreg->getTotal();
									$ref=$enreg->getTitre();
									if(($enreg->getCloture() == 1)&&($secteur != '')){$cloture=1;$arraycloture[$secteur]=1;}
									if(($totreleve != 0) && ($secteur0 != '') && ($secteur != '') && ($secteur0 != $secteur) && (!isset($arraycloture[$secteur0]))){$alerte=$secteur0;}
									$concatSecteur=$secteur.'('.$secteur0.')-';
									if($secteur != ''){$secteur0=$secteur;}
								}


									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $concatSecteur);
									if($cloture == 1){
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('008000');
									}
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, $alerte);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
									if($alerte != 0){
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF0000');
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
									$alerte=0;
									}

							}
							else{
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, '');
									$k++;
									$sheet->setCellValueByColumnAndRow($k, $q, '');
									$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);

							}


					}//fin dates

				}
			}

		}
			$q++;
		}//fin count

foreach(range('A','BQ') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

		return $spreadsheet;
	}

	public function exportChantiersEnCours ($feuille, $spreadsheet, $defauts, $commentaire, $cumulRel,$cumulInf, $causersite, $casite, $cadaysite, $cadaytotal, $catotal, $enregs, $totday, $totdaySite, $donnees, $users, $date1,$date2,$chantiers,  $chantiersencours, $totaleffect,  $mail = null){

		$em = $this->getDoctrine()->getManager();
		$sheet = $spreadsheet->getActiveSheet();

		$startDate = $date1;
		$stopDate = $date2;
		$datenow=new \DateTime('now - 12 hours');
		//$sheet->setCellValueByColumnAndRow(1, 2, $date1->format('d-m-Y'));
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);

        $borderDateOn = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF3469F7'),
        ),
    ),
);
		//$phpExcel->getActiveSheet()->getCell('A1')->setValue('Some text');
		//$phpExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
		//$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setColor( $phpColor );
		//$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('FF0000');
//$objPHPExcel->getActiveSheet()->duplicateStyle($objPHPExcel->getActiveSheet()->getStyle('B2'), 'B3:B7');
		$sheet = $spreadsheet->getSheet(0);
		$q=1;
		$k=1;
		$sheet->setCellValue('A'.$q, 'CHANTIERS EN COURS');
		$sheet->getStyleByColumnAndRow( 1, 1)->applyFromArray($styleArray);//->applyFromArray($styleArray);
		$q++;
		$sheet->setCellValue('A'.$q, 'Site');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('B'.$q, 'Client');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('C'.$q, 'Responsable');
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('D'.$q, 'Date début');$k++;
		$sheet->setCellValue('E'.$q, 'Date fin');$k++;
		$sheet->setCellValue('F'.$q, 'Prévu');$k++;
		$sheet->setCellValue('G'.$q, 'Réalisé');$k++;
		$sheet->setCellValue('H'.$q, '% Réal.');$k++;
		$sheet->setCellValue('I'.$q, 'Jours');$k++;
		$sheet->setCellValue('J'.$q, 'Delta Compteur');$k++;
		$sheet->setCellValue('K'.$q, 'Releves / jour');$k++;
		$sheet->setCellValue('L'.$q, 'Nb agents');$k++;
		$sheet->setCellValue('M'.$q, 'Cadence A');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('N'.$q, 'Cadence A-1');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('O'.$q, 'Cadence OK');
										$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
										$k++;
		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);
		$sheet->getColumnDimension('G')->setAutoSize(true);
		$sheet->getColumnDimension('H')->setAutoSize(true);
		$sheet->getColumnDimension('I')->setAutoSize(true);
		$sheet->getColumnDimension('J')->setAutoSize(true);
		$sheet->getColumnDimension('K')->setAutoSize(true);
		$sheet->getColumnDimension('L')->setAutoSize(true);
		$sheet->getColumnDimension('M')->setAutoSize(true);
		$sheet->getColumnDimension('N')->setAutoSize(true);
		$sheet->getColumnDimension('O')->setAutoSize(true);
		$sheet->getColumnDimension('P')->setAutoSize(true);
		$q++;
		foreach ($chantiersencours as $chantier){
			$cadence=$cadence_ok=$delta='';
			$totalr=$chantier->getTotalreleve();
			$totaleffectp=$totaleffect[$chantier->getId()];
			if(isset($totalr)&&($totalr > 0)){
			$pourcent_effect=(round(($totaleffectp/$totalr)*10000)/100);
			}
			else{$pourcent_effect='';}
		$avancement=$nbdays=$curdays=0;$pourcent_days='';
		$Datedebut=strtotime($chantier->getDatedebut()->format('Y-m-d'));
		$Datefin=strtotime($chantier->getDatefin()->format('Y-m-d'));
		$Datecur=strtotime('today');
		$nbdays=$this->fonctions->get_nb_open_days($Datedebut, $Datefin);
		$curdays=$this->fonctions->get_nb_open_days($Datedebut, $Datecur);
		if($Datedebut >= $Datecur){$curdays=1;}
		if($curdays == 0){$curdays = 1;}
		//echo 'chantier'.$chantier->getId().':'.$nbdays."<br>\n";
		if($nbdays ==0){$nbdays = 1;}
		$cadence = round(($totaleffect[$chantier->getId()]/$curdays));
		$cadence_ok= round(($chantier->getTotalreleve()/$nbdays));
		if($pourcent_effect != ''){
		$pourcent_days=(round(($curdays/$nbdays)*10000)/100);
		if($pourcent_days > $pourcent_effect){$avancement = 2;}else{$avancement=1;}
		}
		$TR=$chantier->getCadencehomme();
		if($TR==0){$TR='';}
		if($chantier->getTotalreleve() >0){$delta=$totaleffect[$chantier->getId()]-($curdays*$cadence_ok);}
		$k=1;
		$sheet->setCellValue('A'.$q, $chantier->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('B'.$q, $chantier->getClient()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('C'.$q, $chantier->getZone()->getResponsable()->getPrenom().' '.$chantier->getZone()->getResponsable()->getNom());
										$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
										$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
		$k++;
		$sheet->setCellValue('D'.$q, $chantier->getDatedebut()->format('d-m-Y'));$k++;
		$sheet->setCellValue('E'.$q, $chantier->getDatefin()->format('d-m-Y'));$k++;
		$sheet->setCellValue('F'.$q, $chantier->getTotalreleve());$k++;
		$sheet->setCellValue('G'.$q, $totaleffect[$chantier->getId()]);$k++;
		$sheet->setCellValue('H'.$q, $pourcent_effect);$k++;
		$sheet->setCellValue('I'.$q, $curdays.'/'.$nbdays);$k++;
		$sheet->setCellValue('J'.$q, $delta);$k++;
		$sheet->setCellValue('K'.$q, $cadence);	$k++;
		$sheet->setCellValue('L'.$q, $chantier->getReleveur());$k++;
		if($chantier->getReleveur() > 0){$cadence_j=$cadence/$chantier->getReleveur();}else{$cadence_j='';}
		if($chantier->getReleveur() > 0){$cadence_ok2=$cadence_ok/$chantier->getReleveur();}else{$cadence_ok2='';}
		$sheet->setCellValue('M'.$q, $cadence_j);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('N'.$q, $TR);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		$sheet->setCellValue('O'.$q, $cadence_ok2);
						$sheet->getStyleByColumnAndRow($k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
						$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF000000');
		$k++;
		if($avancement == 2){
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3FF0000');
		}
		if($avancement == 1){
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 10, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3008000');
		}
		$q++;
		}
		return $spreadsheet;
	}

	function rapport_annuel(){
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
		//$spreadsheet = $reader->load("/var/www/vhosts/releve-eae.com/Rapport_annuel_matrice2020_46.xlsx");
		$feuille='';
		for($sem=1;$sem<=53;$sem++){
			$dt1='+'.($sem-1).' weeks Monday Jan 2021';
			$dt2='+'.($sem).' weeks Saturday Jan 2021';
			$date1 = new \DateTime(date('Y-m-d',strtotime($dt1)));
			$date2 = new \DateTime(date('Y-m-d',strtotime($dt2)));
			$feuille='Sem_'.$sem;

			$spreadsheet=$this->calcul_export_2($feuille,$spreadsheet, 'rapport', null, null, $date1, $date2, null, null);

		}
		$current_week=date('W');
		$index=53-$current_week;
		$spreadsheet->setActiveSheetIndex($index);
		// $spreadsheet->setActiveSheetIndex(34);
        $writer = new Xlsx($spreadsheet);
        $fileName = 'Rapport_annuel_'.date('d-m-Y').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

		$date1 = new \DateTime('Monday this week');
		$date2 = new \DateTime('Saturday this week');
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
		$feuille='Avancement';
		$spreadsheet=$this->calcul_export_2($feuille,$spreadsheet, 'planning', null, null, $date1, $date2, null, null);
		$writer = new Xlsx($spreadsheet);
        $fileName2 = 'Avancement_'.date('d-m-Y').'.xlsx';
        $temp_file2 = tempnam(sys_get_temp_dir(), $fileName2);
        $writer->save($temp_file2);

			$em = $this->getDoctrine()->getManager();
			$message_obj = $em->getRepository('App:Messagecron')->find(1);
			$datenow=new \DateTime('now');
			$sujet='suivi-eae.com '.$datenow->format('d-m-Y');
			$mess='Bonjour,<br><br/>
			<br/>'.$message_obj->getMessage().'<br>Pour plus d\'infos, se connecter à son interface : <a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a>
			<br/>Bonne journée à tous';
			$this->sendConfirmationEmailMessage(null, $sujet, $mess, $temp_file, $fileName, $temp_file2, $fileName2);
			return new Response('annuel');
	}


//recherhce de tous les operateurs sans affectation
	public function OperateursSansChantiers()
    {
		$em = $this->getDoctrine()->getManager();
		//Rechercher tous les utilisateurs n'ayant pas declaré le jour même
		//Rechercher tous les utilisateurs qui auraient du declarer
		$k=0;
		$date1 = new \DateTime('now');
		$dateur[0]=$date1->format('Y-m-d');
		$date3=$date1;
		While(true){
			$k++;
			$date4=$date3->add(new \DateInterval('P1D'));
		 	$dateur[$k]=$date4->format('Y-m-d');
			if ($k==6) {
        break;
    	}
		}

		$q=0;
		$zones=$em->getRepository('App:Zone')->getZoneCA();
		foreach($zones as $zone){
						echo'<strong>'.$zone->getNom().'</strong><br/>';
						$tmp_uservalides=array();

						$usersValides=$em->getRepository('App:User')->getUsersValides($zone);
						foreach($usersValides as $user){
								$tmp_uservalides[]=$user->getId();
						}
						foreach($dateur as $date2){
							echo '<strong>'.$date2.'</strong><br/>';
							$tmp_uservalideschantier=array();
								$chantierusers=$em->getRepository('App:ChantierUser')->getUsersValideChantierDate2($date2);
								echo 'Planifiés : '.count($chantierusers).'<br/>';
								echo '<strong>Non planifiés actifs : </strong><br/>';
								//$usersAV=array_diff($usersValides,$users);
									foreach($chantierusers as $chantieruser){
											$tmp_uservalideschantier[]=$chantieruser->getUser()->getId();
											//echo $chantieruser->getUser()->getId().'<br/>';
									}
									foreach($tmp_uservalides as $user){
											if(!in_array($user,$tmp_uservalideschantier)){
												$user2=$em->getRepository('App:User')->find($user);
												echo $user2->getPrenom().' '.$user2->getNom().'('.$user.')<br/>';
											}
									}
						}
		}
	return new Response(2);
	}

	private function is_ferie($date){
		$date=strtotime($date->format('Y-m-d'));
		$open=false;
		$arr_bank_holidays = array(); // Tableau des jours feriés
		$year = (int)date('Y', $date);
		// Liste des jours feriés
		$arr_bank_holidays[] = '1_1_'.$year; // Jour de l'an
		$arr_bank_holidays[] = '1_5_'.$year; // Fete du travail
		$arr_bank_holidays[] = '8_5_'.$year; // Victoire 1945
		$arr_bank_holidays[] = '14_7_'.$year; // Fete nationale
		$arr_bank_holidays[] = '15_8_'.$year; // Assomption
		$arr_bank_holidays[] = '1_11_'.$year; // Toussaint
		$arr_bank_holidays[] = '11_11_'.$year; // Armistice 1918
		$arr_bank_holidays[] = '25_12_'.$year; // Noel

		// Récupération de paques. Permet ensuite d'obtenir le jour de l'ascension et celui de la pentecote
		$easter = easter_date($year);
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + 86400); // Paques
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*39)); // Ascension
		$arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*50)); // Pentecote
		if (!in_array(date('w', $date), array(0, 6))
		&& !in_array(date('j_n_'.date('Y', $date), $date), $arr_bank_holidays)) {
			$open=true;
		}
		return $open;

	}
	function cmp($a, $b) {
    if ($a->getNom() == $b->getNom()) {
        return 0;
    }
    return ($a->getNom() < $b->getNom()) ? -1 : 1;
	}


	function compareTabAndOrder($a, $b) {
    // compare the tab option value
    $diff = $a->getZone()->getId() - $b->getZone()->getId();
    // and return it. Unless it's zero, then compare order, instead.
    if($diff == 0)
    	{if ($a->getNom() == $b->getNom()) {
        return 0;
    	}
    	return ($a->getNom() < $b->getNom()) ? -1 : 1;
    }
    return 0;
	}

	public function getCommentaires($date1, $date2) {
		$em = $this->getDoctrine()->getManager();
 		$commentaire= $indications = $nbjours = array();
		$notes=$em->getRepository('App:Evenement')->findEvenementsNotes($date1, $date2);
		//echo '<br>date'.$date2->format('Y-m-d').'<br>';
		foreach($notes as $note){
			//echo 'ID'.$note->getId().'-'.$note->getCommentaire().'<br>';
			$user_id=$note->getUser()->getId();
			$datedebut=$note->getDatedebut();
			$datefin=$note->getDatefin();
			if($datefin == null){$datefin = $datedebut;}
			//else{$datefin->modify('-1 day');}
			$com=$note->getCommentaire();
			$type=$note->getType();
			if(($type<3)||($type>6)){
				for($date=clone $datedebut; $date<=$datefin; $date->modify('+1 day')){
							$indications[$user_id][$date->format('Y-m-d')][$type]=1;
							//echo 'bd'.$type.' '.$date->format('Y-m-d').' '.$user_id."<br>\n";
				}
			}

		$delta = $this->fonctions->get_nb_open_days2(max(strtotime($date1->format('Y-m-d')),strtotime($datedebut->format('Y-m-d'))),
		min(strtotime($date2->format('Y-m-d')),strtotime($datefin->format('Y-m-d'))));
		//on retire un jour si date reprise dans la semaine
		if(strtotime($datefin->format('Y-m-d')) < strtotime($date2->format('Y-m-d'))){$delta--;}
		//$delta = $this->fonctions->get_nb_open_days(strtotime($date1->format('Y-m-d')),strtotime($datefin->format('Y-m-d')));

			if($type==1){
				$commentaire[$user_id]=($commentaire[$user_id]?? '') . 'AM : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			$nbjours[$user_id][1]= ($nbjours[$user_id][1] ?? 0) + $delta;
			}
			if($type==2){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'CP : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			$nbjours[$user_id][2]=($nbjours[$user_id][2] ?? 0) + $delta;
			}
			if($type==3){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'Accident : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
			}
			if($type==4){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'Reclamation : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
			}
			if($type==5){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'PB matériel : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
			}
			if($type==6){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'Autre : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			}
			if($type==7){
				$commentaire[$user_id]=($commentaire[$user_id]?? '') . 'AT : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			$nbjours[$user_id][7]=($nbjours[$user_id][7] ?? 0) + $delta;
			}
			if($type==8){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'Abs : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			$nbjours[$user_id][8]=($nbjours[$user_id][8] ?? 0) + $delta;
			}
			if($type==9){
			$commentaire[$user_id]=($commentaire[$user_id]?? '').'Comm : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
			}
		}
		return array($commentaire,$nbjours,$indications);
	}

	function calcul_graph(){

				$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$colorArray = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'EB2B02'],
				)
			);

			$date1 = new \DateTime(date('Y-m-d',strtotime('first day of january last year')));
			$date2 = new \DateTime(date('Y-m-d',strtotime('last day of december last year')));
						$enregs=array();
			$em = $this->getDoctrine()->getManager();
			$enregistrementCriteres=null; $zone=null;
		  $enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,1);

   foreach ($enregs as $enreg){
			$date_rel=$enreg->getDatereleve()->format('Y-m-d');
			$user=$enreg->getUser();
			$user_id=$enreg->getUser()->getId();
						//decompte des jours homme si enreg valide
			if($enreg->getValide() == 1){
					$counterT[$date_rel][$user_id]=$enreg;
			}

		}
		//calcul de stotaux jours hommes
		for($date=clone $date1; $date<=$date2; $date->modify('+1 day')){
				if(isset($counterT[$date->format('Y-m-d')])){
					foreach($counterT[$date->format('Y-m-d')] as $enreg){
						$client = $enreg->getChantier()->getClient()->getCle();
						$counter[$date->format('Y-m-d')][$client]=($counter[$date->format('Y-m-d')][$client]??0)+1;
						$counter[$date->format('Y-m-d')]['tot']=($counter[$date->format('Y-m-d')]['tot']??0)+1;
						$reponsable = $enreg->getChantier()->getZone()->getResponsable()->getNom();
						$counter[$date->format('Y-m-d')][$reponsable]=($counter[$date->format('Y-m-d')][$reponsable]??0)+1;
					}
				}
		}
		$feuille='export_agent';
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$spreadsheet->getProperties()->setCreator("SBEAE")->setLastModifiedBy("SBEAE")->setCategory("");
		//$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $feuille);
		//$spreadsheet->addSheet($myWorkSheet, 0);
		$sheet = $spreadsheet->getActiveSheet();
    $k=1;
    $q=2;
    $cran=1;
    $color='FFb1b0b3';
    $sheet->setCellValueByColumnAndRow( $k, $q,'TOTAL AGENTS');
    													$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
															$sheet->getCellByColumnAndRow( 5, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
															$sheet->getStyleByColumnAndRow( 5, $q)->applyFromArray($styleArray);
    $sheet->setCellValueByColumnAndRow( $k, $q+1,'AGENTS VEOLIA');
    $sheet->setCellValueByColumnAndRow( $k, $q+2,'AGENTS SUEZ');
    $sheet->setCellValueByColumnAndRow( $k, $q+3,'AGENTS AUTRES');
    $sheet->setCellValueByColumnAndRow( $k, $q+5,'AGENTS AG');
    $sheet->setCellValueByColumnAndRow( $k, $q+6,'AGENTS AT');
    $sheet->setCellValueByColumnAndRow( $k, $q+7,'AGENTS JBC');
    $k++;
		for($date=clone $date1; $date<=$date2; $date->modify('+1 day')){
			if(($date->format('w')<6)&&($date->format('w')>0)){
				$sheet->setCellValueByColumnAndRow( $k, $q-1,$date->format('d-m-Y').'-'.$date->format('w'));
				if (isset($counter[$date->format('Y-m-d')]['tot'])){$tot = $counter[$date->format('Y-m-d')]['tot'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q,$tot);
				if (isset($counter[$date->format('Y-m-d')]['VEOLIA'])){$tot = $counter[$date->format('Y-m-d')]['VEOLIA'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+1,$tot);

				if (isset($counter[$date->format('Y-m-d')]['SUEZ'])){$tot = $counter[$date->format('Y-m-d')]['SUEZ'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+2,$tot);

				if (isset($counter[$date->format('Y-m-d')]['AUTRES'])){$tot = $counter[$date->format('Y-m-d')]['AUTRES'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+3,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Grolleau'])){$tot = $counter[$date->format('Y-m-d')]['Grolleau'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+5,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Tanguy'])){$tot = $counter[$date->format('Y-m-d')]['Tanguy'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+6,$tot);

				if (isset($counter[$date->format('Y-m-d')]['Cros'])){$tot = $counter[$date->format('Y-m-d')]['Cros'];}else {$tot = 0;}
				$sheet->setCellValueByColumnAndRow( $k, $q+7,$tot);
				$k=$k + 1;
			}
		}

				$spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $fileName = 'export-agents.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);

	}

public function testMail() {
$em = $this->getDoctrine()->getManager();
$chantier1 = $em->getRepository('App:Chantier')->find(12);

$sujet='SBEAE '.$chantier1->getClient()->getNom();
$mess='Bonjour,<br/><br/>
Nous vous prions de trouver ci joint le rapport et le graphique d\'activité<br>
Pour plus d\'informations, nous vous invitons à vous connecter à votre interface sur
<a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a><br><br>Cordialement,<br><br>
L\'équipe EAE<br><img src=\''.$chantier1->getSociete()->getLogo().'\' width="150">';

$temp_file= $fileName= $temp_file2= $fileName2= '';
$emails[]='j.b@hebline.com';$emails[]='f.m@hebline.com';
$this->sendConfirmationEmailMessage($emails, $sujet, $mess, $temp_file, $fileName, $temp_file2, $fileName2);
return new Response('ExportClient');
		}
}

