<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

use App\Service\FonctionsService;

use App\Entity\Enregistrement;
use App\Entity\Chantier;

use App\Form\AdminStatsClotureType;
use App\Form\AdminStatsOperateursType;
use App\Form\SuperAdminStatsClotureType;
use App\Form\HeblineStatsRenouType;
use App\Form\ActiviteJourType;

// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class StatsRenouController extends AbstractController
{

  public function __construct(FonctionsService $fonctions, \Swift_Mailer $mailer, ValidatorInterface $validator)
    {
        $this->titre = 'Statistiques Prestations';
        $this->fonctions = $fonctions;
        $this->mailer = $mailer;
        $this->validator = $validator;
    }

function rapport_annuel_renou(){
    $em = $this->getDoctrine()->getManager();
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
$spreadsheet->getActiveSheet()->getDefaultRowDimension()->setRowHeight(100);
    //selection de tous les chantiers renou
    //provisoirement une chantier
/*
    $chantier = $em->getRepository('App:Chantier')->find(129);
*/
    if(isset($chantier)){$chantiersencours[]=$chantier;}
    else{
      $date1 = new \DateTime('first day of january');
      $date2 = new \DateTime('last day of december');
      $chantiersencours = $em->getRepository('App:Chantier')->findChantiersEnCours(null, $date1, $date2,2);
    }

    //$chantiersencours[]=$em->getRepository('App:Chantier')->find(129);
    //$chantiersencours[]=$em->getRepository('App:Chantier')->find(153);

     //recup des arrets
      $date1 = new \DateTime('first day of january');
      $date2 = new \DateTime('last day of december');
      $indications=array();
      $notes=$em->getRepository('App:Evenement')->findEvenementsNotes($date1, $date2);
      //echo 'count'.count($notes).'-';
      foreach($notes as $note){

        $user_id=$note->getUser()->getId();
        //echo '_'.$user_id.'-';
        $datedebut=$note->getDatedebut();
        $datefin=$note->getDatefin();
        $datefin->modify('-1 day');
        $type=$note->getType();
        if(($type<3)||($type>6)){
          for($date=clone $datedebut; $date<=$datefin; $date->modify('+1 day')){
            $sem=(int)date('W',strtotime($date->format('Y-m-d')));

                $indications[$user_id][$sem]=$type;
                //echo 'ids'.$user_id.'-'.$type.'-'.$sem.'<br>';
          }
        }
      }

    $q=0;

    foreach($chantiersencours as $chantier){
      $q++;
      //echo $chantier->getId().'<br>';
        $spreadsheet2 = $reader->load("/var/www/vhosts/releve-eae.com/matrice_annuel.xlsx");
        $clonedWorksheet = $spreadsheet2->getSheetByName('Suivi1');
        //$clonedWorksheet = $spreadsheet2->getSheet(0);
        //$clonedWorksheet->setTitle($chantier->getNom());
        $spreadsheet->addExternalSheet($clonedWorksheet);
        $spreadsheet->setActiveSheetIndex($q);
        $spreadsheet->getActiveSheet()->setTitle($chantier->getNom());
        $spreadsheet=$this->calcul_annuel_renou($spreadsheet, $chantier,$q,$indications);
    }

    $spreadsheet->removeSheetByIndex(0);

        $writer = new Xlsx($spreadsheet);
        $fileName = 'Rapport_Renou_annuel_'.date('d-m-Y').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);


      $em = $this->getDoctrine()->getManager();
      $datenow=new \DateTime('now');
      $sujet='Suivi-eae.com Renou '.$datenow->format('d-m-Y');
      $mess='Bonjour,<br><br/>
      <br/>Rapport Renou joint<br>Pour plus d\'infos, se connecter à son interface : <a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a>
      <br/>Bonne journée à tous';
      $this->sendConfirmationEmailMessage(null, $sujet, $mess, $temp_file, $fileName);
      return new Response('annuelRenou');
}

function calcul_annuel_renou($spreadsheet, $chantier, $index,$indications){

      $em = $this->getDoctrine()->getManager();
      //init
      $total=$totalcomp=0;
      $nb_p=7;//nombre prestation;
      $enregs = $detailRenous=$matfacture= $totuser=$totuserm=$totm=$totmatuser=$totuserps=$totuseru=$totuserp=$totps=$tots=$totp=$totmatps=$totmatp=$totm_comp=$prestation_materiel=$prestations=$users=$jours_travail= array();
      $indicateur_p=$indicateur_pm=$facturation=array();
      $enregistrementCriteres = new Enregistrement();
      $enregistrementCriteres->setChantier($chantier);

    for($sem=1;$sem<=52;$sem++){

        $jours_travail_user = array();

        $annee=date('Y');
        $date1 = new \DateTime();
        $date1->setISOdate($annee, $sem);
        date_modify($date1, '-1 day');
        $date2 = new \DateTime();
        $date2->setISOdate($annee, $sem);
        date_modify($date2, '+6 day');
        //echo 'date'.$date1->format('Y-m-d h:m').':'.$date2->format('Y-m-d h:m').'<br>';
        $enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, '', $date1, $date2,2);
        //echo "-ct".count($enregs);
        $chantiersusersencours=$chantier->getChantierUsers();
        if(count($chantiersusersencours)){
            foreach($chantiersusersencours as $chantiersusersencour){
              $user=$chantiersusersencour->getUser();
              //echo $user->getNom().'-';
                if(!isset($userencours) || ($userencours == $user)){
                $users[$user->getId()]=$user;
                }
            }
        }


        foreach ($enregs as $enreg){

          $user=$enreg->getUser();
          $user_id=$enreg->getUser()->getId();
          //numero de semaine
          $date_r=$enreg->getDatereleve()->format('Y-m-d');
          //recup des jouys travaillés
          $sem=(int)date('W',strtotime($date_r));
          //echo "sem".$sem.'-'.$date_r.' ';
          if($em->getRepository('App:Totalrenou')->findTotalRenousEnreg($enreg) > 0){
           $jours_travail_user[$user_id][$date_r]=1;
           //echo 'travail'.$enreg->getId().':'.$date_r.'<br>';
          }

          $detailRenous=$em->getRepository('App:EnregistrementChantierMateriel')->findDetailRenou($enreg);
          foreach ($detailRenous as $renou) {
            $p=$renou['prestation'];
            $n=$renou['nombre'];
            $m=$renou['materiel'];
            $t=$renou['tarif'];
            //echo "|$p|$n|$m|$t|";
            //recherche si prestation facturée
            if($t > 0){
              $matfacture[$sem]=($matfacture[$sem]??0) + $n;
              $facturation[$m]=1;
            }
            $indicateur_p[$p]=1;
            $indicateur_pm[$p][$m]=1;
            $totmatuser[$user_id][$sem][$p][$m]=$n;
            $totmatps[$sem][$p][$m]=($totmatps[$sem][$p][$m]??0) + $n;
            $totmatp[$p][$m]=($totmatp[$p][$m]??0) + $n;
            $totuser[$user_id][$sem][$m]=($totuser[$user_id][$sem][$m]??0) + $n;
            if($p <= $nb_p){
              $totuserps[$user_id][$sem]=($totuserps[$user_id][$sem]??0) + $n;
              $totuseru[$user_id]=($totuseru[$user_id]??0) + $n;
            }
            $totuserp[$user_id][$p]=($totuserp[$user_id][$p]??0) + $n;//total prestation
            $totuserm[$user_id][$m]=($totuserm[$user_id][$m]??0) + $n;//total materiel
            $totps[$sem][$p]=($totps[$sem][$p]??0) + $n;
            $totm[$m]=($totm[$m]??0) + $n;
            if($p <= $nb_p){
              $tots[$sem]=($tots[$sem]??0) + $n;
              $total=($total??0) + $n;
            }
            else{
              $totm_comp[$sem]=($totm_comp[$sem]??0) + $n;
              $totalcomp=($totalcomp??0) + $n;
            }
            }
        }
        //total_jour
        foreach ($users as $id => $user){

          if(isset($jours_travail_user[$id])){
            if(isset($jours_travail[$sem])){
              $jours_travail[$sem] = $jours_travail[$sem] + count($jours_travail_user[$id]);
            }
            else{
              $jours_travail[$sem] = count($jours_travail_user[$id]);
            }
            //echo 'ID'.$id.'-'.$sem.':'.$jours_travail[$sem].'<br>';
          }
        }
        //recherche de toutes les prestations
        $prestations=$em->getRepository('App:Prestation')->findAll();
        foreach($prestations as $key => $prestation){
            if(!isset($indicateur_p[$prestation->getId()])){unset($prestations[$key]);}
            $materiels=$em->getRepository('App:Materiel')->findByPrestation($prestation);
            foreach($materiels as $materiel){
                if($indicateur_pm[$prestation->getId()][$materiel->getId()]=1){$prestation_materiel[$prestation->getId()][$materiel->getId()]=$materiel;}
            }
        }
    }//fin semaine


   return $this->exportRapportAnnuel ($spreadsheet,$index,$totuser, $totmatuser, $totuserm, $totuserps, $totuseru, $totuserp,$totps,$tots,$totm,$total, $totmatps, $totmatp, $totm_comp,$totalcomp, $prestation_materiel, $prestations, $users, $jours_travail, $indications, $chantier,$matfacture,$facturation);

  //return $spreadsheet;
}

function exportRapportAnnuel ($spreadsheet,$index,$totuser, $totmatuser, $totuserm, $totuserps, $totuseru, $totuserp, $totps,$tots,$totm,$total,$totmatps, $totmatp,  $totm_comp, $totalcomp, $prestation_materiel, $prestations, $users, $jours_travail,$indications, $chantier,$matfacture,$facturation){

  $sheet = $spreadsheet->getSheet($index);

    $borderLight= array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
            ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
         ),
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
         ),
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
         )
        ),
    );
$borderEpais= array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
            ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         ),
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         ),
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         )
        ),
    );
$borderEpaisRight= array(
    'borders' => array(
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         ),
        'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         )
        ),
    );
$borderEpaisLeft= array(
    'borders' => array(
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM
         )
        ),
    );
  $borderRightThin = array(
    'borders' => array(
        'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
         )
        ),
    );
  $borderBottomThick = array(
    'borders' => array(
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
         )
        ),
    );
    $borderBottomThin = array(
    'borders' => array(
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
         )
        ),
    );
  //return $spreadsheet;

  $q=9;
  $sheet->setCellValueByColumnAndRow(2, 5, $chantier->getNom());
  $sheet->setCellValueByColumnAndRow(2, 2, 'PLANNING RT COMPTEURS '.$chantier->getNom().' '.date('Y'));
  $objectif=$chantier->getTotalafaire();
  //recup semaine fin debut chantier
  $datedebut=$chantier->getDatedebut()->format('Y-m-d');
  $datefin=$chantier->getDatefin()->format('Y-m-d');

  $semdeb=(int)date('W',strtotime($datedebut));
  $semfin=(int)date('W',strtotime($datefin));
  if($chantier->getDatefin()->format('Y') > date('Y')){$semfin=52;}
$datedebut2=$chantier->getDatedebut();
$datefin2=$chantier->getDatefin();
$nbj = $datedebut2->diff($datefin2)->format('%a');
  if($objectif>0){
    $sheet->setCellValueByColumnAndRow(57, 7, $objectif);
    $k=2;
    $delta = $nbj/7;
    $nbsem=intval($objectif/$delta);
            for($sem=1;$sem<=52;$sem++){
              $col=$k+2+$sem;
              if(($sem>=$semdeb) && ($sem<= $semfin)){$sheet->setCellValueByColumnAndRow($col, 7, $nbsem);}
            }
  }
  //exit;
  $k=2;
  $nbj=0;
  for($sem=1;$sem<=52;$sem++){
              $col=$k+2+$sem;
              $sheet->setCellValueByColumnAndRow($col, 8, ($jours_travail[$sem]??0));
              $nbj+=($jours_travail[$sem]??0);
  }
  $sheet->setCellValueByColumnAndRow(($col+1), 8, ($nbj??0));
  $sheet->getCellByColumnAndRow(($col+1), 8)->getStyle()->applyFromArray($borderEpaisRight);
$sheet->getStyle('E:BE')->getAlignment()->setHorizontal('center');
        // on ne sort plus par prestation mais par materiel
        $materiel_sortie=array (1,2,6,10,11,13);
        $titre_sortie = array("COMPTEUR Accessible","COMPTEUR Inaccessible","COMPTEUR COAXIAL + MODULE","MODULE  Accessible","MODULE  Inaccessible","ANTENNE");
        $color_sortie = array('ffff00','ffff00','ffff00','00b050','00b050','69aafa','fa82b6');
$total_sem = array();
$total_tout_user = array();
  foreach ($users as $user){
        $user_id=$user->getId();
        $k=2;
        $sheet->setCellValueByColumnAndRow($k, $q, $user->getPrenom()."\r\n".$user->getNom());
        $k++;
        $q0=$q;
        $total_user = array();

        foreach($materiel_sortie as $key => $m){
          $sheet->setCellValueByColumnAndRow($k, $q,  $titre_sortie[$key]);
          $color = $color_sortie[$key];
          $sheet = $this->fill_color($sheet, $color, $k, $q);
          $sheet = $this->fill_color($sheet, $color, $k+1, $q);

          if($key == 0 || $key == 3){
            $sheet->getCellByColumnAndRow($k, $q)->getStyle()->applyFromArray($borderBottomThin);
            $sheet->getCellByColumnAndRow($k+1, $q)->getStyle()->applyFromArray($borderBottomThin);
          }
          else{
            $sheet->getCellByColumnAndRow($k, $q)->getStyle()->applyFromArray($borderBottomThick);
            $sheet->getCellByColumnAndRow($k+1, $q)->getStyle()->applyFromArray($borderBottomThick);
          }
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);

          //$sheet->mergeCells('C'.$k.':D'.$k);
            //Remplissage des semaines
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              if($sem == 1) {$sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisLeft);}
              $sheet->setCellValueByColumnAndRow($col, $q, ($totuser[$user_id][$sem][$m]??0));
              $total_user[$sem]=($total_user[$sem]??0)+($totuser[$user_id][$sem][$m]??0);
              $total_tout_user[$sem][$m]=($total_tout_user[$sem][$m]??0)+($totuser[$user_id][$sem][$m]??0);
              if(($sem >= $semdeb) && ($sem <= $semfin)){$sheet=$this->fill_color($sheet, $color, $col, $q);}
              else{$sheet=$this->fill_color($sheet, 'ffc000', $col, $q);}

              if($key == 0 || $key == 3){$sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThin);}
              else{$sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);}
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderRightThin);
            }
            $col++;
            $sheet->setCellValueByColumnAndRow($col, $q, ($totuserm[$user_id][$m]??0));
            $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpaisRight);
            $q++;

        }//fin prestattino
        $sheet->setCellValueByColumnAndRow($k, $q, 'Toutes prestations');
        $sheet->getCellByColumnAndRow($k, $q)->getStyle()->applyFromArray($borderBottomThick);
        $sheet->getCellByColumnAndRow(($k+1), $q)->getStyle()->applyFromArray($borderBottomThick);
        $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);
        $total = 0;
        for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $sheet->setCellValueByColumnAndRow($col, $q, ($total_user[$sem]??0));
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);
              $total = $total + ($total_user[$sem]??0);
              $total_sem[$sem]=($total_sem[$sem]??0) + ($total_user[$sem]??0);
        }
        $col++;
        $sheet->setCellValueByColumnAndRow($col, $q, $total);
        $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpaisRight);
        $sheet->mergeCells('B'.$q0.':B'.$q);
        for($z=$q0;$z<=$q;$z++){
          $sheet->getCellByColumnAndRow(2, $z)->getStyle()->applyFromArray($borderEpaisLeft);
        }
        $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThick);
        $q++;
  }
  //TOTAUX
  //total prestation
        foreach($materiel_sortie as $key => $m){
          $color = $color_sortie[$key];
          $sheet->setCellValueByColumnAndRow(2, $q, 'TOTAL '.$titre_sortie[$key]);
          $sheet=$this->fill_color($sheet, $color, 2, $q);
          $sheet=$this->fill_color($sheet, $color, 3, $q);
          $sheet=$this->fill_color($sheet, $color, 4, $q);
          $sheet->mergeCells('B'.$q.':D'.$q);
          if($key == 0 || $key == 3){
            $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThin);
            $sheet->getCellByColumnAndRow(3, $q)->getStyle()->applyFromArray($borderBottomThin);
            $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderBottomThin);
          }
          else{
            $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThick);
            $sheet->getCellByColumnAndRow(3, $q)->getStyle()->applyFromArray($borderBottomThick);
            $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderBottomThick);
          }
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);

            //Remplissage des semaines
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $sheet->setCellValueByColumnAndRow($col, $q, ($total_tout_user[$sem][$m]??0));
              if(($sem >= $semdeb) && ($sem <= $semfin)){$sheet=$this->fill_color($sheet, $color, $col, $q);}
              else{$sheet=$this->fill_color($sheet, 'ffc000', $col, $q);}
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderRightThin);
              if($key == 0 || $key == 3){
                $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThin);
              }
              else{
                $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);
              }
              //remplissage selon CPAM
            }
            $col++;
            if(array_key_exists($key, $totm)){
            $totcategp = array_sum($totm);
            }
            $sheet->setCellValueByColumnAndRow($col, $q,($totm[$m]??0));
            $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpaisRight);
          $q++;
        }//
  $sheet->setCellValueByColumnAndRow(59, 7, ($totm[1]??0));
  $sheet->setCellValueByColumnAndRow(60, 7, ($totm[2]??0));
  $sheet->setCellValueByColumnAndRow(61, 7, ($totm[5]??0));
  $sheet->setCellValueByColumnAndRow(59, 14, ($totm[10]??0));
  $sheet->setCellValueByColumnAndRow(60, 14, ($totm[11]??0));

  //total par semaine
  $sheet->setCellValueByColumnAndRow(2, $q, 'TOTAL');
         $color='fa82b6';
          $sheet=$this->fill_color($sheet, $color, 2, $q);
          $sheet=$this->fill_color($sheet, $color, 3, $q);
          $sheet=$this->fill_color($sheet, $color, 4, $q);
          $sheet->mergeCells('B'.$q.':D'.$q);
          $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(3, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);
            //Remplissage des semaines
          $total1=array();
          $totalprestation=0;
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $total1[$sem]=0;
              foreach($materiel_sortie as $key => $m){
              $total1[$sem]=$total1[$sem] + ($total_tout_user[$sem][$m]??0);
              }
              $totalprestation=$totalprestation+($total1[$sem]??0);
              $sheet->setCellValueByColumnAndRow($col, $q, ($total1[$sem]??0));
              $sheet=$this->fill_color($sheet, $color, $col, $q);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderRightThin);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);
            }
            $col++;
            $sheet->setCellValueByColumnAndRow($col, $q, ($totalprestation??0));
            $sheet=$this->fill_color($sheet, $color, $col, $q);
            $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpais);
            $total=($totm[1]??0)+($totm[2]??0)+($totm[5]??0)+($totm[10]??0)+($totm[11]??0);
            $sheet->setCellValueByColumnAndRow(59, 18, ($total??0));
            $q++;
            $sheet->setCellValueByColumnAndRow(59, 24, ($total??0));
            if($objectif>0){
            $sheet->setCellValueByColumnAndRow(60, 24, ($objectif-$total));
            $fait=intval(($total/$objectif)*100);
            $sheet->setCellValueByColumnAndRow(59, 23, $fait.'%');
            $sheet->setCellValueByColumnAndRow(60, 23, (100-$fait).'%');
            }

  //TOTAL autres prestations facturées
  $sheet->setCellValueByColumnAndRow(2, $q, 'TOTAL autres prestations facturées');
         $color='fa82b6';
          $sheet=$this->fill_color($sheet, $color, 2, $q);
          $sheet=$this->fill_color($sheet, $color, 3, $q);
          $sheet=$this->fill_color($sheet, $color, 4, $q);
          $sheet->mergeCells('B'.$q.':D'.$q);
          $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(3, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);

            //Remplissage des semaines
          $totalfac=array();
          $totalprestation=0;
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $totalfac[$sem] = ($matfacture[$sem]??0) - ($total1[$sem]??0);
              $totalprestation = $totalprestation + ($totalfac[$sem]??0);
              $sheet->setCellValueByColumnAndRow($col, $q, ($totalfac[$sem]??0));
              $sheet=$this->fill_color($sheet, $color, $col, $q);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderRightThin);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);
            }
            $col++;
            $sheet->setCellValueByColumnAndRow($col, $q, ($totalprestation??0));
            $sheet=$this->fill_color($sheet, $color, $col, $q);
            $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpais);
            $q++;

  //TOTAL toutes prestations facturées
  $sheet->setCellValueByColumnAndRow(2, $q, 'Toutes prestations facturées');
         $color='fa82b6';
          $sheet=$this->fill_color($sheet, $color, 2, $q);
          $sheet=$this->fill_color($sheet, $color, 3, $q);
          $sheet=$this->fill_color($sheet, $color, 4, $q);
          $sheet->mergeCells('B'.$q.':D'.$q);
          $sheet->getCellByColumnAndRow(2, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(3, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderBottomThick);
          $sheet->getCellByColumnAndRow(4, $q)->getStyle()->applyFromArray($borderEpaisRight);
            //Remplissage des semaines
          $total1=$totalfac=array();
          $totalprestation=0;
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $totalprestation = $totalprestation + ($matfacture[$sem]??0);
              $sheet->setCellValueByColumnAndRow($col, $q, ($matfacture[$sem]??0));
              $sheet=$this->fill_color($sheet, $color, $col, $q);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderRightThin);
              $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderBottomThick);
            }
            $col++;
            $sheet->setCellValueByColumnAndRow($col, $q, ($totalprestation??0));
            $sheet=$this->fill_color($sheet, $color, $col, $q);
            $sheet->getCellByColumnAndRow($col, $q)->getStyle()->applyFromArray($borderEpais);
            $q++;

$q++;
   //ajout des ref factures
          $titres = array('N° de facture','Montant de la facture','Commande','Solde après déduction de la facture');
          $q0=$q;
          foreach ($titres as $titre){
          $sheet->setCellValueByColumnAndRow(2, $q, $titre);
          $sheet->mergeCells('B'.$q.':D'.$q);
            //Remplissage des semaines
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $sheet->getCellByColumnAndRow( $col, $q)->getStyle()->applyFromArray($borderLight);
            }
            $q++;
          }
          $sheet->getStyle("B".$q0.":BD".($q-1))->applyFromArray($borderEpais);
    //prestations complémentaires
          $q++;
          foreach($prestations as $prestation){
          $prestation_id=$prestation->getId();
          if($prestation_id > 0){
            //affichage des materiels dans prestation
          $q++;
          $sheet->setCellValueByColumnAndRow(2, $q, $prestation->getNom());
          //$sheet->mergeCells('B'.$q.':BE'.$q);
          $sheet->getStyle("B".$q.":BE".$q)->applyFromArray($borderEpais);
          $q++;
         $sheet->setCellValueByColumnAndRow(2, $q, 'Semaine');
          $sheet->mergeCells('B'.$q.':D'.$q);
          $sheet->getStyle("B".$q.":D".$q)->applyFromArray($borderEpais);
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $sheet->setCellValueByColumnAndRow($col, $q, $sem);
            }
            $q++;
            $materiels = $prestation_materiel[$prestation_id];
            /**/
            $color1="00b050";
            foreach($materiels as $materiel){
              $materiel_id=$materiel->getId();
              $sheet->setCellValueByColumnAndRow(2, $q, $materiel->getNom());

              if(array_key_exists ( $materiel_id , $facturation )){
                $sheet=$this->fill_color($sheet, $color1, 2, $q);
              }

              $sheet->mergeCells('B'.$q.':D'.$q);
                //Remplissage des semaines
                for($sem=1;$sem<=52;$sem++){
                  $col=$k+1+$sem;
                  $sheet->setCellValueByColumnAndRow($col, $q, ($totmatps[$sem][$prestation_id][$materiel_id]??0));
                  $sheet->getCellByColumnAndRow( $col, $q)->getStyle()->applyFromArray($borderLight);
                }
                $sheet->getStyle("B".$q.":BE".$q)->applyFromArray($borderLight);
                $col++;
                $sheet->setCellValueByColumnAndRow($col, $q, ($totmatp[$prestation_id][$materiel_id]??0));
                $color='fcf305';
              if(array_key_exists ( $materiel_id , $facturation )){
                   $color = $color1;
                }
                $sheet=$this->fill_color($sheet, $color, $col, $q);

                $sheet->getCellByColumnAndRow( $col, $q)->getStyle()->applyFromArray($borderLight);
              $q++;
            }

          }
        }//fin prestattino
        /*
         $sheet->setCellValueByColumnAndRow(2, $q, 'TOTAUX');
          $sheet->mergeCells('B'.$q.':D'.$q);
          $sheet->getStyle("B".$q.":D".$q)->applyFromArray($borderEpais);
            //Remplissage des semaines
            for($sem=1;$sem<=52;$sem++){
              $col=$k+1+$sem;
              $sheet->setCellValueByColumnAndRow($col, $q, ($totm_comp[$sem]??0));
              $sheet->getCellByColumnAndRow( $col, $q)->getStyle()->applyFromArray($borderEpais);
            }
            $col++;
            $sheet->setCellValueByColumnAndRow($col, $q, ($totalcomp??0));
            $sheet->getCellByColumnAndRow( $col, $q)->getStyle()->applyFromArray($borderEpais);
            */
  return $spreadsheet;
}

function rapport_hebdo_renou(){
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
    //$spreadsheet = $reader->load("/var/www/vhosts/releve-eae.com/Rapport_annuel_matrice.xlsx");
    $feuille='';
    for($sem=1;$sem<=52;$sem++){
      $dt1='+'.($sem-2).' weeks Monday Jan 2021';
      $dt2='+'.($sem-1).' weeks Saturday Jan 2021';
      $date1 = new \DateTime(date('Y-m-d',strtotime($dt1)));
      $date2 = new \DateTime(date('Y-m-d',strtotime($dt2)));
      $feuille='Sem_'.$sem;
      $spreadsheet=$this->calcul_export_renou($feuille,$spreadsheet, 'rapport', null, null, $date1, $date2, null, null);
    }
    $current_week=date('W');
    $index=52-$current_week;
    $spreadsheet->setActiveSheetIndex($index);
    // $spreadsheet->setActiveSheetIndex(34);
        $writer = new Xlsx($spreadsheet);
        $fileName = 'Rapport_Renou_hebdo_'.date('d-m-Y').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);


      $em = $this->getDoctrine()->getManager();
      $datenow=new \DateTime('now');
      $sujet='Suivi-eae.com Renou '.$datenow->format('d-m-Y');
      $mess='Bonjour,<br><br/>
      <br/>Rapport Renou joint<br>Pour plus d\'infos, se connecter à son interface : <a href="https://www.suivi-eae.com">https://www.suivi-eae.com</a>
      <br/>Bonne journée à tous';
      $this->sendConfirmationEmailMessage(null, $sujet, $mess, $temp_file, $fileName);
      return new Response('hebdoRenou');
  }

  public function rapportHeblineRenou(Request $request)
    {
    $this->titre = 'Rapport';

    //-------------FORMULAIRE---------------------
    $date1='';
    $date2='';
    $zone = null;
    $enregs=$totaleffect=array();
    $enregistrement = new Enregistrement();
    $form = $this->get('form.factory')->create(HeblineStatsRenouType::class, $enregistrement, array());
    $em = $this->getDoctrine()->getManager();

    $form->handleRequest($request);
    $enregistrementCriteres=$form->getData();
    $date1=$form->get('date1')->getData();
    $date2=$form->get('date2')->getData();
    if ($form->isSubmitted() && $form->isValid()) {

      $zone_id=$form->get('zone')->getData();
      if($zone_id!=null){$zone=$em->getRepository('App:Zone')->find($zone_id);}
      $excel=$form->get('excel')->getData();

    }
    if($date1==""){
      $date1 = new \DateTime('Monday this week');
    }
    if($date2==""){
      $date2 = new \DateTime('Saturday this week');
    }


    if(isset($excel)){$action = 'excel';}else {$action='frontadmin';}
    $feuille='export';
    return $this->calcul_export_renou( $feuille,null, $action, $enregistrementCriteres, $zone, $date1, $date2, $form, $this->titre);

  }

public function calcul_export_renou($feuille,$spreadsheet,  $action, $enregistrementCriteres = null, $zone, $date1, $date2, $form = null, $titre = null ) {

    $em = $this->getDoctrine()->getManager();
//echo $date2->format('Y-m-d');
    $cumulRel=$cumulInf=$commentaire=array();
    //$enregistrementCriteres=null;
    $enregs=array();
    //echo $zone;
    $enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsOperateurs($enregistrementCriteres, $zone, $date1, $date2,2);
    //echo 'T'.count($enregs).'|';



    $users=$totaleffect=array();

    if ($enregistrementCriteres != '') {

      if($enregistrementCriteres->getChantier() != null){
      $chantier[]=$enregistrementCriteres->getChantier();
      $userencours=$enregistrementCriteres->getUser();
      }
    }

    if(isset($chantier)){$chantiersencours=$chantier;}
    else{
    $chantiersencours = $em->getRepository('App:Chantier')->findChantiersEnCours($zone, $date1, $date2,2);}
    //echo count($chantiersencours);
    //exit;

    $enregistrementCriteres = new Enregistrement();

    foreach($chantiersencours as $chantier){
      $idch=$chantier->getId();

      $chantiersusersencours=$chantier->getChantierUsers();
      if(count($chantiersusersencours)){
          foreach($chantiersusersencours as $chantiersusersencour){
            $user=$chantiersusersencour->getUser();
            //echo $user->getNom().'-';
              if(!isset($userencours) || ($userencours == $user)){
              //foreach($usersencours as $user){
              $user_id=$user->getId();
              $users[$idch][$user_id]=$user;
              $commentaire[$user_id]='';
              $commentaires = $em->getRepository('App:Commentaire')->findCommentairesEnCours($user, $date1, $date2);
                foreach($commentaires as $comm){
                  $commentaire[$user_id].='Du '.$comm->getDatedebut()->format('d-m-Y').' au '.$comm->getDatefin()->format('d-m-Y').' : '.$comm->getCommentaire().' | ';
                  //echo $commentaire[$user_id].'-';
                }
              }
            //echo $idch.'-'.$user_id.'___';
          //}
          }
      }
    }
    if(isset($chantier)){$enregistrementCriteres->setChantier($chantier);}


    $catotal=0;
    $donnees=$chantiers=$totjourchantieruser=$defauts=$totaljourchantieruser=$totdayuser=$totuser=$paniers=$prestation_materiel=$prestations=$indicateur_p=$indicateur_pm=$cadayusersite=$indicateur_p=$causersite=$casite=$cadaysite=$totuserdaym=$totm=$totuserm=$totuserp=$totuserdayp=Array();
    foreach ($enregs as $enreg){
      $date_rel=$enreg->getDatereleve()->format('Y-m-d');
      $user=$enreg->getUser();
      $user_id=$enreg->getUser()->getId();
      $chantier_id=$enreg->getChantier()->getId();
      $totaljourchantieruser[$chantier_id][$user_id][$date_rel]=1;
      $donnees[$chantier_id][$date_rel][$user_id][]=$enreg;
      $chantiers[$chantier_id]=$enreg->getChantier();

      $totreleve=($enreg->getReleve()??0);
      $totinfructueux=($enreg->getInfructueux()??0);
      if($enreg->getUser()->getNom() !=''){
      $users[$enreg->getChantier()->getId()][$user_id]=$enreg->getUser();
      }

      //echo $ca.'-';
      if($enreg->getDefaut() == 1){$defauts[$date_rel][$user_id][$chantier_id]=1;}

//recup des données jour
      $renous=$em->getRepository('App:Totalrenou')->findTotalRenousEnreg($enreg);
      foreach($renous as $renou){
            $totaux[$enreg->getId()][$renou['prestation']]=$renou['total'];
      }
      $tot=$em->getRepository('App:EnregistrementChantierMateriel')->findTotalCA($enreg);
      //CA

      $cadayusersite[$user_id][$date_rel][$chantier_id]=($cadayusersite[$user_id][$date_rel][$chantier_id]??0)+$tot['total'];
      $causersite[$user_id][$chantier_id]=($causersite[$user_id][$chantier_id]??0)+$tot['total'];
      $casite[$chantier_id]=($casite[$chantier_id]??0)+$tot['total'];
      $catotal=($catotal??0)+$tot['total'];

      $detailRenous=$em->getRepository('App:EnregistrementChantierMateriel')->findDetailRenou($enreg);
      foreach ($detailRenous as $renou) {
        $p=$renou['prestation'];
        $n=$renou['nombre'];
        $m=$renou['materiel'];
        $totdayuser[$chantier_id][$user_id][$date_rel]=($totdayuser[$chantier_id][$user_id][$date_rel]??0)+$n;
        $indicateur_p[$p]=1;
        $indicateur_pm[$p][$m]=1;
        $totuser[$user_id][$chantier_id][$p][$m]=($totuser[$user_id][$chantier_id][$p][$m]??0)+$n;
        $totuserdayp[$user_id][$chantier_id][$p][$date_rel]=($totuserdayp[$user_id][$chantier_id][$p][$date_rel]??0) + $n;//total materiel
        $totuserdaym[$user_id][$chantier_id][$m][$date_rel]=($totuserdaym[$user_id][$chantier_id][$m][$date_rel]??0) + $n;//total materiel
        $totuserm[$user_id][$chantier_id][$m]=($totuserm[$user_id][$chantier_id][$m]??0) + $n;//total materiel
        $totm[$m][$chantier_id]=($totm[$m][$chantier_id]??0) + $n;
        $totuserp[$user_id][$chantier_id][$p]=($totuserp[$user_id][$chantier_id][$p]??0) + $n;
      }
    }
    //recherche de toutes les prestations
    $prestations=$em->getRepository('App:Prestation')->findAll();
    foreach($prestations as $key => $prestation){
        if(!isset($indicateur_p[$prestation->getId()])){unset($prestations[$key]);}
        $materiels=$em->getRepository('App:Materiel')->findByPrestation($prestation);
        foreach($materiels as $materiel){
            if($indicateur_pm[$prestation->getId()][$materiel->getId()]=1){$prestation_materiel[$prestation->getId()][]=$materiel;}
        }
    }

    /* tri des chantiers par nom */
    //usort($chantiers, array($this, "cmp"));
    foreach($chantiers as $chantier){
      foreach($chantier->getChantierusers() as $chantieruser){
        if(!isset($userencours) || ($userencours == $chantieruser)){
          $users[$chantieruser->getChantier()->getId()][$chantieruser->getUser()->getId()]=$chantieruser->getUser();
        }
      }
      foreach($users[$chantier->getId()] as $user){
        $chantiersusers=$em->getRepository('App:ChantierUser')->findByChantierandUser($chantier, $user);
        if(count($chantiersusers)>0){
        $paniers[$chantier->getId()][$user->getId()]=$chantiersusers[0]->getPanier();}
      }
    }

//exit;
    $alertes_array=$em->getRepository('App:Alerte')->findAlertes($date1,$date2);
    $alertes=array();
    if(is_array($alertes_array)){
      foreach ($alertes_array as $alerte){
        $user_alerte_id=$alerte->getUser()->getId();
        $user_alerte_date=$alerte->getDatealerte()->format('Y-m-d');
        $alertes[$user_alerte_id][$user_alerte_date]=1;
      }
    }
    //echo$totaljourchantieruser
    if($action=='rapport'){
          return $this->exportRapport_mail ($feuille,$spreadsheet,$defauts,$commentaire,$totdayuser, $totuser, $prestation_materiel,$prestations,$catotal, $cadayusersite,$causersite, $casite, $users, $donnees, $date1, $date2,$chantiers,  $chantiersencours,  $alertes, $totuserdaym, $totm,$totuserm,$totuserdayp, $totuserp);
    }

    if($action=='excel'){
        $nom='export_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
        $spreadsheet=$this->exportRapport_mail ($feuille,$spreadsheet,$defauts,$commentaire,$totdayuser, $totuser, $prestation_materiel,$prestations,$catotal, $cadayusersite,$causersite, $casite, $users, $donnees, $date1, $date2,$chantiers,  $chantiersencours,  $alertes, $totuserdaym, $totm,$totuserm,$totuserdayp, $totuserp);

        $spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $fileName = $nom.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    if($action=='controle'){
        $nom='controle_'.$date1->format('d-m-Y').'_'.$date2->format('d-m-Y');

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
        $spreadsheet=$this->exportRapport_hebline($feuille,$spreadsheet,$defauts,$commentaire,$cumulRel,$cumulInf,$causersite, $casite, $cadaysite, $cadaytotal, $catotal,$enregs, $totday, $totdaySite, $donnees, $users, $date1, $date2,$chantiers,  $chantiersencours, $totaleffect, $alertes);

        $spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);
        $fileName = $nom.'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    if($action=='frontadmin'){

    $zone = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone);

        return $this->render('Generique/Stats/Rapport/indexRenou.html.twig', array(
            'repo' => $repo,
            'action' => $action,
            'titre' => $this->titre,
            'form'      => $form->createView(),
            'date1' => $date1,
            'date2' => $date2,
        ));
    }
  }

public function exportRapport_mail ($feuille,$spreadsheet,$defauts,$commentaire,$totdayuser, $totuser, $prestation_materiel,$prestations, $catotal, $cadayusersite,$causersite, $casite, $users, $donnees, $date1, $date2,$chantiers,  $chantiersencours,  $alertes, $totuserdaym, $totm,$totuserm,$totuserdayp, $totuserp){

    $em = $this->getDoctrine()->getManager();
    $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $feuille);
    $spreadsheet->addSheet($myWorkSheet, 0);
      //$sheet = $spreadsheet->getActiveSheet();
    $startDate = $date1;
    $stopDate = $date2;
    $datenow=new \DateTime('now - 12 hours');

    //STYLES
    $styleArray = array(
      'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => '337ab7'),
        'size'  => 10,
        'name'  => 'Verdana'
      ));
    $styleArray2 = array(
      'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'ffffff'),
        'size'  => 10,
        'name'  => 'Verdana'
      ));
    $styleArray3 = array(
      'font'  => array(
        'bold'  => true,
        'color' => array('rgb' => 'ffffff'),
        'size'  => 10,
        'name'  => 'Verdana'
      ));
    $colorArray = array(
        'fill' => array(
          'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
          'color' => ['argb' => 'EB2B02'],
        )
      );

    $borderDateOn = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF3469F7'),
            ),
        ),
    );
    $borderRight = array(
    'borders' => array(
        'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => array('argb' => 'FF000000'),
            ),
        ),
    );
     $borderalertLeft = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
            ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
         ),
         'left' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
         )
        ),
    );
     $borderalertRight = array(
    'borders' => array(
        'top' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
            ),
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
         ),
         'right' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF4b1476'),
         )
        ),
    );
     $borderBottom = array(
    'borders' => array(
        'bottom' => array(
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => array('argb' => 'FF000000'),
         )
        ),
    );
      $notes = $commentaire = $indications = $nbjours = array();
      $notes = $this->getCommentaires($date1, $date2);
      $commentaire = $notes[0];
      $nbjours = $notes[1];
      $indications = $notes[2];


    $sheet = $spreadsheet->getSheet(0);
    $q=1;
    //echo $date1->format('d-m-Y').':'.count($donnees).'-'."<br>\n";
    if(count($donnees)>0){
    $sheet->setCellValue('A'.$q, 'TABLEAU DE PRESTATIONS '.$feuille);
    $sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
    $q++;
    $sheet->setCellValue('A'.$q, 'Site');
    $sheet->setCellValue('B'.$q, 'Utilisateur');
    $sheet->setCellValue('C'.$q, 'H');
    $sheet->setCellValue('D'.$q, 'CA');
    $sheet->setCellValue('E'.$q, 'Soc');
    $sheet->setCellValue('F'.$q, 'JH');
    $sheet->getColumnDimension('A')->setAutoSize(true);
    $sheet->getColumnDimension('B')->setAutoSize(true);
    $sheet->getColumnDimension('C')->setAutoSize(true);
    $sheet->getColumnDimension('D')->setAutoSize(true);
    $sheet->getColumnDimension('E')->setAutoSize(true);
    $sheet->getColumnDimension('F')->setAutoSize(true);
    $sheet->getColumnDimension('Z')->setAutoSize(true);
    $cran=2;
    $k=7;
    $nbday=0;
    $week_name = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
    $CAtotaljournee=array();
    for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
      $sheet->setCellValueByColumnAndRow( $k, $q,$week_name[date('w', strtotime($date->format('d-m-Y')))].' '.$date->format('d-m-Y'));
      $k=$k + $cran;
      $nbday++;

    }

    $sheet->setCellValueByColumnAndRow($k, $q, 'Total Compteurs');
    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $k++;
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $k++;
    $sheet->setCellValueByColumnAndRow($k, $q, 'Coax + Mod');
    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $k++;
    $sheet->setCellValueByColumnAndRow($k, $q, 'Total Modules');
    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $k++;
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $k++;
    $sheet->setCellValueByColumnAndRow($k, $q, 'Total Infr.');
    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
    $q++;

    $k=6;
    for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Compteurs');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Modules');
    }
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Access.');
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Inaccess.');
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Access.');
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Inaccess.');
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Commentaires');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'CA');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'CA Quotidien');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Panier');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'CP');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'AM');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'AT');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Abs');
      $k++;
      $sheet->setCellValueByColumnAndRow($k, $q, 'Infos');

      //$prestation_materiel,
      $k++;
      foreach($prestations as $prestation){
              $sheet->setCellValueByColumnAndRow($k, $q, $prestation->getNom());
              $materiels=$prestation_materiel[$prestation->getId()];
              foreach ($materiels as $materiel) {
                  $sheet->setCellValueByColumnAndRow($k, $q+1, $materiel->getNom());
                  $k++;
                  $col = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($k);
                  $sheet->getColumnDimension($col)->setAutoSize(TRUE);
              }
      }

    $lastC = 0;
    $CAsitejour=array();
    $CAT=0;

$zone_encours=0;

usort($chantiers, function($a, $b) {
    $c = $a->getZone()->getId() - $b->getZone()->getId();
    if($a->getNom() > $b->getNom()){
    $c .= 1;
    }
    if($a->getNom() < $b->getNom()){
    $c .= -1;
    }
    return $c;
});

    foreach ($chantiers as $chantier){
      $CATS=0;
      $idCh = $chantier->getId();

      if ($lastC > 0 & $lastC != $idCh ){
        $q++;
        $k=7;
        $tots=0;
        $sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE');
        for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
        $k=$k + $cran;
        }
        $sheet->setCellValueByColumnAndRow( $k, $q,$casite[$lastC]);
        $lastC = $idCh;
        for($h=1;$h<=$k+1;$h++){
        $sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
        $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
        //$sheet->getStyleByColumnAndRow( $h, $q);
        }
      }
      if ($lastC == 0) {$lastC = $idCh;}
      for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
      $CAsitejour[$date->format('Y-m-d')]=0;
      }
      $listk=0;
      if(isset($users[$idCh])){
      usort($users[$idCh], array($this, "cmp"));}
      $totchantier=$totprestations=array();
      foreach ($users[$idCh] as $user ){
        $listk++;
        $user_id=$user->getId();
        if ($chantier->getNom() != '' ){
                  $charge=$chantier->getZone()->getAbrev();
                  $societe=$chantier->getSociete()->getNom();
                  $k=0;
                  $username=$user->getNom().' '.$user->getPrenom().' ('.($user->getMobile()??'indéfini').')';
                  $horaire=$user->getHoraire()->getNom();
                  $q++;
                  $k++;
                  if($listk == 1){
                  if($chantier->getZone()->getId() != $zone_encours){
                    $sheet->setCellValueByColumnAndRow($k, $q, $chantier->getZone()->getNom());
                    $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
                    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
                    $sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                    $sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
                    $zone_encours = $chantier->getZone()->getId();
                    $q++;
                  }
                  $sheet->setCellValueByColumnAndRow($k, $q, $chantier->getNom().' '.$chantier->getClient()->getNom().' ('.count($users[$idCh]).' agents) - Du '.$chantier->getDatedebut()->format('d-m').' au '.$chantier->getDatefin()->format('d-m-Y'));
                  }
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $username);
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $horaire);
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $charge);
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $societe);
                  $k++;
                $totreleveT=0;
                $totinfructT=0;
                $tottotalT=0;
                $dayswork=array();
          for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
              $color='D3FFFFFF';

/* cas enregistrement défini */
              if (isset($totdayuser[$idCh][$user_id][$date->format('Y-m-d')])){

                  $dayswork[$user_id][$date->format('Y-m-d')]=1;
                  $defaut=0;
                  if(isset($defauts[$date->format('Y-m-d')][$user_id][$idCh])){
                    if($defauts[$date->format('Y-m-d')][$user_id][$idCh] == 1){
                      $color='D3FF0000';
                      $defaut= 1;
                    }
                  }

                  $info='';
                  if (isset($indications[$user_id][$date->format('Y-m-d')][2]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][2])){
                          $color='D3318CE7';
                          $info="CP";
                          //echo '_'.$user_id.':'.$date->format('Y-m-d');
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][1]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][1])){
                          $color='D3318CE7';
                          $info="AM";
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][3]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][3])){
                        $color='D3318CE7';
                        $info="AT";
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][4]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][4])){
                          $color='D3318CE7';
                          $info="Abs";
                  }
//compteurs+modules

                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $info.' '.($totuserdayp[$user_id][$idCh][1][$date->format('Y-m-d')]??0));
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $info.' '.($totuserdayp[$user_id][$idCh][3][$date->format('Y-m-d')]??0));
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
                  if(isset($alertes[$user->getId()][$date->format('Y-m-d')])){
                    if($alertes[$user->getId()] [$date->format('Y-m-d')]== 1){
                      $color='D3FFA500';
                      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
                    }
                  }
              }
/* cas pas d'enregistrement défini */
              else{

                  $color='';
                  if($user->isEnabled()){$color='D3FFFF00';}else{$color='D3FFFFFF';}
                  if(isset($defauts[$date->format('Y-m-d')][$user_id][$idCh])){
                    if($defauts[$date->format('Y-m-d')][$user_id][$idCh] == 2){
                      $color='D3FFC0AC';
                      $defaut= 1;
                    }
                  }
                    $info='';
                  if (isset($indications[$user_id][$date->format('Y-m-d')][2]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][2])){
                          $color='D3318CE7';
                          $info="CP";
                          //echo '_'.$user_id.':'.$date->format('Y-m-d');
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][1]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][1])){
                          $color='D3318CE7';
                          $info="AM";
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][3]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][3])){
                        $color='D3318CE7';
                        $info="AT";
                  }
                  if (isset($indications[$user_id][$date->format('Y-m-d')][4]) && isset($indications[$user_id][(clone $date)->modify("+1 day")->format('Y-m-d')][4])){
                          $color='D3318CE7';
                          $info="Abs";
                  }
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $info);
                  if($color != ''){
                              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
                  }
                  $k++;

                  $sheet->setCellValueByColumnAndRow($k, $q, '');
                    if($color != ''){
                              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB($color);
                    }
                  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->applyFromArray($borderRight);
              }
          }//fin dates

//totaux
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserp[$user_id][$idCh][1]??0)-($totuserm[$user_id][$idCh][2]??0) );
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserm[$user_id][$idCh][2]??0));
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserp[$user_id][$idCh][2]??0));
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserp[$user_id][$idCh][3]??0)-($totuserm[$user_id][$idCh][11]??0) );
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserm[$user_id][$idCh][11]??0));
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');
$k++;
$sheet->setCellValueByColumnAndRow($k, $q, ($totuserp[$user_id][$idCh][7]??0));
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');

                  if(isset($dayswork[$user_id])){
                  $countdays = array_sum($dayswork[$user_id]);}
                  else{$countdays = 0;}
                  if($countdays  > 0){
                  $CAday=round(( $causersite[$user->getId()][$idCh]??0)/$countdays );
                  }else{$CAday=0;}
                  $k++;
//COMMENTAIRES
                  $sheet->setCellValueByColumnAndRow($k, $q, ($commentaire[$user->getId()]??''));
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, ($causersite[$user->getId()][$idCh])??0);
                        $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
                        $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                        $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
                  $k++;
                  $sheet->setCellValueByColumnAndRow($k, $q, $CAday);
                        $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
                        $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                        $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
                   //$k++;
                  $sheet->setCellValueByColumnAndRow(6, $q, $countdays);
                        $sheet->getStyleByColumnAndRow( 6, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
                        $sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                        $sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');

                  $k++;
                  $chantiersusers=$em->getRepository('App:ChantierUser')->findByChantierandUser($chantier, $user);
                  $panier=0;
                  if(count($chantiersusers)>0){
                  $panier=$chantiersusers[0]->getPanier();
                  $comment=$chantiersusers[0]->getCommentaire();}
                  else{$panier=0;$comment='';}

                  if(($panier>0)&&($countdays>0)){
                    $sheet->setCellValueByColumnAndRow($k, $q, $countdays);
                    }
                  else{
                    $sheet->setCellValueByColumnAndRow($k, $q, 0);
                  }
                    $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
                    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                    $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF3469F7');
                  $k++;
                    $sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][2] ?? 0));
                  $k++;
                    $sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][1] ?? 0));
                  $k++;
                    $sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][7] ?? 0));
                  $k++;
                    $sheet->setCellValueByColumnAndRow($k, $q, ($nbjours[$user_id][8] ?? 0));
                  $k++;
                    $sheet->setCellValueByColumnAndRow($k, $q, $comment);
                    $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
                  //$k++;
                    /*
                    $sheet->setCellValueByColumnAndRow(19, $q, ($commentaire[$user->getId()]??''));
                    //$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray3);//->applyFromArray($styleArray);
                    $sheet->getCellByColumnAndRow( 19, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                    $sheet->getCellByColumnAndRow( 19, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3D0D0D0');*/
                  foreach($prestations as $prestation){
                          $p=$prestation->getId();
                          $materiels=$prestation_materiel[$p];
                          foreach ($materiels as $materiel) {
                              $m=$materiel->getId();
                              //echo $p.'-'.$m.'-'.$user_id;
                              $k++;
                              $sheet->setCellValueByColumnAndRow($k, $q, ($totuser[$user_id][$idCh][$p][$m]??0));
                              $totchantier[$idCh][$p][$m]=($totchantier[$idCh][$p][$m]??0)+($totuser[$user_id][$idCh][$p][$m]??0);
                              $totprestations[$p][$m]=($totprestations[$p][$m]??0)+($totuser[$user_id][$idCh][$p][$m]??0);
                          }
                  }

        }
      }

    }
      $q++;
      $k=6;
      $tots=0;
      $sheet->setCellValueByColumnAndRow( 1, $q,'TOTAL SITE');
      for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
        $k=$k + $cran;
      }
      $sheet->setCellValueByColumnAndRow(  $k+8, $q,($casite[$lastC]??0));
      for($h=1;$h<=$k+10;$h++){
      $sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
      $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
      $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
      //$sheet->getStyleByColumnAndRow( $h, $q);
      }
      $k=$k+15;
      foreach($prestations as $prestation){
                          $p=$prestation->getId();
                          $materiels=$prestation_materiel[$p];
                          foreach ($materiels as $materiel) {
                              $m=$materiel->getId();
                              //echo $p.'-'.$m.'-'.$user_id;
                              $k++;
                              $sheet->setCellValueByColumnAndRow($k, $q, ($totchantier[$idCh][$p][$m]??0));
      $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray);//->applyFromArray($styleArray);
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D398FB98');
                          }
      }
      $q++;
      $k=7;
      $tots=0;
      for($date=clone $startDate; $date<=$stopDate; $date->modify('+1 day')){
        $sheet->setCellValueByColumnAndRow( 1, $q,'TOTAUX');
        /*if (isset($totday[$date->format('Y-m-d')])){$tot = $totday[$date->format('Y-m-d')] ;$tots=$tots+$tot;}else {$tot = 0;}
        $sheet->setCellValueByColumnAndRow( $k, $q,$tot);

        $sheet->setCellValueByColumnAndRow( $k+1,$q,($cadaytotal[$date->format('Y-m-d')]??0));
        */
        $k=$k + $cran;
      }
      $k=$k+7;
      $sheet->setCellValueByColumnAndRow( $k, $q,$catotal);
      for($h=1;$h<=$k+2;$h++){
      $sheet->getStyleByColumnAndRow( $h, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
      $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
      $sheet->getCellByColumnAndRow( $h, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
      }

      $k=$k+7;
      foreach($prestations as $prestation){
                          $p=$prestation->getId();
                          $materiels=$prestation_materiel[$p];
                          foreach ($materiels as $materiel) {
                              $m=$materiel->getId();
                              //echo $p.'-'.$m.'-'.$user_id;
                              $k++;
                              $sheet->setCellValueByColumnAndRow($k, $q, ($totprestations[$p][$m]??0));
      $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);//->applyFromArray($styleArray);
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
      $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3000000');
      }
      }
    $q++;$q++;
    }//fin count

    return $spreadsheet;
  }

public function exportRenou()
    {

      $this->titre = 'Export client';
      $em = $this->getDoctrine()->getManager();

        //-------------FORMULAIRE---------------------
      $date2=new \DateTime('last Sunday');
      $date1 = clone $date2;
      $date1 = $date1->sub(new \DateInterval('P6D'));

      $feuille='export-client';
      $action = 'rapport';


      $chantiers2 = $em->getRepository('App:Chantier')->findChantiersEnCoursClientEnCours($date1,$date2,2);


      foreach ($chantiers2 as $chantier1){
        $emails=array('jean-luc.douaran@sbeae.com','arnaud.tanguy@sbeae.com','jean-baptiste.cros@sbeae.com','mathilde.douaran@sbeae.com','alan.grolleau@sbeae.com','f.m@hebline.com','j.b@hebline.com','michael.hervouet@sbeae.com');
      if(($chantier1->getEmails() != '')&&($chantier1->getTypechantier()->getId() == 2)){//que chantier releve
                  $enregistrementCriteres = new Enregistrement();
                  $enregistrementCriteres->setChantier($chantier1);
                  $idch = $chantier1->getId();
                  $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
                  $spreadsheet->getProperties()->setCreator("SBEAE")->setLastModifiedBy("SBEAE")->setCategory("");
                  $spreadsheet= $this->calcul_export_renou( $feuille,$spreadsheet, $action, $enregistrementCriteres, null, $date1, $date2, null, $this->titre);
                  $writer = new Xlsx($spreadsheet);
                  $fileName = 'Rapport_hebdo_renou_'.date('d-m-Y').'.xlsx';
                  $temp_file = tempnam(sys_get_temp_dir(), $fileName);
                  $writer->save($temp_file);

                  $datenow=new \DateTime('now');

                  //$fileName2 = 'Export-SBEAE_'.$idch.'_'.$datenow->format('d-m-Y').'.pdf';
                  //$temp_file2 = $this->getParameter('upload').'uploads/chantier'.$idch.'.pdf';
$temp_file2="";
$fileName2 ="";

$sujet='SBEAE '.$chantier1->getClient()->getNom().'/'.$chantier1->getNom().' '.$datenow->format('d-m-Y');
$mess='Bonjour,<br/><br/>
Nous vous prions de trouver ci joint le rapport pour la période du '.$date1->format('d-m-Y').' au '.$date2->format('d-m-Y').'<br>
Pour plus d\'informations, nous vous invitons à contacter notre chargé d\'affaire.<br><br>Cordialement,<br><br>
L\'équipe EAE<br><img src=\''.$chantier1->getSociete()->getLogo().'\' width="150">';

$emailConstraint = new Assert\Email();
$emailConstraint->message = 'Invalid email address';

                  $emails2=explode(';',$chantier1->getEmails());
                  foreach($emails2 as $ml){
                        $errors = $this->validator->validate(
                            $ml,
                            $emailConstraint
                        );
                        if (0 === count($errors)) {
                          $emails[]=$ml;
                        } else {
                              //echo $errors[0]->getMessage();
                        }

                  }
                  $emails[]='j.b@hebline.com';
                  $emails[]='f.m@hebline.com';
                  $emails[]='jean-luc.douaran@sbeae.com';
                  $emails[]='arnaud.tanguy@sbeae.com';

                    $this->sendConfirmationEmailMessage($emails, $sujet, $mess, $temp_file, $fileName, $temp_file2, $fileName2);
//unlink($temp_file2);
          }
      }
      return new Response('ExportClient');
    }


public function getCommentaires($date1, $date2) {
    $em = $this->getDoctrine()->getManager();
    $commentaire= $indications = $nbjours = array();
    $notes=$em->getRepository('App:Evenement')->findEvenementsNotes($date1, $date2);
    //echo '<br>date'.$date2->format('Y-m-d').'<br>';
    foreach($notes as $note){
      //echo 'ID'.$note->getId().'-'.$note->getCommentaire().'<br>';
      $user_id=$note->getUser()->getId();
      $datedebut=$note->getDatedebut();
      $datefin=$note->getDatefin();
      if($datefin == null){$datefin = $datedebut;}
      //else{$datefin->modify('-1 day');}
      $com=$note->getCommentaire();
      $type=$note->getType();
      if(($type<3)||($type>6)){
        for($date=clone $datedebut; $date<=$datefin; $date->modify('+1 day')){
              $indications[$user_id][$date->format('Y-m-d')][$type]=1;
              //echo 'bd'.$type.' '.$date->format('Y-m-d').' '.$user_id."<br>\n";
        }
      }

    $delta = $this->fonctions->get_nb_open_days2(max(strtotime($date1->format('Y-m-d')),strtotime($datedebut->format('Y-m-d'))),
    min(strtotime($date2->format('Y-m-d')),strtotime($datefin->format('Y-m-d'))));
    //on retire un jour si date reprise dans la semaine
    if(strtotime($datefin->format('Y-m-d')) < strtotime($date2->format('Y-m-d'))){$delta--;}
    //$delta = $this->fonctions->get_nb_open_days(strtotime($date1->format('Y-m-d')),strtotime($datefin->format('Y-m-d')));

      if($type==1){
        $commentaire[$user_id]=($commentaire[$user_id]?? '') . 'AM : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      $nbjours[$user_id][1]= ($nbjours[$user_id][1] ?? 0) + $delta;
      }
      if($type==2){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'CP : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      $nbjours[$user_id][2]=($nbjours[$user_id][2] ?? 0) + $delta;
      }
      if($type==3){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'Accident : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
      }
      if($type==4){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'Reclamation : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
      }
      if($type==5){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'PB matériel : '.$datedebut->format('d-m-Y').' : '.$com.' | ';
      }
      if($type==6){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'Autre : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      }
      if($type==7){
        $commentaire[$user_id]=($commentaire[$user_id]?? '') . 'AT : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      $nbjours[$user_id][7]=($nbjours[$user_id][7] ?? 0) + $delta;
      }
      if($type==8){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'Abs : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      $nbjours[$user_id][8]=($nbjours[$user_id][8] ?? 0) + $delta;
      }
      if($type==9){
      $commentaire[$user_id]=($commentaire[$user_id]?? '').'Comm : Du '.$datedebut->format('d-m-Y').' au '.$datefin->format('d-m-Y').' : '.$com.' | ';
      }
    }
    return array($commentaire,$nbjours,$indications);
  }

public function sendConfirmationEmailMessage($emails, $subject, $mess, $data = null, $file= null)
      {
        $emailcontact = 'j.b@hebline.com';

        $message = (new \Swift_Message($subject));
        $message->setFrom('contact@releve-eae.com');

        if($emails == null){
        $emails=array('jean-luc.douaran@sbeae.com','sylvie.caron@sbeae.com','arnaud.tanguy@sbeae.com','jean-baptiste.cros@sbeae.com','anne-marie.payen@sbeae.com','mathilde.douaran@sbeae.com','alan.grolleau@sbeae.com','eae@hebline.com','f.m@hebline.com','j.b@hebline.com','michael.hervouet@sbeae.com');
        }
        //$emails=array('j.b@hebline.com');
foreach($emails as $email){echo $email;}

        if($data != null){
          $data=file_get_contents($data);
          $attachment = new \Swift_Attachment($data, $file, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          $message->attach($attachment);
        }

        $message->setTo($emails)->setBody($mess, 'text/html');

        try {
          $result = $this->mailer->send($message);
        }
        catch (\Swift_TransportException $e) {
          echo $e->getMessage();
        }

  }

  function cmp($a, $b) {
    if ($a->getNom() == $b->getNom()) {
        return 0;
    }
    return ($a->getNom() < $b->getNom()) ? -1 : 1;
  }


  function compareTabAndOrder($a, $b) {
    // compare the tab option value
    $diff = $a->getZone()->getId() - $b->getZone()->getId();
    // and return it. Unless it's zero, then compare order, instead.
    if($diff == 0)
      {if ($a->getNom() == $b->getNom()) {
        return 0;
      }
      return ($a->getNom() < $b->getNom()) ? -1 : 1;
    }
    return 0;
  }
  function fill_color($sheet, $color, $k, $q){
              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
              $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('D3'.$color);
              return $sheet;
  }
}
