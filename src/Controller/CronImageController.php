<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\User;
use App\Controller\ImageController;

class CronImageController extends AbstractController
{
	public function __construct(ImageController $ocr)
    {
        $this->titre = 'Sites';
        $this->ocr = $ocr;
    }
	public function Analyse()
    {
		//echo exec("/usr/local/bin/python3.8 ");

		$path = getenv('PATH'); putenv("PATH=$path:/usr/local/bin");
		$command = escapeshellcmd('/var/www/vhosts/releve-eae.com/test2.py');
		$output = shell_exec($command);
		echo $output;
		/**/

		$em = $this->getDoctrine()->getManager();
		$enregs=$em->getRepository('App:Enregistrement')->findByOcr(2);
		$q=0;
		foreach($enregs as $enregistrement){
			$q=$q+1;
			$chantier=$enregistrement->getChantier();
			$nomclient=$chantier->getClient()->getNom();
			$user=$enregistrement->getUser();
			//echo 'client'.$nomclient;
			if( strstr($nomclient, 'VEOLIA') || strstr($nomclient, 'SUEZCOMMUNE')) {
			//echo 'analyse id'.$enregistrement->getId().'-';
			$retour=$this->ocr->analyse($enregistrement);
			echo $retour;
			}

			//controle cloture precedente VEOLIA
			$nom_tournee=$enregistrement->getTitre();
			if( strstr($nomclient, 'VEOLIA')&&($nom_tournee != '')) {
			$id_enreg=$enregistrement->getId();
			$SUM=$em->getRepository('App:Enregistrement')->findSumReleveOcr($id_enreg);
			$SUMRL=$em->getRepository('App:Enregistrement')->findSumReleve($nom_tournee, $user);
			if(null != $SUM){
			//echo 'SOMME'.$SUM['total'];
			$totrltr=$SUM['total']-$SUM['reste'];
			$enregistrement->setSumtournee($totrltr);
			$enregistrement->setSumtotal($SUMRL['total']);
			if($totrltr != $SUMRL['total']){
			$message="Delta RL OCR / RL";
			echo "delta veolia";
			$enregistrement->setAlerte(1);
			$enregistrement->setTextalerte($message);
			}
			}
			$lastenreg = $em->getRepository('App:Enregistrement')->findLastEnregistrement($id_enreg, $user);
			if(isset($lastenreg)){
				$lasttotal = $lastenreg->getTotal();
				/*
				if($lasttotal !=  $SUMRL['total']){
					$message="Delta OCR / RL";
					$enregistrement->setAlerte(1);
					$enregistrement->setTextalerte($message);
				}
				*/
				$nom_tournee2= $lastenreg->getTitre();
				$lastcloture= $lastenreg->getCloture();
				//echo 'TR2'.$nom_tournee2;
				if(($nom_tournee2 != $nom_tournee) && ($lastcloture != 1)){
					//mail alerte
					$enregistrement->setAlerte(1);
					 $headers = 'From: contact@releve-eae.com.com' . "\r\n";
					mail('j.b@hebline.com;f.m@hebline.com;geraldineallouard@orange.fr','PB cloture enreg '.$id_enreg,$id_enreg,$headers);
				}
			}
		}

		if(strstr($nomclient, 'SUEZ')) {
				if($enregistrement->getTotalOcr() != $enregistrement->getTotal()){
						$enregistrement->setAlerte(1);
						$id_enreg=$enregistrement->getId();
					 $headers = 'From: contact@releve-eae.com.com' . "\r\n";
					mail('j.b@hebline.com;f.m@hebline.com;geraldineallouard@orange.fr','PB total SUEZ '.$id_enreg,$id_enreg,$headers);
				}
		}

			$enregistrement->setOcr(1);

			$em->flush();
			if(isset($id_enreg)&&($id_enreg != null)){
			$enregl=$em->getRepository('App:Enregistrement')->findLastEnregistrement($id_enreg, $user, $enregistrement->getTitre());
			if($enregl != null){
					$total_last=$enregl->getCloturetotal() + $enregistrement->getTotal();
					$enregistrement->setCloturetotall($total_last);
					$em->flush();
			}
			}

	}
	return new Response(1);
}
}
