<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use App\Entity\User;

class CommentaireController extends AbstractController
{

	public function __construct()
    {
        $this->titre = 'Commentaires';
    }

	 public function index(Request $request,$page)
    {
		$zone = $this->getUser()->getZone();
		$em = $this->getDoctrine()->getManager();
		if ($page < 1) {
			  $page=1;
		}
		$nbPerPage = $this->getParameter('nbPerPage');
		$enregs = $em->getRepository('App:Commentaire')->findCommentaires($page, $nbPerPage, $zone);
		if(!empty($enregs)){
		$nbPages = ceil(count($enregs) / $nbPerPage);}
		else{$nbPages =0;}

        return $this->render('Admin/Commentaire/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
        ));

    }


	 public function edit(Request $request, Commentaire $commentaire)
    {

	    $em = $this->getDoctrine()->getManager();
		$zone_id = $this->getUser()->getZone()->getId();
        $editForm =  $this->createForm('App\Form\AdminCommentaireType', $commentaire, array('zone' => $zone_id));
        $editForm->handleRequest($request);

		$em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$commentaire=$editForm->getData();
			$this->addFlash('success',
            'Vos modifications ont été enregistrées'
			);
			$this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_commentaires');
        }
        return $this->render('Admin/Commentaire/edit.html.twig', array(
            'enreg' => $commentaire,
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre,
        ));
    }

	public function add(Request $request)
    {

		$zone_id = $this->getUser()->getZone()->getId();
        $editForm =  $this->createForm('App\Form\AdminCommentaireType', null, array('zone' => $zone_id));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$zone = $this->getUser()->getZone();
			$commentaire=$editForm->getData();
			$commentaire->setZone($zone);
			$em = $this->getDoctrine()->getManager();
			$em->persist($commentaire);
            $em->flush();
			$this->addFlash('success','Nouveau commentaire');
            return $this->redirectToRoute('admin_commentaires');
        }

        return $this->render('Admin/Commentaire/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre,
        ));
    }
	 public function delete(Request $request,Commentaire $commentaire)
    {

        $em = $this->getDoctrine()->getManager();

		$em->remove($commentaire);
        $em->flush();
		$this->addFlash('success',
            'Commentaire supprimmé'
			);
        return $this->redirectToRoute('admin_commentaires');
    }
	 public function indexHebline(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
		if ($page < 1) {
			  $page=1;
		}
		$nbPerPage = $this->getParameter('nbPerPage');
		$enregs = $em->getRepository('App:Commentaire')->findCommentaires($page, $nbPerPage);
		$nbPages = ceil(count($enregs) / $nbPerPage);
		$zone = $this->getUser()->getZone()->getId();
		if($zone==1001){$rep="SuperAdmin";}
		else if($zone==1000){$rep="Hebline";}

        return $this->render($rep.'/Commentaire/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
        ));
    }


	 public function editHebline(Request $request, Commentaire $commentaire)
    {

	    $em = $this->getDoctrine()->getManager();
		$zone = null;

        $editForm =  $this->createForm('App\Form\AdminCommentaireType', $commentaire, array('zone' => $zone));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$commentaire=$editForm->getData();
			$this->addFlash('success',
            'Vos modifications ont été enregistrées'
			);
			$this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('hebline_commentaires');
        }

		$zone = $this->getUser()->getZone()->getId();
		if($zone==1001){$rep="SuperAdmin";}
		else if($zone==1000){$rep="Hebline";}

        return $this->render($rep.'/Commentaire/edit.html.twig', array(
            'enreg' => $commentaire,
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre,
        ));
    }


	public function addHebline(Request $request)
    {

        $editForm =  $this->createForm('App\Form\AdminCommentaireType', null, array('zone' => null));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$commentaire=$editForm->getData();
			$em->persist($commentaire);
            $em->flush();
			$this->addFlash('success','Nouvel enregistrement');
            return $this->redirectToRoute('hebline_commentaires');
        }

        $zone = $this->getUser()->getZone()->getId();
		if($zone==1001){$rep="SuperAdmin";}
		else if($zone==1000){$rep="Hebline";}

        return $this->render($rep.'/Commentaire/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
			'titre' => $this->titre,
        ));
    }
	 public function deleteHebline(Request $request,Commentaire $commentaire)
    {

        $em = $this->getDoctrine()->getManager();

		$em->remove($commentaire);
        $em->flush();
		$this->addFlash('success',
            'Commentaire supprimmé'
			);
		$zone = $this->getUser()->getZone()->getId();
		if($zone==1001){ return $this->redirectToRoute('superadmin_commentaires');}
		else if($zone==1000){ return $this->redirectToRoute('hebline_commentaires');}
    }
}
