<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Zone;
use Doctrine\ORM\EntityManagerInterface;
//use thiagoalessio\TesseractOCR\TesseractOCR;

class FonctionsController extends AbstractController
{

	public function __construct(EntityManagerInterface $entityManager, String $upload = '')
	{
		$this->em = $entityManager;
	}

	 public function getRepo($zone = null)
    {
		$em = $this->em ;
 		$repo="admin";
        if($zone==1001){$repo="superadmin";}
        if($zone==1000){$repo="hebline";}
    return $repo;
		}

  public function get_nb_open_days($date_start, $date_stop) {
  $arr_bank_holidays = array(); // Tableau des jours feriés
  // On boucle dans le cas où l'année de départ serait différente de l'année d'arrivée
  $diff_year = date('Y', $date_stop) - date('Y', $date_start);
  //$diff_year=1;
  for ($i = 0; $i <= $diff_year; $i++) {
    $year = date('Y', $date_start) + $i;
    // Liste des jours feriés
    $arr_bank_holidays[] = '1_1_'.$year; // Jour de l'an
    $arr_bank_holidays[] = '1_5_'.$year; // Fete du travail
    $arr_bank_holidays[] = '8_5_'.$year; // Victoire 1945
    $arr_bank_holidays[] = '14_7_'.$year; // Fete nationale
    $arr_bank_holidays[] = '15_8_'.$year; // Assomption
    $arr_bank_holidays[] = '1_11_'.$year; // Toussaint
    $arr_bank_holidays[] = '11_11_'.$year; // Armistice 1918
    $arr_bank_holidays[] = '25_12_'.$year; // Noel

    // Récupération de paques. Permet ensuite d'obtenir le jour de l'ascension et celui de la pentecote
    $easter = easter_date($year);
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + 86400); // Paques
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*39)); // Ascension
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*50)); // Pentecote
  }
  //print_r($arr_bank_holidays);
  $nb_days_open = 0;
  // Mettre <= si on souhaite prendre en compte le dernier jour dans le décompte
  while ($date_start < $date_stop) {
    // Si le jour suivant n'est ni un dimanche (0) ou un samedi (6), ni un jour férié, on incrémente les jours ouvrés
    if (!in_array(date('w', $date_start), array(0, 6))
    && !in_array(date('j_n_'.date('Y', $date_start), $date_start), $arr_bank_holidays)) {
      $nb_days_open++;
    }
    $date_start = mktime(date('H', $date_start), date('i', $date_start), date('s', $date_start), date('m', $date_start), date('d', $date_start) + 1, date('Y', $date_start));
  }
  return $nb_days_open;
  }

public function get_nb_open_days2($date_start, $date_stop) {
  $arr_bank_holidays = array(); // Tableau des jours feriés
  // On boucle dans le cas où l'année de départ serait différente de l'année d'arrivée
  $diff_year = date('Y', $date_stop) - date('Y', $date_start);
  //$diff_year=1;
  for ($i = 0; $i <= $diff_year; $i++) {
    $year = date('Y', $date_start) + $i;
    // Liste des jours feriés
    $arr_bank_holidays[] = '1_1_'.$year; // Jour de l'an
    $arr_bank_holidays[] = '1_5_'.$year; // Fete du travail
    $arr_bank_holidays[] = '8_5_'.$year; // Victoire 1945
    $arr_bank_holidays[] = '14_7_'.$year; // Fete nationale
    $arr_bank_holidays[] = '15_8_'.$year; // Assomption
    $arr_bank_holidays[] = '1_11_'.$year; // Toussaint
    $arr_bank_holidays[] = '11_11_'.$year; // Armistice 1918
    $arr_bank_holidays[] = '25_12_'.$year; // Noel

    // Récupération de paques. Permet ensuite d'obtenir le jour de l'ascension et celui de la pentecote
    $easter = easter_date($year);
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + 86400); // Paques
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*39)); // Ascension
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*50)); // Pentecote
  }
  //print_r($arr_bank_holidays);
  $nb_days_open = 0;
  // Mettre <= si on souhaite prendre en compte le dernier jour dans le décompte
  while ($date_start <= $date_stop) {
    // Si le jour suivant n'est ni un dimanche (0) ou un samedi (6), ni un jour férié, on incrémente les jours ouvrés
    if (!in_array(date('w', $date_start), array(0, 6))
    && !in_array(date('j_n_'.date('Y', $date_start), $date_start), $arr_bank_holidays)) {
      $nb_days_open++;
    }
    $date_start = mktime(date('H', $date_start), date('i', $date_start), date('s', $date_start), date('m', $date_start), date('d', $date_start) + 1, date('Y', $date_start));
  }
  return $nb_days_open;
  }
public function get_open_days($date_start, $date_stop) {
  $arr_bank_holidays = array(); // Tableau des jours feriés

  // On boucle dans le cas où l'année de départ serait différente de l'année d'arrivée
  $diff_year = date('Y', $date_stop) - date('Y', $date_start);
  for ($i = 0; $i <= $diff_year; $i++) {
    $year = (int)date('Y', $date_start) + $i;
    // Liste des jours feriés
    $arr_bank_holidays[] = '1_1_'.$year; // Jour de l'an
    $arr_bank_holidays[] = '1_5_'.$year; // Fete du travail
    $arr_bank_holidays[] = '8_5_'.$year; // Victoire 1945
    $arr_bank_holidays[] = '14_7_'.$year; // Fete nationale
    $arr_bank_holidays[] = '15_8_'.$year; // Assomption
    $arr_bank_holidays[] = '1_11_'.$year; // Toussaint
    $arr_bank_holidays[] = '11_11_'.$year; // Armistice 1918
    $arr_bank_holidays[] = '25_12_'.$year; // Noel

    // Récupération de paques. Permet ensuite d'obtenir le jour de l'ascension et celui de la pentecote
    $easter = easter_date($year);
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + 86400); // Paques
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*39)); // Ascension
    $arr_bank_holidays[] = date('j_n_'.$year, $easter + (86400*50)); // Pentecote
  }
  //print_r($arr_bank_holidays);
  $days_open = array();
  // Mettre <= si on souhaite prendre en compte le dernier jour dans le décompte
  while ($date_start <= $date_stop) {
    // Si le jour suivant n'est ni un dimanche (0) ou un samedi (6), ni un jour férié, on incrémente les jours ouvrés
    if (!in_array(date('w', $date_start), array(0, 6))
    && !in_array(date('j_n_'.date('Y', $date_start), $date_start), $arr_bank_holidays)) {
      $days_open[]=date('Y-m-d',$date_start);
    }
    $date_start = mktime(date('H', $date_start), date('i', $date_start), date('s', $date_start), date('m', $date_start), date('d', $date_start) + 1, date('Y', $date_start));
  }
  return $days_open;
  }
}
