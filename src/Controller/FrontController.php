<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Intervention\Image\ImageManager;

use App\Entity\Enregistrement;
use App\Repository\UserRepository;
use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\EnregistrementChantierMateriel;
use App\Entity\Totalrenou;
use App\Entity\Pointage;



class FrontController extends AbstractController
{
		public function __construct()
	{


	}

	public function Message()
    {
		$titre="Message";
			return $this->render('FrontUser/message.html.twig', array(
				'titre'			=> $titre
			));

	}

	public function Tutorial()
    {
		$titre="Tutorial";
			return $this->render('FrontUser/tutorial.html.twig', array(
				'titre'			=> $titre
			));

	}
		//home
	public function Index()
    {
		$titre="Accueil";
			return $this->render('FrontUser/index.html.twig', array(
				'titre'			=> $titre
			));

	}

	//aide
	public function aide()
    {
		$titre="Aide";
			return $this->render('FrontUser/aide.html.twig', array(
				'titre'			=> $titre
			));

	}

	//contact
	public function Contact()
    {
		$titre="Votre contact SBEAE";
        $em = $this->getDoctrine()->getManager();
		$user=$this->getUser();
		$responsable=$user->getZone()->getResponsable();

			return $this->render('FrontUser/contact.html.twig', array(
				'titre'			=> $titre,
				'responsable'			=> $responsable
			));

	}

	//choix chantier
	public function Acces(Request $request)
    {
		$titre="Sélectionner un site";
		$messageform = '';
		$date = new \DateTime();

    $em = $this->getDoctrine()->getManager();
		$user=$this->getUser();
		//echo $this->getUser()->getId();
		$responsable=$user->getZone()->getResponsable();
		$chantiersuser = $em->getRepository('App:ChantierUser')->findUserChantierActuel($user);
		$pointeur = 0;
	      foreach($chantiersuser as $enreg){
	      	//echo $enreg->getChantier()->getId();
	      		if($enreg->getChantier()->getPointage() == 1){$pointeur = 1;}
	      }
			//aucun chantier en cours
			if(count($chantiersuser) == 0){$message="Désolé, aucun chantier défini. Merci de contacter votre chargé d'affaire.";return $this->redirectToRoute('erreur',array('message' => $message));}
			//1 seul chantier -> pa le choix
			else if((count($chantiersuser) == 1) && ($pointeur == 0)){
				$chantier=$chantiersuser[0]->getChantier();
				$nomclient=$chantier->getClient()->getNom();
				$idchantier=$chantier->getId();
				return $this->Calendrier($chantier,0);
			}

			else{
				$pointage=new Pointage();
			 $form = $this->get('form.factory')->create(\App\Form\PointageType::class, $pointage);
			 $form->handleRequest($request);
		   if ($form->isSubmitted()) {
		   			$pointage->setUser($user);
				    $em->persist($pointage);
	          $em->flush();
	          $messageform="Pointage enregistré";
	      }
	      if($messageform == ''){
	      	$pointages = $em->getRepository('App:Pointage')->recherchePointageDateUser($date, $user);
	      	//echo 'count'.count($pointages);

	      	if(isset($pointages[0])){$pointage2 = $pointages[0];$messageform= "Dernier pointage le ".$pointage2->getDatecrea()->format('d-m-Y')." à ".$pointage2->getDatecrea()->format('H:i');}
	      }

				//selection chantier
				return $this->render('FrontUser/selectionchantier.html.twig', array(
					'messageform' => $messageform,
					'form' 			=> $form->createView(),
					'enregs' 			=> $chantiersuser,
					'titre'			=> $titre,
					'responsable'			=> $responsable,
					'pointage' => $pointeur
				));
			}
	}


	public function Cloture($id)
    {
		$titre="Sélectionner un type d'enregistrement";
    $em = $this->getDoctrine()->getManager();
    $chantier = $em->getRepository("App:Chantier")->find($id);
		$user=$this->getUser();
		$responsable=$user->getZone()->getResponsable();

		//on recupere le type de formulaire si veolia selection sinon redirection
		$typeformulaire=$chantier->getClient()->getFormulaire()->getNom();
		$client=$chantier->getClient();
		$nomclient=$client->getNom();
		$idchantier=$chantier->getId();
		//echo $nomclient;
		/*
		if( (strstr($nomclient, 'VEOLIA')&&(!strstr($nomclient, 'VENDEE'))) || strstr($nomclient, 'SPL') || ($idchantier == 75) || ($idchantier == 62)|| ($idchantier == 134)) {
			return $this->render('FrontUser/selectioncloture.html.twig', array(
				'chantier' 			=> $chantier,
				'titre'			=> $titre,
				'responsable'			=> $responsable
			));
		}
		else{
			return $this->Calendrier($chantier,0);
		}*/
		return $this->Calendrier($chantier,0);
    }

	public function Calendrier($chantier, $cloture=0) {
		$titre="Sélectionner une date";

		$em = $this->getDoctrine()->getManager();
    $chantier = $em->getRepository("App:Chantier")->find($chantier);
		$user=$this->getUser();
		$responsable=$user->getZone()->getResponsable();

		$jours=array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
		$dates=array();
		$date0=date('Y-m-d');
		$date1=date('Y-m-d',strtotime("monday this week"));
		$dates[]=$date1;
		$dates[]=date('Y-m-d',strtotime($date1 .' +1 day '));
		$dates[]=date('Y-m-d',strtotime($date1 .' +2 day '));
		$dates[]=date('Y-m-d',strtotime($date1 .' +3 day '));
		$dates[]=date('Y-m-d',strtotime($date1 .' +4 day '));
		$dates[]=date('Y-m-d',strtotime($date1 .' +5 day '));
		$dates[]=date('Y-m-d',strtotime($date1 .' +6 day '));

		$dateenregs = $em->getRepository('App:Enregistrement')->rechercheDateFront($user, $chantier, $date1);
		$dateenregs2=array();
		foreach($dateenregs as $dater){
			$dateenregs2[$dater->getDatereleve()->format('Y-m-d')]=1;
			//echo $dater->getDatereleve()->format('Y-m-d');
		}

		$alertheure=0;
		$heure = date('H')+2;
		if($heure > 19){$alertheure = 1;}

			return $this->render('FrontUser/selectionjour.html.twig', array(
				'chantier' 		=> $chantier,
				'titre'			=> $titre,
				'dates'			=> $dates,
				'date0'			=> $date0,
				'jours'		    => $jours,
				'cloture'	    => $cloture,
				'dateenregs'	=> $dateenregs2,
				'responsable'			=> $responsable,
				'alertheure' => $alertheure
			));

	}

	public function Formulaire(Request $request,$chantier, $cloture=0, $date)
    {
		$titre="Formulaire";
    $em = $this->getDoctrine()->getManager();
    $chantier = $em->getRepository("App:Chantier")->find($chantier);
		$user=$this->getUser();
		$responsable=$user->getZone()->getResponsable();
		$dateur=\DateTime::createFromFormat('Y-m-d', $date);



//echo 'chantier'.$chantier->getTypechantier()->getId();

		//-------------FORMULAIRE RELEVE---------------------
		if($chantier->getTypechantier()->getId() == 1){
			$typeformulaire=$chantier->getClient()->getFormulaire();
			$nomformulaire=$typeformulaire->getNom();
			if($nomformulaire == 'sansimage'){$formulaire='formulaire_sans_image.html.twig';$capture=0;}
			else{$formulaire='formulaire_veolia.html.twig';$capture=1;}
//echo $formulaire;
			$enregistrement = new Enregistrement();
			$client=$chantier->getClient();
			$nomclient=$client->getNom();
			$idchantier=$chantier->getId();
			$clotureaff=0;
			if( (strstr($nomclient, 'VEOLIA')&&(!strstr($nomclient, 'VENDEE'))) || strstr($nomclient, 'SPL') || ($idchantier == 75) || ($idchantier == 62)|| ($idchantier == 134)) {$clotureaff=1;}
			/*if($cloture == 0){$titre='Rapport journalier';}else{$titre='Rapport journalier (Clôture)';}*/
			$form = $this->get('form.factory')->create(\App\Form\EnregistrementType::class, $enregistrement, array('titre' => $titre,'nomformulaire' => $nomformulaire, 'cloture' => $clotureaff, 'capture' => $capture));
			$form->handleRequest($request);


//echo $enregistrement->getNewcompteur();
			if ($form->isSubmitted() && $form->isValid()) {
			//var_dump($form);

				$releve=$enregistrement->getReleve();

				//echo $chantier->getClient()->getFormulaire()->getId();
				if($chantier->getClient()->getFormulaire()->getId() == 4){
					$totaltournee = $enregistrement->getTotaltournee();
					$newtitre="Tournée ".$totaltournee;
					$enregistrement->setTitre($newtitre);
				}
				$nomclient=$chantier->getClient()->getNom();
				if(strstr($nomclient, 'VENDEE')){
					$cloture=1;
				}

				$releve=$enregistrement->getReleve();
				$infructueux=$enregistrement->getInfructueux();
				$total=$releve+$infructueux;
				if($chantier->getForfait() >0){
						if($releve <= $chantier->getCadenceforfait()){$enregistrement->setTarifjour(1);}
				}
				$enregistrement->setTotal($total);
				$enregistrement->setUser($user);
				$enregistrement->setZone($user->getZone());
				//$enregistrement->setCloture($cloture);
				$enregistrement->setTypeformulaire($typeformulaire);
				$enregistrement->setChantier($chantier);
				$enregistrement->setDatereleve($dateur);
				$enregistrement->setTypechantier($em->getRepository('App:TypeChantier')->find(1));
				$images = $enregistrement->getDocuments();
				foreach($enregistrement->getNcompteurs() as $ncompteur){
					$em->persist($ncompteur);
				}
				//echo 'CT'.count($images);
//controle format
						$images = $enregistrement->getDocuments();
						$format_autorise = array('png','jpeg','gif','jpg','JPG','JPEG','GIF','PNG');
				$em->persist($enregistrement);
	      $em->flush();
	      //redim image
	      $manager = new ImageManager();
	      $images = $enregistrement->getDocuments();

			      foreach ($images as $image){

			      	$path_parts = pathinfo($image->getFileName());
                    if (!in_array($path_parts['extension'], $format_autorise)){
                    	$em->remove($enregistrement);
	                    $em->flush();
                    	$this->addFlash('warning','Problème sur le format de l\'image');
											return $this->render('FrontUser/'.$formulaire, array(
																'form' 			=> $form->createView(),
																'date'          => $date,
																'chantier'          => $chantier,
																'titre'			=> $titre,
																'responsable'			=> $responsable,
											        ));

                    }
							if($image->getFileName() != ''){
										 	$filepath = $this->upload = $this->getParameter('upload').'uploads/documents/'.$image->getFileName();
												$manager->make($filepath)->resize(1480, null, function($constraint){
												    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
												    $constraint->upsize(); // empêche l'image d'être agrandie
												})->save($filepath);
							}
						}
				  $this->addFlash('success','Enregistrement créé');
				  //echo 'ok';
					return $this->redirectToRoute('acces_sbeae');
				}
				else {
				$this->addFlash('danger', $form->getErrors(true));
				}
			}

      //-------------FORMULAIRE RENOU---------------------
    if($chantier->getTypechantier()->getId() == 2){
			$formulaire='formulaire_renou.html.twig';
			$enregistrement = new Enregistrement();
			$client=$chantier->getClient();
			$nomclient=$client->getNom();
			$idchantier=$chantier->getId();
			$obligatoire=$chantier->getCaptureobligatoire();
			if($obligatoire == 1){$obligatoire = true;}else{$obligatoire = false;}
			/*if($cloture == 0){$titre='Rapport journalier';}else{$titre='Rapport journalier (Clôture)';}*/
			$form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouNewType::class, $enregistrement, array('obligatoire' => $obligatoire));
			$form->handleRequest($request);
				if (!$form->isSubmitted()) {
						//recherche des documents
						$chantiersmateriels=$chantier->getChantierMateriels();
								foreach($chantiersmateriels as $chantiermateriel){
										$enregistrementchantiermateriel = new EnregistrementChantierMateriel();
										$enregistrementchantiermateriel->setEnregistrement($enregistrement);
										$enregistrementchantiermateriel->setChantierMateriel($chantiermateriel);
										$enregistrementchantiermateriel->setNombre(1);
										$enregistrement->addEnregistrementChantierMateriel($enregistrementchantiermateriel);
								}
						$form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouNewType::class, $enregistrement, array('obligatoire' => $obligatoire));
				}


		   if ($form->isSubmitted()) {
		   			$enregistrement->setUser($user);
						$enregistrement->setZone($user->getZone());
						//$enregistrement->setTypeformulaire($typeformulaire);
						$enregistrement->setChantier($chantier);
						$enregistrement->setDatereleve($dateur);
						$enregistrement->setCloture(0);
						$enregistrement->setTypechantier($em->getRepository('App:TypeChantier')->find(2));
						/**/
						$enregistrementchantiermateriels=$_POST['enregistrement_renou_new']['enregistrementchantiermateriels'];
						$q=0;
						while(isset($enregistrementchantiermateriels[$q])){
							//print_r ($enregistrementchantiermateriels[$q]);
							$nombre=$enregistrementchantiermateriels[$q]['nombre'];
							$id=$enregistrementchantiermateriels[$q]['id'];
							$enregistrementchantiermateriel= new EnregistrementChantierMateriel();
							$enregistrementchantiermateriel->setEnregistrement($enregistrement);
							$enregistrementchantiermateriel->setNombre($nombre);
							$enregistrementchantiermateriel->setChantierMateriel($em->getRepository('App:ChantierMateriel')->find($id));
							$em->persist($enregistrementchantiermateriel);
							$enregistrement->addEnregistrementchantiermateriel($enregistrementchantiermateriel);
							$q++;
						}



	      		$images = $enregistrement->getDocuments();
			      foreach ($images as $image){
							if(empty($image->getFileName())){
			      	$enregistrement->removeDocument($image);
			      	$em->remove($image);
							}
						}

						//controle format
						$images = $enregistrement->getDocuments();
						$format_autorise = array('png','jpeg','gif','jpg','JPG','JPEG','GIF','PNG');
			      foreach ($images as $image){

						    $imagefile = $image->getFileName();
    						if($imagefile) {
        						$path_parts = pathinfo($imagefile);
                    if (!in_array($path_parts['extension'], $format_autorise)){
                    	$this->addFlash('success','Problème sur le format de l\'image');
											return $this->render('FrontUser/'.$formulaire, array(
																'form' 			=> $form->createView(),
																'date'          => $date,
																'chantier'          => $chantier,
																'titre'			=> $titre,
																'responsable'			=> $responsable,
											        ));

                    }
        				}
        		}

						$em->persist($enregistrement);
			      $em->flush();
						$images = $enregistrement->getDocuments();
			      foreach ($images as $image){
							if(empty($image->getFileName())){
						 		$filepath = $this->getParameter('upload').'uploads/documents/'.$image->getFileName();
								$manager->make($filepath)->resize(1080, null, function($constraint){
								    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
								    $constraint->upsize(); // empêche l'image d'être agrandie
								})->save($filepath);
							}
						}
						//calcul des totaux par categ
						$totalrenous=$em->getRepository('App:Totalrenou')->findBy(['enregistrement' => $enregistrement]);
						foreach($totalrenous as $totalrenou){$em->remove($totalrenou);}
						$em->flush();
						/**/
			      $totaux = $em->getRepository('App:EnregistrementChantierMateriel')->findTotaux($enregistrement->getId());
			      	foreach($totaux as $total){
			      			$totalrenou=new Totalrenou();
			      			$totalrenou->setEnregistrement($enregistrement);
			      			$totalrenou->setTotal($total['total']);
			      			$totalrenou->setPrestation($em->getRepository('App:Prestation')->find($total['id']));
			      			$em->persist($totalrenou);
									$enregistrement->addTotalrenou($totalrenou);
			      	}
			      $em->flush();
			      $chants=$enregistrement->getEnregistrementChantierMateriels();
				    $this->addFlash('success','Enregistrement créé');
						return $this->redirectToRoute('acces_sbeae');
						}
			    }

        return $this->render('FrontUser/'.$formulaire, array(
					'form' 			=> $form->createView(),
					'date'          => $date,
					'chantier'          => $chantier,
					'titre'			=> $titre,
					'responsable'			=> $responsable,
        ));
    }

	public function histo(Request $request, $page, $type)
    {
		$this->titre='Historique';
	  	$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$page=1;
		$nbPerPage = $this->getParameter('nbPerPage');
		//-------------FORMULAIRE---------------------
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(\App\Form\FrontEnregistrementType::class, $enregistrement);
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$cloture=$form->get('cloture')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('now -1 year');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$enregs = $em->getRepository('App:Enregistrement')->findFrontEnregistrements($page, $nbPerPage, $enregistrementCriteres, $user, $date1, $date2, $cloture,$type);
		$nbPages = ceil(count($enregs) / $nbPerPage);

		$totaux=array();
		if($type == 1 ){$twig='enregistrement.html.twig';}
		else{
    foreach ($enregs as $enreg){
      $renous=$em->getRepository('App:Totalrenou')->findTotalRenousEnreg($enreg);
        foreach($renous as $renou){
            $totaux[$enreg->getId()][$renou['prestation']]=$renou['total'];
        }
    }
			$twig='enregistrementRenou.html.twig';
		}

        return $this->render('FrontUser/'.$twig, array(
        	'totaux' => $totaux,
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
			'form' 			=> $form->createView(),
        ));
    }

		public function detailhisto($id)
    {
		//$tournees = $enregistrement->getTournees();
    $em = $this->getDoctrine()->getManager();
    $enregistrement = $em->getRepository("App:Enregistrement")->find($id);
        return $this->render('FrontUser/detailenregistrement.html.twig', array(
            'enreg' => $enregistrement
		));
    }

	public function indexPointage(Request $request, $page)
    {
		$this->titre='Pointages';
	  $em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$page=1;
		$nbPerPage = $this->getParameter('nbPerPage');
		//-------------FORMULAIRE---------------------
		$pointage = new Pointage();
		$form = $this->get('form.factory')->create(\App\Form\FrontPointageType::class, $pointage);
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('now -1 year');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$pointages = $em->getRepository('App:Pointage')->findFrontPointages($page, $nbPerPage, $user, $date1, $date2);
		$nbPages = ceil(count($pointages) / $nbPerPage);

		$totaux=array();

		$twig='pointages.html.twig';


        return $this->render('FrontUser/'.$twig, array(
        	'totaux' => $totaux,
            'pointages' => $pointages,
					'titre' => $this->titre,
					'nbPages'     	=> $nbPages,
					'page'        	=> $page,
			'form' 			=> $form->createView(),
        ));
    }
		//choix chantier
	public function Erreur($message)
    {
		return new Response($message);
	}
}
