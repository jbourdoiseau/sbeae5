<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Service\FonctionsService;

use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\Materielrenou;

use App\Repository\MaterielrenouRepository;


class MaterielController extends AbstractController
{

    public function __construct(FonctionsService $fonctions)
    {
        $this->titre = 'Matériel';
        $this->fonctions = $fonctions;
    }

	public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);
        $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
        if($flag_hebline == true){$zone=null;}

    		$enregs = $em->getRepository('App:Chantier')->findChantierZone($zone);

    		$countUsers=$countPaniers=array();
    		if(isset($enregs)){
    		foreach($enregs as $chantier){
    		$countUsers[$chantier->getId()] = count($chantier->getChantierusers());
    		}
    		}

        return $this->render('Generique/Chantier/index.html.twig', array(
            'repo'      => $repo,
            'enregs' => $enregs,
      			'titre' => $this->titre,
      			'countUsers' => $countUsers
        ));
    }


		public function users(Request $request, Chantier $chantier)
    {
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();
     if($chantier->getTypechantier()->getId() == 1){$role='FRONT';}
    if($chantier->getTypechantier()->getId() == 2){$role='RENOU';}
    $listusers=$em->getRepository('App:User')->getUsersFrontZone(null, $role);
		$listusersOK=$listusersPanierOK=$listusersCommentaire=$chantieruserId=array();
		foreach($chantier->getChantierusers() as $chantieruser){
			$user_id = $chantieruser->getUser()->getId();
		$listusersOK[$user_id]=1;
		$listusersPanierOK[$user_id]= $chantieruser->getPanier();
		$listusersCommentaire[$user_id]= $chantieruser->getCommentaire();
		$chantieruserId[$user_id]= $chantieruser->getId();
		}
        return $this->render('Admin/Chantier/users.html.twig', array(
            'chantier' => $chantier,
			'listusers' => $listusers,
			'listusersOK' => $listusersOK,
			'listusersPanierOK' => $listusersPanierOK,
			'listusersCommentaire' => $listusersCommentaire,
			'chantieruserId' => $chantieruserId,
        ));
    }
  public function addGen(Request $request, Chantier $chantier = null)
    {
        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);

        $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
        $editForm =  $this->createForm('App\Form\GeneriqueChantierType', $chantier, array('flag_hebline' => $flag_hebline));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone();
        $enreg=$editForm->getData();
        if($flag_hebline == true){$zone = $editForm->get('zone')->getData();}

        $enreg->setZone($zone);
        $enreg->addZone($zone);
        $em = $this->getDoctrine()->getManager();
        $em->persist($enreg);
        $em->flush();
         //$this->addFlash('success','Nouvel enregistrement ok');
              //return $this->redirectToRoute('admin_chantiers');
          return $this->redirectToRoute($repo.'_chantiers_add2', array('id' => $enreg->getId()));
        }

        return $this->render('Generique/Chantier/edit.html.twig', array(
            'repo' => $repo,
            'edit_form' => $editForm->createView(),
            'titre' => $this->titre,
        ));
    }

    public function addGen2(Request $request, Chantier $chantier)
    {
        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);

        if($chantier->getTypechantier()->getId() == 1){
        $editForm =  $this->createForm('App\Form\ChantierReleveType', $chantier);
        }
        if($chantier->getTypechantier()->getId() == 2){
        $editForm =  $this->createForm('App\Form\ChantierRenouType', $chantier);
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $enreg=$editForm->getData();
          $em = $this->getDoctrine()->getManager();
          $em->persist($enreg);
          $em->flush();
          $this->addFlash('success','Nouvel enregistrement ok');
                //return $this->redirectToRoute('admin_chantiers');
          return $this->redirectToRoute($repo.'_chantiers');
        }

        return $this->render('Generique/Chantier/edit.html.twig', array(
            'repo' => $repo,
            'edit_form' => $editForm->createView(),
            'titre' => $this->titre,
        ));
    }


	public function usersHebline(Request $request, Chantier $chantier)
    {
		$em = $this->getDoctrine()->getManager();
		$zone = $chantier->getZone();
    $role = null;
    if($chantier->getTypechantier()->getId() == 1){$role='FRONT';}
    if($chantier->getTypechantier()->getId() == 2){$role='RENOU';}
		$listusers=$em->getRepository('App:User')->getUsersFrontZone(null, $role);
		$listusersOK=$listusersPanierOK=$listusersCommentaire=$chantieruserId=array();
		foreach($chantier->getChantierusers() as $chantieruser){
			$user_id = $chantieruser->getUser()->getId();
  		$listusersOK[$user_id]=1;
  		$listusersPanierOK[$user_id]= $chantieruser->getPanier();
  		$listusersCommentaire[$user_id]= $chantieruser->getCommentaire();
  		$chantieruserId[$user_id]= $chantieruser->getId();
		}
        return $this->render('Hebline/Chantier/users.html.twig', array(
            'chantier' => $chantier,
			'listusers' => $listusers,
			'listusersOK' => $listusersOK,
			'listusersPanierOK' => $listusersPanierOK,
			'listusersCommentaire' => $listusersCommentaire,
			'chantieruserId' => $chantieruserId,
        ));
    }



	public function useradd(Request $request, Chantier $chantier, User $user, $panier)
    {
		$em = $this->getDoctrine()->getManager();
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){$em->remove($chantieruser);}
		$chantieruser=new ChantierUser();
		$chantieruser->setChantier($chantier);
		$chantieruser->setUser($user);
		$chantieruser->setPanier($panier);
		$em->persist($chantieruser);
		$em->flush();
		return new Response(1);
	}

	public function userremove(Request $request, Chantier $chantier, User $user, $panier)
    {
		$em = $this->getDoctrine()->getManager();
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){$em->remove($chantieruser);}
		$em->flush();
		return new Response(1);
	}

	public function commentaire(Request $request, Chantier $chantier, User $user, $commentaire = null)
    {
		$em = $this->getDoctrine()->getManager();
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){
		$chantieruser->setCommentaire($commentaire);
		$em->persist($chantieruser);
		$em->flush();
		}
		return new Response(1);
	}

}
