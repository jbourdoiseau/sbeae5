<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Service\FonctionsService;
use App\Service\SMSService;


use App\Entity\User;

use App\Form\UserAdminType;
use App\Form\UserHeblineType;

/**
 * User controller.
 *
 */
class UserController extends AbstractController
{


  public function __construct(FonctionsService $fonctions, SMSService $sms, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->fonctions = $fonctions;
        $this->sms = $sms;
        $this->passwordEncoder = $passwordEncoder;
    }

	public function userActive(Request $request, $id, $active)
    {
		$em = $this->getDoctrine()->getManager();
    $user = $em->getRepository("App:User")->find($id);
    $user->setEnabled($active);
		$em->flush();
		return new Response(1);
	}

public function getUsersGen(Request $request)
    {

    $message='';
    $message=$request->get('message');

    $em = $this->getDoctrine()->getManager();
    $zone = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone);

    $ref=null;

    $form = $this->get('form.factory')->create(UserAdminType::class, null, array('ref' => $ref));

    $form->handleRequest($request);

    $ref=$form->get('ref')->getData();
    $page=$form->get('page')->getData();
    if ($page < 1) {
        $page=1;
    }
    $nbPerPage = $this->getParameter('nbPerPage');



    // liste de toutes les users
    if($this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI') == false){
     $users = $this->getDoctrine()
      ->getManager()
      ->getRepository('App:User')
      ->getUsersFront($page, $nbPerPage, $ref, null)
    ;
    }
    else
    {
     $users = $this->getDoctrine()
      ->getManager()
      ->getRepository('App:User')
      ->getUsers($page, $nbPerPage, $ref, null);
    }

    $nbPages = ceil(count($users) / $nbPerPage);
        //$userManager = $this->get('fos_user.user_manager');
        //$users = $userManager->findUsers();
        return $this->render('Generique/User/front.html.twig', array(
          'repo'      => $repo,
      'nbPages'       => $nbPages,
      'page'          => $page,
            'users' => $users,
      'form'      => $form->createView(),
      'message' => $message
        ));
    }

  public function detailUserGen($id)
  {
    $em = $this->getDoctrine()->getManager();
    $user= $em->getRepository("App:User")->find($id);
    $zone = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone);
    $message='';
        return $this->render('Generique/User/detailfront.html.twig', array('user' => $user, 'repo' => $repo));

  }

  public function addUserGen(Request $request, $id = null)
    {
    $message='';

    $em = $this->getDoctrine()->getManager();
    if(null != $id){
    $user = $em->getRepository("App:User")->find($id);}
    $zone = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone);

    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    $editForm = $this->createForm('App\Form\UtilisateurGeneriqueType', null, array('password' => true,'flag_hebline' => $flag_hebline));

    $editForm->handleRequest($request);

    $session = $request->getSession();

      if ($editForm->isSubmitted() && $editForm->isValid()){

      $utilisateur=$editForm->getData();
      $listUtilisateurs = $this->getDoctrine()
      ->getManager()
      ->getRepository('App:User')
      ->findByUsername($utilisateur->getUsername());

      if(count($listUtilisateurs) == 0){
      $em = $this->getDoctrine()->getManager();
      if($utilisateur->getPlain() != ''){
         $utilisateur->setPassword($this->passwordEncoder->encodePassword($utilisateur, $utilisateur->getPlain()));
      }
/**/
        $roles=$editForm->get('roles')->getData();
        if($roles != ''){
                $utilisateur->setRoles($roles);
        }

      if($flag_hebline == false){
        $user=$this->getUser();
        $zone=$user->getZone();
        $utilisateur->setZone($zone);
      }


      $em->persist($utilisateur);
      $em->flush();


      $message='Merci de votre enregistrement';

      return $this->redirectToRoute($repo.'_users', array('message' => $message));

      }
      else{
      $message='Ce pseudo est déja enregistré';
      }
    }

    return $this->render('Generique/User/register.html.twig', array('edit_form' => $editForm->createView(),'message' => $message, 'repo' => $repo));
    }



    public function editUserGen(Request $request, $id)
    {
    $em = $this->getDoctrine()->getManager();
    $user= $em->getRepository("App:User")->find($id);
    $zone = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone);
    $message= '';
    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    $editForm = $this->createForm('App\Form\UtilisateurGeneriqueType', $user, array('password' => false,'flag_hebline' => $flag_hebline));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $user->setLastLogin(new \DateTime());
            if($user->getPlain() != ''){
               $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlain()));
            }
            $roles=$editForm->get('roles')->getData();
            if($roles != ''){
              $user->setRoles($roles);

            }
            $em->flush();
            //return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('Generique/User/register.html.twig', array('edit_form' => $editForm->createView(),'message' => $message, 'repo' => $repo));

  }

	public function SMSPassword($id, $page)
	{

		$message='';
    $em = $this->getDoctrine()->getManager();
    $utilisateur = $em->getRepository("App:User")->find($id);
		$nom= $utilisateur->getNom();
		$prenom= $utilisateur->getPrenom();
		$mobile= $utilisateur->getMobile();
		$login= $utilisateur->getUsername();
		$pass= $utilisateur->getPlain();
		$url=$this->getParameter('url');
		$message="Bonjour $prenom $nom, Pour vous connecter, utilisez votre identifiant : $login\n et votre passe : $pass\na l'adresse $url\nSBEAE\n";
		$message=$this->sms->smsSend($mobile, $message);
        return $this->redirectToRoute('admin_users', array('message' => $message, 'page' => $page));
	}

	public function HeblineSMSPassword($id, $page)
	{

		$message='';
    $em = $this->getDoctrine()->getManager();
    $utilisateur = $em->getRepository("App:User")->find($id);
		$nom= $utilisateur->getNom();
		$prenom= $utilisateur->getPrenom();
		$mobile= $utilisateur->getMobile();
		$login= $utilisateur->getUsername();
		$pass= $utilisateur->getPlain();
		$url=$this->getParameter('url');
		$message="Bonjour $prenom $nom, Pour vous connecter, utilisez votre identifiant : $login\n et votre passe : $pass\na l'adresse $url\nSBEAE\n";
		$message=$this->sms->smsSend($mobile, $message);
        return $this->redirectToRoute('hebline_users', array('message' => $message, 'page' => $page));
	}

	public function SMSUser(Request $request, $id = null)
    {
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone()->getId();
		$message='';
		if($id!=null){$user=$em->getRepository('App:User')->find($id);}else{$user=null;}
        $editForm = $this->createForm('App\Form\SMSUserType', null, array('zone' => $zone, 'user' => $user));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
 		    $id__user=$editForm->get('user')->getData();
			$userf = $em->getRepository('App:User')->find($id__user);
		    $mess=$editForm->get('message')->getData();
			if($userf->getMobile() != ''){
						$mobile= $userf->getMobile();
						$message=$this->sms->smsSend($mobile, $mess);

			}

        }

		if($zone==1001){$rep="SuperAdmin";}
		else if($zone==1000){$rep="Hebline";}
		else{$rep="Admin";}

        return $this->render($rep.'/User/sms.html.twig', array(
            'edit_form' => $editForm->createView(),
			'message' => $message

        ));

	}
}
