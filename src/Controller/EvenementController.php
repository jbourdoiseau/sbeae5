<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use App\Service\FonctionsService;

use App\Entity\User;
use App\Entity\Evenement;

use Intervention\Image\ImageManager;

class EvenementController extends AbstractController
{

	private $mailer;

	public function __construct(MailerInterface $mailer, FonctionsService $fonctions)
	{
		    $this->upload = '/var/www/vhosts/releve-eae.com/public/uploads/documents/';
		    $this->sujets=array("","Demande arret maladie","Demande conges","Accident","Reclamation client","Problème matériel","Autre demande","Accident du travail","Autre absence","Commentaire CA");
        $this->mailer = $mailer;
        $this->fonctions = $fonctions;
    }
	public function edit(Request $request, $id)
    {

			    $titre="Editer un événement";
					$em = $this->getDoctrine()->getManager();
					$evenement=$em->getRepository("App:Evenement")->find($id);
					$user=$this->getUser();
		      $zone=$user->getZone();
			    $repo = $this->fonctions->getRepo($zone->getId());
					$responsable=$user->getZone()->getResponsable();

					if(($evenement->getType() < 3) || ($evenement->getType() > 6)){
						$form = $this->get('form.factory')->create(\App\Form\EvenementType::class, $evenement,array('type' => $evenement->getType()));
					}else{
						$form = $this->get('form.factory')->create(\App\Form\EvenementType2::class, $evenement,array('type' => $evenement->getType()));
					}
					$form->handleRequest($request);

				if ($form->isSubmitted() && $form->isValid()) {
					//$evenement->setEtat(1);

					$target = $evenement->getType();
					$sujet = $this->sujets[$target];

					$em->persist($evenement);
		      $em->flush();
		      //redim image
		      /*
		      $manager = new ImageManager();
					if($evenement->getDocname1()!=''){

								 	$filepath = $this->upload.$evenement->getDocname1();
										$manager->make($filepath)->resize(1080, null, function($constraint){
										    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
										    $constraint->upsize(); // empêche l'image d'être agrandie
										})->save($filepath);

					}
					if($evenement->getDocname2()!=''){
								 	$filepath = $this->upload.$evenement->getDocname2();
										$manager->make($filepath)->resize(1080, null, function($constraint){
										    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
										    $constraint->upsize(); // empêche l'image d'être agrandie
										})->save($filepath);
					}
					*/
					$this->addFlash('success','Enregistrement modifié');
					return $this->redirectToRoute($repo.'_evenements2', array('target' => $target));
				}

				return $this->render('Generique/Evenement/evenement_add.html.twig', array(
				'titre'			=> $titre,
				'repo'      => $repo,
				'edit_form' 			=> $form->createView(),
			));

	}


	public function Index(Request $request, $target = null)
    {
		$titre="Déclarer un événement";
		$em = $this->getDoctrine()->getManager();
		$user=$this->getUser();
		$zone=$user->getZone();
		$responsable=$user->getZone()->getResponsable();
		$evenement = new Evenement();
		$form_conges = $this->get('form.factory')->create(\App\Form\EvenementType::class, $evenement,array('type' => 0));
		$form_demande = $this->get('form.factory')->create(\App\Form\EvenementType2::class, $evenement,array('type' => 0));


		$form_conges->handleRequest($request);
		$form_demande->handleRequest($request);

		$conges=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 2), array('datedebut' => 'DESC'));
		$maladies=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 1), array('datedebut' => 'DESC'));
		$accidents=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 3), array('datedebut' => 'DESC'));
		$reclams=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 4), array('datedebut' => 'DESC'));
		$materiels=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 5), array('datedebut' => 'DESC'));
		$demandes=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 6), array('datedebut' => 'DESC'));
		$travails=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 7), array('datedebut' => 'DESC'));
		$absences=$em->getRepository('App:Evenement')->findBy(array('user' => $user, 'type' => 8), array('datedebut' => 'DESC'));

		if (($form_conges->isSubmitted() && $form_conges->isValid())||($form_demande->isSubmitted() && $form_demande->isValid())) {
			$evenement->setEtat(1);
			$target = $evenement->getType();
			$sujet = $this->sujets[$target];
			//echo 'sujet'.$sujet;exit;
			if($evenement->getType() == 1){$evenement->setEtat(0);}
			if($evenement->getType() == 2){$evenement->setEtat(0);}
			if($evenement->getType() == 7){$evenement->setEtat(0);}
			if($evenement->getType() == 8){$evenement->setEtat(0);}


			//echo 'type'.$evenement->getType();
			$evenement->setUser($user);
			$evenement->setZone($zone);
			$em->persist($evenement);
      $em->flush();
      $message="Nouvelle demande de ".$user->getNom().' '.$user->getPrenom()."\nLe : ".$evenement->getDatecrea()->format('d-m-Y')." (".$evenement->getId().")\n";
      $email= $user->getZone()->getResponsable()->getEmail();
      $this->sendConfirmationEmailMessage($email, $sujet, $message);

      //redim image
      /*
      $manager = new ImageManager();
			if($evenement->getDocname1()!=''){

						 	$filepath = $this->upload.$evenement->getDocname1();
								$manager->make($filepath)->resize(1080, null, function($constraint){
								    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
								    $constraint->upsize(); // empêche l'image d'être agrandie
								})->save($filepath);

			}
			if($evenement->getDocname2()!=''){
						 	$filepath = $this->upload.$evenement->getDocname2();
								$manager->make($filepath)->resize(1080, null, function($constraint){
								    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
								    $constraint->upsize(); // empêche l'image d'être agrandie
								})->save($filepath);
			}
			*/
			$this->addFlash('success','Enregistrement créé');
			//return $this->redirectToRoute('front_evenements2', array('target' => $target));
		}


			return $this->render('FrontUser/evenement.html.twig', array(
				'titre'			=> $titre,
				'target'     => $target,
				'responsable'			=> $responsable,
				'form_conges' 			=> $form_conges->createView(),
				'form_demande' 			=> $form_demande->createView(),
				'conges' 			=> $conges,
				'maladies' 			=> $maladies,
				'accidents' 			=> $accidents,
				'reclams' 			=> $reclams,
				'materiels' 			=> $materiels,
				'demandes' 			=> $demandes,
				'travails' 			=> $travails,
				'absences' 			=> $absences,
			));

	}

public function IndexHebline(Request $request, $target = null)
    {
		$titre="Déclarer un événement";
		$em = $this->getDoctrine()->getManager();

		$zone = $this->getUser()->getZone()->getId();
		$repo="admin";
		if($zone==1001){;$repo="superadmin";}
		if($zone==1000){$repo="hebline";}

		$evenement = new Evenement();
		$form_conges = $this->get('form.factory')->create(\App\Form\EvenementGenType::class, $evenement,array('type' => 0));
		$form_demande = $this->get('form.factory')->create(\App\Form\EvenementGenType2::class, $evenement,array('type' => 0));


$form_conges->handleRequest($request);
$form_demande->handleRequest($request);


$conges=$em->getRepository('App:Evenement')->findBy(array('type' => 2), array('datedebut' => 'DESC'));
$maladies=$em->getRepository('App:Evenement')->findBy(array('type' => 1), array('datedebut' => 'DESC'));
$accidents=$em->getRepository('App:Evenement')->findBy(array('type' => 3), array('datedebut' => 'DESC'));
$reclams=$em->getRepository('App:Evenement')->findBy(array('type' => 4), array('datedebut' => 'DESC'));
$materiels=$em->getRepository('App:Evenement')->findBy(array('type' => 5), array('datedebut' => 'DESC'));
$demandes=$em->getRepository('App:Evenement')->findBy(array('type' => 6), array('datedebut' => 'DESC'));
$travails=$em->getRepository('App:Evenement')->findBy(array('type' => 7), array('datedebut' => 'DESC'));
$absences=$em->getRepository('App:Evenement')->findBy(array('type' => 8), array('datedebut' => 'DESC'));
$commentaires=$em->getRepository('App:Evenement')->findBy(array('type' => 9), array('datedebut' => 'DESC'));

		if (($form_conges->isSubmitted() && $form_conges->isValid())||($form_demande->isSubmitted() && $form_demande->isValid())) {
			$evenement->setEtat(1);
			$target = $evenement->getType();
			$sujet = $this->sujets[$target];
			$evenement->setZone($evenement->getUser()->getZone());
			$em->persist($evenement);
      $em->flush();
      //redim image
      /*
      $manager = new ImageManager();
			if($evenement->getDocname1()!=''){

						 	$filepath = $this->upload.$evenement->getDocname1();
								$manager->make($filepath)->resize(1080, null, function($constraint){
								    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
								    $constraint->upsize(); // empêche l'image d'être agrandie
								})->save($filepath);

			}
			if($evenement->getDocname2()!=''){
						 	$filepath = $this->upload.$evenement->getDocname2();
								$manager->make($filepath)->resize(1080, null, function($constraint){
								    $constraint->aspectRatio(); // Conserve le ratio largeur / hauteur
								    $constraint->upsize(); // empêche l'image d'être agrandie
								})->save($filepath);
			}
			*/
			$this->addFlash('success','Enregistrement créé');
			return $this->redirectToRoute($repo.'_evenements2', array('target' => $target));
		}


			return $this->render('Generique/Evenement/evenement.html.twig', array(
				'titre'			=> $titre,
				'repo'      => $repo,
				'target' => $target,
				'form_conges' 			=> $form_conges->createView(),
				'form_demande' 			=> $form_demande->createView(),
				'conges' 			=> $conges,
				'maladies' 			=> $maladies,
				'accidents' 			=> $accidents,
				'reclams' 			=> $reclams,
				'materiels' 			=> $materiels,
				'demandes' 			=> $demandes,
				'travails' 			=> $travails,
				'absences' 			=> $absences,
				'commentaires' => $commentaires
			));

	}

	public function histo(Request $request,$page)
    {
		$this->titre='Historique';
	  	$em = $this->getDoctrine()->getManager();
		$user = $this->getUser();
		$page=1;
		$nbPerPage = $this->getParameter('nbPerPage');
		//-------------FORMULAIRE---------------------
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(\App\Form\FrontEnregistrementType::class, $enregistrement);
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$cloture=$form->get('cloture')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('now -1 year');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$enregs = $em->getRepository('App:Enregistrement')->findFrontEnregistrements($page, $nbPerPage, $enregistrementCriteres, $user, $date1, $date2, $cloture);
		$nbPages = ceil(count($enregs) / $nbPerPage);
        return $this->render('FrontUser/enregistrement.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
			'form' 			=> $form->createView(),
        ));
    }
	public function delete ($id)
    {
		$em = $this->getDoctrine()->getManager();
		$em->remove($em->getRepository("App:Evenement")->find($id));
		$em->flush();
		$this->addFlash('success','Evenement supprimé');
		return $this->redirectToRoute('hebline_evenements2');

	}

	public function update ($id)
    {
		$em = $this->getDoctrine()->getManager();
		$evenement = $em->getRepository("App:Evenement")->find($id);
		$etat=$evenement->getEtat();
		$etat=$etat-1;
		if($etat == -1){$etat=2;}
		$evenement->setEtat($etat);
		$em->flush();
		return new Response($etat);
	}

		//choix chantier
	public function Erreur($message)
    {
		return new Response($message);
	}

	public function sendConfirmationEmailMessage($email, $subject, $mess)
			{
/*
				$transport = (new \Swift_SmtpTransport('smtp.devz2.com', 25))
				  ->setUsername('contact@devz2.com')
				  ->setPassword('loulou24*')
				  ;
				$mailer = new \Swift_Mailer($transport);

				$emailcontact = 'j.b@hebline.com';

				$message = (new \Swift_Message($subject));
				$message->setFrom($this->getParameter('email'));
				if($email == ''){$email = 'j.b@hebline.com';}



				$message->setTo($emails)->setBody($mess, 'text/html');

				try {
					$result = $mailer->send($message);
				}
				catch (\Swift_TransportException $e) {
					echo $e->getMessage();
					exit;
				}*/
				if($email == ''){$email = 'j.b@hebline.com';}
				$emails=array($email,'j.b@hebline.com','f.m@hebline.com');
				$email = (new Email())
                ->from($this->getParameter('email'))
                ->to(...$emails)
                ->subject($subject)
                ->html($mess);
            $this->mailer->send($email);

	}
}
