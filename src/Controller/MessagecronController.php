<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use App\Entity\Messagecron;


class MessagecronController extends AbstractController
{

	public function __construct()
    {
        $this->titre = 'Message';
    }


	 public function edit(Request $request)
    {

	    $em = $this->getDoctrine()->getManager();
        $message=$em->getRepository("App:Messagecron")->find(1);
        $editForm =  $this->createForm('App\Form\MessagecronType', $message);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$em->flush();
        }

        return $this->render('Hebline/Messagecron/edit.html.twig', array(
            'form' => $editForm->createView(),
        ));
    }


}
