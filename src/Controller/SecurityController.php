<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SecurityController extends AbstractController{


    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
            return $this->render('Security/login_utilisateur.html.twig', array(
                'last_username' => $lastUsername,'error' => $error
            ));
    }

    public function logout()
    {
      return $this->redirect('https://www.releve-eae.com');
      //throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    public function myLogoutAction(TokenStorageInterface $tokenStorage, Request $request)
        {
            $token = new AnonymousToken('default', 'anon.');
            $tokenStorage->setToken($token);
            $request->getSession()->invalidate();
            return $this->redirect('https://www.releve-eae.com');
    }



	public function requestAction()
    {
			return $this->render('Security/reset.html.twig', array(
				'error' => $error
			));
    }

    public function loginCheck()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
    public function checkUser($info){
        if($info->getUser() == null){
               return $this->render('Security/login_utilisateur.html.twig', array(
                'error' => $error
            ));
        }
    }

}
