<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

// Include PhpSpreadsheet required namespaces
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use App\Service\FonctionsService;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\Facture;

class FactureController extends AbstractController
{

	public function __construct(FonctionsService $fonctions)
    {
        $this->titre = 'Export facturation';
        $this->fonctions = $fonctions;
    }
	public function Index(Request $request)
    {

    $em = $this->getDoctrine()->getManager();
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();

    $repo = $this->fonctions->getRepo($zone_id);
    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    if($flag_hebline == true){$zone=null;}

    $page=1;
    $nbPerPage = $this->getParameter('nbPerPage');

		//-------------FORMULAIRE---------------------
		$excel = 0;
		$enregs=array();
		$facture = new Facture();
		$form = $this->get('form.factory')->create(\App\Form\FactureType::class, $facture, array('zone' => $zone));

		$form->handleRequest($request);
		$excel=$form->get('excel')->getData();

		if ($form->isSubmitted() && $form->isValid()) {
		if($excel == 1){return $this->exportFactures($facture->getChantier(),$facture->getDatedebut(), $facture->getDatefin());}
		}

        return $this->render('Generique/Facture/index.html.twig', array(
        		'repo' => $repo,
						'titre' => $this->titre,
						'form' 			=> $form->createView(),
        ));
    }


    public function exportFactures ($chantier, $date1, $date2){
$em = $this->getDoctrine()->getManager();
if($chantier != null){
		//recup des clotures
		$enregistrementCriteres = new Enregistrement();
		$enregistrementCriteres->setChantier($chantier);
    $clotures = $em->getRepository('App:Enregistrement')->findEnregistrementsClotures($enregistrementCriteres, null, $date1, $date2, 1);
		if (count ($clotures) == 0) {return new Response('Aucune cloture'); exit;}
		//recup des journées forfaits
		$forfaits = $em->getRepository('App:Enregistrement')->findEnregistrementsForfait($chantier, $date1, $date2);

		//recup des journées forfaits
		$forfaitsUser = $em->getRepository('App:Enregistrement')->findEnregistrementsForfaitUser($chantier, $date1, $date2);

		//decompte des jours travaillés
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrementsChantierDates($chantier, $date1, $date2);
		$dayswork = array();
		foreach ($enregs as $enreg){
			$date_rel=$enreg->getDatereleve()->format('Y-m-d');
			$user_id=$enreg->getUser()->getId();
			if($enreg->getTotal() >= 0){
			$dayswork[$user_id][$date_rel]=1;
			}
		}
$nbdays = count($dayswork, COUNT_RECURSIVE);


		$datenow=new \DateTime('now');
		//echo $date1->format('Y-m-d');;
		$date3='Export_'.$datenow->format('d-m-Y').'-'.time();
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $spreadsheet->getProperties()->setCreator("Hebline.com")->setLastModifiedBy("Hebline.com")->setCategory("");
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Export_'.$datenow->format('d-m-Y'));

		//$sheet = $spreadsheet->getSheet(0);

		$startDate = $date1;
		$stopDate = $date2;

		//$sheet->setCellValueByColumnAndRow(1, 2, $date1->format('d-m-Y'));
		$styleArray = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '337ab7'),
				'size'  => 10,
				'name'  => 'Verdana'
			));
		$styleArray2 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => 'ffffff'),
				'size'  => 12,
				'name'  => 'Verdana'
			));
		$styleArray3 = array(
			'font'  => array(
				'bold'  => true,
				'color' => array('rgb' => '000000'),
				'size'  => 12,
				'name'  => 'Verdana'
			));
		$fill_blue = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => '0000FF'],
				)
			);
		$fill_yellow = array(
				'fill' => array(
					'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
					'color' => ['argb' => 'FFFF00'],
				)
			);
    $borderDateOn = array(
				'borders' => array(
				'top' => array(
					'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
					'color' => array('argb' => 'FF3469F7'),
				),
			),
		);

		$sheet->getColumnDimension('A')->setAutoSize(true);
		$sheet->getColumnDimension('B')->setAutoSize(true);
		$sheet->getColumnDimension('C')->setAutoSize(true);
		$sheet->getColumnDimension('D')->setAutoSize(true);
		$sheet->getColumnDimension('E')->setAutoSize(true);
		$sheet->getColumnDimension('F')->setAutoSize(true);

		$q=1;
		$titre='Facturation '.$chantier->getNom();
		$sheet->setCellValue('A'.$q, $titre);
		$q++;
		$titre='Période : Du '.$date1->format('d-m-Y').' au '.$date2->format('d-m-Y');
		$sheet->setCellValue('A'.$q, $titre);
		$q++;
		$q++;
		$k=1;
		$spreadsheet->getActiveSheet()->mergeCells("A$q:K$q");
		$sheet->setCellValue('A'.$q, 'FACTURATION - Détail Clotûre');
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
		$q++;
		$sheet->setCellValue('A'.$q, 'Id Enreg');
		$sheet->setCellValue('B'.$q, 'Id Secteur');
		$sheet->setCellValue('C'.$q, 'Secteur');
		$sheet->setCellValue('D'.$q, 'Date');
		$sheet->setCellValue('E'.$q, 'Zone');
		$sheet->setCellValue('F'.$q, 'Soc');
		$sheet->setCellValue('G'.$q, 'Site');
		$sheet->setCellValue('H'.$q, 'RR');
		$sheet->setCellValue('I'.$q, 'Utilisateur');
		$sheet->setCellValue('J'.$q, 'Relevé');
		$sheet->setCellValue('K'.$q, 'Infructueux');
		$sheet->setCellValue('L'.$q, 'Total');
		$sheet->setCellValue('M'.$q, 'Commentaire');
		for($k=1; $k<12; $k++){
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('00FFFF');
		}

// tri des clotures par secteur $clotures
				$releveT = 0;
				$infructueuxT = 0;
				$restantT = 0;
				$totalT = 0;
				$releveRR = 0;
				$infructueuxRR = 0;
				$restantRR = 0;
				$totalRR = 0;
				$secteur0='';
				$z=0;

		$arraysecteur=$total_releve_secteur=$total_inf_secteur=$total_releve_RR_secteur=$total_inf_RR_secteur=array();
		foreach ($clotures as $enreg){
				foreach ($enreg->getTournees() as $tournee ){
						//recup du secteur
						$code=$tournee->getCode();
						if(preg_match('#(\w+)-(\w+)-(\w+)#', $code, $matches)){
							$code=$matches[1];
						}
						$arraysecteur[$code][]=$tournee;
				}
		}
		$secteurs = array_keys( $arraysecteur);

		foreach ($secteurs as $secteur){

			$tournees = $arraysecteur[$secteur];

			foreach ($tournees as $tournee){
			//echo $enreg->getId()."<br>\n";

			$k=0;
				$q++;
				$z++;
				$secteur=$tournee->getEnregistrement()->getSecteur();
				if($z == 1){$secteur0=$secteur;}

				if($secteur0 != $secteur){
				$sheet->setCellValue('A'.$q, 'TOTAL SECTEUR');
				$sheet->setCellValue('B'.$q, '');
				$sheet->setCellValue('C'.$q, '');
				$sheet->setCellValue('D'.$q, '');
				$sheet->setCellValue('E'.$q, '');
				$sheet->setCellValue('F'.$q, '');
				$sheet->setCellValue('G'.$q, '');
				$sheet->setCellValue('H'.$q, '');
				$sheet->setCellValue('I'.$q, '');
				$sheet->setCellValue('J'.$q, $releveT);
				$sheet->setCellValue('K'.$q, $infructueuxT);
				$sheet->setCellValue('L'.$q, $totalT);
				$sheet->setCellValue('M'.$q, '');
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->getStartColor()->setARGB('00FFFF');
				$total_rel_secteur[$secteur0]=$releveT;
				$total_inf_secteur[$secteur0]=$infructueuxT;
				$q++;
				$sheet->setCellValue('A'.$q, 'TOTAL SECTEUR RR');
				$sheet->setCellValue('B'.$q, '');
				$sheet->setCellValue('C'.$q, '');
				$sheet->setCellValue('D'.$q, '');
				$sheet->setCellValue('E'.$q, '');
				$sheet->setCellValue('F'.$q, '');
				$sheet->setCellValue('G'.$q, '');
				$sheet->setCellValue('H'.$q, '');
				$sheet->setCellValue('I'.$q, '');
				$sheet->setCellValue('J'.$q, $releveRR);
				$sheet->setCellValue('K'.$q, $infructueuxRR);
				$sheet->setCellValue('L'.$q, $totalRR);
				$sheet->setCellValue('M'.$q, '');
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->getStartColor()->setARGB('00FFFF');
				$total_rel_RR_secteur[$secteur0]=$releveRR;
				$total_inf_RR_secteur[$secteur0]=$infructueuxRR;

				$secteur0 = $secteur;
				$releveT = 0;
				$infructueuxT = 0;
				$restantT = 0;
				$totalT = 0;
				$releveRR = 0;
				$infructueuxRR = 0;
				$restantRR = 0;
				$totalRR = 0;
				$q++;
				}

				$releve = 0;
				$infructueux = 0;
				$restant = 0;
				$total = 0;

				$releve = $tournee->getReleve();
				$infructueux = $tournee->getInfructueux();
				$total=$releve+$infructueux;

					if($enreg->getRadioreleve() == 1){
					$releveRR= $releveRR+$releve;
					$infructueuxRR= $infructueuxRR+$infructueux;
					$totalRR= $totalRR+$total;
					}
					else{
					$releveT= $releveT+$releve;
					$infructueuxT= $infructueuxT+$infructueux;
					$totalT= $totalT+$total;
					}
				$enreg =	$tournee->getEnregistrement();
				$commentaire=$tournee->getEnregistrement()->getCommentaire();
				$sheet->setCellValue('A'.$q, $enreg->getId());
				$sheet->setCellValue('B'.$q, $tournee->getId());
				$sheet->setCellValue('C'.$q, $secteur);
				$sheet->setCellValue('D'.$q, $enreg->getDatereleve()->format('d-m-Y'));
				$sheet->setCellValue('E'.$q, $enreg->getZone()->getAbrev());
				$sheet->setCellValue('F'.$q, $enreg->getChantier()->getSociete()->getNom());
				$sheet->setCellValue('G'.$q, $enreg->getChantier()->getNom());
				$sheet->setCellValue('H'.$q, $enreg->getRadioreleve());
				$sheet->setCellValue('I'.$q, $enreg->getUser()->getNom().' '.$enreg->getUser()->getPrenom());
				$sheet->setCellValue('J'.$q, $releve);
				$sheet->setCellValue('K'.$q, $infructueux);
				$sheet->setCellValue('L'.$q, $total);
				$sheet->setCellValue('M'.$q, $commentaire);
			}
}
			$q++;
				$sheet->setCellValue('A'.$q, 'TOTAL SECTEUR');
				$sheet->setCellValue('B'.$q, '');
				$sheet->setCellValue('C'.$q, '');
				$sheet->setCellValue('D'.$q, '');
				$sheet->setCellValue('E'.$q, '');
				$sheet->setCellValue('F'.$q, '');
				$sheet->setCellValue('G'.$q, '');
				$sheet->setCellValue('H'.$q, '');
				$sheet->setCellValue('I'.$q, '');
				$sheet->setCellValue('J'.$q, $releveT);
				$sheet->setCellValue('K'.$q, $infructueuxT);
				$sheet->setCellValue('L'.$q, $totalT);
				$sheet->setCellValue('M'.$q, '');
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->getStartColor()->setARGB('00FFFF');
				$total_rel_secteur[$secteur]=$releveT;
				$total_inf_secteur[$secteur]=$infructueuxT;
				$q++;
				$sheet->setCellValue('A'.$q, 'TOTAL SECTEUR RR');
				$sheet->setCellValue('B'.$q, '');
				$sheet->setCellValue('C'.$q, '');
				$sheet->setCellValue('D'.$q, '');
				$sheet->setCellValue('E'.$q, '');
				$sheet->setCellValue('F'.$q, '');
				$sheet->setCellValue('G'.$q, '');
				$sheet->setCellValue('H'.$q, '');
				$sheet->setCellValue('I'.$q, '');
				$sheet->setCellValue('J'.$q, $releveRR);
				$sheet->setCellValue('K'.$q, $infructueuxRR);
				$sheet->setCellValue('L'.$q, $totalRR);
				$sheet->setCellValue('M'.$q, '');
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->getStartColor()->setARGB('00FFFF');
				$total_rel_RR_secteur[$secteur]=$releveRR;
				$total_inf_RR_secteur[$secteur]=$infructueuxRR;
				$q++;
				//echo 'tot'.$total_rel_secteur['45'];
		$q++;
		$q++;
		$k=1;
		$forfaits_journaliers=array();
		$spreadsheet->getActiveSheet()->mergeCells("A$q:F$q");
		$sheet->setCellValue('A'.$q, 'FACTURATION - Forfaits');
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
		$q++;
		$sheet->setCellValue('A'.$q, 'DATE');
		$sheet->setCellValue('B'.$q, 'FORFAIT');
		$sheet->setCellValue('C'.$q, 'Nbre relevé');
		$sheet->setCellValue('D'.$q, 'Nbre Inf');
		$sheet->setCellValue('E'.$q, 'TRAITE');
		$sheet->setCellValue('F'.$q, 'Agent');
		$sheet->setCellValue('G'.$q, 'Commentaire');
		for($k=1; $k<7; $k++){
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FFFF00');
		}
		$nbforfait=0;
		$forfaitbysecteur=$deltarel=$deltainf=array();
		$secteur0="";
		$z=0;
		foreach ($forfaits as $forfait){

			$k=1;

			$secteur=$forfait->getSecteur();
			if($z == 0){$secteur0 = $secteur;}

			if($secteur != $secteur0){
			$q++;
			$sheet->setCellValue('A'.$q, 'Total '.$secteur0);
			$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		  $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
			if(isset($forfaitbysecteur[$secteur0])){
				$sheet->setCellValue('B'.$q, $forfaitbysecteur[$secteur0]);
				$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  	$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		  	$sheet->getStyleByColumnAndRow( $k+1, $q)->applyFromArray($styleArray2);
			}
			$secteur0 = $secteur;
			}

			if(isset($forfaitbysecteur[$secteur])){
				$forfaitbysecteur[$secteur]=$forfaitbysecteur[$secteur]+1;
				$deltarel[$secteur]+=$forfait->getReleve();
				$deltainf[$secteur]+=$forfait->getInfructueux();
			}
			else{
				$forfaitbysecteur[$secteur]=1;
				$deltarel[$secteur]=$forfait->getReleve();
				$deltainf[$secteur]=$forfait->getInfructueux();
			}
			$q++;
			$sheet->setCellValue('A'.$q, $forfait->getDatereleve()->format('Y-m-d'));
			$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FFFF00');
			$sheet->setCellValue('B'.$q, 1);
			$sheet->setCellValue('C'.$q, $forfait->getReleve());
			$sheet->setCellValue('D'.$q, $forfait->getInfructueux());
			$sheet->setCellValue('E'.$q, $forfait->getSecteur());
			$sheet->setCellValue('F'.$q, $forfait->getUser()->getNom().' '.$forfait->getUser()->getPrenom());
			$sheet->setCellValue('G'.$q, $forfait->getCommentaire());
			$nbforfait++;
			$z++;
		}
		$q++;
			$sheet->setCellValue('A'.$q, 'Total '.$secteur);
			$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		  $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
		  if(isset($forfaitbysecteur[$secteur])){$sheet->setCellValue('B'.$q, $forfaitbysecteur[$secteur]);
				$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  	$sheet->getCellByColumnAndRow( $k+1, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		  	$sheet->getStyleByColumnAndRow( $k+1, $q)->applyFromArray($styleArray2);
		}
		$q++;
			$sheet->setCellValue('A'.$q, 'Total');
			$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		  $sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
			$sheet->setCellValue('B'.$q, $nbforfait);
//forfait par utilisateur

$q++;
		$q++;
		$k=1;
		$spreadsheet->getActiveSheet()->mergeCells("A$q:F$q");
		$sheet->setCellValue('A'.$q, 'Contrôle Forfaits Utilisateur');
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
		$q++;
		$sheet->setCellValue('A'.$q, 'DATE');
		$sheet->setCellValue('B'.$q, 'FORFAIT');
		$sheet->setCellValue('C'.$q, 'Nbre relevé');
		$sheet->setCellValue('D'.$q, 'Nbre Inf');
		$sheet->setCellValue('E'.$q, 'TRAITE');
		$sheet->setCellValue('F'.$q, 'Agent');
		for($k=1; $k<7; $k++){
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FFFF00');
		}

		$secteur0="";
		$z=0;
		foreach ($forfaitsUser as $forfait){
			$k=1;

			$date=$forfait->getDatereleve()->format('Y-m-d');
			$user_id=$forfait->getUser()->getId();
			if($z == 0){
				$date0='';
				$user_id0='';
			}

			$q++;
			$sheet->setCellValue('A'.$q, $forfait->getDatereleve()->format('Y-m-d'));
			$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		  $sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('FFFF00');
			$sheet->setCellValue('B'.$q, 1);
			$sheet->setCellValue('C'.$q, $forfait->getReleve());
			$sheet->setCellValue('D'.$q, $forfait->getInfructueux());
			$sheet->setCellValue('E'.$q, $forfait->getSecteur());
			$sheet->setCellValue('F'.$q, $forfait->getUser()->getNom().' '.$forfait->getUser()->getPrenom());
			if(($user_id0 == $user_id) && ($date0 == $date)){
				$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getCellByColumnAndRow( 6, $q)->getStyle()->getFill()->getStartColor()->setARGB('FF0000');
			  $sheet->setCellValue('F'.$q, $forfait->getUser()->getNom().' '.$forfait->getUser()->getPrenom());
			}
			$date0=$date;
			$user_id0=$user_id;
			$z++;
		}
		$q++;
		$q++;
		$q++;
//tableau de synthese
		$spreadsheet->getActiveSheet()->mergeCells("A$q:F$q");
		$sheet->setCellValue('A'.$q, 'FACTURATION - Synthèse');
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( $k, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		$sheet->getStyleByColumnAndRow( $k, $q)->applyFromArray($styleArray2);
		$q++;
		$sheet->setCellValue('A'.$q, 'N Secteur');
		$sheet->setCellValue('B'.$q, 'Nature');
		$sheet->setCellValue('C'.$q, 'Quantité');
		$sheet->setCellValue('D'.$q, 'Déduction rel (forfaits jr)');
		$sheet->setCellValue('E'.$q, 'Prix unitaire');
		$sheet->setCellValue('F'.$q, 'Montant à facturer');
		$index=$q+1;
		foreach ($secteurs as $secteur){
		$q++;
		$sheet->setCellValue('A'.$q, $secteur);
		$sheet->setCellValue('B'.$q, 'Relevés');
		$sheet->setCellValue('C'.$q, ($total_rel_secteur[$secteur]??0));
		$sheet->setCellValue('D'.$q, ($deltarel[$secteur]??0));
		$sheet->setCellValue('E'.$q, ($chantier->getTarifreleve()??0));
		$sheet->setCellValue('F'.$q, '=(C'.$q.'-D'.$q.')*E'.$q);

		$q++;
		$sheet->setCellValue('A'.$q, '');
		$sheet->setCellValue('B'.$q, 'Inf');
		$sheet->setCellValue('C'.$q, ($total_inf_secteur[$secteur]??0));
		$sheet->setCellValue('D'.$q, ($deltainf[$secteur]??0));
		$sheet->setCellValue('E'.$q, ($chantier->getTarifinfructueux()??0));
		$sheet->setCellValue('F'.$q, '=(C'.$q.'-D'.$q.')*E'.$q);
		$q++;
		$sheet->setCellValue('A'.$q, '');
		$sheet->setCellValue('B'.$q, 'RR');
		$sheet->setCellValue('C'.$q, ($total_rel_RR_secteur[$secteur]??0));
		$sheet->setCellValue('D'.$q, '');
		$sheet->setCellValue('E'.$q, ($chantier->getTarifradioreleve()??0));
		$sheet->setCellValue('F'.$q, '=C'.$q.'*E'.$q);
		$q++;
		$sheet->setCellValue('A'.$q, '');
		$sheet->setCellValue('B'.$q, 'Forfait journalier');
		$sheet->setCellValue('C'.$q, ($forfaitbysecteur[$secteur]??0));
		$sheet->setCellValue('D'.$q, '');
		$sheet->setCellValue('E'.$q, ($chantier->getForfait()??0));
		$sheet->setCellValue('F'.$q, '=C'.$q.'*E'.$q);
		}
		$q++;
		$k=$q-1;


		$spreadsheet->getActiveSheet()->mergeCells("A$q:E$q");
		$sheet->setCellValue('A'.$q, 'TOTAL à FACTURER');
		$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$sheet->getCellByColumnAndRow( 1, $q)->getStyle()->getFill()->getStartColor()->setARGB('0000FF');
		$sheet->getStyleByColumnAndRow( 1, $q)->applyFromArray($styleArray2);
		$sheet->setCellValue('F'.$q, '=SUM(F'.$index.':F'.$k.')');
		$q++;
		$q++;
		$sheet->setCellValue('A'.$q,'Jours travaillés');
		$sheet->setCellValue('B'.$q, ($nbdays??0));


        // Create a Temporary file in the system
        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = 'Facture_'.$chantier->getNom().'_'.$date1->format('Y-m-d').'.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
	}
	 //return $this->render('Generique/test.html.twig');
	return new Response(1);
}

	public function sum()
	{
	    return array_sum(func_get_args());
	}
}
