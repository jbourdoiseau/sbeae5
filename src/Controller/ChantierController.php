<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Service\FonctionsService;

use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\ChantierUser;
use App\Entity\ChantierMateriel;
use App\Entity\Materiel;

use App\Repository\ChantierRepository;
use App\Repository\ChantierMaterielRepository;

class ChantierController extends AbstractController
{

	public function __construct(FonctionsService $fonctions)
    {
        $this->titre = 'Sites';
        $this->fonctions = $fonctions;
    }
	 public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);
        $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
        if($flag_hebline == true){$zone=null;}

    		$enregs = $em->getRepository('App:Chantier')->findChantierZone($zone);

    		$countUsers=$countPaniers=array();
    		if(isset($enregs)){
    		foreach($enregs as $chantier){
    		$countUsers[$chantier->getId()] = count($chantier->getChantierusers());
    		}
    		}

        return $this->render('Generique/Chantier/index.html.twig', array(
            'repo'      => $repo,
            'enregs' => $enregs,
      			'titre' => $this->titre,
      			'countUsers' => $countUsers
        ));
    }


		public function users(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
        $chantier=$em->getRepository("App:Chantier")->find($id);
		$zone = $this->getUser()->getZone();
     if($chantier->getTypechantier()->getId() == 1){$role='FRONT';}
    if($chantier->getTypechantier()->getId() == 2){$role='RENOU';}
    $listusers=$em->getRepository('App:User')->getUsersFrontZone(null, $role);
		$listusersOK=$listusersPanierOK=$listusersCommentaire=$chantieruserId=array();
		foreach($chantier->getChantierusers() as $chantieruser){
			$user_id = $chantieruser->getUser()->getId();
		$listusersOK[$user_id]=1;
		$listusersPanierOK[$user_id]= $chantieruser->getPanier();
		$listusersCommentaire[$user_id]= $chantieruser->getCommentaire();
		$chantieruserId[$user_id]= $chantieruser->getId();
		}
        return $this->render('Admin/Chantier/users.html.twig', array(
            'chantier' => $chantier,
			'listusers' => $listusers,
			'listusersOK' => $listusersOK,
			'listusersPanierOK' => $listusersPanierOK,
			'listusersCommentaire' => $listusersCommentaire,
			'chantieruserId' => $chantieruserId,
        ));
    }
  public function addGen(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getManager();
        if(null!= $id){
        $chantier=$em->getRepository("App:Chantier")->find($id);
        }
        else{$chantier = new Chantier();}
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);

        $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
        $editForm =  $this->createForm('App\Form\GeneriqueChantierType', $chantier, array('flag_hebline' => $flag_hebline));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone();
        $enreg=$editForm->getData();
        if($flag_hebline == true){$zone = $editForm->get('zone')->getData();}

        $enreg->setZone($zone);
        $enreg->addZone($zone);
        $em = $this->getDoctrine()->getManager();
        $em->persist($enreg);
        $em->flush();
         //$this->addFlash('success','Nouvel enregistrement ok');
              //return $this->redirectToRoute('admin_chantiers');
          return $this->redirectToRoute($repo.'_chantiers_add2', array('id' => $enreg->getId()));
        }

        return $this->render('Generique/Chantier/edit.html.twig', array(
            'repo' => $repo,
            'edit_form' => $editForm->createView(),
            'titre' => $this->titre,
        ));
    }

    public function addGen2(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $zone = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone);
        $chantier=$em->getRepository("App:Chantier")->find($id);

        if($chantier->getTypechantier()->getId() == 1){
        $editForm =  $this->createForm('App\Form\ChantierReleveType', $chantier);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $enreg=$editForm->getData();
          $em = $this->getDoctrine()->getManager();
          $em->persist($enreg);
          $em->flush();

          $this->addFlash('success','Nouvel enregistrement ok');
                //return $this->redirectToRoute('admin_chantiers');
          return $this->redirectToRoute($repo.'_chantiers');
        }
        return $this->render('Generique/Chantier/edit.html.twig', array(
            'repo' => $repo,
            'edit_form' => $editForm->createView(),
            'titre' => $this->titre,
        ));
        }

        if($chantier->getTypechantier()->getId() == 2){

        $editForm =  $this->createForm('App\Form\ChantierRenouType', $chantier);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
          $em->persist($chantier);
          $em->flush();
          $this->addFlash('success','Enregistrement ok');
        }


        $materielsOK=array();
        $chantiersmateriels=$em->getRepository('App:ChantierMateriel')->findBy(array('chantier' => $chantier));
        foreach ($chantiersmateriels as $chantiermateriel){
                $materielsOK[$chantiermateriel->getMateriel()->getId()]=$chantiermateriel->getTarif();
        }
        }
        $prestations=$em->getRepository('App:Prestation')->findByType(1);

        for($q=1;$q<=7;$q++){
        $materiels[$q]=$em->getRepository('App:Materiel')->findByPrestation($em->getRepository('App:Prestation')->find($q));
        }

        return $this->render('Generique/Materiel/renou.html.twig', array(
            'chantier' => $chantier,
            'edit_form' => $editForm->createView(),
            'repo' => $repo,
            'materielsOK'  => $materielsOK,
            'materiels'  => $materiels,
            'prestations'  => $prestations,
            'titre' => $this->titre,
        ));

    }

    public function materielEdit($action, $chantier, $materiel, $tarif){
        $em = $this->getDoctrine()->getManager();
        $chantier=$em->getRepository("App:Chantier")->find($chantier);
        $materiel=$em->getRepository("App:Materiel")->find($materiel);
        if($action == 'remove'){

            $materielchantier=$em->getRepository('App:ChantierMateriel')->findOneBy(array('materiel' => $materiel, 'chantier' => $chantier));
            $enregs = $em->getRepository('App:EnregistrementChantierMateriel')->findOneBy(array('chantiermateriel' => $materielchantier));
            if(isset($materielchantier) && !isset($enregs)){$em->remove($materielchantier);}
            else {return new Response(2);}
            $em->flush();

        }
        if($action == 'update'){
            $materielchantier=$em->getRepository('App:ChantierMateriel')->findOneBy(array('materiel' => $materiel, 'chantier' => $chantier));
            $materielchantier->setTarif($tarif);
            $em->flush();
        }
        if($action == 'add'){
            $materielchantier=$em->getRepository('App:ChantierMateriel')->findOneBy(array('materiel' => $materiel, 'chantier' => $chantier));
            if(!isset($materielchantier)){
            $materielchantier=new ChantierMateriel();
            $materielchantier->setMateriel($materiel);
            $materielchantier->setChantier($chantier);
            $materielchantier->setTarif($tarif);
            $em->persist($materielchantier);
            $em->flush();
            }
        }
        return new Response(1);
    }
	public function usersHebline(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
        $chantier=$em->getRepository("App:Chantier")->find($id);
		$zone = $chantier->getZone();
    $role = null;
    if($chantier->getTypechantier()->getId() == 1){$role='FRONT';}
    if($chantier->getTypechantier()->getId() == 2){$role='RENOU';}
		$listusers=$em->getRepository('App:User')->getUsersFrontZone(null, $role);
		$listusersOK=$listusersPanierOK=$listusersCommentaire=$chantieruserId=array();
		foreach($chantier->getChantierusers() as $chantieruser){
			$user_id = $chantieruser->getUser()->getId();
  		$listusersOK[$user_id]=1;
  		$listusersPanierOK[$user_id]= $chantieruser->getPanier();
  		$listusersCommentaire[$user_id]= $chantieruser->getCommentaire();
  		$chantieruserId[$user_id]= $chantieruser->getId();
		}
        return $this->render('Hebline/Chantier/users.html.twig', array(
            'chantier' => $chantier,
			'listusers' => $listusers,
			'listusersOK' => $listusersOK,
			'listusersPanierOK' => $listusersPanierOK,
			'listusersCommentaire' => $listusersCommentaire,
			'chantieruserId' => $chantieruserId,
        ));
    }



	public function useradd(Request $request, $chantier, $user, $panier)
    {
		$em = $this->getDoctrine()->getManager();
        $chantier=$em->getRepository("App:Chantier")->find($chantier);
        $user=$em->getRepository("App:User")->find($user);
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){$em->remove($chantieruser);}
		$chantieruser=new ChantierUser();
		$chantieruser->setChantier($chantier);
		$chantieruser->setUser($user);
		$chantieruser->setPanier($panier);
		$em->persist($chantieruser);
		$em->flush();
		return new Response(1);
	}

	public function userremove(Request $request, $chantier, $user, $panier)
    {
		$em = $this->getDoctrine()->getManager();
        $chantier=$em->getRepository("App:Chantier")->find($chantier);
        $user=$em->getRepository("App:User")->find($user);
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){$em->remove($chantieruser);}
		$em->flush();
		return new Response(1);
	}

	public function commentaire(Request $request, $chantier, $user, $commentaire = null)
    {
		$em = $this->getDoctrine()->getManager();
                $chantier=$em->getRepository("App:Chantier")->find($chantier);
        $user=$em->getRepository("App:User")->find($user);
		$chantieruser=$em->getRepository('App:ChantierUser')->findOneBy(['chantier'=>$chantier, 'user' => $user]);
		if(isset($chantieruser)){
		$chantieruser->setCommentaire($commentaire);
		$em->persist($chantieruser);
		$em->flush();
		}
		return new Response(1);
	}

}
