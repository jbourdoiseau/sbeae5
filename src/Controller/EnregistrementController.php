<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

use App\Service\FonctionsService;

use App\Entity\Enregistrement;
use App\Entity\Chantier;
use App\Entity\User;
use App\Entity\EnregistrementChantierMateriel;
use App\Entity\Totalrenou;

use App\Form\EnregistrementAdminGeneriqueType;
use App\Form\SuperAdminEnregistrementType;
use App\Form\ClientEnregistrementType;

class EnregistrementController extends AbstractController
{
	public function __construct(FonctionsService $fonctions)
    {
        $this->fonctions = $fonctions;
        $this->titre = 'Enregistrements';
    }

   public function indexGen(Request $request,$page, $id = null)
    {
        $user = null;
    $em = $this->getDoctrine()->getManager();
    if(null != $id){$user = $em->getRepository('App:User')->find($id);}
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();

    $repo = $this->fonctions->getRepo($zone_id);

    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    if($flag_hebline == true){$zone=null;}
    $page=1;
    $nbPerPage = $this->getParameter('nbPerPage');
    //-------------FORMULAIRE---------------------
    $enregistrement = new Enregistrement();
    $form = $this->get('form.factory')->create(EnregistrementAdminGeneriqueType::class, $enregistrement, array('zone' => $zone,'ref' => null,'flag_hebline' => $flag_hebline));
    $form->handleRequest($request);
    $page=$form->get('page')->getData();
    $date1=$form->get('date1')->getData();
    $date2=$form->get('date2')->getData();
    $ref=$form->get('ref')->getData();
    $cloture=$form->get('cloture')->getData();
    $secteur=$form->get('secteur')->getData();
    $newcompteur=$form->get('newcompteur')->getData();
    $enregistrementCriteres=$form->getData();
    if($date1==""){
      $date1 = new \DateTime('now -1 year');
    }
    if($date2==""){
      $date2 = new \DateTime('now');
    }
    $typechantier=$em->getRepository('App:TypeChantier')->find(1);
    $this->titre=$this->titre.' '.$typechantier->getNom();
    $enregs = $em->getRepository('App:Enregistrement')->findEnregistrements($page, $nbPerPage, $enregistrementCriteres, $zone, $date1, $date2, $cloture, $ref, $user, $secteur, $typechantier, $newcompteur);
    //echo count($enregs);
    $nbPages = ceil(count($enregs) / $nbPerPage);
    //$nbPages = 1;
        return $this->render('Generique/Enregistrement/index.html.twig', array(
            'flag_hebline' => $flag_hebline,
            'repo' => $repo,
            'enregs' => $enregs,
            'titre' => $this->titre,
            'nbPages'       => $nbPages,
            'page'          => $page,
            'form'      => $form->createView(),
        ));
    }

    public function indexGenRenou(Request $request,$page, $id = null)
    {
$user = null;
    $em = $this->getDoctrine()->getManager();
    if(null != $id){$user = $em->getRepository('App:User')->find($id);}
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();

    $repo = $this->fonctions->getRepo($zone_id);
    $flag_hebline = $this->container->get('security.authorization_checker')->isGranted('ROLE_HEBLI');
    if($flag_hebline == true){$zone=null;}
    $page=1;
    $nbPerPage = $this->getParameter('nbPerPage');

    //-------------FORMULAIRE---------------------
    $enregistrement = new Enregistrement();
    $form = $this->get('form.factory')->create(EnregistrementAdminGeneriqueType::class, $enregistrement, array('zone' => $zone,'ref' => null,'flag_hebline' => $flag_hebline));
    $form->handleRequest($request);
    $page=$form->get('page')->getData();
    $date1=$form->get('date1')->getData();
    $date2=$form->get('date2')->getData();
    $ref=$form->get('ref')->getData();
    $cloture=$form->get('cloture')->getData();
    $secteur=$form->get('secteur')->getData();
    $enregistrementCriteres=$form->getData();

    if($date1==""){
      $date1 = new \DateTime('now -1 year');
    }
    if($date2==""){
      $date2 = new \DateTime('now');
    }
    $typechantier=$em->getRepository('App:TypeChantier')->find(1);
    $this->titre=$this->titre.' '.$typechantier->getNom();
    $totaux=array();
    $enregs = $em->getRepository('App:Enregistrement')->findEnregistrements($page, $nbPerPage, $enregistrementCriteres, $zone, $date1, $date2, $cloture, $ref, $user, $secteur,2);
    $nbPages = ceil(count($enregs) / $nbPerPage);
    foreach ($enregs as $enreg){
      $renous=$em->getRepository('App:Totalrenou')->findTotalRenousEnreg($enreg);
        foreach($renous as $renou){
            $totaux[$enreg->getId()][$renou['prestation']]=($totaux[$enreg->getId()][$renou['prestation']]??0)+$renou['total'];
        }
        $tot=$em->getRepository('App:EnregistrementChantierMateriel')->findTotalCA($enreg);
        $totaux[$enreg->getId()][0]=$tot['total'];
    }
        return $this->render('Generique/Enregistrement/indexRenou.html.twig', array(
            'flag_hebline' => $flag_hebline,
            'repo' => $repo,
            'enregs' => $enregs,
            'titre' => $this->titre,
            'nbPages'       => $nbPages,
            'page'          => $page,
            'totaux'          => $totaux,
            'form'      => $form->createView(),
        ));
    }


	 public function index(Request $request,$page, User $user = null)
    {
	  	$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();
		$page=1;
		$nbPerPage = $this->getParameter('nbPerPage');
		//-------------FORMULAIRE---------------------
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(AdminEnregistrementType::class, $enregistrement, array('zone' => $zone,'ref' => null));
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$ref=$form->get('ref')->getData();
		$cloture=$form->get('cloture')->getData();
		$enregistrementCriteres=$form->getData();
		if($date1==""){
			$date1 = new \DateTime('now -1 year');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$enregs = $em->getRepository('App:Enregistrement')->findEnregistrements($page, $nbPerPage, $enregistrementCriteres, $zone, $date1, $date2, $cloture, $ref, $user,1);
		$nbPages = ceil(count($enregs) / $nbPerPage);
        return $this->render('Admin/Enregistrement/index.html.twig', array(
            'enregs' => $enregs,
      			'titre' => $this->titre,
      			'nbPages'     	=> $nbPages,
      			'page'        	=> $page,
      			'form' 			=> $form->createView(),
        ));
    }



	public function detail($id)
    {
        $em = $this->getDoctrine()->getManager();
        $enregistrement = $em->getRepository('App:Enregistrement')->find($id);
		$tournees = $enregistrement->getTournees();
        return $this->render('Admin/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }

	public function detailSA($id)
    {
        $em = $this->getDoctrine()->getManager();
        $enregistrement = $em->getRepository('App:Enregistrement')->find($id);
		$tournees = $enregistrement->getTournees();
        return $this->render('SuperAdmin/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }
	public function detailHebline($id)
    {
        $em = $this->getDoctrine()->getManager();
        $enregistrement = $em->getRepository('App:Enregistrement')->find($id);
        $tournees = $enregistrement->getTournees();
        return $this->render('Hebline/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }
	public function capture($id)
    {
        $em = $this->getDoctrine()->getManager();
        $enregistrement=$em->getRepository("App:Enregistrement")->find($id);
        return $this->render('Generique/Enregistrement/capture.html.twig', array(
            'enreg' => $enregistrement
		));
    }


	public function navig(Enregistrement $enregistrement, $action)
    {
		$nav=array();
		$em = $this->getDoctrine()->getManager();
		$nav=$em->getRepository('App:Enregistrement')->filterNextPrevious($enregistrement->getId());
		$idnav=$nav[$action];
		if($idnav == ''){$idnav=$enregistrement->getId();}

        return $this->redirectToRoute('admin_enregistrements_edit', array('id' => $idnav));
    }

	public function navigSA (Enregistrement $enregistrement, $action)
    {
		$nav=array();
		$em = $this->getDoctrine()->getManager();
		$nav=$em->getRepository('App:Enregistrement')->filterNextPrevious($enregistrement->getId());
		$idnav=$nav[$action];
		if($idnav == ''){$idnav=$enregistrement->getId();}

        return $this->redirectToRoute('superadmin_enregistrements_edit', array('id' => $idnav));
    }

		public function navigHebline (Enregistrement $enregistrement, $action)
    {
		$nav=array();
		$em = $this->getDoctrine()->getManager();
		$nav=$em->getRepository('App:Enregistrement')->filterNextPrevious($enregistrement->getId());
		$idnav=$nav[$action];
		if($idnav == ''){$idnav=$enregistrement->getId();}

        return $this->redirectToRoute('hebline_enregistrements_edit', array('id' => $idnav));
    }

	public function edit(Request $request, $id)
    {

		$em = $this->getDoctrine()->getManager();
        $enregistrement=$em->getRepository("App:Enregistrement")->find($id);
        $zone_id = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone_id);

		$OriginalPools = new ArrayCollection();
		foreach ($enregistrement->getTournees() as $pool) {
		$OriginalPools -> add($pool);
		}
		$nomformulaire = $enregistrement->getTypeformulaire()->getNom();

        $editForm = $this->createForm('App\Form\AdminEnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

				foreach ($OriginalPools as $pool) {
					if (false === $enregistrement->getTournees()->contains($pool)) {
						$em->remove($pool);
					}
				}
				$cloture=$enregistrement->getCloture();
				foreach($enregistrement->getTournees() as $tournee){
					$tournee->setCloture($cloture);
				}
			$em->persist($enregistrement);
			$em->flush();

        }
        $editForm = $this->createForm('App\Form\AdminEnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);
        return $this->render('Generique/Enregistrement/edit.html.twig', array(
            'enreg' => $enregistrement,
            'repo' => $repo,
			'form' 			=> $editForm->createView(),
        ));
    }

	public function editHebline(Request $request, $id)
    {

		$em = $this->getDoctrine()->getManager();
        $enregistrement=$em->getRepository("App:Enregistrement")->find($id);
        $zone_id = $this->getUser()->getZone()->getId();
        $repo = $this->fonctions->getRepo($zone_id);

		$OriginalPools = new ArrayCollection();
		foreach ($enregistrement->getTournees() as $pool) {
		$OriginalPools -> add($pool);
		}
		$nomformulaire = $enregistrement->getTypeformulaire()->getNom();

        $editForm = $this->createForm('App\Form\HeblineEnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

				foreach ($OriginalPools as $pool) {
					if (false === $enregistrement->getTournees()->contains($pool)) {
						$em->remove($pool);
					}
				}
				$cloture=$enregistrement->getCloture();
				foreach($enregistrement->getTournees() as $tournee){
					$tournee->setCloture($cloture);
				}
			$em->persist($enregistrement);
			$em->flush();
			return $this->redirectToRoute('hebline_enregistrements');
        }
		else {
			$this->addFlash('danger', $editForm->getErrors(true));
		}

        $editForm = $this->createForm('App\Form\HeblineEnregistrementTourneeType', $enregistrement, array('nomformulaire' => $nomformulaire, 'required' => false));
        $editForm->handleRequest($request);
        return $this->render('Generique/Enregistrement/edit.html.twig', array(
            'enreg' => $enregistrement,
            'repo' => $repo,
			'form' 			=> $editForm->createView(),
        ));
    }

  public function editGenRenou(Request $request, $id)
    {

    $em = $this->getDoctrine()->getManager();
    $enregistrement =$em->getRepository("App:Enregistrement")->find($id);
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone_id);

    $form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouEditType::class, $enregistrement, array('obligatoire' => 0));
    $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {

        $enregistrementchantiermateriels=$enregistrement->getEnregistrementchantiermateriels();
            if(count($enregistrementchantiermateriels) == 0)
            {
                             //recherche des documents
                        $chantier = $enregistrement->getChantier();
                        $chantiersmateriels=$chantier->getChantierMateriels();
                                foreach($chantiersmateriels as $chantiermateriel){
                                        $enregistrementchantiermateriel = new EnregistrementChantierMateriel();
                                        $enregistrementchantiermateriel->setEnregistrement($enregistrement);
                                        $enregistrementchantiermateriel->setChantierMateriel($chantiermateriel);
                                        $enregistrementchantiermateriel->setNombre(0);
                                        $enregistrement->addEnregistrementChantierMateriel($enregistrementchantiermateriel);
                                }

            }

            $em->persist($enregistrement);
            $em->flush();
             $images = $enregistrement->getDocuments();
                  foreach ($images as $image){
                    if($image->getFileName() == ''){
                        $enregistrement->removeDocument($image);
                        $em->remove($image);
                    }
                }
            $totalrenous=$em->getRepository('App:Totalrenou')->findBy(['enregistrement' => $enregistrement]);
            foreach($totalrenous as $totalrenou){$em->remove($totalrenou);}
            $em->flush();
            //calcul des totaux par categ
            $totaux = $em->getRepository('App:EnregistrementChantierMateriel')->findTotaux($enregistrement->getId());
              foreach($totaux as $total){
                  $totalrenou=new Totalrenou();
                  $totalrenou->setEnregistrement($enregistrement);
                  $totalrenou->setTotal($total['total']);
                  $totalrenou->setPrestation($em->getRepository('App:Prestation')->find($total['id']));
                  $em->persist($totalrenou);
                  $enregistrement->addTotalrenou($totalrenou);
              }
            $em->flush();
            $this->addFlash('success','Enregistrement modifié');
            //return $this->redirectToRoute('hebline_enregistrements_renou');
            }
        else {
        $this->addFlash('danger', $form->getErrors(true));
        }
        $form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouEditType::class, $enregistrement,
            array('obligatoire' => 0,
                'chantier'=>$enregistrement->getChantier(),
                'zone' =>$enregistrement->getChantier()->getZone()->getId()
            )
        );
        return $this->render('Generique/Enregistrement/editRenou.html.twig', array(
            'enreg' => $enregistrement,
            'repo' => $repo,
            'form'      => $form->createView(),
        ));
    }

public function addGenRenou(Request $request)
    {

    $em = $this->getDoctrine()->getManager();
    $zone = $this->getUser()->getZone();
    $zone_id = $this->getUser()->getZone()->getId();
    $repo = $this->fonctions->getRepo($zone_id);

    $enregistrement = new Enregistrement();
    $enregistrement->setCloture(0);
    $enregistrement->setTypechantier($em->getRepository('App:Typechantier')->find(2));

    $form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouType::class, $enregistrement, array('obligatoire' => 0));
    $form->handleRequest($request);

       if ($form->isSubmitted() && $form->isValid()) {
        $enregistrementchantiermateriels=$enregistrement->getEnregistrementchantiermateriels();
            if(count($enregistrementchantiermateriels) == 0)
            {
                             //recherche des documents
                        $chantier = $enregistrement->getChantier();
                        $chantiersmateriels=$chantier->getChantierMateriels();
                                foreach($chantiersmateriels as $chantiermateriel){
                                        $enregistrementchantiermateriel = new EnregistrementChantierMateriel();
                                        $enregistrementchantiermateriel->setEnregistrement($enregistrement);
                                        $enregistrementchantiermateriel->setChantierMateriel($chantiermateriel);
                                        $enregistrementchantiermateriel->setNombre(0);
                                        $enregistrement->addEnregistrementChantierMateriel($enregistrementchantiermateriel);
                                }

            }
            $enregistrement->setZone($enregistrement->getChantier()->getZone());
            $em->persist($enregistrement);
            $em->flush();
             $images = $enregistrement->getDocuments();
                  foreach ($images as $image){
                    if($image->getFileName() == ''){
                        $enregistrement->removeDocument($image);
                        $em->remove($image);
                    }
                }
            $totalrenous=$em->getRepository('App:Totalrenou')->findBy(['enregistrement' => $enregistrement]);
            foreach($totalrenous as $totalrenou){$em->remove($totalrenou);}
            $em->flush();
            //calcul des totaux par categ
            $totaux = $em->getRepository('App:EnregistrementChantierMateriel')->findTotaux($enregistrement->getId());
              foreach($totaux as $total){
                  $totalrenou=new Totalrenou();
                  $totalrenou->setEnregistrement($enregistrement);
                  $totalrenou->setTotal($total['total']);
                  $totalrenou->setPrestation($em->getRepository('App:Prestation')->find($total['id']));
                  $em->persist($totalrenou);
                  $enregistrement->addTotalrenou($totalrenou);
              }
            $em->flush();
            $this->addFlash('success','Enregistrement créé');
            return $this->redirectToRoute($repo.'_enregistrements_renou_edit', array('id' => $enregistrement->getId()));
            }
        else {
        $this->addFlash('danger', $form->getErrors(true));
        }
        $form = $this->get('form.factory')->create(\App\Form\EnregistrementRenouType::class, $enregistrement, array('obligatoire' => 0));
        return $this->render('Generique/Enregistrement/editRenou.html.twig', array(
            'enreg' => $enregistrement,
            'repo' => $repo,
            'form'      => $form->createView(),
        ));
    }

	public function add (Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone();
		$enregistrement= new Enregistrement();
		$nomformulaire = 'admin';
        $editForm = $this->createForm('App\Form\AddEnregistrementTourneeType', $enregistrement, array('zone' => $zone));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			//check cohérence utilisateur chantier
			//echo $enregistrement->getChantier()->getId();
			$chantier = $enregistrement->getChantier();
			$utilisateur = $enregistrement->getUser();
			$chantiers = $em->getRepository('App:ChantierUser')->findBy(array('chantier' => $chantier, 'user' => $utilisateur));
			if(isset ($chantiers) && count($chantiers)>0){
			//$enregistrement->setType('admin');
            $enregistrement->setTypechantier($em->getRepository('App:Typechantier')->find(1));
			$enregistrement->setTypeFormulaire($em->getRepository('App:Formulaire')->find(1));
			$enregistrement->setZone($zone);
			$em->persist($enregistrement);
			$em->flush();
					$type='success';
					$message='Enregistrement créé';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
					return $this->redirectToRoute('admin_enregistrements_edit', array('id' => $enregistrement->getId()));
			}
			else{
					$type='error';
					$message='Opération impossible : cet utilisateur n\'est pas utilisé sur ce chantier';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
			}
        }

        return $this->render('Admin/Enregistrement/add.html.twig', array(
			'form' 			=> $editForm->createView(),
        ));
    }

	public function addHebline (Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$enregistrement= new Enregistrement();
		$nomformulaire = 'admin';
        $editForm = $this->createForm('App\Form\HeblineAddEnregistrementTourneeType', $enregistrement, array());
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $enregistrement->setTypechantier($em->getRepository('App:Typechantier')->find(1));
			$chantier = $enregistrement->getChantier();
			$utilisateur = $enregistrement->getUser();
			$chantiers = $em->getRepository('App:ChantierUser')->findBy(array('chantier' => $chantier, 'user' => $utilisateur));
			if(isset ($chantiers) && count($chantiers)>0){
			//$enregistrement->setType('admin');
			$enregistrement->setTypeFormulaire($em->getRepository('App:Formulaire')->find(1));
			$em->persist($enregistrement);
			$em->flush();
					$type='success';
					$message='Enregistrement créé';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
					return $this->redirectToRoute('hebline_enregistrements_edit', array('id' => $enregistrement->getId()));
			}
			else{
					$type='error';
					$message='Opération impossible : cet utilisateur n\'est pas utilisé sur ce chantier';
					$flashbag = $this->get('session')->getFlashBag();
					$flashbag->add($type, $message);
			}
        }

        return $this->render('Hebline/Enregistrement/add.html.twig', array(
			'form' 			=> $editForm->createView(),
        ));
    }
	public function controle (Request $request, $id, $controle)
    {
		$em = $this->getDoctrine()->getManager();
        $enregistrement=$em->getRepository("App:Enregistrement")->find($id);
		if($controle <2){
		$enregistrement->setAlerte(0);
        $enregistrement->setControle($controle);
		}
		if(($controle ==2 )||($controle ==3 )){
		$controle=$controle-2;
		$enregistrement->setAlerte(0);
        $enregistrement->setDefaut($controle);
		}
    if(($controle ==4 )||($controle ==5 )){
    $controle=$controle-4;
        $enregistrement->setTarifjour($controle);
    }

		$em->flush();
		return new Response(1);
	}



	public function indexClient(Request $request,$page, User $user = null)
    {
	  	$em = $this->getDoctrine()->getManager();
		$page=1;
		$nbPerPage = $this->getParameter('nbPerPage');
		$zone = $this->getUser()->getZone();
		$chantiers = $zone -> getChantiers();
		//-------------FORMULAIRE---------------------
		$enregistrement = new Enregistrement();
		$form = $this->get('form.factory')->create(ClientEnregistrementType::class, $enregistrement, array( 'chantiers' => $chantiers, 'ref' => null));
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$ref=$form->get('ref')->getData();
		if($date1==""){
			$date1 = new \DateTime('now -1 year');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$cloture=$form->get('cloture')->getData();
		$enregistrementCriteres=$form->getData();
//$date1=$date1->format('Y-m-d');
//$date2=$date2->format('Y-m-d');
$chantiers2=array();
foreach($chantiers as $ch){$chantiers2[]=$ch->getId();}

		$enregs = $em->getRepository('App:Enregistrement')->findClientEnregistrements($page, $nbPerPage, $enregistrementCriteres, $chantiers2, $date1, $date2, $cloture, $ref, $user);
		$nbPages=1;
		//$nbPages = ceil(count($enregs) / $nbPerPage);
		//echo count($enregs).'/'.$nbPerPage.'/'.$zone.'/'.$date1->format('d m Y');;
        return $this->render('Client/Enregistrement/index.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
			'form' 			=> $form->createView(),
        ));
    }

	public function detailClient($id)
    {
        $em = $this->getDoctrine()->getManager();
        $enregistrement = $em->getRepository('App:Enregistrement')->find($id);
		$tournees = $enregistrement->getTournees();
        return $this->render('Client/Enregistrement/show.html.twig', array(
            'enreg' => $enregistrement,
			'tournees' => $tournees
		));
    }

}
