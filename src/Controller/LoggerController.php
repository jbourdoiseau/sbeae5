<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Logger;
use App\Entity\User;
use App\Form\LoggerType;

class LoggerController extends AbstractController
{

	public function __construct()
    {
        $this->titre = 'Accès';

    }

	 public function index(Request $request,$page)
    {
	  	$em = $this->getDoctrine()->getManager();
		$zone = $this->getUser()->getZone()->getId();
		$page=1;

		$nbPerPage = $this->getParameter('nbPerPage');
		//-------------FORMULAIRE---------------------
		$logger= new Logger();
		$form = $this->get('form.factory')->create(LoggerType::class, $logger, array('zone' => $zone));
		$form->handleRequest($request);
		$page=$form->get('page')->getData();
		echo $page;
		$date1=$form->get('date1')->getData();
		$date2=$form->get('date2')->getData();
		$role=$form->get('role')->getData();

		if($date1==""){
			$date1 = new \DateTime('now -1 week');
		}
		if($date2==""){
			$date2 = new \DateTime('now');
		}
		$date2->modify('+24 hours');
		$enregs = $em->getRepository('App:Logger')->findlogs($page, $nbPerPage, $date1, $date2, $role, $zone);
		$nbPages = ceil(count($enregs) / $nbPerPage);

		if($zone==1001){$rep="SuperAdmin";}
		elseif($zone==1000){$rep="Hebline";}
		else{$rep="Admin";}
        return $this->render($rep.'/Stats/log.html.twig', array(
            'enregs' => $enregs,
			'titre' => $this->titre,
			'nbPages'     	=> $nbPages,
			'page'        	=> $page,
			'form' 			=> $form->createView(),
        ));
    }

}
