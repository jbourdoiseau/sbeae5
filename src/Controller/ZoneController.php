<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

use App\Entity\Zone;
use App\Repository\ZoneRepository;


class ZoneController extends AbstractController
{



	 public function index(Request $request,$page)
    {

        $em = $this->getDoctrine()->getManager();
		if ($page < 1) {
			  $page=1;
		}

		$zones = $em->getRepository('App:Zone')->findAll();

        return $this->render('Admin/Zone/indexzone.html.twig', array(
            'zones' => $zones
        ));


    }


	public function detail(Zone $zone)
    {
        return $this->render('Admin/Zone/show.html.twig', array(
            'zone' => $zone,
        ));
    }


	 public function edit(Request $request, Zone $zone)
    {

        $editForm =  $this->createForm('App\Form\AdminZoneType', $zone);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
			$this->addFlash('success',
            'Vos modifications ont été enregistrées'
			);
			$this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_zones');
        }

        return $this->render('Admin/Zone/edit.html.twig', array(
            'zone' => $zone,
            'edit_form' => $editForm->createView(),
        ));
    }

	public function add(Request $request)
    {

        $editForm =  $this->createForm('App\Form\AdminZoneType');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

			$zone=$editForm->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($zone);
            $em->flush();
			$this->addFlash('success',
            'Nouvelle zone enregistrée'
			);
            return $this->redirectToRoute('admin_zones');
        }

        return $this->render('Admin/Zone/edit.html.twig', array(
            'edit_form' => $editForm->createView(),
        ));
    }

}
