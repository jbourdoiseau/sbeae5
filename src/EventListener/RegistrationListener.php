<?php
namespace App\EventListener;

#use FOS\UserBundle\FOSUserEvents;
#use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\Entity\User;

class RegistrationListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {

    return array(
        /*FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',*/
    );
    }

    public function onRegistrationSuccess(FormEvent $event)
    {

            $user = $event->getForm()->getData();
			$user->setPlain($user->getPlainPassword());
			$event->setResponse($user);

    }
}
