<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use App\Entity\Logger;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AfterLoginRedirection
 *
 * @package AppBundle\AppListener
 */
class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{
    private $router;

    /**
     * AfterLoginRedirection constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager)
    {
        $this->router = $router;
		$this->em = $entityManager;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $roles = $token->getRoles();

        $rolesTab = array_map(function ($role) {
            return $role->getRole();
        }, $roles);

        $em = $this->em;
		$user=$token->getUser();

        if (in_array('ROLE_ADLUC', $rolesTab, true)) {
            // c'est un administrateur : on le rediriger vers l'espace admin
			$log=new Logger();
			$log->setTitre('JEANLUC');
			$log->setUser($user);
			$em->persist($log);
            $em->flush();
            $redirection = new RedirectResponse($this->router->generate('superadmin_enregistrements'));
        }
		elseif (in_array('ROLE_HEBLI', $rolesTab, true)) {
			$log=new Logger();
			$log->setTitre('HEBLINE');
			$log->setUser($user);
			$em->persist($log);
            $em->flush();
            // c'est un administrateur : on le rediriger vers l'espace admin
            $redirection = new RedirectResponse($this->router->generate('hebline_enregistrements'));
        }
		elseif (in_array('ROLE_ADMIN', $rolesTab, true)) {
			$log=new Logger();
			$log->setTitre('AFFAIRE');
			$log->setUser($user);
			$em->persist($log);
            $em->flush();
            // c'est un administrateur : on le rediriger vers l'espace admin
            $redirection = new RedirectResponse($this->router->generate('admin_enregistrements'));
        }
		elseif (in_array('ROLE_CLIEN', $rolesTab, true)) {

			$log=new Logger();
			$log->setTitre('CLIENT');
			$log->setUser($user);
			$em->persist($log);
            $em->flush();
            // c'est un administrateur : on le rediriger vers l'espace admin
            $redirection = new RedirectResponse($this->router->generate('client_graph_activite_html'));
        }
		elseif ((in_array('ROLE_FRONT', $rolesTab, true))||(in_array('ROLE_RENOU', $rolesTab, true))) {
			$log=new Logger();
			$log->setTitre('TERRAIN');
			$log->setUser($user);
			$em->persist($log);
            $em->flush();
			if($request->request->get('_target_path') != ''){
				return new RedirectResponse($request->request->get('_target_path'));
			}
            $redirection = new RedirectResponse($this->router->generate('acces_sbeae'));
        }
		else {
            // c'est un utilisaeur lambda : on le redirige vers l'accueil
            $redirection = new RedirectResponse($this->router->generate('acces_sbeae_home'));
        }

        return $redirection;
    }
}
