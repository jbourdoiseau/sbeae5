<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;


class TourneeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
		->add('code', TextType::class, array(
				'label' => 'Code',
				'required'     => true,
				'error_bubbling' => true,
				))

		->add('total', NumberType::class, array(
				'label' => 'Total',
				'required'     => true,
				'error_bubbling' => true,
				))	
		->add('releve', NumberType::class, array(
				'label' => 'Relevé',
				'required'     => true,
				'error_bubbling' => true,
				))			
		->add('infructueux', NumberType::class, array(
				'label' => 'Infructueux',
				'required'     => true,
				'error_bubbling' => true,
				))	
		->add('restant', NumberType::class, array(
				'label' => 'Reste',
				'required'     => true,
				'error_bubbling' => true,
				))
		->add('commentaire',   TextareaType::class, array('required'      => false));
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Tournee'
        ));
    }
   
}
