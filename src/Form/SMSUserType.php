<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Repository\UserRepository;


class SMSUserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

	$zone=$options['zone'];
	$user=$options['user'];

		if($zone <1000){
			$builder
				->add('user', EntityType::class, array(
						'class'        => 'App:User',
						'attr'=> array('class'=>'col-xs-4'),
						'choice_label' => function (\App\Entity\User $user) {
							return $user->getNom() . ' ' . $user->getPrenom();
						},
						'required'     => true,
						'attr' => array(
							'class' => ''
						),
						'data' => $user,
						'query_builder' => function(UserRepository $repository){
						return $repository
						->createQueryBuilder('f')
						->orderBy('f.nom', 'ASC')
						;
						}
			));
		}
		else{
			$builder
		->add('user', EntityType::class, array(
				'class'        => 'App:User',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => function (\App\Entity\User $user) {
					return $user->getNom() . ' ' . $user->getPrenom();
				},
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'data' => $user,
				'query_builder' => function(UserRepository $repository){
				return $repository
				->createQueryBuilder('f')
				->andWhere('f.roles LIKE :role')
				->setParameter('role', '%FRONT%')
        ->orWhere('f.roles LIKE :role2')
        ->setParameter('role2', '%RENOU%')
				->orderBy('f.nom', 'ASC')
				;
				}
		));
		}
		$builder->add('message',   TextareaType::class, array('required'      => false,'mapped' => false,'label' => 'Message',));
		$builder->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Envoyer'
		));



    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
			'zone' => null,
			'user' => null,
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_user';
    }


}
