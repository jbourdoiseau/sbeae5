<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class LoggerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		$role=$options['role'];
		$zone=$options['zone'];
		
		if(($zone != 1000)&&($zone != 1001)){
		$builder
		->add('zone', HiddenType::class, array(
		'required'     => false,
		'mapped' => false,
		'data' => $zone
		));
			$select=array('Sélectionner...' => '',
			'Chargé affaire' => 'AFFAIRE',
			'Terrain' => 'TERRAIN');
		}
		else{
			$select=array('Sélectionner...' => '',
			'Jean-Luc' => 'JEANLUC',
			'Hebline' => 'HEBLINE',
			'Chargé affaire' => 'AFFAIRE',
			'Terrain' => 'TERRAIN');
		}
		$builder
		->add('role', ChoiceType::class, array(
		'label' => 'Rôle ',
		'mapped' => false,
		'required'     => false,
		'choices'  => $select))
		->add('date1', DateType::class, array(
			'label' => 'Date début recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('date2', DateType::class, array(
			'label' => 'Date fin recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('page', HiddenType::class, array(
		'required'     => false,
		'mapped' => false,
		'data' => 1
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));
		
		 $builder->get('date1')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now -1 week');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
		
		$builder->get('date2')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now + 2 hour');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
	
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Logger',
			'role' => null,
			'zone' => null
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_log';
    }


}
