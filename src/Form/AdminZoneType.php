<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AdminZoneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
		->add('nom', TextType::class, array(
				'label' => 'Nom de la zone (*)',
				'required'     => true,
				'error_bubbling' => true,
				))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Zone',
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_zone';
    }


}
