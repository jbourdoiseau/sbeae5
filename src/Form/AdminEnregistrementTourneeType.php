<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

use App\Entity\Enregistrement;
use App\Entity\Tournee;
use App\Form\TourneeType;

class AdminEnregistrementTourneeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

	$nomformulaire=$options['nomformulaire'];
	$required=$options['required'];
    $select=array();
    for($q=0;$q<51;$q++){
            $select [$q]=$q;
    }
	$builder
        ->add('valide', ChoiceType::class, array(
        'label' => 'Valide',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
	  ->add('releve',    TextType::class, array('label'      => 'Relevé'))
	  ->add('infructueux',    TextType::class, array('label'      => 'Infructueux'))
	  ->add('total',    TextType::class, array('label'      => 'Total Opérateur'))
      ->add('newcompteur', ChoiceType::class, array(
                                    'label' => 'Nouveaux compteurs',
                                    'choices' => $select,
                                    'multiple' => false,
                                    'required' => true,
                            ))
	   ->add('cloture', ChoiceType::class, array(
				'label' => 'Cloture',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
	   		->add('tarifjour', ChoiceType::class, array(
				'label' => 'Forfait',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
		->add('cloturereleve',    NumberType::class, array('label'      => 'Relevé Cloture'))
	  ->add('clotureinfructueux',    NumberType::class, array('label'      => 'Infructueux Cloture'))
	  ->add('cloturetotal',   NumberType::class, array('label'      => 'Total Cloture'))
	  ->add('typeocr', EntityType::class, array(
				'class'        => 'App:Typeocr',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => true,
	))
	  ->add('totalocr',    TextType::class, array('label'      => 'Total OCR'))
	  ->add('totalOcrTournee',    TextType::class, array('label'      => 'Total Tournée OCR'))
    ->add('commentaire',   TextareaType::class, array('required'      => false));
	   if($nomformulaire != 'sansimage'){
       $builder ->add(
            'documents',
            CollectionType::class,
            array(
                'entry_type' => DocumentType::class,
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Capture(s) :',
                'entry_options' => ['label' => false],
                'prototype' => true
            )
        );
	}
	$builder
      ->add('tournees', CollectionType::class, array(
        'entry_type'   => TourneeType::class,
        'allow_add'    => true,
        'allow_delete' => true,
		'label' => 'Secteur(s) :',
		'entry_options' => ['label' => false],
		'prototype' => true,
		'by_reference' => false
      ))
      ->add('save',  SubmitType::class, array('label'=>'Enregistrer'));
    }


	public function configureOptions(OptionsResolver $resolver)
	  {
		$resolver->setDefaults(array(
		  'data_class' => 'App\Entity\Enregistrement',
		  'nomformulaire' => null,
		  'required' => true
		));
	  }
}
