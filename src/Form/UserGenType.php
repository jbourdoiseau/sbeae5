<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserGenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $flag_hebline=$options['flag_hebline'];

        if($flag_hebline){
         $builder->add('zone', EntityType::class, array(
                'class'        => 'App:Zone',
                'choice_label' => 'nom',
                'label' => 'Zones',
                'required'     => true,
                'attr' => array(
                    'class' => ''
                ),
                'placeholder' => 'Tous les zones'
        ));
        }
        $builder
		->add('email', TextType::class, array(
				'label' => 'Email (login)',
				'required'     => true,
				'error_bubbling' => true,
				))

		->add('nom', TextType::class, array(
				'label' => 'Nom',
				'required'     => true,
				'error_bubbling' => true,
				))
		->add('prenom', TextType::class, array(
				'label' => 'Prénom',
				'required'     => true,
				'error_bubbling' => true,
				))
		->add('zone', EntityType::class, array(
				'class'        => 'App:Zone',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => true,
		))
		->add('enabled', ChoiceType::class, array(
                'attr'  =>  array('class' => 'form-control','style' => 'margin:5px 0;'),
				'label' => 'Actif',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
		->add('plainPassword', RepeatedType::class, array(
                                                    'type' => PasswordType::class,
                                                    'options' => array('translation_domain' => 'FOSUserBundle'),
                                                    'first_options' => array('label' => 'form.password'),
                                                    'second_options' => array('label' => 'form.password_confirmation'),
                                                    'invalid_message' => 'fos_user.password.mismatch',
													'required' => false,
                                                    ))
		->add('save',  SubmitType::class, array('label'=>'Enregistrer'));
		;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LEO\CommerceBundle\Entity\User',
            'flag_hebline' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'leo_commercebundle_user';
    }


}
