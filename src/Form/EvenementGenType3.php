<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Vich\UploaderBundle\Form\Type\VichFileType;
use App\Repository\UserRepository;
use App\Entity\User;

class EvenementGenType3 extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $type=$options['type'];
    $builder->add('user', EntityType::class, array(
        'class'        => 'App:User',
        'label' => 'Agent (*)',
        'attr'=> array('class'=>'col-xs-4'),
        'choice_label' => function (User $user) {
          return $user->getNom() . ' ' . $user->getPrenom();
        },
        'required'     => true,
        'attr' => array(
          'class' => ''
        ),
        'query_builder' => function(UserRepository $repository){
        return $repository
        ->createQueryBuilder('f')
        ->andWhere('f.roles LIKE :role')
        ->setParameter('role', '%FRONT%')
        ->orderBy('f.nom', 'ASC')
        ;
        }
    ));
      if($type==1 || $type==2){
    		$builder
    		->add('datedebut', DateType::class, array(
    			'label' => 'Date début (*)',
    			'error_bubbling' => true,
    			'required'     => true,
    		))
        ->add('datefin', DateType::class, array(
        'label' => 'Date reprise (*)',
          'error_bubbling' => true,
          'required'     => true,
        ));
      }
      else{
            $builder->add('datedebut', DateType::class, array(
              'label' => 'Date (*)',
              'error_bubbling' => true,
              'required'     => true,
            ));
          }
    $builder->add('commentaire',   TextareaType::class, array('required'      => false));
    if($type!=2){
      $builder->add('imageFile1', VichFileType::class, array(
          'label' => 'Capture 1',
          'required'      => false,
          'allow_delete'  => false, // not mandatory, default is true
          'download_link' => true, // not mandatory, default is true
          //'disabled' => $valide,
        ))
   ->add('imageFile2', VichFileType::class, array(
          'label' => 'Capture 2',
          'required'      => false,
          'allow_delete'  => false, // not mandatory, default is true
          'download_link' => true, // not mandatory, default is true
          //'disabled' => $valide,
        ));
 }

		$builder->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

		 $builder->get('datedebut')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
    ));
      if($type==1 || $type==2){
      	$builder->get('datefin')->addModelTransformer(new CallbackTransformer(
              function ($value) {
                  if(!$value) {
                      return new \DateTime('now + 30 day');
                  }
                  return $value;
              },
              function ($value) {
                  return $value;
              }
          ));
      }
    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Evenement',
            'type' => null
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_3';
    }


}
