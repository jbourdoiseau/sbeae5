<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Component\Form\AbstractType;


class DisplayType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $nombres = range(0,50);
        $resolver->setDefaults(array(
                'choices' => $nombres
            )
        );
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'display';
    }
}
