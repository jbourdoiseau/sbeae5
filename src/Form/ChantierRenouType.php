<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ChantierRenouType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
		 ->add('totalafaire', NumberType::class, array(
		'label' => 'Total à faire',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('budgetannuel', NumberType::class, array(
		'label' => 'Budget annuel',
			'error_bubbling' => true,
			'required'     => false,
		))
     ->add('captureobligatoire', ChoiceType::class, array(
        'label' => 'Capture obligatoire',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Chantier',
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_chantier';
    }


}
