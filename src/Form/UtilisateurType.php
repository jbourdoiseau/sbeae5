<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Zone;
use App\Repository\ZoneRepository;

class UtilisateurType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$password=$options['password'];
		$register=$options['register'];
		$timer=time();
		$year=date("Y")-10;
		$builder
    ->add('roles', ChoiceType::class, array(
                'attr'  =>  array('class' => 'form-control',
                    'style' => 'margin:5px 0;height:80px;'),
                'choices' =>
                    array
                    (
                      'Releve' => 'ROLE_FRONT',
                      'Renou' => 'ROLE_RENOU'
                    ) ,
                'multiple' => true,
                'required' => true,
        'mapped' =>false
    ))
    ->add('zone', EntityType::class, array(
        'class'        => 'App:Zone',
        'label' => 'Zone (*)',
        'attr'=> array('class'=>'col-xs-4'),
        'choice_label' => function (Zone $zone) {
          return $zone->getNom();
        },
        'required'     => true,
        'attr' => array(
          'class' => ''
        ),
        'query_builder' => function(ZoneRepository $repository){
            return $repository
            ->createQueryBuilder('z')->join('z.responsable','u')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('role', '%ADMIN%')
            ->orderBy('u.nom', 'ASC')
            ;
            }
        ))
		->add('username', null, array('label' => 'Login', 'required' => true, 'attr'=> array('placeholder' => 'Login','autocomplete' => 'new-password')))
		->add('mobile',  null, array('label' => 'Mobile', 'required' => false, 'attr'=> array('placeholder' => 'Mobile')))
		->add('email', null, array('label' => 'Email', 'required' => false, 'attr'=> array('placeholder' => 'Email')))
		->add('nom', null, array('label' => '', 'required' => true, 'attr'=> array('placeholder' => 'Nom')))
		->add('prenom',  null, array('label' => '', 'required' => true, 'attr'=> array('placeholder' => 'Prénom')))
		->add('civilite', EntityType::class, array(
				'class'        => 'App:Civilite',
				'label' => 'Civilité',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => false
				))
      ->add('horaire', EntityType::class, array(
        'class'        => 'App:Horaire',
        'choice_label' => 'nom',
        'label'     => 'Horaires (*)',
        'required'     => true,
        'attr' => array()
        ))
		->add('enabled', ChoiceType::class, array(
		'label' => 'Actif ',
		'required'     => true,
		'choices'  => array(
			'Oui' => 1,
			'Non' => 0
		)))
		->add('adresse',  null, array('label' => '', 'required' => false, 'attr'=> array('placeholder' => 'Adresse')))
		->add('cp',  null, array('label' => 'Code postal', 'required' => false, 'attr'=> array('placeholder' => 'CP')))
		->add('ville',  null, array('label' => '', 'required' => false, 'attr'=> array('placeholder' => 'Ville')));
		//if($password == true){
		$builder->add('plainPassword', RepeatedType::class, array(
                                                    'type' => PasswordType::class,
													'attr' => array('autocomplete' => 'new-password'),
                                                    'options' => array('translation_domain' => 'FOSUserBundle'),
                                                    'first_options' => array('attr' => array('autocomplete' => 'new-password'),'label' => 'form.password'),
                                                    'second_options' => array('label' => 'form.password_confirmation'),
                                                    'invalid_message' => 'fos_user.password.mismatch',
													'required' => $password
                                                    ))
		;
		//}
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User',
			'password' => true,
			'register' => true,
			'plainPassword' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_user_add';
    }


}
