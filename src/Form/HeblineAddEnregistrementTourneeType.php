<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

use App\Entity\Enregistrement;
use App\Entity\Tournee;
use App\Entity\Typeocr;
use App\Entity\User;
use App\Form\TourneeType;

use App\Repository\ChantierRepository;
use App\Repository\UserRepository;

class HeblineAddEnregistrementTourneeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('zone', EntityType::class, array(
				'class'        => 'App:Zone',
				'attr'=> array(),
				'choice_label' => 'nom',
				'label' => 'Zones',
				'required'     => true,
				'attr' => array(
					'class' => ''
				)
		))
		->add('user', EntityType::class, array(
				'class'        => 'App:User',
				'attr'=> array(),
				'choice_label' => function (\App\Entity\User $user) {
					return $user->getNom() . ' ' . $user->getPrenom();
				},
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'query_builder' => function(\App\Repository\UserRepository $repository){
				return $repository
				->createQueryBuilder('f')
				->andWhere('f.roles LIKE :role')
				->setParameter('role', '%FRONT%')
				->orderBy('f.nom', 'ASC')
				;
				}
		))
		->add('chantier', EntityType::class, array(
				'class'        => 'App:Chantier',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => 'nom',
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'query_builder' => function(ChantierRepository $repository){
				return $repository
				->createQueryBuilder('f')
				->orderBy('f.nom', 'ASC')
				;
				}
		))
		->add('datereleve', DateType::class, array(
			'label' => 'Date Relève',
			'attr'=> array('class'=>'col-xs-12'),
			'error_bubbling' => true,
			'required'     => true,
		))

		->add('typeocr', EntityType::class, array(
				'class'        => 'App:Typeocr',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => true,
		))
	  ->add('releve',    NumberType::class, array('label'      => 'Relevé'))
	  ->add('infructueux',    NumberType::class, array('label'      => 'Infructueux'))
	  ->add('sumtotal',    NumberType::class, array('label'      => 'Somme Tot RL'))
	  /*->add('sumtournee',    NumberType::class, array('label'      => 'Somme Trns'))*/
	  ->add('cloture', ChoiceType::class, array(
		'label' => 'Cloture ',
		'required'     => true,
		'choices'  => array(
			'Oui' => 1,
			'Non' => 0,
		)))
		->add('cloturereleve',    NumberType::class, array('label'      => 'Relevé Cloture'))
	  ->add('clotureinfructueux',    NumberType::class, array('label'      => 'Infructueux Cloture'))
	  ->add('cloturetotal',   NumberType::class, array('label'      => 'Total Cloture'))
	  ->add('totalocr',    NumberType::class, array('label'      => 'Total Relève OCR'))
	  ->add('totalocrtournee',    NumberType::class, array('label'      => 'Total Tournée OCR'))
		->add('commentaire',   TextareaType::class, array('required'      => false))
		 ->add(
            'documents',
            CollectionType::class,
            array(
                'entry_type' => DocumentType::class,
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Capture(s) :',
                'prototype' => true
            )
        )

     ->add('tournees', CollectionType::class, array(
        'entry_type'   => TourneeType::class,
        'allow_add'    => true,
        'allow_delete' => true,
		'label' => false,
		'entry_options' => ['label' => false],
		'prototype' => true,
		'by_reference' => false
      ))
      ->add('save',  SubmitType::class, array('label'=>'Enregistrer'));

	  $builder->get('datereleve')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
    ));

    }


	public function configureOptions(OptionsResolver $resolver)
	  {
		$resolver->setDefaults(array(
		  'data_class' => 'App\Entity\Enregistrement',
		  'zone' => null
		));
	  }
}
