<?php
namespace App\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use App\Entity\Document;

class EnregistrementRenouNewType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

	 $obligatoire=$options['obligatoire'];

	  $builder
	  ->add(
            'enregistrementchantiermateriels',
            CollectionType::class,
            array(
                'entry_type' => EnregistrementChantierMaterielNewType::class,
                'by_reference' => false,
                'allow_add'    => false,
                'allow_delete' => false,
                'label' => false,
                'entry_options' => ['label' => false]
            )
        )
    ->add('commentaire',   TextareaType::class, array('label' => 'Commentaire ','required'      => false))
    ->add('documents',
            CollectionType::class,
            array(
                'entry_type' => Document2Type::class,
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Document(s) : ',
                'prototype' => true,
                'entry_options'  => array('obligatoire' => $obligatoire),
            )
        );
    /*
    $builder->add('save',  SubmitType::class, array(
      'attr' => array('class' => 'btn-success'),
      'label' => 'Enregistrer'
    ));*/

  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Enregistrement',
		  'obligatoire' => 0,
      'csrf_protection' => false,
      'validation_groups' => false,
      'obligatoire' => false,
    ));
  }
}
