<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use App\Entity\Document;

class EnregistrementType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // Arbitrairement, on récupère toutes les catégories qui commencent par "D"
    $pattern = 'D%';
	$titre=$options['titre'];
	$nomformulaire=$options['nomformulaire'];
	$cloture=$options['cloture'];
	$capture=$options['capture'];
    $builder
    ->add('releve',    NumberType::class, array('label'      => 'Relevés (sans les infructueux ou illisibles)'));
	  if($nomformulaire != 'veolia_v'){
	  $builder->add('infructueux',    NumberType::class, array('label'      => 'Infructueux'));
	  }
		if($nomformulaire == 'form_total'){
			  $builder->add('totaltournee',    NumberType::class, array('label'      => 'Total tournée'));
		}
	  if($nomformulaire != 'veolia_v'){
			$builder->add('infructueux',    NumberType::class, array('label'      => 'Infructueux'));
	  }
	  if($cloture == 1){
		$builder->add('cloture', ChoiceType::class, array(
		              'attr'  =>  array('class' => 'form-control alert alert-warning','style' => 'margin:5px 0;border-color: green;'),
									'label' => 'Cloture',
					                'choices' =>
					                    array
					                    (
					                            'Non' => '0',
					                            'Oui' => '1'
					                    ) ,
					                'multiple' => false,
					                'required' => true,
					        ));
		}
		else{
			$builder->add('cloture', HiddenType::class, array('empty_data' => 0));
		}
		$select=array();
		for($q=0;$q<51;$q++){
			$select [$q]=$q;
		}
		$builder
		->add('newcompteur', ChoiceType::class, array(
									'label' => 'Compteur trouvé absent du PDA',
					                'choices' => $select,
					                'multiple' => false,
					                'required' => true,
					        ))

	->add('commentaire',   TextareaType::class, array('required'      => false))
		;
    //->add('newcompteur',   NumberType::class, array('label'      => 'Compteur trouvé absent du PDA (Préciser détails en commentaire)'));

		$builder
	->add('ncompteurs', CollectionType::class, array(
			'attr'  =>  array('style' => 'border: 1px solid grey;width:100%; '),
        'entry_type'   => NcompteurType::class,
        'allow_add'    => true,
        'allow_delete' => true,
		'label' => 'Détails compteurs trouvés',
		'entry_options' => ['label' => false],
		'prototype' => true,
		'by_reference' => false
      ));
	if($capture == 1){
		$builder->add(
            'documents',
            CollectionType::class,
            array(
                'entry_type' => DocumentType::class,
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Capture(s) :',
                'prototype' => true
            )
        );
	}
		$builder->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

 if($nomformulaire != 'veolia_v'){
		 $builder->get('infructueux')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return 0;
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
 }

  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Enregistrement',
		  'titre' => 'Total relève jour',
		  'nomformulaire' => 'image',
		  'cloture' => 0,
		  'capture' => 0
    ));
  }
}
