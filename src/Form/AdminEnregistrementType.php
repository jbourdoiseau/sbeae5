<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use App\Repository\ChantierRepository;

class AdminEnregistrementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		$zone=$options['zone'];
		$ref = $options['ref'];
		/*->add('zone', EntityType::class, array(
				'class'        => 'LEOCommerceBundle:Type',
				'choice_label' => 'nom',
				'label'     => false,
				'required'     => false,
				'attr' => array(
					'class' => 'hidden'
				),
				'query_builder' => function(TypeRepository $repository) use ($type){
				return $repository
				->createQueryBuilder('tp')
				->where('tp.id = :type')
				->setParameter('type', $type)
				;
				}
		))

		->add('type', HiddenType::class, array(
			'data' => $type))*/
		/*
				 ->add('zone', EntityType::class, array(
				'class'        => 'App:Zone',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => false,
				'placeholder' => 'Tous les états',
				))
		*/
		$builder
		->add('chantier', EntityType::class, array(
				'class'        => 'App:Chantier',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => 'nom',
				'label' => 'Sites',
				'required'     => false,
				'attr' => array(
					'class' => ''
				),
				'placeholder' => 'Tous les sites',
				'query_builder' => function(ChantierRepository $repository) use ($zone){
				return $repository
				->createQueryBuilder('f')
				->where('f.zone = :zone')
				->setParameter('zone', $zone)
				->orderBy('f.nom', 'ASC')
				;
				}
		))
		->add('cloture', ChoiceType::class, array(
		'label' => 'Cloture ',
		'required'     => false,
		'choices'  => array(
		'Oui/Non' => '',
			'Oui' => 1,
			'Non' => 0,
		)))
		->add('tarifjour', ChoiceType::class, array(
				'label' => 'Forfait',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
		->add('secteur',    TextType::class, array('label'      => 'Secteur'))
		->add('ref', TextType::class, array(
				'label' => 'Nom utilisateur',
				'required'     => false,
				'mapped' => false,
				'error_bubbling' => true,
				'data' => $ref
		))
		->add('date1', DateType::class, array(
			'label' => 'Date début recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('date2', DateType::class, array(
			'label' => 'Date fin recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('page', HiddenType::class, array(
		'required'     => false,
		'mapped' => false,
		'data' => 1
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

		 $builder->get('date1')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now -1 year');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));

		$builder->get('date2')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));

    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Enregistrement',
						'zone' => null,
						'ref' => null,
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_enregistrement';
    }


}
