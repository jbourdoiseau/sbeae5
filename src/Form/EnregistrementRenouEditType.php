<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use App\Entity\Document;
use App\Repository\ChantierRepository;


class EnregistrementRenouEditType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

	 $obligatoire=$options['obligatoire'];
   $chantier=$options['chantier'];
    $zone=$options['zone'];
      $builder->add('user', EntityType::class, array(
        'class'        => 'App:User',
        'mapped' =>true,
        'choice_label' => function (\App\Entity\User $user) {
          return $user->getNom() . ' ' . $user->getPrenom();
        },
        'required'     => true,
        'query_builder' => function(\App\Repository\UserRepository $repository) use ($zone){
        return $repository
        ->createQueryBuilder('f')
        ->andWhere('f.roles LIKE :role')
        ->setParameter('role', '%RENOU%')
        ->orderBy('f.nom', 'ASC')
        ;
        }
    ));
    $builder->add('valide', ChoiceType::class, array(
        'label' => 'Valide',
                'choices' =>
                    array
                    (
                            'Oui' => '1',
                            'Non' => '0'
                    ) ,
                'multiple' => false,
                'required' => true,
        ))
     ->add('datereleve', DateType::class, array(
      'label' => 'Date Renou',
      'attr'=> array('class'=>'col-xs-12'),
      'error_bubbling' => true,
      'required'     => true,
    ))
	  ->add(
            'enregistrementchantiermateriels',
            CollectionType::class,
            array(
                'entry_type' => EnregistrementChantierMaterielType::class,
                'by_reference' => false,
                'allow_add'    => false,
                'allow_delete' => false,
                'label' => false,
                'entry_options' => ['label' => false]
            )
        )
    ->add('commentaire',   TextareaType::class, array('required'      => false));
    if($obligatoire == 1){
    $builder->add('documents',
            CollectionType::class,
            array(
                'entry_type' => DocumentType::class,
                'by_reference' => false,
                'allow_add'    => true,
                'required' => false,
                'allow_delete' => true,
                'label' => 'Document(s) :',
                'prototype' => true
            )
        );
    }
    else{
          $builder->add('documents',
            CollectionType::class,
            array(
                'entry_type' => Document2Type::class,
                'by_reference' => false,
                'required' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Document(s) :',
                'prototype' => true
            )
        );
    }
    $builder->add('save',  SubmitType::class, array(
      'attr' => array('class' => 'btn-success'),
      'label' => 'Enregistrer'
    ));

    $builder->addEventListener(
    FormEvents::SUBMIT,
    function (FormEvent $event) {
        foreach ($event->getData()->getDocuments() as $document) {
            $document->setEnregistrement($event->getData());
        }
    });


  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Enregistrement',
		  'obligatoire' => 0,
      'csrf_protection' => false,
      'validation_groups' => false,
      'chantier' => null,
      'zone' => null,
    ));
  }
}
