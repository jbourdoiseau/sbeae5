<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use App\Repository\ChantierRepository;

class AdminStatsClotureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		$zone=$options['zone'];
		
		/*->add('zone', EntityType::class, array(
				'class'        => 'LEOCommerceBundle:Type',
				'choice_label' => 'nom',
				'label'     => false,
				'required'     => false,
				'attr' => array(
					'class' => 'hidden'
				),
				'query_builder' => function(TypeRepository $repository) use ($type){
				return $repository
				->createQueryBuilder('tp')
				->where('tp.id = :type')
				->setParameter('type', $type)
				;
				}
		))

		->add('type', HiddenType::class, array(
			'data' => $type))*/		
		/*
				 ->add('zone', EntityType::class, array(
				'class'        => 'App:Zone',
				'choice_label' => 'nom',
				'multiple'     => false,
				'required'     => false,
				'placeholder' => 'Tous les états',
				))
		*/
		$builder
		->add('chantier', EntityType::class, array(
				'class'        => 'App:Chantier',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => 'nom',
				'label' => 'Sites',
				'required'     => false,
				'attr' => array(
					'class' => ''
				),
				'placeholder' => 'Tous les sites',
				'query_builder' => function(ChantierRepository $repository) use ($zone){
				return $repository
				->createQueryBuilder('f')
				->where('f.zone = :zone')
				->setParameter('zone', $zone)
				->orderBy('f.nom', 'ASC')
				;
				}
		))	
		->add('date1', DateType::class, array(
			'label' => 'Date début recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('date2', DateType::class, array(
			'label' => 'Date fin recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))

		->add('cloture', HiddenType::class, array(
		'data'  =>  1
		))
		->add('excel', HiddenType::class, array(		
		'mapped' => false,
		'data'  =>  0
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-info'),
			'label' => 'Afficher'
		));
		
		 $builder->get('date1')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('first day of this month');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
		
		$builder->get('date2')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
	
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Enregistrement',
			'zone' => null
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_enregistrement';
    }


}
