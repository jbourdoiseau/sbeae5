<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HeblineChantierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
		 ->add('zone', EntityType::class, array(
				'class'        => 'App:Zone',
				'choice_label' => 'nom',
				'label' => 'Zones',
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'placeholder' => 'Tous les zones'
		))
		->add('nom', TextType::class, array(
				'label' => 'Nom  (*)',
				'required'     => true,
				'error_bubbling' => true,
				))
    ->add('societe', EntityType::class, array(
        'class'        => 'App:Societe',
        'choice_label' => 'nom',
        'label'     => 'Societe (*)',
        'required'     => true,
        'attr' => array()
    ))
		->add('client', EntityType::class, array(
				'class'        => 'App:Client',
				'choice_label' => 'nom',
				'label'     => 'Client (*)',
				'required'     => true,
				'attr' => array()
		))
    ->add('typechantier', EntityType::class, array(
        'class'        => 'App:Typechantier',
        'choice_label' => 'nom',
        'label'     => 'Type (*)',
        'required'     => true,
        'attr' => array()
    ))
		->add('releveur', TextType::class, array(
		'label' => 'Nbre Releveur',
			'error_bubbling' => true,
			'required'     => true,
		))
		->add('commentaire',   TextareaType::class, array('required'      => false))
		->add('datedebut', DateType::class, array(
			'label' => 'Date début (*)',
			'error_bubbling' => true,
			'required'     => true,
		))
		->add('datefin', DateType::class, array(
		'label' => 'Date fin (*)',
			'error_bubbling' => true,
			'required'     => true,
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

		 $builder->get('datedebut')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
    ));

	$builder->get('datefin')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now + 30 day');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
    ));
    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Chantier',
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_chantier';
    }


}
