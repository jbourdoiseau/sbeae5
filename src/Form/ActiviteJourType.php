<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

use App\Repository\ChantierRepository;

class ActiviteJourType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		$zone=$options['zone'];
		
		if($zone < 1000){
		$builder
		->add('chantier', EntityType::class, array(
				'class'        => 'App:Chantier',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => 'nom',
				'label' => 'Sites',
				'required'     => false,
				'attr' => array(
					'class' => ''
				),
				'placeholder' => 'Tous les sites',
				'query_builder' => function(ChantierRepository $repository) use ($zone){
				return $repository
				->createQueryBuilder('f')
				->where('f.zone = :zone')
				->setParameter('zone', $zone)
				->orderBy('f.id', 'DESC')
				;
				}
		));
		}
		else{
		$builder
		->add('chantier', EntityType::class, array(
				'class'        => 'App:Chantier',
				'attr'=> array('class'=>'col-xs-4'),
				'choice_label' => 'nom',
				'label' => 'Sites',
				'required'     => false,
				'attr' => array(
					'class' => ''
				),
				'placeholder' => 'Tous les sites'))	;
		}
		$builder->add('date1', DateType::class, array(
			'label' => 'Date début recherche',
			'mapped' => false,
			'required'     => true,
			'attr' => ['class' => 'dateur'],
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-info'),
			'label' => 'Afficher'
		));
		
		 $builder->get('date1')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return new \DateTime('now');
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Enregistrement',
			'zone' => null
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_enregistrement';
    }


}
