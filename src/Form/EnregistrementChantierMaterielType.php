<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

use App\Entity\EnregistrementChantierMateriel;

class EnregistrementChantierMaterielType extends AbstractType
{

/**
 * @param FormBuilderInterface $builder
 * @param array                $options
 */
public function buildForm(FormBuilderInterface $builder, array $options)
{

  $builder
    ->addEventListener(FormEvents::PRE_SET_DATA,
      function (FormEvent $event)
      {

        $form = $event->getForm();
        $child= new EnregistrementChantierMateriel();
        $child = $event->getData();

        if ($child instanceof \App\Entity\EnregistrementChantierMateriel) {
          $chantiermateriel=$child->getChantierMateriel();
          $nom = $chantiermateriel->getMateriel()->getNom();
          $id = $chantiermateriel->getId();
          //var_dump($child);
          $form->add('nombre',    NumberType::class, array('label'      => $nom));
        }
     }
    );



}

/**
 * @param OptionsResolver $resolver
 */
public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults(array(
        'data_class' => EnregistrementChantierMateriel::class,
    ));
}

}
