<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;


class AdminCommentaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$zone=$options['zone'];
		
		$builder
		->add('datedebut', DateType::class, array(
			'label' => 'Date début (*)',
			'error_bubbling' => true,
			'required'     => true,
		))
		->add('datefin', DateType::class, array(
		'label' => 'Date fin (*)',
			'error_bubbling' => true,
			'required'     => true,
		))
		->add('commentaire',   TextareaType::class, array('required'      => false));

		if($zone == null){
		$builder->add('user', EntityType::class, array(
				'class'        => 'App:User',
				'attr'=> array(),
				'choice_label' => function (\App\Entity\User $user) {
					return $user->getNom() . ' ' . $user->getPrenom();
				},
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'query_builder' => function(\App\Repository\UserRepository $repository){
				return $repository
				->createQueryBuilder('f')
				->andWhere('f.roles LIKE :role')
				->setParameter('role', '%FRONT%')
				->orderBy('f.nom', 'ASC');
				}
		));
		}
		else{
			$builder->add('user', EntityType::class, array(
				'class'        => 'App:User',
				'attr'=> array(),
				'choice_label' => function (\App\Entity\User $user) {
					return $user->getNom() . ' ' . $user->getPrenom();
				},
				'required'     => true,
				'attr' => array(
					'class' => ''
				),
				'query_builder' => function(\App\Repository\UserRepository $repository) use ($zone){
				return $repository
				->createQueryBuilder('f')
				->andWhere('f.roles LIKE :role')
				->setParameter('role', '%FRONT%')
				->orderBy('f.nom', 'ASC')
				;
				}
		));
		}
		
		$builder->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));
		
	$builder->get('datedebut')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
		));
	
	$builder->get('datefin')->addModelTransformer(new CallbackTransformer(
        function ($value) {
            if(!$value) {
                return new \DateTime('now + 7 day');
            }
            return $value;
        },
        function ($value) {
            return $value;
        }
    ));
    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Commentaire',
			'zone' => null,
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_chantier';
    }


}
