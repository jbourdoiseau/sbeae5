<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class HeblineChantierRenouType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder
		 ->add('totalreleve', NumberType::class, array(
		'label' => 'Total relève',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('tarifreleve', NumberType::class, array(
		'label' => 'Tarif relève',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('tarifinfructueux', NumberType::class, array(
		'label' => 'Tarif infructueux',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('tarifradioreleve', NumberType::class, array(
		'label' => 'Tarif radiorelève',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('tauxreleve', NumberType::class, array(
		'label' => 'Taux relève N-1',
			'error_bubbling' => true,
			'required'     => false,
		))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Chantier',
        ));
    }


    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_chantier';
    }


}
