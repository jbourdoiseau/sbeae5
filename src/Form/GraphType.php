<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\CallbackTransformer;

use App\Repository\ChantierRepository;


class GraphType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $zone=$options['zone'];

        $builder->add('chantier', EntityType::class, array(
                'class'        => 'App:Chantier',
                'attr'=> array('class'=>'col-xs-4'),
                'choice_label' => 'nom',
                'label' => 'Sites',
                'required'     => true,
                'attr' => array(
                    'class' => ''
                ),
                'placeholder' => 'Sélectionner un site',
                'query_builder' => function(ChantierRepository $repository) use ($zone){
                $rep = $repository->createQueryBuilder('f');
                if($zone != null){
                    $rep->andWhere('f.zone = :zone')
                    ->setParameter('zone', $zone);
                }
                $rep->orderBy('f.nom', 'ASC');
                return $rep;
                }
        ))
		->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Rechercher'
		));

    }

	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Graph',
            'zone' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_graph';
    }

}
