<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ClotureType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {

    $builder
      ->add('releve',    TextType::class, array('label'      => 'Total relève'))
      ->add('imageFile1', VichFileType::class, array(
					'label' => 'Capture 1',
					'required'      => true,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile2', VichFileType::class, array(
					'label' => 'Capture 2',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile3', VichFileType::class, array(
					'label' => 'Capture 3',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))					
	->add('commentaire',   TextareaType::class, array('required'      => false))
    ->add('save',      SubmitType::class)
    ;


  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Enregistrement'
    ));
  }
}
