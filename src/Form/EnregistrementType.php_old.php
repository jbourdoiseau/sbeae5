<?php
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;
use App\Entity\Document;

class EnregistrementType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // Arbitrairement, on récupère toutes les catégories qui commencent par "D"
    $pattern = 'D%';
	$titre=$options['titre'];
	$nomformulaire=$options['nomformulaire'];
	$cloture=$options['cloture'];
    $builder
    ->add('releve',    NumberType::class, array('label'      => 'Relevés (sans les infructueux ou illisibles)'));
	  if($nomformulaire != 'veolia_v'){
	  $builder->add('infructueux',    NumberType::class, array('label'      => 'Infructueux'));
	  }
		if($nomformulaire == 'form_total'){
			  $builder->add('totaltournee',    NumberType::class, array('label'      => 'Total tournée'));
		}
	  if($nomformulaire != 'veolia_v'){
			$builder->add('infructueux',    NumberType::class, array('label'      => 'Infructueux'));
	  }
	  if($cloture == 1){
		$builder->add('cloture', ChoiceType::class, array(
		              'attr'  =>  array('class' => 'form-control alert alert-warning','style' => 'margin:5px 0;border-color: green;'),
									'label' => 'Cloture',
					                'choices' =>
					                    array
					                    (
					                            'Non' => '0',
					                            'Oui' => '1'
					                    ) ,
					                'multiple' => false,
					                'required' => true,
					        ));
		}
		else{
			$builder->add('cloture', HiddenType::class, array('empty_data' => 0));
		}
	  $builder->add('commentaire',   TextareaType::class, array('required'      => false))
	  ->add(
            'documents',
            CollectionType::class,
            array(
                'entry_type' => DocumentType::class,
                'by_reference' => false,
                'allow_add'    => true,
                'allow_delete' => true,
                'label' => 'Capture(s) :',
                'prototype' => true
            )
        );
	  if($nomformulaire != 'sansimage'){
       $builder->add('imageFile1', VichFileType::class, array(
					'label' => 'Capture 1',
					'required'      => true,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile2', VichFileType::class, array(
					'label' => 'Capture 2',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile3', VichFileType::class, array(
					'label' => 'Capture 3',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile4', VichFileType::class, array(
					'label' => 'Capture 4',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile5', VichFileType::class, array(
					'label' => 'Capture 5',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
->add('imageFile6', VichFileType::class, array(
					'label' => 'Capture 6',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile7', VichFileType::class, array(
					'label' => 'Capture 7',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				))
	 ->add('imageFile8', VichFileType::class, array(
					'label' => 'Capture 8',
					'required'      => false,
					'allow_delete'  => false, // not mandatory, default is true
					'download_link' => true, // not mandatory, default is true
					//'disabled' => $valide,
				));
	}
	$builder->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Enregistrer'
		));

 if($nomformulaire != 'veolia_v'){
		 $builder->get('infructueux')->addModelTransformer(new CallbackTransformer(
			function ($value) {
				if(!$value) {
					return 0;
				}
				return $value;
			},
			function ($value) {
				return $value;
			}
		));
 }

  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'App\Entity\Enregistrement',
		  'titre' => 'Total relève jour',
		  'nomformulaire' => 'image',
		  'cloture' => 0
    ));
  }
}
