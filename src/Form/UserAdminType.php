<?php
/** formulaire front fromation **/
namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserAdminType extends AbstractType
{
    /**
     * {@inheritdoc}
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

	$ref = $options['ref'];
	 $builder
		->add('ref', TextType::class, array(
				'label' => 'Mot clé',
				'required'     => false,
				'mapped' => false,
				'error_bubbling' => true,
				'data' => $ref
		))
		->add('page', HiddenType::class, array(
		'required'     => false,
		'mapped' => false,
		'data' => 1
		))
		 ->add('save',  SubmitType::class, array(
			'attr' => array('class' => 'btn-success'),
			'label' => 'Afficher'
		));
		

    }
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User',
			'zone' => null,
			'ref' => null,
        ));
    }

	
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'sbeae_user';
    }


}
