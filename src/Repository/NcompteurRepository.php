<?php

namespace App\Repository;

use App\Entity\Ncompteur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ncompteur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ncompteur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ncompteur[]    findAll()
 * @method Ncompteur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NcompteurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ncompteur::class);
    }

    // /**
    //  * @return Ncompteur[] Returns an array of Ncompteur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ncompteur
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
