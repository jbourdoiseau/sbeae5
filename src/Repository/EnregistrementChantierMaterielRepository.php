<?php

namespace App\Repository;

use App\Entity\EnregistrementChantierMateriel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class EnregistrementChantierMaterielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnregistrementChantierMateriel::class);
    }

    public function findTotaux($enregistrement){

      $qb = $this->createQueryBuilder('a')->join('a.chantiermateriel', 'b')->join('b.materiel', 'm')->join('m.prestation', 'p');
      $qb->select('SUM(a.nombre) AS total, p.id');

      if ($enregistrement != '') {
      $qb->andwhere('a.enregistrement = :enreg');
      $testparam['enreg'] = $enregistrement;
      }
      $qb->setParameters($testparam);

      //$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
      $qb->groupBy('p.id');
      return $qb->getQuery()->getResult();
      /**/

  }

  public function findDetailRenou($enregistrement){

      $qb = $this->createQueryBuilder('a')->join('a.chantiermateriel', 'b')->join('b.materiel', 'm')->join('m.prestation', 'p');
      $qb->select('a.nombre as nombre, p.id as prestation, m.id as materiel,b.tarif as tarif');

      if ($enregistrement != '') {
      $qb->andwhere('a.enregistrement = :enreg');
      $testparam['enreg'] = $enregistrement;
      }
      $qb->setParameters($testparam);

      //$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
      //$qb->groupBy('p.id');
      return $qb->getQuery()->getResult();
      /**/

  }

   public function findTotalCA($enregistrement){

      $qb = $this->createQueryBuilder('a')->join('a.chantiermateriel', 'b')->join('b.materiel', 'm')->join('m.prestation', 'p');
      $qb->select('SUM(a.nombre * b.tarif) as total');

      if ($enregistrement != '') {
      $qb->andwhere('a.enregistrement = :enreg');
      $testparam['enreg'] = $enregistrement;
      }
      $qb->setParameters($testparam);

      //$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
      //$qb->groupBy('p.id');
      return $qb->getQuery()->getOneOrNullResult();
      /**/

  }
}
