<?php

namespace App\Repository;

use App\Entity\Logger;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class LoggerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Logger::class);
    }
	public function findlogs($page, $nbPerPage, $date1, $date2, $role, $zone){



			$qb = $this->createQueryBuilder('a')->innerJoin('a.user', 'u');

			if ($date1 != '') {
			$qb->andwhere('a.datecrea >= :date1');
			$testparam['date1'] = $date1;
			}
			if(($zone != 1000)&&($zone != 1001)){
			$qb->andwhere('u.zone = :zone');
			$testparam['zone'] = $zone;
			}
			if ($date2 != '') {
			$qb->andwhere('a.datecrea <= :date2');
			$testparam['date2'] = $date2;
			}

			if ($role != '') {
			$qb->andwhere('a.titre = :titre');
			$testparam['titre'] = $role;
			}

			if(isset($testparam)){
			$qb->setParameters($testparam);}
			$qb->orderBy('a.id', 'DESC');
			/**/

			$qb
			  // On définit l'annonce à partir de laquelle commencer la liste
			  ->setFirstResult(($page-1) * $nbPerPage)
			  // Ainsi que le nombre d'annonce à afficher sur une page
			  ->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
	}

}
