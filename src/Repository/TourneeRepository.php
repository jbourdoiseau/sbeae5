<?php

namespace App\Repository;

use App\Entity\Tournee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TourneeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tournee::class);
    }

    public function finLastInf($commune, $total){

      $qb = $this->createQueryBuilder('a');
      $qb->select('a.infructueux');

      if ($commune != '') {
      $qb->andwhere('a.code = :commune');
      $testparam['commune'] = $commune;
      }

      if ($total != '') {
      $qb->andwhere('(a.infructueuxsuez + a.restant) = :total');
      $testparam['total'] = $total;
      }
      $qb->setParameters($testparam);
      $qb->setMaxResults(1);

      return $qb->getQuery()->getOneOrNullResult();
    }

}
