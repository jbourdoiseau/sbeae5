<?php

namespace App\Repository;

use App\Entity\Pointage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;


class PointageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pointage::class);
    }

    public function recherchePointageDateUser($date, $user ){

      $qb = $this->createQueryBuilder('a');

      if ($date != '') {
      $qb->andwhere('SUBSTRING(a.datecrea,1,10) = :date');
      $testparam['date'] = $date->format('Y-m-d');
      }

      if ($user != '') {
      $qb->andwhere('a.user = :user');
      $testparam['user'] = $user;
      }
      $qb->setParameters($testparam);
      return $qb->getQuery()->getResult();
      /**/

  }

  public function findFrontPointages($page, $nbPerPage,  $user, $date1, $date2){
$date2->add(new \DateInterval('P1D'));
$date1->sub(new \DateInterval('P1D'));
      $qb = $this->createQueryBuilder('a');
      if ($date1 != '') {
      $qb->andwhere('a.datecrea >= :date1');
      $testparam['date1'] = $date1;
      }

      if ($date2 != '') {
      $qb->andwhere('a.datecrea <= :date2');
      $testparam['date2'] = $date2;
      }

      if ($user != '') {
      $qb->andwhere('a.user = :user');
      $testparam['user'] = $user;
      }

      if(isset($testparam)){
      $qb->setParameters($testparam);}
      $qb->orderBy('a.id', 'DESC');
      /**/

      $qb
        // On définit l'annonce à partir de laquelle commencer la liste
        ->setFirstResult(($page-1) * $nbPerPage)
        // Ainsi que le nombre d'annonce à afficher sur une page
        ->setMaxResults($nbPerPage)
      ;

      // Enfin, on retourne le résultat
      return new Paginator($qb, true);
  }

  public function findPointages($enregistrementCriteres, $zone, $date1, $date2, $ref){
$date2->add(new \DateInterval('P1D'));
$date1->sub(new \DateInterval('P1D'));
      $qb = $this->createQueryBuilder('a')->leftJoin('a.user', 'u');
      if ($date1 != '') {
      $qb->andwhere('a.datecrea >= :date1');
      $testparam['date1'] = $date1;
      }

      if ($date2 != '') {
      $qb->andwhere('a.datecrea <= :date2');
      $testparam['date2'] = $date2;
      }

      if($ref != null){
      $qb->andWhere('u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
      $testparam['ref'] = '%'.$ref.'%';
      }

      if(isset($testparam)){
      $qb->setParameters($testparam);}
      $qb->orderBy('a.id', 'DESC');
       return $qb->getQuery()->getResult();
  }
}
