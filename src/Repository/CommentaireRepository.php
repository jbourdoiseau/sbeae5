<?php

namespace App\Repository;

use App\Entity\Commentaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

		public function findCommentaires($page, $nbPerPage, $zone = null){

			$qb = $this->createQueryBuilder('a');
			$qb->orderBy('a.id', 'DESC');
			/**/
			if($zone != null){
					$qb->where('a.zone = :zone');
					$testparam['zone'] = $zone;
					$qb->setParameters($testparam);
			}
			$qb
			  // On définit l'annonce à partir de laquelle commencer la liste
			  ->setFirstResult(($page-1) * $nbPerPage)
			  // Ainsi que le nombre d'annonce à afficher sur une page
			  ->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
	}

		public function findCommentairesEnCours($user,$date1,$date2 ){

			$qb = $this->createQueryBuilder('c');
			$qb->where('c.user = :user');
			$testparam['user'] = $user;

			$qb->andWhere('(c.datedebut >= :date1 and c.datefin <= :date2) or (c.datedebut <= :date1 and c.datefin >= :date1) or (c.datedebut <= :date2 and c.datefin >= :date2)');

			$testparam['date1'] = $date1;
			$testparam['date2'] = $date2;
			/**/

			$qb->setParameters($testparam);
			return $qb->getQuery()->getResult();
			/**/

	}
}
