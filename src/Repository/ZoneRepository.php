<?php

namespace App\Repository;

use App\Entity\Zone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class ZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Zone::class);
    }

	public function getZoneCA()
    {
      $testparam= array();
      $qb = $this->createQueryBuilder('u');
      $qb->andWhere('u.id < 1000 and u.id>1');
      return $qb->getQuery()->getResult();
    }

}
