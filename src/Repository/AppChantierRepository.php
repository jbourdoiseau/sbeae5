<?php

namespace App\Repository;

use App\Entity\AppChantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppChantier|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppChantier|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppChantier[]    findAll()
 * @method AppChantier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppChantier::class);
    }

    // /**
    //  * @return AppChantier[] Returns an array of AppChantier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppChantier
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
