<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

use App\Entity\ChantierUser;
use App\Entity\User;
use App\Entity\Chantier;

class ChantierUserRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChantierUser::class);
    }
	public function findByChantierandUser(Chantier $chantier,User $user)
	{

			$qb = $this->createQueryBuilder('a');

			$qb->andWhere('a.user = :user')
			->setParameter('user', $user);
			$qb->andWhere('a.chantier = :chantier')
			->setParameter('chantier', $chantier);

			return $qb
			  ->getQuery()
			  ->getResult()
			;
	}
	public function findUserChantierActuel($user)
	{

			$date = date('Y-m-d');
			$date2= date('Y-m-d', strtotime("-3 days"));
			$qb = $this->createQueryBuilder('a');
			$qb
			  ->innerJoin('a.chantier', 't')
			  ->addSelect('t');

			$qb->where('t.datedebut <= :datedebut')
			->setParameter('datedebut', $date);

			$qb->andWhere('t.datefin >= :datefin')
			->setParameter('datefin', $date2);

			$qb->andWhere('a.user = :user')
			->setParameter('user', $user);

			return $qb
			  ->getQuery()
			  ->getResult()
			;
	}

	public function getUsersChantierDate($date1, $chantier = null)
		{
			$testparam= array();

			$qb = $this->createQueryBuilder('cu');
			$qb->leftJoin('cu.user', 'u')->leftJoin('cu.chantier', 'c')->addSelect('u')->addSelect('c');

			if($chantier != null){
			$qb->andWhere('c.id = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if($date1 != null){
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date1;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;
			}
			if(count($testparam) > 0){
			$qb->setParameters($testparam);
			}
			$qb->groupBy('u.id');
			$qb->addOrderBy('c.id', 'DESC');//(' ', 'u.id DESC');
			$qb->addOrderBy('u.id', 'DESC');
			return $qb->getQuery()->getResult();
		}

	public function getUsersValideChantierDate($date1, $chantier = null)
		{
			$testparam= array();

			$qb = $this->createQueryBuilder('cu');
			$qb->leftJoin('cu.user', 'u')->leftJoin('cu.chantier', 'c')->addSelect('u')->addSelect('c');
			$qb->andWhere('u.enabled = 1');
			$qb->andWhere('c.id NOT IN (92,95,85) and (u.id < 2000 or u.id >=3000)');

			if($chantier != null){
			$qb->andWhere('c.id = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if($date1 != null){
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date1;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;
			}
			if(count($testparam) > 0){
			$qb->setParameters($testparam);
			}

			$qb->addOrderBy('c.id', 'DESC');//(' ', 'u.id DESC');
			$qb->addOrderBy('u.id', 'DESC');
			$qb->groupBy('u.id');
			return $qb->getQuery()->getResult();
		}
		public function getUsersValideChantierDate2($date1, $chantier = null)
		{
			$testparam= array();

			$qb = $this->createQueryBuilder('cu');
			$qb->leftJoin('cu.user', 'u')->leftJoin('cu.chantier', 'c')->addSelect('u')->addSelect('c');
			$qb->andWhere('u.enabled = 1');
			$qb->andWhere('(u.id < 2000 or u.id >=3000)');

			if($chantier != null){
			$qb->andWhere('c.id = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if($date1 != null){
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date1;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;
			}
			if(count($testparam) > 0){
			$qb->setParameters($testparam);
			}

			$qb->addOrderBy('c.id', 'DESC');//(' ', 'u.id DESC');
			$qb->addOrderBy('u.id', 'DESC');
			return $qb->getQuery()->getResult();
		}
}
