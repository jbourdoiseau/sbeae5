<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    public function findEvenements($type,$date1,$date2 ){

      $qb = $this->createQueryBuilder('a');

      if (($date1 != '') && ($date2 != '')){
      $qb->andWhere('(a.datedebut <= :date1 and a.datefin >= :date1)');
      $qb->orWhere('(a.datedebut >= :date1 and a.datedebut <= :date2)');
      $testparam['date1'] = $date1;
      $testparam['date2'] = $date2;
      }

     if ($type != '') {
      $qb->andwhere('a.type = :type');
      $testparam['type'] = $type;
      }

      $qb->setParameters($testparam);
      return $qb->getQuery()->getResult();
      /**/

  }

   public function findEvenementsNotes($date1,$date2 ){

      $qb = $this->createQueryBuilder('a');

      if (($date1 != '') && ($date2 != '')){
      $qb->andWhere('(a.datedebut <= :date1 and a.datefin >= :date1)');
      $qb->orWhere('(a.datedebut >= :date1 and a.datedebut <= :date2)');
      $testparam['date1'] = $date1;
      $testparam['date2'] = $date2;
      }
      $qb->andWhere('a.etat = 1');

      $qb->setParameters($testparam);
      return $qb->getQuery()->getResult();
      /**/

  }
	}
