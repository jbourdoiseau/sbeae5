<?php

namespace App\Repository;

use App\Entity\Alerte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class AlerteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Alerte::class);
    }

    public function findAlertes($date1,$date2 ){

      $qb = $this->createQueryBuilder('a');

      if ($date1 != '') {
      $qb->andwhere('a.datealerte >= :date1');
      $testparam['date1'] = $date1;
      }

      if ($date2 != '') {
      $qb->andwhere('a.datealerte <= :date2');
      $testparam['date2'] = $date2;
      }
      $qb->setParameters($testparam);
      return $qb->getQuery()->getResult();
      /**/

  }
}
