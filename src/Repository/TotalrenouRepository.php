<?php

namespace App\Repository;

use App\Entity\Totalrenou;
use App\Entity\Enregistrement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TotalrenouRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Totalrenou::class);
    }


public function findTotalRenousEnreg(Enregistrement $enregistrement ){

      $qb = $this->createQueryBuilder('a')->join('a.prestation', 'p');;
      $qb->select('SUM(a.total) AS total, p.id as prestation');

      if ($enregistrement != '') {
      $qb->andwhere('a.enregistrement = :enregistrement');
      $testparam['enregistrement'] = $enregistrement;
      }

      $qb->setParameters($testparam);
      $qb->groupBy('p.id');
      return $qb->getQuery()->getResult();
      /**/
  }

}
