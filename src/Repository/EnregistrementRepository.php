<?php
namespace App\Repository;

use App\Entity\Enregistrement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

use App\Entity\User;
use App\Entity\Zone;
use App\Entity\Chantier;
/**
 * @method Enregistrement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enregistrement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enregistrement[]    findAll()
 * @method Enregistrement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnregistrementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Enregistrement::class);
    }

		public function findDistinctDay(Chantier $chantier)
    {

     $qb=$this->createQueryBuilder('a')
      ->select('COUNT(a.datereleve) AS totalday')
      ->andWhere('a.total > 0')
			->andWhere('a.chantier = :chantier')
			->setParameter('chantier', $chantier);
			return $qb->getQuery()->getOneOrNullResult();
    }

		public function findTotalEnreg(Chantier $chantier)
    {

     $qb=$this->createQueryBuilder('a')
      ->select('SUM(a.total) as totalrel')
      ->andWhere('a.total > 0')
      ->andWhere('a.valide = 1')
			->andWhere('a.chantier = :chantier')
			->setParameter('chantier', $chantier);
			return $qb->getQuery()->getOneOrNullResult();
    }
    /**/
    public function rechercheDate(User $user, Chantier $chantier, $date, $cloture)
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.datereleve >= :val')
            ->setParameter('val', $date)
			->andWhere('e.user = :user')
			->setParameter('user', $user)
			->andWhere('e.chantier = :chantier')
			->setParameter('chantier', $chantier)
			->andWhere('e.cloture = :cloture')
			->setParameter('cloture', $cloture)
			->groupBy('e.datereleve')
			->getQuery()
			->getResult();
    }
        /**/
    public function calculJourHomme(Chantier $chantier)
    {

        return
			$this->createQueryBuilder('e')
			->select('count(e) as nb, SUBSTRING(e.datereleve, 1, 10) as eday')
			->andWhere('e.chantier = :chantier')
			->setParameter('chantier', $chantier)
			->groupBy('e.user')
			->addGroupBy('eday')
			->getQuery()
			->getResult();
    }
//select , SUBSTRING(datereleve, 1, 10) as day from enregistrement where chantier_id = 61 group by user_id,day
        /**/
    public function rechercheDateFront(User $user, Chantier $chantier, $date)
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.datereleve >= :val')
            ->setParameter('val', $date)
			->andWhere('e.user = :user')
			->setParameter('user', $user)
			->andWhere('e.chantier = :chantier')
			->setParameter('chantier', $chantier)
			->groupBy('e.datereleve')
			->getQuery()
			->getResult();
    }

        /**/
    public function rechercheDate2(User $user, Chantier $chantier, $date)
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.datereleve >= :val')
            ->setParameter('val', $date)
			->andWhere('e.user = :user')
			->setParameter('user', $user)
			->andWhere('e.chantier = :chantier')
			->setParameter('chantier', $chantier)
			->groupBy('e.datereleve')
			->getQuery()
			->getResult();
    }
    public function findTotalDiff()
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.totalOcr <> e.totalOcrTournee')
			->andWhere('e.controle <> 1')
			->andWhere('e.cloture = 1')
			->getQuery()
			->getResult();
    }
	public function findTotalDiffOcr()
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.totalOcr -e.restantOcr <> e.total')
			->andWhere('e.controle <> 1')
			->andWhere('e.cloture = 1')
			->getQuery()
			->getResult();
    }

	public function findLastEnregistrement($id_enreg, $user, $titre=null)
    {
           	$qb = $this->createQueryBuilder('b')
			->where('b.user = :user');
			$testparam['user'] = $user;
			$qb->andwhere('b.id < :id');
			$testparam['id'] = $id_enreg;
			if($titre!=null){
				$qb->andwhere('b.titre = :titre');
				$testparam['titre'] = $titre;
			}
			$qb->setParameters($testparam);
			$qb->orderBy('b.id', 'DESC');
			$qb->setMaxResults(1);
			return $qb->getQuery()->getOneOrNullResult();
    }
	public function findEnregistrements($page, $nbPerPage, $enregistrementCriteres, $zone, $date1, $date2, $clot, $ref = null, $user = null, $secteur = null, $typechantier = null, $newcompteur = null){


			$chantier='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			}

			$qb = $this->createQueryBuilder('a')->join('a.user','u');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($clot==0 && is_int($clot)) {
			$qb->andwhere('a.cloture = 0');
			}
			if ($clot>0) {
			$qb->andwhere('a.cloture = 1');
			}
			if ($newcompteur > 0) {
			$qb->andwhere('a.newcompteur > 0');
			}
			if ($typechantier != null) {
			$qb->andwhere('a.typechantier = :type');
			$testparam['type'] = $typechantier;
			}
			if ($secteur != '') {
			$qb->andwhere('a.secteur LIKE :secteur');
			$testparam['secteur'] = $secteur;
			}
			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if($ref != null){
			$qb->andWhere('u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
			$testparam['ref'] = '%'.$ref.'%';
			}
			if ($user != null) {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			$qb->orderBy('a.id', 'DESC');
			/**/

			$qb
			  // On définit l'annonce à partir de laquelle commencer la liste
			  ->setFirstResult(($page-1) * $nbPerPage)
			  // Ainsi que le nombre d'annonce à afficher sur une page
			  ->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
	}

public function findEnregistrementsChantierDates($chantier,$date1,$date2 ){

			$qb = $this->createQueryBuilder('a');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);
			return $qb->getQuery()->getResult();
			/**/

	}

	public function findFrontEnregistrements($page, $nbPerPage, $enregistrementCriteres, $user, $date1, $date2, $clot, $typechantier = null){



			$qb = $this->createQueryBuilder('a');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}

			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($clot==0 && is_int($clot)) {
			$qb->andwhere('a.cloture = 0');
			}
			if ($clot>0) {
			$qb->andwhere('a.cloture = 1');
			}
			if ($typechantier != null) {
			$qb->andwhere('a.typechantier = :type');
			$testparam['type'] = $typechantier;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			$qb->orderBy('a.id', 'DESC');
			/**/

			$qb
			  // On définit l'annonce à partir de laquelle commencer la liste
			  ->setFirstResult(($page-1) * $nbPerPage)
			  // Ainsi que le nombre d'annonce à afficher sur une page
			  ->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
	}
	public function findEnregistrementsClotures($enregistrementCriteres, $zone,$date1,$date2 , $type){
			$cloture=1;
			$chantier='';
			$typechantier=1;
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			}

			$qb = $this->createQueryBuilder('a')->join('a.chantier','c');

			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($cloture != '') {
			$qb->andwhere('a.cloture = :cloture');
			$testparam['cloture'] = $cloture;
			}

			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}
			if ($typechantier != null) {
			$qb->andwhere('a.typechantier = :type');
			$testparam['type'] = $typechantier;
			}
			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);
			$qb->orderBy('c.nom', 'ASC');
			//$qb->addOrderBy('a.secteur', 'ASC');
			$qb->addOrderBy('a.datereleve', 'DESC');
			return $qb->getQuery()->getResult();
			/**/

	}

	public function findEnregistrementsOperateurs($enregistrementCriteres, $zone,$date1,$date2, $typechantier=null ){
			$cloture=1;
			$chantier='';
			$user='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			$user=$enregistrementCriteres->getUser();
			}
			//echo 'rep'.$chantier->getId();
			$qb = $this->createQueryBuilder('a');

			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($typechantier != null) {
			$qb->andwhere('a.typechantier = :type');
			$testparam['type'] = $typechantier;
			}
			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr != 1)");
			$qb->orderBy('a.zone', 'ASC');
			$qb->addOrderBy('a.user', 'DESC');
			$qb->addOrderBy('a.datereleve', 'ASC');
			$qb->addOrderBy('a.datecrea', 'ASC');
			//var_dump($qb->getQuery());
			//echo $qb->getDQL();

			return $qb->getQuery()->getResult();
			/**/

	}


	public function findClientEnregistrements($page, $nbPerPage, $enregistrementCriteres, $chantiers, $date1, $date2, $clot, $ref = null, $user = null){

			$chantier='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			}

			$qb = $this->createQueryBuilder('a')->join('a.user','u')->leftJoin('a.chantier','c');
			$qb->where('c.id IN  (:ch)');
			$testparam['ch']= $chantiers;

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($clot==0 && is_int($clot)) {
			$qb->andwhere('a.cloture = 0');
			}
			if ($clot>0) {
			$qb->andwhere('a.cloture = 1');
			}



			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if($ref != null){
			$qb->andWhere('u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
			$testparam['ref'] = '%'.$ref.'%';
			}
			if ($user != null) {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			$qb->orderBy('a.id', 'DESC');
			/**/

			$qb
			  // On définit l'annonce à partir de laquelle commencer la liste
			  ->setFirstResult(($page-1) * $nbPerPage)
			  // Ainsi que le nombre d'annonce à afficher sur une page
			  ->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
	}

	public function findEnregistrementsOperateursTotal($enregistrementCriteres, $zone,$date1,$date2, $typechantier=null ){
			$cloture=1;
			$chantier='';
			$user='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			$user=$enregistrementCriteres->getUser();
			}

			$qb = $this->createQueryBuilder('a');

			$qb->select('SUM(a.total) AS total, a.datereleve');
			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($typechantier != null) {
			$qb->andwhere('a.typechantier = :type');
			$testparam['type'] = $typechantier;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}

			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.datereleve');

			return $qb->getQuery()->getResult();
			/**/

	}

	public function findEnregistrementsSiteTotal($enregistrementCriteres, $zone,$date1,$date2, $type = null){
			$cloture=1;
			$chantier='';
			$user='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			$user=$enregistrementCriteres->getUser();
			}

			$qb = $this->createQueryBuilder('a');
			$qb->select('SUM(a.total) AS total, SUM(a.releve) AS totreleve, SUM(a.infructueux) AS totinfructueux, a.datereleve, (a.chantier) as chantier');

			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.datereleve')->addGroupBy('a.chantier');
			return $qb->getQuery()->getResult();
			/**/

	}

		public function findEnregistrementsSiteTotalDate($chantier, $date2 ){

			$qb = $this->createQueryBuilder('a');
			$qb->select('SUM(a.total) AS total');
			$qb->andwhere('a.valide = 1');
			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}


			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.chantier');
			return $qb->getQuery()->getOneOrNullResult();
			/**/

	}

	public function findEnregistrementsTotalSite($chantier){


			$qb = $this->createQueryBuilder('a');
			$qb->select('SUM(a.total) AS total, SUM(a.releve) AS totreleve, SUM(a.infructueux) AS totinfructueux, a.datereleve, (a.chantier) as chantier');
			//$qb->select('SUM(a.total) AS total');
			$qb->andwhere('a.valide = 1');

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			//$qb->groupBy('a.chantier');
			return $qb->getQuery()->getResult();
			/**/

	}
	public function findEnregistrementsSiteUserTotal($enregistrementCriteres, $zone,$date1,$date2, $type = null){
			$cloture=1;
			$chantier='';
			$user='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			$user=$enregistrementCriteres->getUser();
			}

			$qb = $this->createQueryBuilder('a');
			$qb->select('SUM(a.total) AS total, SUM(a.releve) AS totreleve, SUM(a.infructueux) AS totinfructueux, a.datereleve, (a.chantier) as chantier, (a.user) as user');

			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.chantier')->addGroupBy('a.user');
			return $qb->getQuery()->getResult();
			/**/

	}
	public function findEnregistrementsSiteUserTotal2($enregistrementCriteres, $zone,$date1,$date2 ){
			$cloture=1;
			$chantier='';
			$user='';
			if ($enregistrementCriteres != '') {
			$chantier=$enregistrementCriteres->getChantier();
			$user=$enregistrementCriteres->getUser();
			}

			$qb = $this->createQueryBuilder('a');
			$qb->select('SUM(a.total) AS total, SUM(a.releve) AS totreleve, SUM(a.infructueux) AS totinfructueux, a.datereleve, (a.chantier) as chantier, (a.user.id) as user');

			$qb->andwhere('a.valide = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}

			if ($zone != '') {
			$qb->andwhere('a.zone = :zone');
			$testparam['zone'] = $zone;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.chantier')->addGroupBy('a.user');
			return $qb->getQuery()->getResult();
			/**/

	}


	public function findEnregistrementsDate($date1, $chantier){


			$qb = $this->createQueryBuilder('a');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve = :date1');
			$testparam['date1'] = $date1;
			}
			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			$qb->orderBy('a.id', 'DESC');
			/**/
			return $qb->getQuery()->getResult();
	}
		public function findEnregistrementsDateUser($date1, $user){


			$qb = $this->createQueryBuilder('a');
			$qb->select('COUNT(a.id) AS total');

			$date1=$date1->format('Y-m-d');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}
			if ($user != '') {
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			/**/

		return $qb->getQuery()->getOneOrNullResult();
	}

	public function filterNextPrevious($id)
    {
        $expr = $this->_em->getExpressionBuilder();
		$next = $this->createQueryBuilder('a')
			->select($expr->min('a.id'))
			->where($expr->gt('a.id', ':id'));
		$previous = $this->createQueryBuilder('b')
			->select($expr->max('b.id'))
			->where($expr->lt('b.id', ':id'));
		$query = $this->createQueryBuilder('o')
			->select('COUNT(o.id) as total')
			->addSelect('(' . $previous->getDQL() . ') as previous')
			->addSelect('(' . $next->getDQL() . ') as next')
			->setParameter('id', $id)
			->getQuery();

		/* optionally enable caching
		 * $query->useQueryCache(true)->useResultCache(true, 3600);
		 */

		return $query->getSingleResult();

    }

	public function recherchenoncloture(){
		$date = new \DateTime('- 10 days');
		$date1 = $date->format('Y-m-d');
		$date = new \DateTime('- 60 days');
		$date2 = $date->format('Y-m-d');

					//echo $date1.'_'.$date2;

					$con = $this->_em->getConnection();

					//$rsm->addRootEntityFromClassMetadata('App\Entity\Tournee', 'u');
					$sql="SELECT max(u.id) as id, u.code as code from tournee u where u.datecrea > '".$date2."' group by u.code having max(u.datecrea) < '".$date1."'";
					$statement = $con->prepare($sql);
					$statement->execute();
					return $statement->fetchAll();

	}

	public function findSumReleveOCR($id_enreg){

			$qb = $this->createQueryBuilder('a')->join('a.tournees', 'b');

			$qb->select('SUM(b.total) AS total, SUM(b.restant) AS reste');

			$qb->andwhere('a.id = :id');
			$testparam['id'] = $id_enreg;
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('b.enregistrement');
			//echo "$id_enreg"."_";
			return $qb->getQuery()->getOneOrNullResult();
			/**/

	}
	public function findSumReleve($titre,$user ){

			$qb = $this->createQueryBuilder('a');

			$qb->select('SUM(a.total) AS total');

			$qb->andwhere('a.titre = :titre');
			$testparam['titre'] = $titre;
			$qb->andwhere('a.user = :user');
			$testparam['user'] = $user;
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->groupBy('a.titre')->addGroupBy('a.user');
			return $qb->getQuery()->getOneOrNullResult();
			/**/
	}

	public function findEnregistrementsForfait($chantier,$date1,$date2 ){

			$qb = $this->createQueryBuilder('a')->join('a.user', 'u');;

			//forfait
      $qb->andwhere('a.tarifjour = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->addOrderBy('a.secteur');
			$qb->addOrderBy('a.datereleve');
			$qb->addOrderBy('u.nom');
			return $qb->getQuery()->getResult();
			/**/
	}
		public function findEnregistrementsForfaitUser($chantier,$date1,$date2 ){

			$qb = $this->createQueryBuilder('a')->join('a.user', 'u');;

			//forfait
      $qb->andwhere('a.tarifjour = 1');

			if ($date1 != '') {
			$qb->andwhere('a.datereleve >= :date1');
			$testparam['date1'] = $date1;
			}

			if ($date2 != '') {
			$qb->andwhere('a.datereleve <= :date2');
			$testparam['date2'] = $date2;
			}

			if ($chantier != '') {
			$qb->andwhere('a.chantier = :chantier');
			$testparam['chantier'] = $chantier;
			}
			$qb->setParameters($testparam);

			//$qb->andwhere("(a.cloture = 0 and a.typeocr=1) or (a.cloture = 1 and a.typeocr!=1)");
			$qb->addOrderBy('u.nom');
			$qb->addOrderBy('a.datereleve');
			return $qb->getQuery()->getResult();
			/**/
	}
}
