<?php

namespace App\Repository;

use App\Entity\Chantier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class ChantierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Chantier::class);
    }

	public function findChantierZone($zone = null)
		{

			$qb = $this->createQueryBuilder('u');
			if($zone != null){
			$qb->andWhere(':zone MEMBER OF u.zones');
			$testparam['zone'] = $zone;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			return $qb->orderBy('u.nom', 'ASC')->getQuery()->getResult();
		}



    public function rechercheDate($date, $zone = null)
    {

        return
			$this->createQueryBuilder('e')
            ->andWhere('e.datereleve >= :val')
            ->setParameter('val', $date)
			->andWhere('e.user = :user')
			->setParameter('user', $user)
			->andWhere('e.chantier = :chantier')
			->setParameter('chantier', $chantier)
			->andWhere('e.cloture = :cloture')
			->setParameter('cloture', $cloture)
			->groupBy('e.datereleve')
			->getQuery()
			->getResult();
    }
	public function rechercheDateChantier($date = null, $zone = null)
    {

			$qb = $this->createQueryBuilder('c');
			if(($zone != 1000)&&($zone != 1001)){
			$qb->andWhere(':zone MEMBER OF c.zones');
			$testparam['zone'] = $zone;
			}

            if($date != null){
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date;
			}
			if(isset($testparam)){
			$qb->setParameters($testparam);}
			return $qb->getQuery()->getResult();
    }
	public function rechercheChantiersActif($zone = null)
    {

			$qb = $this->createQueryBuilder('c');
			if(($zone != 1000)&&($zone != 1001)){
			$qb->andWhere(':zone MEMBER OF c.zones');
			$testparam['zone'] = $zone;
			}

			$date1=new \DateTime('first day of this month');
			$date2=new \DateTime('last day of this month');

			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date2;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;

			if(isset($testparam)){
			$qb->setParameters($testparam);}
			return $qb->getQuery()->getResult();
    }


	public function findChantiersEnCours($zone,$date1,$date2, $typechantier ){

			$qb = $this->createQueryBuilder('c')->leftJoin('c.zones','j');
			if($zone != null){
				$qb->andWhere('j.id = :zone');
				$testparam['zone'] = $zone;
			}
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date2;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;
			if ($typechantier != null) {
			$qb->andwhere('c.typechantier = :type');
			$testparam['type'] = $typechantier;
			}
			$qb->setParameters($testparam);
			$qb->orderBy('j.id', 'ASC');
			$qb->addOrderBy('c.nom', 'ASC');
			return $qb->getQuery()->getResult();
			/**/
	}

	public function findChantiersEnCoursUser($zone,$date1,$date2, $ref ){

			$qb = $this->createQueryBuilder('c')->leftJoin('c.zones','j')->leftJoin('c.chantierusers','ch')->leftJoin('ch.user','u');
			if($zone != null){
				$qb->andWhere('j.id = :zone');
				$testparam['zone'] = $zone;
			}
			$qb->andWhere('c.pointage = 1');
			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date2;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;
			if($ref != null){
			$qb->andWhere('u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
			$testparam['ref'] = '%'.$ref.'%';
			}
			$qb->setParameters($testparam);
			$qb->orderBy('j.id', 'ASC');
			$qb->addOrderBy('c.nom', 'ASC');
			return $qb->getQuery()->getResult();
			/**/
	}

	public function findChantiersEnCoursClientEnCours($date1,$date2, $typechantier){

			$qb = $this->createQueryBuilder('c')->leftJoin('c.zones','j');//->leftJoin('j.user','u');

			//$qb->andWhere('u.enabled = 1');



			$qb->andWhere('c.datedebut <= :date1');
			$testparam['date1'] = $date2;
			$qb->andWhere('c.datefin >= :date2');
			$testparam['date2'] = $date1;

			$qb->setParameters($testparam);
			$qb->orderBy('j.id', 'ASC');
			$qb->addOrderBy('c.datedebut', 'ASC');
			return $qb->getQuery()->getResult();
			/**/
	}

	public function findByClient(Zone $zone)
		{
		    $qb = $this->createQueryBuilder("p")
		        ->where(':zone MEMBER OF p.zones')
		        ->setParameters(array('zone' => $zone))
		    ;
		    return $qb->getQuery()->getResult();
		}
}
