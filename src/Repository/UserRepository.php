<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }


		public function getUsersFront($page, $nbPerPage, $ref = null, $zone = null)
		{

			$qb = $this->createQueryBuilder('u')
			->where('u.roles LIKE :roles');
			$testparam['roles'] = '%FRONT%';
			$qb->orWhere('u.roles LIKE :roles2');
			$testparam['roles2'] = '%RENOU%';
			if($zone != null){
			$qb->andWhere('u.zone = :zone');
			$testparam['zone'] = $zone;
			}
			if($ref != null){
			$qb->andWhere('u.id LIKE :ref OR u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
			$testparam['ref'] = '%'.$ref.'%';
			}
			$qb->setParameters($testparam);
			$qb->orderBy('u.nom', 'ASC')
     		->setFirstResult(($page-1) * $nbPerPage)
			->setMaxResults($nbPerPage)
			;

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);

		}
		public function getUsersFrontZone($zone, $role = null)
		{

			$qb = $this->createQueryBuilder('u');
			if($zone != null){
			$qb->andWhere('u.zone = :zone');
			$testparam['zone'] = $zone;
			}
			if($role != null){
			$qb->andWhere('u.roles LIKE  :role');
			$testparam['role'] = "%".$role."%";
			}
			$qb->setParameters($testparam);
			return $qb->orderBy('u.nom', 'ASC')
			->getQuery()
			->getResult();

		}

		public function getUsers($page, $nbPerPage, $ref = null, $zone = null)
		{
			$testparam= array();
			$qb = $this->createQueryBuilder('u');
			if($ref != null){
			$qb->andWhere('u.id LIKE :ref OR u.nom LIKE :ref OR u.prenom LIKE :ref OR u.email LIKE :ref OR u.mobile LIKE :ref');
			$testparam['ref'] = '%'.$ref.'%';
			}
			if($zone != null){
			$qb->andWhere('u.zone = :zone');
			$testparam['zone'] = $zone;
			}
			if(count($testparam) > 0){
			$qb->setParameters($testparam);}
			$qb->orderBy('u.nom', 'ASC')
			->setFirstResult(($page-1) * $nbPerPage)
			->setMaxResults($nbPerPage);

			// Enfin, on retourne le résultat
			return new Paginator($qb, true);
		}

		public function getUsersValides($zone = null)
		{
			$testparam= array();
			$qb = $this->createQueryBuilder('u');
			$qb->andWhere('u.enabled = 1');
			if($zone != null){
			$qb->andWhere('u.zone = :zone');
			$testparam['zone'] = $zone;
			}
			$qb->setParameters($testparam);
			return $qb->getQuery()->getResult();
		}

}
